﻿using McD_Schichtplaner.GUI.Fehlerbehandlung;
using System.Net.Mail;
using System.Windows.Forms;
using System.Net;

namespace McD_Schichtplaner.Mailing
{
    class MailManager
    {
        private Database.Database data;

        private string server;
        private string addr;
        private string user;
        private string password;

        public MailManager()
        {
            data = Database.Database.DB;

            var strings = data.GetMailAccount();
            server = strings[0];
            addr = strings[1];
            user = strings[2];
            password = strings[3];

            bool update = false;
            if (server == "UNDEF")
            {
                StringError se = new StringError("Kein Server angegeben.\nBitte Serveraddresse inkls. Port angeben.", "example.online.de:123");
                if (se.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    server = se.GetString();
                    update = true;
                }
            }
            if (addr == "UNDEF")
            {
                StringError se = new StringError("Keine Mail Addresse für Absender angegeben.\nBitte Mailaddresse angeben.", "example@online.de");
                if (se.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    addr = se.GetString();
                    update = true;
                    if (!Database.Database.CheckMailValidity(addr))
                    {
                        addr = Database.Database.CorrectMail();
                    }
                }
            }
            if (user == "UNDEF")
            {
                StringError se = new StringError("Kein Username angegeben.\nBitte Username eingeben.");
                if (se.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    user = se.GetString();
                    update = true;
                }
            }
            if (password == "UNDEF")
            {
                StringError se = new StringError("Kein Passwort angegeben.\nBitte Passwort eingeben.");
                if (se.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    password = se.GetString();
                    update = true;
                }
            }
            if (update)
            {
                data.SetMailAccount(server, addr, user, password);
            }
        }

        public void SendMail(string reciever, string subject, string message, string attachment)
        {
            if (reciever is null)
            {
                System.Console.WriteLine("Mitarbeiter ohne mail soll mail bekommen...");
                return;
            }
            MailMessage mail = new MailMessage(addr, reciever, subject, message);
            mail.Attachments.Add(new Attachment(attachment));

            string[] sub = server.Split(':');
            int port;
            if (!int.TryParse(sub[1], out port))
            {
                MessageBox.Show("Fehler beim Initialieseren der Mail.\nBitte Account Info neu eingeben.");
                return;
            }
            var credentials = new NetworkCredential(user, password);
            using (SmtpClient client = new SmtpClient(sub[0]))
            {
                client.Port = port;
                client.UseDefaultCredentials = false;
                client.Credentials = credentials;
                client.EnableSsl = true;
                client.Send(mail); 
            }
            
        }
    }
}
