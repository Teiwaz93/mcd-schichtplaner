﻿using System;
using System.Windows.Forms;

namespace McD_Schichtplaner
{
    static class MainApp
    {
        /// <summary>
        /// Der Haupteinstiegspunkt für die Anwendung.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Mainwindow());
        }
    }
}
