CREATE TABLE VorlagenSatz (
	ID INTEGER PRIMARY KEY,
	Name varchar(20)
);
	
CREATE TABLE VorlagenSchicht (  
    ID INTEGER PRIMARY KEY,  
	Vorlage INTEGER REFERENCES VorlagenSatz(ID) ON UPDATE CASCADE ON DELETE CASCADE,
    Start INTEGER NOT NULL,  
	Ende INTEGER NOT NULL,  
    Pause INTEGER NULL,  
	Station int REFERENCES Station(ID) ON UPDATE CASCADE ON DELETE CASCADE,  
	Bemerkungen varchar(200)  
	);
	
UPDATE DatenbankInfo SET Version = 2 WHERE Instanz = 1;