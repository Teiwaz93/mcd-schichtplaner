BEGIN;
CREATE TABLE neu_Mitarbeiter (
	Nummer INTEGER PRIMARY KEY,
	Name varchar(30) NOT NULL DEFAULT 'Neuer Mitarbeiter',
	Mail varchar(40) CHECK(Mail LIKE '%@%.%') NULL,
	Geburtstag date NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Status varchar(3) CHECK(Status IN('VZ', 'TZ', 'GV', 'Mgm', 'GVS', 'Hsm', '000')) NOT NULL DEFAULT '000',
	Wochenstunden int CHECK(Wochenstunden > 0  AND Wochenstunden < 40) NOT NULL DEFAULT 39,
	Stundenlohn float NOT NULL DEFAULT 9,
	Cafe boolean NOT NULL DEFAULT 0,
	Arbeitsbeginn date NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Befristet date NULL
	);
INSERT INTO neu_Mitarbeiter SELECT * FROM Mitarbeiter;
DROP TABLE Mitarbeiter;
ALTER TABLE neu_Mitarbeiter RENAME TO Mitarbeiter;

CREATE TABLE neu_NonSchicht (
	ID int PRIMARY KEY,
	Mitarbeiter int REFERENCES Mitarbeiter(Nummer) ON UPDATE CASCADE ON DELETE CASCADE,
	Datum date NOT NULL,
	Status varchar(3) CHECK (Status IN ('S', 'BS', 'K', 'U', 'W', 'KUG')) NOT NULL
	);
INSERT INTO neu_NonSchicht SELECT * FROM NonSchicht;
DROP TABLE NonSchicht;
ALTER TABLE neu_NonSchicht RENAME TO NonSchicht;


UPDATE DatenbankInfo SET Version = 3 WHERE Instanz = 1;
COMMIT;