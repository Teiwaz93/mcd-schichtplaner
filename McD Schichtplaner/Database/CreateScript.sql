CREATE TABLE Mitarbeiter (
	Nummer INTEGER PRIMARY KEY,
	Name varchar(30) NOT NULL DEFAULT 'Neuer Mitarbeiter',
	Mail varchar(40) CHECK(Mail LIKE '%@%.%') NULL,
	Geburtstag date NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Status varchar(3) CHECK(Status IN('VZ', 'TZ', 'GV', 'Mgm', '000')) NOT NULL DEFAULT '000',
	Wochenstunden int NOT NULL DEFAULT 40,
	Stundenlohn float NOT NULL DEFAULT 9,
	Cafe boolean NOT NULL DEFAULT 0,
	Arbeitsbeginn date NOT NULL DEFAULT CURRENT_TIMESTAMP,
	Befristet date NULL
	);

CREATE TABLE MitarbeiterJob (  
	Mitarbeiter int REFERENCES Mitarbeiter(Nummer) ON UPDATE CASCADE ON DELETE CASCADE,  
	Job varchar(15) CHECK(Job IN('Küche', 'McDrive', 'Theke/Kasse', 'Service/Lobby', 'McCafe', 'Frühstück')),  
	PRIMARY KEY(Mitarbeiter, Job)  
	);

CREATE TABLE Umsatz(
	Tag int CHECK(1 <= Tag <= 31),
	Monat varchar(15) CHECK(Monat IN('Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember')) NOT NULL,
	Jahr int,
	Erwartet int DEFAULT 0,
	Tatsächlich int DEFAULT 0,
	Notiz varchar(300) DEFAULT '',
	PRIMARY KEY (Tag, Monat, Jahr)
	);
	
CREATE TABLE Station(
	ID INTEGER PRIMARY KEY,
	Bereich varchar(15) CHECK(Bereich IN('Küche', 'McDrive', 'Theke/Kasse', 'Service/Lobby', 'McCafe', 'Management')),
	Station varchar(20),
	Abkürzung varchar(3)
	);
	
CREATE TABLE Schicht (  
    ID INTEGER PRIMARY KEY,  
    Mitarbeiter int REFERENCES Mitarbeiter(Nummer) ON UPDATE CASCADE ON DELETE SET NULL,  
    Start TEXT NOT NULL,  
	Ende TEXT NOT NULL,  
    Pause TEXT NULL,  
	Pause_Minderjährig TEXT NULL,  
	Station int REFERENCES Station(ID) ON UPDATE CASCADE ON DELETE CASCADE,  
	Bemerkungen varchar(13)  
	);

CREATE TABLE DatenbankInfo (  
	Instanz int PRIMARY KEY,  
	Version int,  
	Server varchar(20),  
	Addresse varchar(30),  
	User varchar(30), 
	Passwort varchar(50) 
	);
	
CREATE TABLE NonSchicht (
	ID int PRIMARY KEY,
	Mitarbeiter int REFERENCES Mitarbeiter(Nummer) ON UPDATE CASCADE ON DELETE CASCADE,
	Datum date NOT NULL,
	Status varchar(2) CHECK (Status IN ('S', 'BS', 'K', 'U', 'W')) NOT NULL
	);
	
CREATE TABLE Verfügbarkeit (
	Mitarbeiter int REFERENCES Mitarbeiter(Nummer) ON UPDATE CASCADE ON DELETE CASCADE,
	Wochentag varchar(10) CHECK (Wochentag IN('Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag')),
	Start long NOT NULL,
	Ende long NOT NULL,
	PRIMARY KEY (Mitarbeiter, Wochentag)
	);
	
INSERT INTO Station (Bereich, Station, Abkürzung) VALUES  
	('Küche', 'Küche', 'K'),  
	('Küche', 'Seite 1', 'S1'),  
	('Küche', 'Seite 2', 'S2'),  
	('Küche', 'Toaster', 'T'),  
	('Küche', 'Garniertisch', 'GT'),  
	('Küche', 'UHC', 'UHC'),  
	('Küche', 'Grill', 'Gr'),  
	('Küche', 'Fritteuse', 'Fr'),  
	('McDrive', 'Bunker', 'Bu'),  
	('McDrive', 'Drive Runner', 'DRu'),  
	('Theke/Kasse', 'Theken Runner', 'TRu'),  
	('Theke/Kasse', 'Kasse', 'Ka'),  
	('Theke/Kasse', 'Getränkestation', 'Get'),  
	('Theke/Kasse', 'Pommesstation', 'PF'),  
	('Theke/Kasse', 'Eis', 'Eis'),  
	('Service/Lobby', 'Lobby', 'L'),  
	('Service/Lobby', 'Presenter', 'Pr'),  
	('Service/Lobby', 'Jogi', 'J'),  
	('McCafe', 'McCafe', 'Caf'), 
	('Management', 'Schichtführung', 'SF'), 
	('Management', 'Aushilfe', 'H'), 
	('Management', 'Inventur', 'Inv');