﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace McD_Schichtplaner.Database
{
    public class Shift : IComparable<Shift>
    {
        private long id;

        public long ID
        {
            get => id;
            internal set => id = value;
        }

        private long worker;
        public long WorkerID
        {
            get
            {
                return worker;
            }
            set
            {
                if (value > 0 && value < 300) worker = value;
            }
        }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public DateTime Rest { get; set; }
        public DateTime Rest_Minor { get; set; }
        public long Station { get; set; }
        public string Notes { get; set; }
        public bool B_Rest { get; set; }
        public bool B_Rest_Minor { get; set; }

        public string Label => $"{Start.Hour} - {End.Hour}";

        public string LabelLong => $"{Start.Hour}:{(Start.Minute < 10 ? $"0{Start.Minute}" : Start.Minute.ToString())} - {End.Hour}:{(End.Minute < 10 ? $"0{End.Minute}" : End.Minute.ToString())}";

        public string LabelPicker => $"{LabelLong} - {Database.DB.GetStationName(Station)}";

        public Shift()
        {

        }

        public Shift(Shift s)
        {
            id = s.id;
            worker = s.worker;
            Start = s.Start;
            End = s.End;
            Rest = s.Rest;
            Rest_Minor = s.Rest_Minor;
            Station = s.Station;
            Notes = s.Notes;
        }

        public Shift(long l)
        {
            id = l;
        }

        public Shift(DateTime s, DateTime e)
        {
            Start = s;
            End = e;
        }

        public Shift(DateTime s, DateTime e, DateTime r) : this(s, e)
        {
            Rest = r;
        }


        public Shift(DateTime s, DateTime e, long st) : this(s, e)
        {
            Station = st;
        }

        public Shift(DateTime s, DateTime e, DateTime r, long st) : this(s, e, r)
        {
            Station = st;
        }

        public Shift(long w, DateTime s, DateTime e, DateTime r, long st) : this(s, e, r, st)
        {
            WorkerID = w;
        }


        public Shift(long w, DateTime s, DateTime e, DateTime r, DateTime r2, long st) : this(w, s, e, r, st)
        {
            Rest_Minor = r2;
        }
        public Shift(long l, long worker, DateTime start, DateTime end, long station, string notes) : this(l)
        {
            WorkerID = worker;
            Start = start;
            End = end;
            Station = station;
            Notes = notes;
        }

        public Shift(long l, long worker, DateTime start, DateTime end, DateTime rest, long station, string notes) : this(l, worker, start, end, station, notes)
        {
            Rest = rest;
        }

        public Shift(long l, long worker, DateTime start, DateTime end, DateTime rest, DateTime rest_M, long station, string notes) : this(l, worker, start, end, rest, station, notes)
        {
            Rest_Minor = rest_M;
        }

        public Shift(long w, DateTime s, DateTime e, DateTime r, long st, string notes) : this(w, s, e, r, st)
        {
            Notes = notes;
        }

        public Shift(long w, DateTime start, DateTime end, int station, string notes)
        {
            WorkerID = w;
            Start = start;
            End = end;
            Station = station;
            Notes = notes;
        }

        public Shift(TemplateShift template, DateTime date)
        {
            Start = date.Add(template.Start);
            End = date.Add(template.End);
            if (template.B_Rest) Rest = date.Add(template.Rest);
            B_Rest = template.B_Rest;
            Station = template.Station;
            Notes = template.Notes;
        }

        public TimeSpan Length => Rest == DateTime.MinValue ? End - Start : End - Start - new TimeSpan(0, 30, 0);

        public string ToString()
        {
            return $"{Start} | {Station} | {End} | {Rest} | {WorkerID}";
        }

        public int CompareTo(Shift other)
        {
            return Start.Day - other.Start.Day;
        }
    }
}
