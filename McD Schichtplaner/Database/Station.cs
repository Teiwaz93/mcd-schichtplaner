﻿using static McD_Schichtplaner.Database.Database;

namespace McD_Schichtplaner.Database
{
    public class Station
    {
        public Area Area { get; }
        public string Name { get; }

        public Station (Area a, string st)
        {
            Area = a;
            Name = st;
        }
    }
}
