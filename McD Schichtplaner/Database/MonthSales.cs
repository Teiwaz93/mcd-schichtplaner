﻿using static McD_Schichtplaner.Database.Database;

namespace McD_Schichtplaner.Database
{
    public class MonthSales
    {
        private Month month;
        private int year;
        private Database data;
        private int[] expected = new int[31];
        private int[] actual = new int[31];

        public Month Month { get => month; }
        public int Year { get => year; }

        internal MonthSales(Month m, int y, Database db)
        {
            month = m;
            year = y;
            data = db;
        }

        internal MonthSales(Month m, int y, Database db, int[] exp, int[] act):this(m, y, db)
        {
            expected = exp;
            actual = act;
        }

    }
}
