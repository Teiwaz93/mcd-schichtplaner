﻿BEGIN;
ALTER TABLE DatenbankInfo 
	ADD Name varchar(100) NULL DEFAULT NULL;

UPDATE DatenbankInfo SET Version = 4 WHERE Instanz = 1;
COMMIT;