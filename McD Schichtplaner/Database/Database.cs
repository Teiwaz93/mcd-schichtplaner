﻿using McD_Schichtplaner.GUI.Fehlerbehandlung;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Runtime.Serialization;
using System.Windows.Forms;

namespace McD_Schichtplaner.Database
{
    /**
    *<summary>Klasse, die für sämtliche Kommunikation mit der Datenbank handhat und darüber hinaus
    *           nützliche Enums und Support für diese beinhaltet.</summary>
    */
    public class Database
    {
        private readonly int VERSION = 5;
        private readonly int INSTANZ = 1;
        private static Database db;

        public static Database DB
        {
            get
            {
                if (db is null)
                {
                    db = new Database();
                }
                return db;
            }
        }
        public int Instanz => INSTANZ;
        /**
         * <summary>Enum, das angbit, welchen Status ein Mitarbeiter hat (vertragsmäßig).</summary>
         */
        public enum Status { VZ, TZ, GV, GVS, Hsm, Mgm, inaktiv };


        /**
         * <summary>Enum, das einen Bereich angibt.</summary>
         */
        public enum Job { kitchen, drive, counter, service, breakfast, cafe };


        public enum Area { kitchen, drive, counter, service, cafe, mgm };

        public enum Month { January = 1, February, March, April, May, June, July, August, September, October, November, December };


        public static string MonthToString(Month m)
        {
            switch (m)
            {
                case Month.April:
                    return "April";
                case Month.August:
                    return "August";
                case Month.December:
                    return "Dezember";
                case Month.February:
                    return "Februar";
                case Month.January:
                    return "Januar";
                case Month.July:
                    return "Juli";
                case Month.June:
                    return "Juni";
                case Month.March:
                    return "März";
                case Month.May:
                    return "Mai";
                case Month.November:
                    return "November";
                case Month.October:
                    return "Oktober";
                case Month.September:
                    return "September";
                default: throw new EnumException("this shouldn't happen, never ever ...");
            }
        }

        public static Month StringToMonth(string str)
        {
            switch (str)
            {
                case "Januar":
                    return Month.January;
                case "Februar":
                    return Month.February;
                case "März":
                    return Month.March;
                case "April":
                    return Month.April;
                case "Mai":
                    return Month.May;
                case "Juni":
                    return Month.June;
                case "Juli":
                    return Month.July;
                case "August":
                    return Month.August;
                case "September":
                    return Month.September;
                case "Oktober":
                    return Month.October;
                case "November":
                    return Month.November;
                case "Dezember":
                    return Month.December;
                default: throw new EnumException("Can not convert from " + str + " to month.");
            }
        }
        public static int MonthToInt(Month m)
        {
            switch (m)
            {
                case Month.April:
                    return 4;
                case Month.August:
                    return 8;
                case Month.December:
                    return 12;
                case Month.February:
                    return 2;
                case Month.January:
                    return 1;
                case Month.July:
                    return 7;
                case Month.June:
                    return 6;
                case Month.March:
                    return 3;
                case Month.May:
                    return 5;
                case Month.November:
                    return 11;
                case Month.October:
                    return 10;
                case Month.September:
                    return 9;
                default: throw new EnumException("this shouldn't happen, never ever ...");
            }
        }

        public static Month IntToMonth(int i)
        {
            switch (i)
            {
                case 1:
                    return Month.January;
                case 2:
                    return Month.February;
                case 3:
                    return Month.March;
                case 4:
                    return Month.April;
                case 5:
                    return Month.May;
                case 6:
                    return Month.June;
                case 7:
                    return Month.July;
                case 8:
                    return Month.August;
                case 9:
                    return Month.September;
                case 10:
                    return Month.October;
                case 11:
                    return Month.November;
                case 12:
                    return Month.December;
                default: throw new EnumException("Can not convert from " + i + " to month.");
            }
        }

        /**
         * <summary>Konvertiert von string zu Status</summary>
         */
        public static Status StringToStatus(string str)
        {
            switch (str)
            {
                case "VZ": return Status.VZ;
                case "TZ": return Status.TZ;
                case "GV": return Status.GV;
                case "GVS": return Status.GVS;
                case "Hsm": return Status.Hsm;
                case "Mgm":
                case "Management": return Status.Mgm;
                case "inaktiv":
                case "000": return Status.inaktiv;
                default: throw new EnumException("Can not convert from " + str + " to Status");
            }
        }

        /**
         * <summary>Konvertiert von Status zu string</summary>
         */
        public static string StatusToString(Status st)
        {
            switch (st)
            {
                case Status.VZ: return "VZ";
                case Status.TZ: return "TZ";
                case Status.GV: return "GV";
                case Status.GVS: return "GVS";
                case Status.Hsm: return "Hsm";
                case Status.Mgm: return "Mgm";
                case Status.inaktiv: return "000";
                default: throw new EnumException("this should not have happened ... EVER");
            }
        }

        /**
         * <summary>Konvertiert von Job zu string</summary>
         */
        public static string JobToString(Job a)
        {
            switch (a)
            {
                case Job.breakfast:
                    return "Frühstück";
                case Job.counter:
                    return "Theke/Kasse";
                case Job.drive:
                    return "McDrive";
                case Job.kitchen:
                    return "Küche";
                case Job.service:
                    return "Service/Lobby";
                case Job.cafe:
                    return "McCafe";
                default: throw new EnumException("this should not have happened ... EVER");
            }
        }

        /**
         * <summary>Konvertiert von string zu Job</summary>
         */
        public static Job StringToJob(string str)
        {
            switch (str)
            {
                case "Frühstück":
                    return Job.breakfast;
                case "Theke/Kasse":
                    return Job.counter;
                case "McDrive":
                    return Job.drive;
                case "Küche":
                    return Job.kitchen;
                case "Service/Lobby":
                    return Job.service;
                case "McCafe":
                    return Job.cafe;
                default: throw new EnumException("Can not convert from " + str + " to Job");
            }
        }

        public static string AreaToString(Area a)
        {
            switch (a)
            {
                case Area.counter:
                    return "Theke/Kasse";
                case Area.drive:
                    return "McDrive";
                case Area.kitchen:
                    return "Küche";
                case Area.service:
                    return "Service/Lobby";
                case Area.cafe:
                    return "McCafe";
                case Area.mgm:
                    return "Management";
                default: throw new EnumException("this should not have happened ... EVER");
            }
        }

        /**
         * <summary>Konvertiert von string zu Area</summary>
         */
        public static Area StringToArea(string str)
        {
            switch (str)
            {
                case "Theke/Kasse":
                    return Area.counter;
                case "McDrive":
                    return Area.drive;
                case "Küche":
                    return Area.kitchen;
                case "Service/Lobby":
                    return Area.service;
                case "McCafe":
                    return Area.cafe;
                case "Cafe":
                    return Area.cafe;
                case "Management":
                    return Area.mgm;
                default: throw new EnumException("Can not convert from " + str + " to Area");
            }
        }

        /**
         * <summary>Prüft, ob angegebener string eine gültige Mailaddresse ist</summary>
         */
        public static bool CheckMailValidity(string mail)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(mail);
                return addr.Address == mail;
            }
            catch
            {
                return false;
            }
        }

        /**
         * <summary>Bringt den Nutzer dazu, eine gülitge MailAddresse einzugeben</summary>
         */
        internal static string CorrectMail()
        {
            string mail = "";
            StringError messageBox = new StringError("Nicht gültige Mail Addresse angegeben.", "example@mail.com");
            messageBox.ShowDialog();
            if (messageBox.DialogResult == DialogResult.OK)
            {
                while (!CheckMailValidity(messageBox.GetString()))
                {
                    mail = CorrectMail();
                }
            }
            return mail;
        }

        public static string ToShortTime(DateTime time)
        {
            string hour = time.Hour >= 10 ? time.Hour.ToString() : "0" + time.Hour;
            string min = time.Minute >= 10 ? time.Minute.ToString() : "0" + time.Minute;
            return hour + ":" + min;
        }

        private static SQLiteConnection connection;
        private static string connectionString = "Data Source=McD Schichtplaner.sqlite;UseUTF16Encoding=True;";

        /**
         * <summary>Konstruktor für eine neues Datenbankobjekt. Erstellt eine Verbindung - und falls nicht vorhanden auch neue Datenbank - falls nötig</summary>
         */
        public Database()
        {
            Console.WriteLine("creating database object");
            if (connection == null)
            {
                Console.WriteLine("no connection established yet");
                connection = new SQLiteConnection();
                connection.ConnectionString = connectionString; ;
                Console.WriteLine(File.Exists("McD Schichtplaner.sqlite"));
                if (File.Exists("McD Schichtplaner.sqlite"))
                {
                    Console.WriteLine("file found, opening connection");
                    connection.Open();
                    Console.WriteLine("connection opened");
                    string query = "SELECT Version FROM DatenbankInfo WHERE Instanz = " + INSTANZ;
                    SQLiteCommand command = new SQLiteCommand(query, connection);
                    int version = (int)command.ExecuteScalar();
                    if (version < VERSION)
                    {
                        var result = MessageBox.Show("Datenbank ist veraltet, soll nach Update für Datenbank (lokal) gesucht werden ?\nAlte Datenbank funktioniert unter umständen nicht korrekt\n\nAbbrechen = von Grund neu erstellen",
                            "Veraltete Datenbank", MessageBoxButtons.YesNoCancel);
                        if (result == DialogResult.Yes)
                        {
                            for (var i = version; i < VERSION; i++)
                            {
                                ApplyUpdate(i);
                            }
                        }
                        else if (result == DialogResult.Cancel)
                        {
                            recreateDatabase();
                        }
                    }
                    else if (version > VERSION)
                    {
                        MessageBox.Show("Version der Datenbank zu neu.\nDies kann folge einer beschädigten Datenbank sein.", "Achtung", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    CreateDatabase();
                }
                ExecuteNonQuery("PRAGMA foreign_keys = ON;");
            }
        }

        private void ApplyUpdate(int version)
        {
            string path = System.IO.Directory.GetCurrentDirectory().ToString();
            Console.WriteLine(path);
            string sql = System.IO.File.ReadAllText($"Database/Update{version}.sql");
            ExecuteNonQuery(sql);
        }

        /**
         * <summary>Erstellt eine neue Datenbankdatei mit allen nötige Tabellen etc.</summary>
         */
        private void CreateDatabase()
        {
            //Datei erstellen und verbinden
            Console.WriteLine("Creating new database");
            MessageBox.Show("Creating new Database ...");
            SQLiteConnection.CreateFile("McD Schichtplaner.sqlite");
            Console.WriteLine("file created");
            connection = new SQLiteConnection();
            connection.ConnectionString = connectionString;
            connection.Open();
            Console.WriteLine("connection open");

            string path = System.IO.Directory.GetCurrentDirectory().ToString();
            Console.WriteLine(path);
            string sql = System.IO.File.ReadAllText("Database/CreateScript.sql");
            ExecuteNonQuery(sql);

            string fillInfo = "INSERT INTO DatenbankInfo (Instanz, Version, Server, Addresse, User, Passwort) VALUES " +
                "(" + INSTANZ + ", " + VERSION + ", 'UNDEF', 'UNDEF', 'UNDEF', 'UNDEF')";
            ExecuteNonQuery(fillInfo);

            for (int i = 1; i < VERSION; i++)
            {
                ApplyUpdate(i);
            }
        }

        /**
         * <summary>Hauptsächlich während Entwicklung nötig. Löscht aktuelle Datenbank und erstellt neue.
         * <para>
         * Sollte nur nötig sein, wenn während der Entwicklung die Datenbank verändert wurde, nach abgeschlossener Entwicklung evtl durch Erweiterung mit Datenerhalt weiterzunutzen.
         * </para></summary>
         */
        public void recreateDatabase()
        {
            connection.Close();
            connection.Dispose();
            File.Delete(connectionString);
            System.Threading.Thread.Sleep(1000);
            CreateDatabase();
        }

        public long GetLastInsert()
        {
            return connection.LastInsertRowId;
        }

        public void FillDummies()
        {
            for (int i = 1; i <= 50; i++)
            {
                string name = "Mitarbeiter " + i;
                Worker w = new Worker(i, name, "devilfish1993@web.de", Status.VZ, new DateTime(1889, 4, 20), 10, false, DateTime.Today, 39);
                AddWorker(w);

                Job[] j = { Job.breakfast, Job.cafe, Job.counter, Job.drive, Job.kitchen, Job.service };
                foreach (var job in j)
                {
                    SetWorkerJob(w, job);
                }
            }
            DateTime date = DateTime.Now;
            if (date.Day > 5) date = date.AddDays(-5);
            date = date.AddMonths(1);
            date = date.Date;
            DateTime start;
            DateTime end;
            DateTime rest;
            string notes;
            int station;
            Shift shift;
            int worker = 1;

            station = 19;
            start = date.Add(new TimeSpan(6, 0, 0));
            end = date.Add(new TimeSpan(14, 30, 0));
            rest = date.Add(new TimeSpan(10, 30, 0));
            notes = "";
            shift = new Shift(worker++, start, end, rest, station, notes);
            CreateShift(shift);


            start = date.Add(new TimeSpan(14, 30, 0));
            end = date.Add(new TimeSpan(23, 0, 0));
            rest = date.Add(new TimeSpan(17, 30, 0));
            shift = new Shift(worker++, start, end, rest, station, notes);
            CreateShift(shift);

            start = date.Add(new TimeSpan(15, 0, 0));
            end = date.Add(new TimeSpan(20, 0, 0));
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);

            station = 4;
            start = date.Add(new TimeSpan(6, 0, 0));
            end = date.Add(new TimeSpan(14, 0, 0));
            rest = date.Add(new TimeSpan(10, 0, 0));
            shift = new Shift(worker++, start, end, rest, station, notes);
            CreateShift(shift);

            start = date.Add(new TimeSpan(14, 0, 0));
            end = date.Add(new TimeSpan(20, 0, 0));
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);

            station = 6;
            rest = date.Add(new TimeSpan(10, 30, 0));
            shift = new Shift(worker++, start, end, rest, station, notes);
            CreateShift(shift);

            start = date.Add(new TimeSpan(10, 0, 0));
            end = date.Add(new TimeSpan(16, 0, 0));
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);

            start = date.Add(new TimeSpan(14, 0, 0));
            end = date.Add(new TimeSpan(23, 0, 0));
            rest = date.Add(new TimeSpan(17, 30, 0));
            notes = "Fett filtern";
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);
            notes = "";

            station = 5;
            start = date.Add(new TimeSpan(12, 0, 0));
            end = date.Add(new TimeSpan(21, 0, 0));
            rest = date.Add(new TimeSpan(17, 0, 0));
            shift = new Shift(worker++, start, end, rest, station, notes);
            CreateShift(shift);

            station = 1;
            start = date.Add(new TimeSpan(21, 0, 0));
            end = date.Add(new TimeSpan(30, 0, 0));
            rest = date.Add(new TimeSpan(25, 0, 0));
            shift = new Shift(worker++, start, end, rest, station, notes);
            CreateShift(shift);

            rest = rest.Add(new TimeSpan(0, 30, 0));
            shift = new Shift(worker++, start, end, rest, station, notes);
            CreateShift(shift);

            station = 10;
            start = date.Add(new TimeSpan(6, 0, 0));
            end = date.Add(new TimeSpan(14, 0, 0));
            rest = date.Add(new TimeSpan(10, 0, 0));
            shift = new Shift(worker++, start, end, rest, station, notes);
            CreateShift(shift);

            start = date.Add(new TimeSpan(14, 0, 0));
            end = date.Add(new TimeSpan(23, 0, 0));
            rest = date.Add(new TimeSpan(17, 30, 0));
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);

            start = date.Add(new TimeSpan(21, 0, 0));
            end = date.Add(new TimeSpan(30, 0, 0));
            rest = date.Add(new TimeSpan(25, 0, 0));
            shift = new Shift(worker++, start, end, rest, station, notes);
            CreateShift(shift);

            station = 9;
            start = date.Add(new TimeSpan(12, 0, 0));
            end = date.Add(new TimeSpan(21, 0, 0));
            rest = date.Add(new TimeSpan(17, 0, 0));
            shift = new Shift(worker++, start, end, rest, station, notes);
            CreateShift(shift);

            station = 11;
            start = date.Add(new TimeSpan(6, 0, 0));
            end = date.Add(new TimeSpan(14, 0, 0));
            rest = date.Add(new TimeSpan(10, 0, 0));
            shift = new Shift(worker++, start, end, rest, station, notes);
            CreateShift(shift);

            start = date.Add(new TimeSpan(14, 0, 0));
            end = date.Add(new TimeSpan(23, 0, 0));
            rest = date.Add(new TimeSpan(17, 30, 0));
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);

            start = date.Add(new TimeSpan(14, 0, 0));
            end = date.Add(new TimeSpan(20, 0, 0));
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);

            start = date.Add(new TimeSpan(21, 0, 0));
            end = date.Add(new TimeSpan(30, 0, 0));
            rest = date.Add(new TimeSpan(25, 0, 0));
            shift = new Shift(worker++, start, end, rest, station, notes);
            CreateShift(shift);

            station = 12;
            start = date.Add(new TimeSpan(12, 0, 0));
            end = date.Add(new TimeSpan(21, 0, 0));
            rest = date.Add(new TimeSpan(16, 0, 0));
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);

            start = date.Add(new TimeSpan(10, 0, 0));
            end = date.Add(new TimeSpan(18, 0, 0));
            rest = date.Add(new TimeSpan(14, 30, 0));
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);

            station = 16;
            start = date.Add(new TimeSpan(6, 0, 0));
            end = date.Add(new TimeSpan(14, 0, 0));
            rest = date.Add(new TimeSpan(10, 0, 0));
            shift = new Shift(worker++, start, end, rest, station, notes);
            CreateShift(shift);

            start = date.Add(new TimeSpan(14, 0, 0));
            end = date.Add(new TimeSpan(23, 0, 0));
            rest = date.Add(new TimeSpan(17, 30, 0));
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);

            station = 17;
            start = date.Add(new TimeSpan(14, 0, 0));
            end = date.Add(new TimeSpan(20, 0, 0));
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);

            start = date.Add(new TimeSpan(12, 0, 0));
            end = date.Add(new TimeSpan(21, 0, 0));
            rest = date.Add(new TimeSpan(17, 0, 0));
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);

            station = 13;
            start = date.Add(new TimeSpan(16, 0, 0));
            end = date.Add(new TimeSpan(22, 0, 0));
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);

            station = 14;
            start = date.Add(new TimeSpan(17, 0, 0));
            end = date.Add(new TimeSpan(23, 0, 0));
            shift = new Shift(worker++, start, end, station, notes);
            CreateShift(shift);
        }

        public bool WorkerIsMinor(long id)
        {
            var w = GetWorker(id);
            return w.IsMinor();
        }

        public bool WorkerIsMinor(long id, DateTime date)
        {
            if (id == 0) return false;
            var w = GetWorker(id);
            return w.IsMinor(date);
        }

        public long GetStationID(Station station)
        {
            return GetStationID(station.Name);
        }

        public long GetStationID(string station)
        {
            string sql = "SELECT ID FROM Station WHERE Station = '" + station + "';";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            return (long)command.ExecuteScalar();
        }

        public Area GetAreaFor(Station station)
        {
            string sql = "SELECT Bereich FROM Station WHERE Station = '" + station.Name + "'";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            return StringToArea((string)command.ExecuteScalar());
        }

        public string GetWorkerName(long id)
        {
            string sql = "SELECT Name FROM Mitarbeiter WHERE Nummer = " + id;
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            return (string)command.ExecuteScalar();
        }

        public string GetStationName(long id)
        {
            string sql = "SELECT Station FROM Station WHERE ID = " + id;
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            return (string)command.ExecuteScalar();
        }

        public string GetStationShort(long id)
        {
            string sql = "SELECT Abkürzung FROM Station WHERE ID = " + id;
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            return (string)command.ExecuteScalar();
        }

        public string GetStationAreaString(long id)
        {
            string sql = "SELECT Bereich FROM Station WHERE ID = " + id;
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            return (string)command.ExecuteScalar() + "(" + GetStationName(id) + ")";
        }


        public long GetTemplateForDay(DateTime date)
        {
            string month = MonthToString(IntToMonth(date.Month));
            string sql = $"SELECT Vorlage FROM Umsatz WHERE Monat = '{month}' AND Tag = {date.Day} AND Jahr = {date.Year};";
            var reader = ExecuteQuery(sql);
            if (reader is null || !(reader.HasRows))
            {
                string create = $"INSERT INTO Umsatz (Jahr, Monat, Tag) VALUES ({date.Year}, '{month}', {date.Day});";
                ExecuteNonQuery(create);
                reader = ExecuteQuery(sql);
            }
            reader.Read();
            return reader.IsDBNull(0) ? -1 : reader.GetInt64(0);
        }

        public int GetExpDaySales(DateTime date)
        {
            return GetExpMonthSales(date.Year, date.Month, date.Day);
        }

        public int GetExpMonthSales(int year, int month, int day)
        {
            return GetExpMonthSales(year, MonthToString(IntToMonth(month)), day);
        }

        public int GetExpMonthSales(int year, Month month, int day)
        {
            return GetExpMonthSales(year, MonthToString(month), day);
        }

        public int GetExpMonthSales(int year, string month, int day)
        {
            string sql = "SELECT Erwartet FROM Umsatz WHERE Monat = '" + month + "' AND Jahr = " + year + " AND Tag = " + day + ";";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            object result = command.ExecuteScalar();
            if (result is null)
            {
                string create = "INSERT INTO Umsatz (Jahr, Monat, Tag) VALUES (" + year + ", '" + month + "', " + day + ");";
                ExecuteNonQuery(create);
                result = command.ExecuteScalar();
            }
            return (int)result;
        }

        public int GetActualDaySales(DateTime date)
        {
            string sql = $"SELECT Tatsächlich FROM Umsatz WHERE Monat = '{Database.MonthToString(Database.IntToMonth(date.Month))}' AND Jahr = {date.Year} AND Tag = {date.Day};";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            object result = command.ExecuteScalar();
            if (result is null)
            {
                string create = "INSERT INTO Umsatz (Jahr, Monat, Tag) VALUES (" + date.Year + ", '" + Database.MonthToString(Database.IntToMonth(date.Month)) + "', " + date.Day + ");";
                ExecuteNonQuery(create);
                result = command.ExecuteScalar();
            }
            return (int)result;
        }


        private static SQLiteDataReader ExecuteQuery(string sql)
        {
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            SQLiteDataReader reader = null;
            try
            {
                reader = command.ExecuteReader();
            }
            catch (SQLiteException e)
            {
                MessageBox.Show("Cannot query: " + sql + "\n" + e.Message);
            }
            finally
            {
                command.Dispose();
            }
            return reader;
        }

        public Station GetStation(long id)
        {
            string sql = "SELECT * FROM Station WHERE ID  = " + id;
            var reader = ExecuteQuery(sql);
            if (!reader.HasRows) return null;
            reader.Read();
            return new Station(StringToArea(reader.GetString(1)), reader.GetString(2));
        }


        /**
         * <summary>Gibt alle Mitarbeiter als Mitarbeiter-Objekte in einer Liste zurück, die nicht inaktiv sind</summary>
         */
        public List<Worker> GetWorkers()
        {
            string sql = "SELECT * FROM Mitarbeiter WHERE Status != '000'";
            return BuildWorkerList(sql);
        }

        public List<Worker> GetWorkers(Status status)
        {
            string sql = "SELECT * FROM Mitarbeiter WHERE Status = '" + StatusToString(status) + "'";
            return WorkerListBuilder(sql);
        }

        public List<Worker> GetCrew()
        {
            string sql = "SELECT * FROM Mitarbeiter WHERE Status = 'VZ' OR Status = 'TZ' OR Status = 'GV'";
            return WorkerListBuilder(sql);
        }

        private List<Worker> BuildWorkerList(string sql)
        {
            List<Worker> workers = new List<Worker>();
            var reader = ExecuteQuery(sql);

            while (reader.Read())
            {
                var id = reader.GetInt64(0);
                var name = reader.GetString(1);
                var mail = reader.IsDBNull(2) ? "" : reader.GetString(2);
                var birthday = reader.GetDateTime(3);
                var status = StringToStatus(reader.GetString(4));
                var hours = reader.GetInt32(5);
                var wage = reader.GetFloat(6);
                var cafe = reader.GetBoolean(7);
                var contract = reader.GetDateTime(8);
                var expires = reader[9] is System.DBNull ? new DateTime() : reader.GetDateTime(9);
                workers.Add(new Worker(id, name, mail, status, birthday, wage, cafe, contract, expires, hours));
            }
            return workers;
        }

        public Worker GetWorker(long id)
        {
            string sql = "SELECT * FROM Mitarbeiter WHERE Nummer = " + id;
            var reader = ExecuteQuery(sql);
            if (!reader.HasRows) return null;
            reader.Read();

            id = reader.GetInt64(0);
            var name = reader.GetString(1);
            var mail = reader.IsDBNull(2) ? "" : reader.GetString(2);
            var birthday = reader.GetDateTime(3);
            var status = StringToStatus(reader.GetString(4));
            var hours = reader.GetInt32(5);
            var wage = reader.GetFloat(6);
            var cafe = reader.GetBoolean(7);
            var contract = reader.GetDateTime(8);
            var expires = reader[9] is System.DBNull ? new DateTime() : reader.GetDateTime(9);
            return new Worker(id, name, mail, status, birthday, wage, cafe, contract, expires, hours);
        }

        public Worker GetWorker(string name)
        {
            string sql = "SELECT * FROM Mitarbeiter WHERE Name = '" + name + "'";
            var reader = ExecuteQuery(sql);
            if (!reader.HasRows) return null;
            reader.Read();

            var id = reader.GetInt64(0);
            name = reader.GetString(1);
            var mail = reader.IsDBNull(2) ? "" : reader.GetString(2);
            var birthday = reader.GetDateTime(3);
            var status = StringToStatus(reader.GetString(4));
            var hours = reader.GetInt32(5);
            var wage = reader.GetFloat(6);
            var cafe = reader.GetBoolean(7);
            var contract = reader.GetDateTime(8);
            var expires = reader[9] is System.DBNull ? new DateTime() : reader.GetDateTime(9);
            return new Worker(id, name, mail, status, birthday, wage, cafe, contract, expires, hours);
        }


        /**
         * <summary>Gibt eine Liste aller Bereich zurück, in der ein angegebener Mitarbeiter arbeiten kann</summary>
         */
        public List<Job> GetWorkerJobs(Worker w)
        {
            List<Job> jobs = new List<Job>();
            string sql = "SELECT Job FROM MitarbeiterJob WHERE Mitarbeiter = " + w.Id + ";";
            var reader = ExecuteQuery(sql);
            while (reader.Read())
            {
                jobs.Add(StringToJob(reader.GetString(0)));
            }
            return jobs;
        }

        public long GetNumShiftsAtDay(DateTime d)
        {
            DateTime date = new DateTime(d.Year, d.Month, d.Day, 6, 0, 0);
            string sql = $"SELECT count(ID) FROM Schicht WHERE Start >= ('{date:yyyy-MM-dd hh:mm}') AND Start <= ('{date.AddDays(1):yyyy-MM-dd hh:mm}') AND Mitarbeiter IS NOT NULL;";
            var command = new SQLiteCommand(sql, connection);
            var result = command.ExecuteScalar();
            if (result is null) return 0;
            return (long)result;
        }

        public Shift GetSingleShift(long id)
        {
            Shift shift = new Shift(-1);
            string sql = $"SELECT * FROM Schicht WHERE ID = {id};";
            var reader = ExecuteQuery(sql);
            if (reader.HasRows)
            {
                reader.Read();
                shift = new Shift()
                {
                    ID = reader.GetInt64(0),
                    Start = reader.GetDateTime(2),
                    End = reader.GetDateTime(3),
                    Rest = reader[4] is System.DBNull ? new DateTime() : reader.GetDateTime(4),
                    Rest_Minor = reader[5] is System.DBNull ? new DateTime() : reader.GetDateTime(5),
                    Station = reader.GetInt64(6),
                    Notes = reader[7] is System.DBNull ? "" : reader.GetString(7),
                    B_Rest = reader[4] is System.DBNull,
                    B_Rest_Minor = reader[5] is System.DBNull
                };
                if (!reader.IsDBNull(1)) shift.WorkerID = reader.GetInt64(1);
            }
            return shift;
        }



        public List<Shift> GetAllShiftsDay(DateTime date)
        {
            DateTime tmp = new DateTime(date.Year, date.Month, date.Day, 6, 0, 0);
            string startString = BuildDateTimeString(tmp);
            tmp = tmp.AddDays(1);
            string endString = BuildDateTimeString(tmp);

            return ShiftListBuilder(startString, endString);
        }

        public List<Shift> GetAllShiftsMonth(Month month, int year)
        {
            DateTime tmp = new DateTime(year, MonthToInt(month), 1, 6, 0, 0);
            string start = BuildDateTimeString(tmp);
            tmp = new DateTime(year, MonthToInt(month) + 1, 1, 6, 0, 0);
            string end = BuildDateTimeString(tmp);

            return ShiftListBuilder(start, end);
        }

        public List<Shift> GetWorkerShiftsMonth(Worker worker, int month, int year)
        {
            return GetWorkerShiftsMonth(worker, Database.IntToMonth(month), year);
        }

        public List<Shift> GetWorkerShiftsMonth(Worker worker, Month month, int year)
        {
            string start = BuildDateTimeString(new DateTime(year, MonthToInt(month), 1, 6, 0, 0));
            string end = BuildDateTimeString(new DateTime(year, MonthToInt(month), 1, 6, 0, 0).AddMonths(1));

            return ShiftListBuilder(start, end, worker.Id);
        }

        public List<Shift> GetWorkerShiftsInterval(Worker worker, DateTime start, DateTime end)
        {
            return ShiftListBuilder(BuildDateString(start), BuildDateString(end.AddDays(1)), worker.Id);
        }

        public List<Worker> GetFreeWorkers(DateTime date)
        {
            DateTime start = date.Date.AddHours(6);
            DateTime end = start.AddDays(1);
            string sql = "SELECT * FROM Mitarbeiter WHERE NOT EXISTS( " +
                "SELECT * FROM Schicht WHERE Schicht.Mitarbeiter = Mitarbeiter.Nummer " +
                "AND Schicht.Start >= " + BuildDateTimeString(start) + " " +
                "AND Schicht.Start < " + BuildDateTimeString(end) + ") " +
                "AND NOT EXISTS( " +
                "SELECT * FROM NonSchicht WHERE Datum = " + BuildDateString(date) + " " +
                "AND NonSchicht.Mitarbeiter = Mitarbeiter.Nummer) " +
                "AND NOT STATUS = '000'";
            return WorkerListBuilder(sql);
        }

        public List<string> FindAboutToExpireWorkers()
        {
            var result = new List<string>();
            var reader = ExecuteQuery("SELECT Name FROM Mitarbeiter WHERE julianday(Befristet) - julianday('now') < 15 AND julianday(Befristet) - julianday('now') > 0");
            while (reader.Read())
            {
                result.Add(reader.GetString(0));
            }
            return result;
        }

        public List<Worker> FindExpiredWorkers()
        {
            var result = new List<Worker>();
            var list = WorkerListBuilder("SELECT * FROM Mitarbeiter WHERE julianday(Befristet) - julianday('now') < 0 AND STATUS != '000'");
            ExecuteNonQuery($"UPDATE Mitarbeiter SET Status = '000' WHERE julianday(Befristet) - julianday('now') < 0 AND STATUS != '000'");
            return list;
        }

        public List<Worker> GetFreeWorkersTalent(DateTime date, string area)
        {
            DateTime start = date.Date.AddHours(6);
            DateTime end = start.AddDays(1);
            string sql = "SELECT * FROM Mitarbeiter WHERE NOT EXISTS( " +
                "SELECT * FROM Schicht WHERE Schicht.Mitarbeiter = Mitarbeiter.Nummer " +
                "AND Schicht.Start >= " + BuildDateTimeString(start) + " " +
                "AND Schicht.Start < " + BuildDateTimeString(end) + ") " +
                "AND NOT EXISTS( " +
                "SELECT * FROM NonSchicht WHERE Datum = " + BuildDateString(date) + " " +
                "AND NonSchicht.Mitarbeiter = Mitarbeiter.Nummer) " +
                "AND EXISTS (SELECT * FROM MitarbeiterJob WHERE MitarbeiterJob.Mitarbeiter = Mitarbeiter.Nummer " +
                "AND MitarbeiterJob.Job = '" + area + "')";
            return WorkerListBuilder(sql);
        }

        private List<Worker> WorkerListBuilder(string sql)
        {
            List<Worker> list = new List<Worker>();
            var reader = ExecuteQuery(sql);
            if (!reader.HasRows) return list;
            while (reader.Read())
            {
                var id = reader.GetInt64(0);
                var name = reader.GetString(1);
                var mail = reader.IsDBNull(2) ? "" : reader.GetString(2);
                var birthday = reader.GetDateTime(3);
                var status = StringToStatus(reader.GetString(4));
                var hours = reader.GetInt32(5);
                var wage = reader.GetFloat(6);
                var cafe = reader.GetBoolean(7);
                var contract = reader.GetDateTime(8);
                var expires = reader[9] is System.DBNull ? new DateTime() : reader.GetDateTime(9);
                list.Add(new Worker(id, name, mail, status, birthday, wage, cafe, contract, expires, hours));
            }
            return list;
        }

        private List<Shift> ShiftListBuilder(string startString, string endString)
        {
            List<Shift> result = new List<Shift>();

            string sql = "SELECT * FROM Schicht WHERE Start < " + endString + " AND Start >= " + startString + " ORDER BY Start ASC;";

            var reader = ExecuteQuery(sql);
            while (reader.Read())
            {
                result.Add(new Shift()
                {
                    ID = reader.GetInt64(0),
                    WorkerID = reader.IsDBNull(1) ? -1 : reader.GetInt64(1),
                    Start = reader.GetDateTime(2),
                    End = reader.GetDateTime(3),
                    Rest = reader[4] is System.DBNull ? new DateTime() : reader.GetDateTime(4),
                    Rest_Minor = reader[5] is System.DBNull ? new DateTime() : reader.GetDateTime(5),
                    Station = reader.GetInt64(6),
                    Notes = reader[7] is System.DBNull ? "" : reader.GetString(7),
                    B_Rest = !(reader[4] is System.DBNull),
                    B_Rest_Minor = !(reader[5] is System.DBNull)
                });
                /*
                var id = reader.GetInt64(0);
                long worker = -1;
                if (!(reader.GetValue(1) is System.DBNull)) worker = reader.GetInt64(1);
                var start = reader.GetDateTime(2);
                var end = reader.GetDateTime(3);
                DateTime rest;
                if (!reader.IsDBNull(4)) rest = reader.GetDateTime(4);
                else rest = end;
                DateTime rest_M;
                if (!reader.IsDBNull(5)) rest_M = reader.GetDateTime(5);
                else rest_M = end;
                var station = reader.GetInt64(6);
                var notes = reader.GetString(7);
                if (rest_M == end)
                {
                    if (rest == end)
                    {
                        result.Add(new Shift(id, worker, start, end, station, notes));
                    }
                    else
                    {
                        result.Add(new Shift(id, worker, start, end, rest, station, notes));
                    }
                }
                else result.Add(new Shift(id, worker, start, end, rest, rest_M, station, notes));*/
            }

            return result;
        }

        private List<Shift> ShiftListBuilder(string startString, string endString, long worker_id)
        {
            List<Shift> result = new List<Shift>();

            string sql = "SELECT * FROM Schicht WHERE Start < " + endString + " AND Start >= " + startString + " AND Mitarbeiter = " + worker_id + " ORDER BY Start;";

            var reader = ExecuteQuery(sql);
            while (reader.Read())
            {
                var id = reader.GetInt64(0);
                var worker = reader.GetInt64(1);
                var start = reader.GetDateTime(2);
                var end = reader.GetDateTime(3);
                var rest = reader[4] is System.DBNull ? new DateTime() : reader.GetDateTime(4);
                var rest_M = reader[5] is System.DBNull ? new DateTime() : reader.GetDateTime(5);
                var station = reader.GetInt64(6);
                var notes = reader[7] is System.DBNull ? "" : reader.GetString(7);
                result.Add(new Shift(id, worker, start, end, rest, rest_M, station, notes));
            }

            return result;
        }

        public List<string> GetAreas()
        {
            string sql = "SELECT Bereich FROM Station GROUP BY Bereich;";
            var reader = ExecuteQuery(sql);
            List<string> result = new List<string>();
            while (reader.Read())
            {
                if (reader.GetString(0) == "Management") continue;
                result.Add(reader.GetString(0));
            }
            return result;
        }

        public List<long> GetAllTemplateIDs()
        {
            var list = new List<long>();
            string sql = "SELECT ID FROM VorlagenSatz";
            var reader = ExecuteQuery(sql);
            if (!reader.HasRows) return list;
            while (reader.Read())
            {
                list.Add(reader.GetInt64(0));
            }
            return list;
        }

        public string GetTemplateName(long id)
        {
            string sql = $"SELECT Name FROM VorlagenSatz WHERE ID = {id}";
            var command = new SQLiteCommand(sql, connection);
            return (string)command.ExecuteScalar();
        }

        public List<string> GetStations(string area)
        {
            string sql = "SELECT Station FROM Station WHERE Bereich = '" + area + "';";
            var reader = ExecuteQuery(sql);
            List<string> result = new List<string>();
            while (reader.Read())
            {
                result.Add(reader.GetString(0));
            }
            return result;
        }

        /**
         * <summary>
         * Index 0: Server, Index 1: Addresse, Index 2: Username, Index 3: Passwort</summary>
         */
        public string[] GetMailAccount()
        {
            string sql = "SELECT Server, Addresse, User, Passwort FROM DatenbankInfo WHERE Instanz = " + INSTANZ + ";";
            var reader = ExecuteQuery(sql);
            string[] res = new string[4];
            reader.Read();
            for (int i = 0; i < 4; i++)
            {
                res[i] = reader.GetString(i);
            }
            return res;
        }

        public string GetRestaurantName()
        {
            string name;
            string sql = $"SELECT Name FROM DatenbankInfo WHERE Instanz = {INSTANZ};";
            var reader = ExecuteQuery(sql);
            reader.Read();
            if (reader.IsDBNull(0))
            {
                var form = new StringError("Bitte Restaurantname eingeben.");
                form.Text = "Restaurantname ?";
                form.ShowDialog();
                name = form.String;
                sql = $"UPDATE DatenbankInfo SET Name = '{name}' WHERE Instanz = {INSTANZ};";
                ExecuteNonQuery(sql);
            }
            else name = reader.GetString(0);
            return name;
        }

        public string GetNotes(int year, string month, int day)
        {
            string sql = "SELECT Notiz FROM Umsatz WHERE Jahr = " + year + " AND Monat = '" + month + "' AND  Tag = " + day + ";";
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            return (string)command.ExecuteScalar();
        }

        public string GetNotes(int year, int month, int day)
        {
            return GetNotes(year, MonthToString(IntToMonth(month)), day);
        }

        public string GetNotes(int year, Month month, int day)
        {
            return GetNotes(year, MonthToString(month), day);
        }

        public Availabilities GetAvailabilities(Worker worker)
        {
            if (worker is null) return null;
            Availabilities av = new Availabilities(worker);

            string sql = "SELECT * FROM Verfügbarkeit WHERE Mitarbeiter = " + worker.Id;
            var reader = ExecuteQuery(sql);
            while (reader.Read())
            {
                int day;
                switch (reader.GetString(1))
                {
                    case "Montag":
                        day = 0;
                        break;
                    case "Dienstag":
                        day = 1;
                        break;
                    case "Mittwoch":
                        day = 2;
                        break;
                    case "Donnerstag":
                        day = 3;
                        break;
                    case "Freitag":
                        day = 4;
                        break;
                    case "Samstag":
                        day = 5;
                        break;
                    case "Sonntag":
                        day = 6;
                        break;
                    default:
                        day = -1;
                        break;
                }
                TimeSpan start = TimeSpan.FromTicks(reader.GetInt64(2));
                TimeSpan end = TimeSpan.FromTicks(reader.GetInt64(3));
                av.Times[day] = new Tuple<TimeSpan, TimeSpan>(start, end);
            }

            return av;
        }

        public List<Tuple<string, long>> GetTemplates()
        {
            string sql = "SELECT * FROM VorlagenSatz";
            var reader = ExecuteQuery(sql);
            var result = new List<Tuple<string, long>>();
            while (reader.Read())
            {
                result.Add(new Tuple<string, long>(reader.GetString(1), reader.GetInt64(0)));
            }
            return result;
        }

        public List<TemplateShift> GetTemplateShifts(long id)
        {
            var result = new List<TemplateShift>();
            string sql = "SELECT * FROM VorlagenSchicht WHERE Vorlage = " + id;
            var reader = ExecuteQuery(sql);
            while (reader.Read())
            {
                result.Add(new TemplateShift()
                {
                    ID = reader.GetInt64(0),
                    Template = reader.GetInt64(1),
                    Start = new TimeSpan(reader.GetInt64(2)),
                    End = new TimeSpan(reader.GetInt64(3)),
                    B_Rest = reader.GetInt64(4) > 0,
                    Rest = reader.GetInt64(4) <= 0 ? new TimeSpan() : new TimeSpan(reader.GetInt64(4)),
                    Station = reader.GetInt64(5),
                    Notes = reader.GetString(6)
                });
            }
            return result;
        }

        public TemplateShift GetSingleTemplateShift(long id)
        {
            string sql = $"SELECT * FROM VorlagenSchicht WHERE ID = {id}";
            var reader = ExecuteQuery(sql);
            reader.Read();
            return new TemplateShift(
                    reader.GetInt64(0),
                    reader.GetInt64(1),
                    new TimeSpan(reader.GetInt64(2)),
                    new TimeSpan(reader.GetInt64(3)),
                    reader[4] is System.DBNull ? new TimeSpan() : new TimeSpan(reader.GetInt64(4)),
                    reader.GetInt64(5),
                    reader.GetString(6)
                    );
        }

        private List<Tuple<string, DateTime>> FreeDayListBuilder(string sql)
        {
            List<Tuple<string, DateTime>> result = new List<Tuple<string, DateTime>>();
            var reader = ExecuteQuery(sql);
            if (!reader.HasRows) return result;
            while (reader.Read())
            {
                result.Add(Tuple.Create(reader.GetString(3), reader.GetDateTime(2)));
            }
            return result;
        }

        public List<Tuple<string, DateTime>> GetFrees(Worker worker, int year)
        {
            if (worker is null) return new List<Tuple<string, DateTime>>();
            DateTime start = new DateTime(year, 1, 1);
            DateTime end = new DateTime(year, 12, 31);
            string sql = "SELECT * FROM NonSchicht WHERE Mitarbeiter = " + worker.Id + " AND Datum >= " + BuildDateString(start) + " AND Datum <= " + BuildDateString(end) + ";";

            return FreeDayListBuilder(sql);
        }

        public List<Tuple<string, DateTime>> GetFrees(Worker worker, DateTime start, DateTime end)
        {
            return FreeDayListBuilder($"SELECT * FROM NonSchicht WHERE Mitarbeiter = {worker.Id} AND Datum >= {BuildDateString(start)} AND Datum <= {BuildDateString(end)};");
        }

        public List<Tuple<string, DateTime>> GetFrees(Worker worker, int year, int month)
        {
            DateTime start = new DateTime(year, month, 1);
            DateTime end = new DateTime(year, month, DateTime.DaysInMonth(year, month));

            return FreeDayListBuilder("SELECT * FROM NonSchicht WHERE Mitarbeiter = " + worker.Id + " AND Datum >= " + BuildDateString(start) + " AND Datum <= " + BuildDateString(end) + ";");
        }

        private static void ExecuteNonQuery(string sql)
        {
            SQLiteCommand command = new SQLiteCommand(sql, connection);
            try
            {
                command.ExecuteNonQuery();
            }
            catch (SQLiteException e)
            {
                MessageBox.Show("Cannot exectue: " + sql + "\n" + e.Message);
            }
            finally
            {
                command.Dispose();
            }
        }

        /**
         * <summary>Fügt einen Mitarbeiter der Datenbank hinzu</summary>
         */
        public void AddWorker(Worker w)
        {
            if (w.DoesExpire())
            {
                ExecuteNonQuery("INSERT INTO Mitarbeiter (Nummer, Name, Mail, Geburtstag, Status, Wochenstunden, Stundenlohn, Cafe, Arbeitsbeginn, Befristet) VALUES( " +
                    w.Id + ", " +
                    "'" + w.Name + "', " +
                    "'" + w.Mail + "', " +
                    BuildDateString(w.Birthday) + ", " +
                    "'" + StatusToString(w.Status) + "', " +
                    w.WeekHours + ", " +
                    w.Wage.ToString().Replace(',', '.') + ", " +
                    w.Cafe + ", " +
                    BuildDateString(w.Contract) + ", " +
                    BuildDateString(w.Expires) + ");");
            }
            else
            {
                ExecuteNonQuery("INSERT INTO Mitarbeiter (Nummer, Name, Mail, Geburtstag, Status, Wochenstunden, Stundenlohn, Cafe, Arbeitsbeginn) VALUES( " +
                    w.Id + ", " +
                    "'" + w.Name + "', " +
                    "'" + w.Mail + "', " +
                    BuildDateString(w.Birthday) + ", " +
                    "'" + StatusToString(w.Status) + "', " +
                    w.WeekHours + ", " +
                    w.Wage.ToString().Replace(',', '.') + ", " +
                    w.Cafe + ", " +
                    BuildDateString(w.Contract) + ");");
            }
        }

        public long CreateWorker()
        {
            ExecuteNonQuery("INSERT INTO Mitarbeiter DEFAULT VALUES");
            return connection.LastInsertRowId;
        }

        public void AbortWorkerTransaction(Tuple<long, SQLiteTransaction> tuple)
        {
            tuple.Item2.Rollback();
        }

        public Tuple<long, SQLiteTransaction> BeginWorkerTransaction()
        {
            SQLiteTransaction transaction = connection.BeginTransaction();
            SQLiteCommand command = new SQLiteCommand()
            {
                Connection = connection,
                Transaction = transaction,
                CommandText = "INSERT INTO Mitarbeiter (Name) VALUES ('neuer Mitarbeiter')"
            };
            command.ExecuteNonQuery();
            return new Tuple<long, SQLiteTransaction>(connection.LastInsertRowId, transaction);
        }

        public void EndWorkerTransaction(Tuple<long, SQLiteTransaction> tuple, Worker worker)
        {
            SQLiteCommand command = new SQLiteCommand()
            {
                Connection = connection,
                Transaction = tuple.Item2
            };
            SQLiteTransaction transaction = tuple.Item2;
            long oldID = tuple.Item1;
            string mail = worker.Mail is null ? $"null" : $"'{worker.Mail}'";
            string expiring = worker.DoesExpire() ? $"{BuildDateString(worker.Expires)}" : $"null";
            string sql = $"UPDATE Mitarbeiter SET " +
                $"Nummer = {worker.Id}, " +
                $"Name = '{worker.Name}', " +
                $"Mail = {mail}, " +
                $"Geburtstag = {BuildDateString(worker.Birthday)}, " +
                $"Status = '{worker.Status}', " +
                $"Wochenstunden = {worker.WeekHours}, " +
                $"Stundenlohn = {worker.Wage.ToString().Replace(',', '.')}, " +
                $"Cafe = {worker.Cafe}, " +
                $"Arbeitsbeginn = {BuildDateString(worker.Contract)}, " +
                $"Befristet = {expiring} " +
                $"WHERE Nummer = {oldID};";
            command.CommandText = sql;
            try
            {
                command.ExecuteNonQuery();
                transaction.Commit();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, e.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Console.WriteLine(e);
                Console.WriteLine($"Mail is {worker.Mail} \t{worker.Mail is null}");
                transaction.Rollback();
            }
        }

        /**
         * <summary>Fügt der Datenbank die Information hinzu, das ein Mitarbeiter an einem Bereich Arbeiten kann</summary>
         */
        public static void SetWorkerJob(Worker w, Job a)
        {
            string sql = "INSERT INTO MitarbeiterJob (Mitarbeiter, Job) VALUES (" + w.Id + ", '" + JobToString(a) + "')";
            ExecuteNonQuery(sql);
        }

        public void ReSetWorkerJobs(Worker w, List<Job> jobs)
        {
            string sql = "DELETE FROM MitarbeiterJob WHERE Mitarbeiter = " + w.Id;
            ExecuteNonQuery(sql);
            foreach (var job in jobs)
            {
                SetWorkerJob(w, job);
            }
        }

        public void CreateEmptyMonth(Month m, int y)
        {
            string sql = "INSERT INTO Monat (Monat, Jahr) VALUES ('" + MonthToString(m) + "', " + y + ")";
            ExecuteNonQuery(sql);
        }

        public void CreateShift(Shift shift)
        {
            /*string sql = "INSERT INTO Schicht (Mitarbeiter, Start, Ende, Pause, Pause_Minderjährig, Station, Bemerkungen) VALUES (" +
                ((shift.WorkerID >= 1) ? shift.WorkerID.ToString() : "NULL") + ", " +
                BuildDateTimeString(shift.Start) + ", " +
                BuildDateTimeString(shift.End) + ", " +
                BuildDateTimeString(shift.Rest) + ", " +
                BuildDateTimeString(shift.Rest_Minor) + ", " +
                shift.Station + ", " +
                "'" + shift.Notes + "'" +
                ")";*/
            string sql = $"INSERT INTO Schicht (Mitarbeiter, Start, Ende, Pause, Pause_Minderjährig, Station, Bemerkungen) VALUES (" +
                $"{(shift.WorkerID >= 1 ? shift.WorkerID.ToString() : "NULL")}, " +
                $"{BuildDateTimeString(shift.Start)}, " +
                $"{BuildDateTimeString(shift.End)}, " +
                $"{(shift.B_Rest ? BuildDateTimeString(shift.Rest) : "NULL")}, " +
                $"{(shift.B_Rest_Minor ? BuildDateTimeString(shift.Rest_Minor) : "NULL")}, " +
                $"{shift.Station}, " +
                $"'{shift.Notes}' " +
                $")";
            ExecuteNonQuery(sql);
            shift.ID = connection.LastInsertRowId;
        }

        public void CreateTemplateShift(TemplateShift shift)
        {
            string sql = $"INSERT INTO VorlagenSchicht (Vorlage, Start, Ende, Pause, Station, Bemerkungen) VALUES (" +
                $"{shift.Template}, {shift.Start.Ticks}, {shift.End.Ticks}, {shift.Rest.Ticks}, {shift.Station}, '{shift.Notes}')";
            ExecuteNonQuery(sql);
        }

        public long CreateTemplate(string name)
        {
            string sql = "INSERT INTO VorlagenSatz(Name) VALUES ('" + name + "')";
            ExecuteNonQuery(sql);
            return connection.LastInsertRowId;
        }

        public void SetFree(Worker worker, int year, int month, int day, string status)
        {
            if (status != "W" &&
                status != "U" &&
                status != "S" &&
                status != "K" &&
                status != "BS" &&
                status != "KUG")
            {
                Console.WriteLine("falscher status für NonSchicht gefunden, Eintrag wird nicht erzeugt.");
                return;
            }
            string sql = "INSERT INTO NonSchicht (Mitarbeiter, Datum, Status) VALUES (" + worker.Id + ", " + BuildDateString(new DateTime(year, month, day)) + ", '" + status + "')";
            ExecuteNonQuery(sql);
        }
        public void SetFree(Worker worker, DateTime date, string status)
        {
            if (status != "W" &&
                status != "U" &&
                status != "S" &&
                status != "K" &&
                status != "BS" &&
                status != "KUG")
            {
                Console.WriteLine("falscher status für NonSchicht gefunden, Eintrag wird nicht erzeugt.");
                return;
            }
            string sql = "INSERT INTO NonSchicht (Mitarbeiter, Datum, Status) VALUES (" + worker.Id + ", " + BuildDateString(date) + ", '" + status + "')";
            ExecuteNonQuery(sql);
        }

        /**
         * <summary>bearbeitet einen Mitarbeiter in der Datenbank inklusive ID änderung</summary>
         */
        public void UpdateWorker(Worker worker, long oldID)
        {
            ExecuteNonQuery($"UPDATE Mitarbeiter SET " +
                $"Nummer = {worker.Id}," +
                $"Name = '{worker.Name}'," +
                $"Mail = {(worker.Mail == "" || worker.Mail is null ? "NULL" : $"'{worker.Mail}'")}," +
                $"Geburtstag = {BuildDateString(worker.Birthday)}," +
                $"Status = '{(worker.Status == Status.inaktiv ? "000" : worker.Status.ToString())}'," +
                $"Wochenstunden = {worker.WeekHours}," +
                $"Stundenlohn = {worker.Wage.ToString().Replace(',', '.')}," +
                $"Cafe = {worker.Cafe}," +
                $"Arbeitsbeginn = {BuildDateString(worker.Contract)}," +
                $"Befristet = {(worker.DoesExpire() ? BuildDateString(worker.Expires) : "NULL")} " +
                $"WHERE Nummer = {oldID};");
        }

        public void UpdateShift(Shift shift)
        {
            ExecuteNonQuery("UPDATE Schicht SET " +
                "Mitarbeiter = " + shift.WorkerID + "," +
                "Start = " + BuildDateTimeString(shift.Start) + "," +
                "Ende = " + BuildDateTimeString(shift.End) + "," +
                "Pause = " + BuildDateTimeString(shift.Rest) + "," +
                "Pause_Minderjährig = " + BuildDateTimeString(shift.Rest_Minor) + "," +
                "Station = " + shift.Station + "," +
                "Bemerkungen = '" + shift.Notes + "' " +
                " WHERE ID = " + shift.ID + ";");
        }

        public void UpdateTemplateShift(TemplateShift shift)
        {
            string sql = $"UPDATE VorlagenSchicht SET " +
                $"Vorlage = {shift.Template}, " +
                $"Start = {shift.Start.Ticks}, " +
                $"Ende = {shift.End.Ticks}, " +
                $"Pause = {shift.Rest.Ticks}, " +
                $"Station = {shift.Station}, " +
                $"Bemerkungen = '{shift.Notes}' " +
                $"WHERE ID = {shift.ID};";
            ExecuteNonQuery(sql);
        }

        /**
         * <summary>bearbeitet einen Mitarbeiter ind er Datenbank</summary>
         */
        public void UpdateWorker(Worker worker)
        {
            UpdateWorker(worker, worker.Id);
        }


        /**
         * <summary>Löscht die Information aus der Datenbank, das ein Mitarbeiter an einem Bereich arbeiten kann</summary>
         */
        public static void UnsetWorkerArea(Worker w, Job a)
        {
            string sql = "DELETE FROM MitarbeiterJob WHERE Mitarbeiter = " + w.Id + " AND Job = '" + JobToString(a) + "';";
            ExecuteNonQuery(sql);
        }

        public void DeleteShift(Shift shift)
        {
            if (shift.ID < 0) return;
            string sql = "DELETE FROM Schicht WHERE ID = " + shift.ID;
            ExecuteNonQuery(sql);
        }

        public void DeleteTemplateShift(TemplateShift shift)
        {
            string sql = $"DELETE FROM VorlagenSchicht WHERE ID = {shift.ID}";
            ExecuteNonQuery(sql);
        }

        public void UnsetFree(Worker worker, DateTime date)
        {
            string sql = "DELETE FROM NonSchicht WHERE Mitarbeiter = " + worker.Id + " AND Datum = " + BuildDateString(date);
            ExecuteNonQuery(sql);
        }

        public void RenameTemplate(string name, long id)
        {
            string sql = "UPDATE VorlagenSatz SET " +
                "Name = '" + name + "'" +
                "WHERE ID = " + id + ";";
            ExecuteNonQuery(sql);
        }

        public void DeleteTemplate(long id)
        {
            string sql = "DELETE FROM VorlagenSatz WHERE ID = " + id;
            ExecuteNonQuery(sql);
        }

        public void SetMailAccount(string server, string addr, string user, string pass)
        {
            string sql = "UPDATE DatenbankInfo SET " +
                "Server = '" + server + "'," +
                "Addresse = '" + addr + "'," +
                "User = '" + user + "'," +
                "Passwort = '" + pass + "' " +
                " WHERE Instanz = " + INSTANZ + ";";
            ExecuteNonQuery(sql);
        }

        public void SetTemplateForDay(DateTime date, long template)
        {
            string sql = $"UPDATE Umsatz SET Vorlage = {template} WHERE Monat = '{MonthToString(IntToMonth(date.Month))}' AND Tag = {date.Day} AND Jahr = {date.Year};";
            ExecuteNonQuery(sql);
        }

        public void SetExpMonthSales(int year, string month, int day, int sales)
        {
            string sql = "UPDATE Umsatz SET " +
                "Erwartet = " + sales +
                " WHERE Jahr = " + year + " AND Monat = '" + month + "' AND Tag = " + day + ";";
            ExecuteNonQuery(sql);
        }

        public void SetActualDaySales(DateTime date, int sales)
        {
            string sql = $"UPDATE Umsatz SET Tatsächlich = {sales} WHERE Jahr = {date.Year} AND Tag = {date.Day} AND Monat = '{Database.MonthToString(Database.IntToMonth(date.Month))}';";
            ExecuteNonQuery(sql);
        }

        public void SetExpMonthSales(int year, int month, int day, int sales)
        {
            SetExpMonthSales(year, MonthToString(IntToMonth(month)), day, sales);
        }

        public void SetExpMonthSales(int year, Month month, int day, int sales)
        {
            SetExpMonthSales(year, MonthToString(month), day, sales);
        }

        public void SetNotes(int year, string month, int day, string notes)
        {
            string sql = "UPDATE Umsatz SET " +
                "Notiz = '" + notes + "'" +
                " WHERE Jahr = " + year + " AND Monat = '" + month + "' AND Tag = " + day + ";";
            ExecuteNonQuery(sql);
        }

        public void SetNotes(int year, int month, int day, string notes)
        {
            SetNotes(year, MonthToString(IntToMonth(month)), day, notes);
        }

        public void SetNotes(int year, Month month, int day, string notes)
        {
            SetNotes(year, MonthToString(month), day, notes);
        }

        public void SetAvailabilities(Availabilities av)
        {
            ExecuteNonQuery("DELETE FROM Verfügbarkeit WHERE Mitarbeiter = " + av.Worker.Id);

            string sql = "INSERT INTO Verfügbarkeit(Mitarbeiter, Wochentag, Start, Ende) VALUES ";
            for (int d = 0; d < 7; d++)
            {
                string day = "";
                switch (d)
                {
                    case 0:
                        day = "Montag";
                        break;
                    case 1:
                        day = "Dienstag";
                        break;
                    case 2:
                        day = "Mittwoch";
                        break;
                    case 3:
                        day = "Donnerstag";
                        break;
                    case 4:
                        day = "Freitag";
                        break;
                    case 5:
                        day = "Samstag";
                        break;
                    case 6:
                        day = "Sonntag";
                        break;
                    default:
                        continue;
                }
                sql += "(" + av.Worker.Id + ", '" + day + "', " + av.Times[d].Item1.Ticks + ", " + av.Times[d].Item2.Ticks + ")";
                if (d == 6) sql += ";";
                else sql += ", ";
            }
            ExecuteNonQuery(sql);
        }

        public void SetStatus(Worker worker, Status status)
        {
            string sql = "UPDATE Mitarbeiter SET Status = '" + StatusToString(status) + "' WHERE Nummer = " + worker?.Id + ";";
            ExecuteNonQuery(sql);
        }

        public void ChangeWorkerInShift(Shift shift, Worker worker)
        {
            string sql = $"UPDATE Schicht SET Mitarbeiter = {worker.Id} WHERE ID = {shift.ID};";
            ExecuteNonQuery(sql);
            shift.WorkerID = worker.Id;
        }

        public void SwitchShiftWorkers(Shift shift1, Shift shift2)
        {
            string sql1 = $"UPDATE Schicht SET Mitarbeiter = {shift1.WorkerID} WHERE ID = {shift2.ID};";
            string sql2 = $"UPDATE Schicht SET Mitarbeiter = {shift2.WorkerID} WHERE ID = {shift1.ID};";

            long tmp = shift1.WorkerID;
            shift1.WorkerID = shift2.WorkerID;
            shift2.WorkerID = tmp;

            ExecuteNonQuery(sql1);
            ExecuteNonQuery(sql2);
        }

        public static bool WorkerIDFree(int id)
        {
            string sql = "SELECT Nummer FROM Mitarbeiter WHERE Nummer = " + id;
            var reader = ExecuteQuery(sql);
            return !reader.HasRows;
        }

        /**
         * <summary>baut einen Datumsstring zur nutzung in SQLite</summary>
         */
        private string BuildDateString(DateTime date)
        {
            if (date == null || date <= DateTime.MinValue) return "NULL";
            string year = date.Year.ToString();
            string month = "";
            if (date.Month < 10)
            {
                month = "0" + date.Month;
            }
            else month = date.Month.ToString();
            string day = "";
            if (date.Day < 10)
            {
                day = "0" + date.Day;
            }
            else day = date.Day.ToString();
            return "DATE('" + year + "-" + month + "-" + day + "')";
        }

        private string BuildDateTimeString(DateTime date)
        {
            if (date == null || date <= DateTime.MinValue) return "NULL";
            string year = date.Year.ToString();
            string month = date.Month < 10 ? "0" + date.Month : date.Month.ToString();
            string day = date.Day < 10 ? "0" + date.Day : date.Day.ToString();
            string hour = date.Hour < 10 ? "0" + date.Hour : date.Hour.ToString();
            string min = date.Minute < 10 ? "0" + date.Minute : date.Minute.ToString();

            return "DATETIME('" + year + "-" + month + "-" + day + " " + hour + ":" + min + "')";
        }
    }

    [Serializable]
    internal class EnumException : Exception
    {
        public EnumException()
        {
        }

        public EnumException(string message) : base(message)
        {
        }

    }
}
