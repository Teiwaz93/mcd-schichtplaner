﻿namespace McD_Schichtplaner.Database
{
    public class Error
    {
        public bool Severe { get; }
        public string Message { get; }

        public Error(bool b, string s)
        {
            Severe = b;
            Message = s;
        }
    }
}