﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace McD_Schichtplaner.Database
{
    public class TemplateShift
    {
        private long template;
        private TimeSpan start;
        private TimeSpan end;
        private TimeSpan rest;
        private long station;
        private string notes;
        private bool b_rest;

        internal long ID { get; set; }
        public long Template
        {
            get
            {
                return template;
            }
            set
            {
                template = value;
            }
        }
        public TimeSpan Start
        {
            get
            {
                return start;
            }
            set
            {
                if (value >= new TimeSpan(6, 0, 0) && value <= new TimeSpan(30, 0, 0))
                {
                    start = value;
                }
            }
        }
        public TimeSpan End
        {
            get
            {
                return end;
            }
            set
            {
                if (value >= new TimeSpan(6, 0, 0) && value <= new TimeSpan(30, 0, 0) && value > start)
                {
                    end = value;
                }
            }
        }
        public TimeSpan Rest
        {
            get
            {
                return rest;
            }
            set
            {
                if (value < new TimeSpan() || (value >= new TimeSpan(6, 0, 0) && value <= new TimeSpan(30, 0, 0) && value < end && value > start))
                {
                    rest = value;
                }
            }
        }
        public long Station
        {
            get { return station; }
            set { station = value; }
        }
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }
        public bool B_Rest
        {
            get { return b_rest; }
            set { b_rest = value; }
        }

        public TemplateShift(long i, long t, TimeSpan s, TimeSpan e, TimeSpan r, long st, string n)
        {
            ID = i;
            template = t;
            start = s;
            end = e;
            rest = r;
            station = st;
            notes = n;
        }

        public TemplateShift()
        {
        }

        public TemplateShift(Shift shift, long template)
        {
            this.template = template;
            start = shift.Start.TimeOfDay;
            end = (shift.End.Day == shift.Start.AddDays(1).Day) ? shift.End.TimeOfDay + new TimeSpan(24, 0, 0) : shift.End.TimeOfDay;
            rest = (shift.Rest.Day == shift.Start.AddDays(1).Day) ? shift.Rest.TimeOfDay + new TimeSpan(24, 0, 0) : shift.Rest.TimeOfDay;
            station = shift.Station;
            notes = shift.Notes;
        }

        public TemplateShift(TemplateShift shift, long template)
        {
            this.template = template;
            start = shift.Start;
            end = shift.End;
            rest = shift.Rest;
            station = shift.Station;
            notes = shift.Notes;
        }

        public TimeSpan Length => Rest == null || (Rest.Hours == 0 && Rest.Minutes == 0) ? End - Start : End - Start - new TimeSpan(0, 30, 0);
    }
}
