﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static McD_Schichtplaner.Database.Database;

namespace McD_Schichtplaner.Database
{
    /**
     * <summary>Stellt einen Mitarbeiter dar.</summary>
     */
    public class Worker
    {
        private long id;
        private string name;
        private string mail;
        private DateTime birthday;
        private double wage;
        private Status status;
        private bool cafe;
        private DateTime contract;
        private DateTime expires;
        private int weekhours;

        public long Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }

        public string Mail
        {
            get
            {
                return mail;
            }
            set
            {
                if (Database.CheckMailValidity(value))
                {
                    mail = value;
                }
            }
        }

        public DateTime Birthday { get => birthday; set => birthday = value; }
        public double Wage { get => wage; set => wage = value; }
        public Status Status { get => status; set => status = value; }
        public bool Cafe { get => cafe; set => cafe = value; }
        public DateTime Contract{ get => contract; set => contract = value; }
        public DateTime Expires { get => expires; set => expires = value; }
        public int WeekHours { get => weekhours; set => weekhours = value; }

        public TimeSpan HolidayPayHours { get
            {
                bool hasShift = false;
                DateTime month = DateTime.Today;
                int num = 0;
                TimeSpan hours = new TimeSpan(0);
                for (int i = 1; i < 12; i++)
                {
                    hasShift = false;
                    month = month.AddMonths(-1);
                    var shifts = Database.DB.GetWorkerShiftsMonth(this, month.Month, month.Year);
                    foreach(var shift in shifts)
                    {
                        hasShift = true;
                        hours = hours.Add(shift.Length);
                    }
                    if (!hasShift)
                    {
                        num = i;
                        break;
                    }
                }
                TimeSpan result = new TimeSpan(hours.Ticks / num);
                switch (status)
                {
                    case Status.GV:
                        if (result < new TimeSpan(0, 54, 0)) result = new TimeSpan(0, 54, 0);
                        else if (result > new TimeSpan(2, 43, 0)) result = new TimeSpan(2, 43, 0);
                        break;
                    case Status.GVS:
                        if (result < new TimeSpan(3, 0, 0)) result = new TimeSpan(3, 0, 0);
                        else if (result > new TimeSpan(4, 0, 0)) result = new TimeSpan(4, 0, 0);
                        break;
                    case Status.Hsm:
                    case Status.Mgm:
                    case Status.VZ:
                        if (result < new TimeSpan(7, 48, 0)) result = new TimeSpan(7, 48, 0);
                        else if (result > new TimeSpan(10, 0, 0)) result = new TimeSpan(10, 0, 0);
                        break;
                    case Status.TZ:
                        if (result < new TimeSpan(2, 54, 0)) result = new TimeSpan(2, 54, 0);
                        else if (result > new TimeSpan(6, 54, 0)) result = new TimeSpan(6, 54, 0);
                        break;
                    default: break;
                }
                return result;
            } 
        }

        /**
         * <summary>leerer Konstruktor</summary>
         */
        public Worker() { }

        /**
         * <summary>Konstruktor, der einen unbefristeten Mitarbeiter aus einer Datenbankzeile erstellt</summary>
         */
        public Worker(long id, string name, string mail, Status status, DateTime birthday, double wage, bool cafe, DateTime since, int hours)
        {
            this.id = id;
            this.name = name;
            if (Database.CheckMailValidity(mail))
            {
                this.mail = mail;
            }
            else
            {
                if (mail != "")
                {
                    this.mail = Database.CorrectMail(); 
                }
            }
            this.birthday = birthday;
            this.wage = wage;
            this.status = status;
            this.cafe = cafe;
            contract = since;
            weekhours = hours;
        }

        public Worker(long id, string name, string mail, Status status, DateTime birthday, double wage, bool cafe, DateTime since, DateTime until, int hours): this(id, name, mail, status, birthday, wage, cafe, since, hours)
        {
            if (until > DateTime.MinValue) expires = until;
        }

        /**
         * <summary>stellt fest ob ein Mitarbeiter minderjährig ist.</summary>
         */
        public bool IsMinor()
        {
            return IsMinor(DateTime.Today);
        }

        public bool IsMinor(DateTime date)
        {
            int offset = 4;
            TimeSpan age = date - birthday;
            if (birthday.Year % 4 == 0) offset = 5;
            return age < new TimeSpan(365 * 18 + offset, 0, 0, 0);
        }

        public bool DoesExpire()
        {
            return !(expires == null || expires <= DateTime.MinValue);
        }

        public override bool Equals(object obj)
        {
            return obj is Worker worker &&
                   id == worker.id;
        }

        public override int GetHashCode()
        {
            return 1877310944 + id.GetHashCode();
        }

        /**
         * <summary>vergleicht zwei Mitarrbeiter auf gleichheit über die ID</summary>
         */
        public static bool operator ==(Worker w1, Worker w2)
        {
            return w1 is null || w2 is null ? false : w1.id == w2.id;
        }

        /**
         * <summary>vergleicht zwei Mitarrbeiter auf ungleichheit über die ID</summary>
         */
        public static bool operator !=(Worker w1, Worker w2)
        {
            return w1 is null || w2 is null ? true : w1.id != w2.id;
        }

        public TimeSpan GetHoursForNonShift(string shift)
        {
            switch (shift)
            {
                case "S":
                case "BS":
                    return new TimeSpan(7, 48, 0);
                    break;
                case "U":
                case "K":
                    return HolidayPayHours;
                    break;
                case "KUG":
                    return new TimeSpan((HolidayPayHours.Ticks / 10) * 9);
                    break;
                default: return new TimeSpan(0);
            }
        }
    }
}
