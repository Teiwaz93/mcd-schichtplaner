﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace McD_Schichtplaner.Database
{
    public class Availabilities
    {
        public readonly Worker Worker;
        private Tuple<TimeSpan, TimeSpan>[] times = new Tuple<TimeSpan, TimeSpan>[7];

        public Tuple<TimeSpan, TimeSpan>[] Times
        {
            get => times;
            protected set
            {
                if (value.Length == 7) times = value;
            }
        }

        public Availabilities(Worker w)
        {
            Worker = w;
            TimeSpan start = new TimeSpan(6, 0, 0);
            TimeSpan end = new TimeSpan(30, 0, 0);
            for (int i = 0; i < 7; i++)
            {
                times[i] = new Tuple<TimeSpan, TimeSpan>(start, end);
            }
        }
    }
}
