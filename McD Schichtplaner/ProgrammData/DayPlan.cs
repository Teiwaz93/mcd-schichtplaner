﻿using McD_Schichtplaner.Database;
using System;
using System.Collections.Generic;

namespace McD_Schichtplaner.ProgrammData
{
    public class DayPlan
    {
        private List<Shift> shifts;
        private DateTime date;

        public DayPlan(DateTime d)
        {
            date = d;
            shifts = new List<Shift>();
        }
    }
}
