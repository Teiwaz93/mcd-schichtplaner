﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace McD_Schichtplaner.Maintenance
{
    public partial class MaintenanceForm : Form
    {
        private Database.Database data;
        public MaintenanceForm(Database.Database d)
        {
            InitializeComponent();
            data = d;
            resolution_Label.Text = "Auflösung: " + System.Windows.Forms.Screen.PrimaryScreen.Bounds;
        }

        private void RecreateEmpty_Button_Click(object sender, EventArgs e)
        {
            data.recreateDatabase();
        }

        private void DummyFill_Button_Click(object sender, EventArgs e)
        {
            data.FillDummies();
        }

        private void Close_Button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
