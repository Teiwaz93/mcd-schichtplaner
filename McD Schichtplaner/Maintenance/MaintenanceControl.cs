﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace McD_Schichtplaner.Maintenance
{
    public partial class MaintenanceControl : UserControl
    {
        private Database.Database data = Database.Database.DB;
        private bool secret = true;

        private MaintenanceControl()
        {
            InitializeComponent();
        }

        public MaintenanceControl(Size s)
        {
            InitializeComponent();
            Size = s;
            var acc = data.GetMailAccount();
            string[] address = acc[0].Split(':');
            if (address.Length == 1)
            {
                server_TextBox.Text = address[0];
            }
            else if (address.Length == 2)
            {
                server_TextBox.Text = address[0];
                port_TextBox.Text = address[1];
            }
            else
            {
                string warning = $"{acc[0]} ist keine gültige Serveraddresse. Setze zurück ...";
                Console.WriteLine(warning);
                MessageBox.Show("Fehler", warning, MessageBoxButtons.OK);
                server_TextBox.Text = "UNDEF";
                port_TextBox.Text = "";
            }
            address_TextBox.Text = acc[1];
            user_TextBox.Text = acc[2];
            password_TextBox.Text = acc[3];
        }

        public void Save()
        {
            Console.WriteLine("Saving Maintenance stuff ...");
            data.SetMailAccount(server_TextBox.Text + ":" + port_TextBox.Text, address_TextBox.Text, user_TextBox.Text, password_TextBox.Text);
        }

        private void passwordChar_Button_Click(object sender, EventArgs e)
        {
            if (secret)
            {
                password_TextBox.PasswordChar = '\0';
            }
            else
            {
                password_TextBox.PasswordChar = '*';
            }
            secret = !secret;
        }
    }
}
