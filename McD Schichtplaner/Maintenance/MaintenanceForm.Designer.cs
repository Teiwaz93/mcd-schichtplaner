﻿namespace McD_Schichtplaner.Maintenance
{
    partial class MaintenanceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.recreateEmpty_Button = new System.Windows.Forms.Button();
            this.dummyFill_Button = new System.Windows.Forms.Button();
            this.close_Button = new System.Windows.Forms.Button();
            this.resolution_Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // recreateEmpty_Button
            // 
            this.recreateEmpty_Button.AutoSize = true;
            this.recreateEmpty_Button.Location = new System.Drawing.Point(12, 12);
            this.recreateEmpty_Button.Name = "recreateEmpty_Button";
            this.recreateEmpty_Button.Size = new System.Drawing.Size(126, 23);
            this.recreateEmpty_Button.TabIndex = 0;
            this.recreateEmpty_Button.Text = "DB neu erzeugen (leer)";
            this.recreateEmpty_Button.UseVisualStyleBackColor = true;
            this.recreateEmpty_Button.Click += new System.EventHandler(this.RecreateEmpty_Button_Click);
            // 
            // dummyFill_Button
            // 
            this.dummyFill_Button.AutoSize = true;
            this.dummyFill_Button.Location = new System.Drawing.Point(231, 12);
            this.dummyFill_Button.Name = "dummyFill_Button";
            this.dummyFill_Button.Size = new System.Drawing.Size(141, 23);
            this.dummyFill_Button.TabIndex = 1;
            this.dummyFill_Button.Text = "Dummy-Eintrage erzeugen";
            this.dummyFill_Button.UseVisualStyleBackColor = true;
            this.dummyFill_Button.Click += new System.EventHandler(this.DummyFill_Button_Click);
            // 
            // close_Button
            // 
            this.close_Button.AutoSize = true;
            this.close_Button.Location = new System.Drawing.Point(15, 71);
            this.close_Button.Name = "close_Button";
            this.close_Button.Size = new System.Drawing.Size(92, 25);
            this.close_Button.TabIndex = 2;
            this.close_Button.Text = "Schließen";
            this.close_Button.UseVisualStyleBackColor = true;
            this.close_Button.Click += new System.EventHandler(this.Close_Button_Click);
            // 
            // resolution_Label
            // 
            this.resolution_Label.AutoSize = true;
            this.resolution_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resolution_Label.Location = new System.Drawing.Point(12, 51);
            this.resolution_Label.Name = "resolution_Label";
            this.resolution_Label.Size = new System.Drawing.Size(117, 17);
            this.resolution_Label.TabIndex = 3;
            this.resolution_Label.Text = "screen resolution";
            // 
            // MaintenanceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 99);
            this.Controls.Add(this.resolution_Label);
            this.Controls.Add(this.close_Button);
            this.Controls.Add(this.dummyFill_Button);
            this.Controls.Add(this.recreateEmpty_Button);
            this.Name = "MaintenanceForm";
            this.Text = "MaintenanceForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button recreateEmpty_Button;
        private System.Windows.Forms.Button dummyFill_Button;
        private System.Windows.Forms.Button close_Button;
        private System.Windows.Forms.Label resolution_Label;
    }
}