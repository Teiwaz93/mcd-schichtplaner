﻿namespace McD_Schichtplaner.Maintenance
{
    partial class MaintenanceControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.mailMain_Panel = new System.Windows.Forms.Panel();
            this.mail_TablePanel = new System.Windows.Forms.TableLayoutPanel();
            this.mailHeader_Label = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.password_TextBox = new System.Windows.Forms.TextBox();
            this.passwordChar_Button = new System.Windows.Forms.Button();
            this.user_TextBox = new System.Windows.Forms.TextBox();
            this.password_Label = new System.Windows.Forms.Label();
            this.user_Label = new System.Windows.Forms.Label();
            this.address_Label = new System.Windows.Forms.Label();
            this.server_Label = new System.Windows.Forms.Label();
            this.server_TablePanel = new System.Windows.Forms.TableLayoutPanel();
            this.port_TextBox = new System.Windows.Forms.TextBox();
            this.colon_Label = new System.Windows.Forms.Label();
            this.server_TextBox = new System.Windows.Forms.TextBox();
            this.address_TextBox = new System.Windows.Forms.TextBox();
            this.mainTableLayout.SuspendLayout();
            this.mailMain_Panel.SuspendLayout();
            this.mail_TablePanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.server_TablePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTableLayout
            // 
            this.mainTableLayout.ColumnCount = 2;
            this.mainTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainTableLayout.Controls.Add(this.mailMain_Panel, 0, 0);
            this.mainTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTableLayout.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.mainTableLayout.Location = new System.Drawing.Point(0, 0);
            this.mainTableLayout.Name = "mainTableLayout";
            this.mainTableLayout.RowCount = 1;
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainTableLayout.Size = new System.Drawing.Size(1264, 891);
            this.mainTableLayout.TabIndex = 0;
            // 
            // mailMain_Panel
            // 
            this.mailMain_Panel.Controls.Add(this.mail_TablePanel);
            this.mailMain_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mailMain_Panel.Location = new System.Drawing.Point(3, 3);
            this.mailMain_Panel.Name = "mailMain_Panel";
            this.mailMain_Panel.Padding = new System.Windows.Forms.Padding(75);
            this.mailMain_Panel.Size = new System.Drawing.Size(626, 885);
            this.mailMain_Panel.TabIndex = 0;
            // 
            // mail_TablePanel
            // 
            this.mail_TablePanel.ColumnCount = 2;
            this.mail_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.mail_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.mail_TablePanel.Controls.Add(this.tableLayoutPanel1, 1, 5);
            this.mail_TablePanel.Controls.Add(this.user_TextBox, 1, 4);
            this.mail_TablePanel.Controls.Add(this.password_Label, 0, 5);
            this.mail_TablePanel.Controls.Add(this.user_Label, 0, 4);
            this.mail_TablePanel.Controls.Add(this.address_Label, 0, 3);
            this.mail_TablePanel.Controls.Add(this.server_Label, 0, 2);
            this.mail_TablePanel.Controls.Add(this.mailHeader_Label, 0, 0);
            this.mail_TablePanel.Controls.Add(this.server_TablePanel, 1, 2);
            this.mail_TablePanel.Controls.Add(this.address_TextBox, 1, 3);
            this.mail_TablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mail_TablePanel.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.mail_TablePanel.Location = new System.Drawing.Point(75, 75);
            this.mail_TablePanel.Name = "mail_TablePanel";
            this.mail_TablePanel.RowCount = 6;
            this.mail_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.mail_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.mail_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.mail_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.mail_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.mail_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.mail_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mail_TablePanel.Size = new System.Drawing.Size(476, 735);
            this.mail_TablePanel.TabIndex = 0;
            // 
            // mailHeader_Label
            // 
            this.mailHeader_Label.AutoSize = true;
            this.mail_TablePanel.SetColumnSpan(this.mailHeader_Label, 2);
            this.mailHeader_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mailHeader_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mailHeader_Label.Location = new System.Drawing.Point(3, 0);
            this.mailHeader_Label.Name = "mailHeader_Label";
            this.mailHeader_Label.Size = new System.Drawing.Size(470, 122);
            this.mailHeader_Label.TabIndex = 0;
            this.mailHeader_Label.Text = "Mail Einstellungen:";
            this.mailHeader_Label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Controls.Add(this.password_TextBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.passwordChar_Button, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(161, 613);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(312, 119);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // password_TextBox
            // 
            this.password_TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.password_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password_TextBox.Location = new System.Drawing.Point(3, 45);
            this.password_TextBox.Name = "password_TextBox";
            this.password_TextBox.PasswordChar = '*';
            this.password_TextBox.Size = new System.Drawing.Size(238, 26);
            this.password_TextBox.TabIndex = 1;
            // 
            // passwordChar_Button
            // 
            this.passwordChar_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.passwordChar_Button.Location = new System.Drawing.Point(265, 45);
            this.passwordChar_Button.Name = "passwordChar_Button";
            this.passwordChar_Button.Size = new System.Drawing.Size(44, 23);
            this.passwordChar_Button.TabIndex = 2;
            this.passwordChar_Button.Text = "*";
            this.passwordChar_Button.UseVisualStyleBackColor = true;
            this.passwordChar_Button.Click += new System.EventHandler(this.passwordChar_Button_Click);
            // 
            // user_TextBox
            // 
            this.user_TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.user_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user_TextBox.Location = new System.Drawing.Point(161, 533);
            this.user_TextBox.Margin = new System.Windows.Forms.Padding(3, 45, 3, 45);
            this.user_TextBox.Name = "user_TextBox";
            this.user_TextBox.Size = new System.Drawing.Size(312, 26);
            this.user_TextBox.TabIndex = 8;
            // 
            // password_Label
            // 
            this.password_Label.AutoSize = true;
            this.password_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.password_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password_Label.Location = new System.Drawing.Point(3, 610);
            this.password_Label.Name = "password_Label";
            this.password_Label.Size = new System.Drawing.Size(152, 125);
            this.password_Label.TabIndex = 4;
            this.password_Label.Text = "Passwort:";
            this.password_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // user_Label
            // 
            this.user_Label.AutoSize = true;
            this.user_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.user_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user_Label.Location = new System.Drawing.Point(3, 488);
            this.user_Label.Name = "user_Label";
            this.user_Label.Size = new System.Drawing.Size(152, 122);
            this.user_Label.TabIndex = 3;
            this.user_Label.Text = "Mail Username:";
            this.user_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // address_Label
            // 
            this.address_Label.AutoSize = true;
            this.address_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.address_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.address_Label.Location = new System.Drawing.Point(3, 366);
            this.address_Label.Name = "address_Label";
            this.address_Label.Size = new System.Drawing.Size(152, 122);
            this.address_Label.TabIndex = 2;
            this.address_Label.Text = "Mail Addresse:";
            this.address_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // server_Label
            // 
            this.server_Label.AutoSize = true;
            this.server_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.server_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.server_Label.Location = new System.Drawing.Point(3, 244);
            this.server_Label.Name = "server_Label";
            this.server_Label.Size = new System.Drawing.Size(152, 122);
            this.server_Label.TabIndex = 1;
            this.server_Label.Text = "SMTP Server:";
            this.server_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // server_TablePanel
            // 
            this.server_TablePanel.ColumnCount = 3;
            this.server_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.server_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.server_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.server_TablePanel.Controls.Add(this.port_TextBox, 2, 1);
            this.server_TablePanel.Controls.Add(this.colon_Label, 1, 1);
            this.server_TablePanel.Controls.Add(this.server_TextBox, 0, 1);
            this.server_TablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.server_TablePanel.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.server_TablePanel.Location = new System.Drawing.Point(161, 247);
            this.server_TablePanel.Name = "server_TablePanel";
            this.server_TablePanel.RowCount = 3;
            this.server_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.server_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.server_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.server_TablePanel.Size = new System.Drawing.Size(312, 116);
            this.server_TablePanel.TabIndex = 5;
            // 
            // port_TextBox
            // 
            this.port_TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.port_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.port_TextBox.Location = new System.Drawing.Point(265, 43);
            this.port_TextBox.Name = "port_TextBox";
            this.port_TextBox.Size = new System.Drawing.Size(44, 26);
            this.port_TextBox.TabIndex = 2;
            // 
            // colon_Label
            // 
            this.colon_Label.AutoSize = true;
            this.colon_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colon_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.colon_Label.Location = new System.Drawing.Point(247, 40);
            this.colon_Label.Name = "colon_Label";
            this.colon_Label.Size = new System.Drawing.Size(12, 35);
            this.colon_Label.TabIndex = 0;
            this.colon_Label.Text = ":";
            this.colon_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // server_TextBox
            // 
            this.server_TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.server_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.server_TextBox.Location = new System.Drawing.Point(3, 43);
            this.server_TextBox.Name = "server_TextBox";
            this.server_TextBox.Size = new System.Drawing.Size(238, 26);
            this.server_TextBox.TabIndex = 1;
            // 
            // address_TextBox
            // 
            this.address_TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.address_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.address_TextBox.Location = new System.Drawing.Point(161, 411);
            this.address_TextBox.Margin = new System.Windows.Forms.Padding(3, 45, 3, 45);
            this.address_TextBox.Name = "address_TextBox";
            this.address_TextBox.Size = new System.Drawing.Size(312, 26);
            this.address_TextBox.TabIndex = 6;
            // 
            // MaintenanceControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainTableLayout);
            this.Name = "MaintenanceControl";
            this.Size = new System.Drawing.Size(1264, 891);
            this.mainTableLayout.ResumeLayout(false);
            this.mailMain_Panel.ResumeLayout(false);
            this.mail_TablePanel.ResumeLayout(false);
            this.mail_TablePanel.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.server_TablePanel.ResumeLayout(false);
            this.server_TablePanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTableLayout;
        private System.Windows.Forms.Panel mailMain_Panel;
        private System.Windows.Forms.TableLayoutPanel mail_TablePanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox password_TextBox;
        private System.Windows.Forms.Button passwordChar_Button;
        private System.Windows.Forms.TextBox user_TextBox;
        private System.Windows.Forms.Label password_Label;
        private System.Windows.Forms.Label user_Label;
        private System.Windows.Forms.Label address_Label;
        private System.Windows.Forms.Label server_Label;
        private System.Windows.Forms.Label mailHeader_Label;
        private System.Windows.Forms.TableLayoutPanel server_TablePanel;
        private System.Windows.Forms.TextBox port_TextBox;
        private System.Windows.Forms.Label colon_Label;
        private System.Windows.Forms.TextBox server_TextBox;
        private System.Windows.Forms.TextBox address_TextBox;
    }
}
