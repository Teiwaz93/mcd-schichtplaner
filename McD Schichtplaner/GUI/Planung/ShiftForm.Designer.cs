﻿namespace McD_Schichtplaner.GUI.Planung
{
    partial class ShiftForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShiftForm));
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.notes_Textbox = new System.Windows.Forms.TextBox();
            this.station_ComboBox = new System.Windows.Forms.ComboBox();
            this.restM_Panel = new System.Windows.Forms.Panel();
            this.rest2_CheckBox = new System.Windows.Forms.CheckBox();
            this.restM_Label = new System.Windows.Forms.Label();
            this.restM_Picker = new System.Windows.Forms.DateTimePicker();
            this.rest_Panel = new System.Windows.Forms.Panel();
            this.rest1_CheckBox = new System.Windows.Forms.CheckBox();
            this.rest_Label = new System.Windows.Forms.Label();
            this.rest_Picker = new System.Windows.Forms.DateTimePicker();
            this.end_Panel = new System.Windows.Forms.Panel();
            this.end_Label = new System.Windows.Forms.Label();
            this.end_Picker = new System.Windows.Forms.DateTimePicker();
            this.start_Panel = new System.Windows.Forms.Panel();
            this.start_Label = new System.Windows.Forms.Label();
            this.start_Picker = new System.Windows.Forms.DateTimePicker();
            this.worker_Panel = new System.Windows.Forms.Panel();
            this.worker_Label = new System.Windows.Forms.Label();
            this.worker_ComboBox = new System.Windows.Forms.ComboBox();
            this.area_ComboBox = new System.Windows.Forms.ComboBox();
            this.button_Panel = new System.Windows.Forms.Panel();
            this.button_Table = new System.Windows.Forms.TableLayoutPanel();
            this.delete_Button = new System.Windows.Forms.Button();
            this.accept_Button = new System.Windows.Forms.Button();
            this.abort_Button = new System.Windows.Forms.Button();
            this.tableLayoutPanel.SuspendLayout();
            this.restM_Panel.SuspendLayout();
            this.rest_Panel.SuspendLayout();
            this.end_Panel.SuspendLayout();
            this.start_Panel.SuspendLayout();
            this.worker_Panel.SuspendLayout();
            this.button_Panel.SuspendLayout();
            this.button_Table.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.notes_Textbox, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.station_ComboBox, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.restM_Panel, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.rest_Panel, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.end_Panel, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.start_Panel, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.worker_Panel, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.area_ComboBox, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.button_Panel, 1, 4);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 5;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(481, 365);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // notes_Textbox
            // 
            this.notes_Textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.notes_Textbox.Location = new System.Drawing.Point(3, 250);
            this.notes_Textbox.Margin = new System.Windows.Forms.Padding(3, 85, 3, 3);
            this.notes_Textbox.MaxLength = 13;
            this.notes_Textbox.Name = "notes_Textbox";
            this.notes_Textbox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.notes_Textbox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.notes_Textbox.Size = new System.Drawing.Size(234, 20);
            this.notes_Textbox.TabIndex = 6;
            this.notes_Textbox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // station_ComboBox
            // 
            this.station_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.station_ComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.station_ComboBox.FormattingEnabled = true;
            this.station_ComboBox.Location = new System.Drawing.Point(243, 78);
            this.station_ComboBox.Name = "station_ComboBox";
            this.station_ComboBox.Size = new System.Drawing.Size(231, 24);
            this.station_ComboBox.TabIndex = 1;
            // 
            // restM_Panel
            // 
            this.restM_Panel.Controls.Add(this.rest2_CheckBox);
            this.restM_Panel.Controls.Add(this.restM_Label);
            this.restM_Panel.Controls.Add(this.restM_Picker);
            this.restM_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.restM_Panel.Location = new System.Drawing.Point(243, 138);
            this.restM_Panel.Name = "restM_Panel";
            this.restM_Panel.Size = new System.Drawing.Size(235, 24);
            this.restM_Panel.TabIndex = 5;
            // 
            // rest2_CheckBox
            // 
            this.rest2_CheckBox.AutoSize = true;
            this.rest2_CheckBox.Enabled = false;
            this.rest2_CheckBox.Location = new System.Drawing.Point(131, 3);
            this.rest2_CheckBox.Name = "rest2_CheckBox";
            this.rest2_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.rest2_CheckBox.TabIndex = 4;
            this.rest2_CheckBox.UseVisualStyleBackColor = true;
            this.rest2_CheckBox.CheckedChanged += new System.EventHandler(this.Rest2_CheckBox_CheckedChanged);
            // 
            // restM_Label
            // 
            this.restM_Label.AutoSize = true;
            this.restM_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restM_Label.Location = new System.Drawing.Point(11, 4);
            this.restM_Label.Name = "restM_Label";
            this.restM_Label.Size = new System.Drawing.Size(64, 17);
            this.restM_Label.TabIndex = 2;
            this.restM_Label.Text = "Pause 2:";
            this.restM_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // restM_Picker
            // 
            this.restM_Picker.Enabled = false;
            this.restM_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.restM_Picker.Location = new System.Drawing.Point(152, 0);
            this.restM_Picker.Name = "restM_Picker";
            this.restM_Picker.ShowUpDown = true;
            this.restM_Picker.Size = new System.Drawing.Size(79, 20);
            this.restM_Picker.TabIndex = 0;
            // 
            // rest_Panel
            // 
            this.rest_Panel.Controls.Add(this.rest1_CheckBox);
            this.rest_Panel.Controls.Add(this.rest_Label);
            this.rest_Panel.Controls.Add(this.rest_Picker);
            this.rest_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rest_Panel.Location = new System.Drawing.Point(3, 138);
            this.rest_Panel.Name = "rest_Panel";
            this.rest_Panel.Size = new System.Drawing.Size(234, 24);
            this.rest_Panel.TabIndex = 4;
            // 
            // rest1_CheckBox
            // 
            this.rest1_CheckBox.AutoSize = true;
            this.rest1_CheckBox.Enabled = false;
            this.rest1_CheckBox.Location = new System.Drawing.Point(131, 3);
            this.rest1_CheckBox.Name = "rest1_CheckBox";
            this.rest1_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.rest1_CheckBox.TabIndex = 3;
            this.rest1_CheckBox.UseVisualStyleBackColor = true;
            this.rest1_CheckBox.CheckedChanged += new System.EventHandler(this.Rest1_CheckBox_CheckedChanged);
            // 
            // rest_Label
            // 
            this.rest_Label.AutoSize = true;
            this.rest_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rest_Label.Location = new System.Drawing.Point(11, 4);
            this.rest_Label.Name = "rest_Label";
            this.rest_Label.Size = new System.Drawing.Size(52, 17);
            this.rest_Label.TabIndex = 2;
            this.rest_Label.Text = "Pause:";
            this.rest_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rest_Picker
            // 
            this.rest_Picker.Enabled = false;
            this.rest_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.rest_Picker.Location = new System.Drawing.Point(152, 0);
            this.rest_Picker.Name = "rest_Picker";
            this.rest_Picker.ShowUpDown = true;
            this.rest_Picker.Size = new System.Drawing.Size(79, 20);
            this.rest_Picker.TabIndex = 0;
            this.rest_Picker.ValueChanged += new System.EventHandler(this.RestChanged);
            // 
            // end_Panel
            // 
            this.end_Panel.Controls.Add(this.end_Label);
            this.end_Panel.Controls.Add(this.end_Picker);
            this.end_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.end_Panel.Location = new System.Drawing.Point(243, 108);
            this.end_Panel.Name = "end_Panel";
            this.end_Panel.Size = new System.Drawing.Size(235, 24);
            this.end_Panel.TabIndex = 3;
            // 
            // end_Label
            // 
            this.end_Label.AutoSize = true;
            this.end_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.end_Label.Location = new System.Drawing.Point(11, 4);
            this.end_Label.Name = "end_Label";
            this.end_Label.Size = new System.Drawing.Size(45, 17);
            this.end_Label.TabIndex = 1;
            this.end_Label.Text = "Ende:";
            this.end_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // end_Picker
            // 
            this.end_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.end_Picker.Location = new System.Drawing.Point(152, 0);
            this.end_Picker.Name = "end_Picker";
            this.end_Picker.ShowUpDown = true;
            this.end_Picker.Size = new System.Drawing.Size(79, 20);
            this.end_Picker.TabIndex = 0;
            this.end_Picker.ValueChanged += new System.EventHandler(this.EndChanged);
            // 
            // start_Panel
            // 
            this.start_Panel.Controls.Add(this.start_Label);
            this.start_Panel.Controls.Add(this.start_Picker);
            this.start_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.start_Panel.Location = new System.Drawing.Point(3, 108);
            this.start_Panel.Name = "start_Panel";
            this.start_Panel.Size = new System.Drawing.Size(234, 24);
            this.start_Panel.TabIndex = 2;
            // 
            // start_Label
            // 
            this.start_Label.AutoSize = true;
            this.start_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.start_Label.Location = new System.Drawing.Point(11, 4);
            this.start_Label.Name = "start_Label";
            this.start_Label.Size = new System.Drawing.Size(42, 17);
            this.start_Label.TabIndex = 1;
            this.start_Label.Text = "Start:";
            this.start_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // start_Picker
            // 
            this.start_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.start_Picker.Location = new System.Drawing.Point(152, 0);
            this.start_Picker.Name = "start_Picker";
            this.start_Picker.ShowUpDown = true;
            this.start_Picker.Size = new System.Drawing.Size(79, 20);
            this.start_Picker.TabIndex = 0;
            this.start_Picker.ValueChanged += new System.EventHandler(this.StartChanged);
            // 
            // worker_Panel
            // 
            this.tableLayoutPanel.SetColumnSpan(this.worker_Panel, 2);
            this.worker_Panel.Controls.Add(this.worker_Label);
            this.worker_Panel.Controls.Add(this.worker_ComboBox);
            this.worker_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.worker_Panel.Location = new System.Drawing.Point(3, 3);
            this.worker_Panel.Name = "worker_Panel";
            this.worker_Panel.Size = new System.Drawing.Size(475, 69);
            this.worker_Panel.TabIndex = 0;
            // 
            // worker_Label
            // 
            this.worker_Label.AutoSize = true;
            this.worker_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.worker_Label.Location = new System.Drawing.Point(9, 21);
            this.worker_Label.Name = "worker_Label";
            this.worker_Label.Size = new System.Drawing.Size(120, 26);
            this.worker_Label.TabIndex = 1;
            this.worker_Label.Text = "Mitarbeiter:";
            // 
            // worker_ComboBox
            // 
            this.worker_ComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.worker_ComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.worker_ComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.worker_ComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.worker_ComboBox.FormattingEnabled = true;
            this.worker_ComboBox.Location = new System.Drawing.Point(135, 15);
            this.worker_ComboBox.Name = "worker_ComboBox";
            this.worker_ComboBox.Size = new System.Drawing.Size(331, 32);
            this.worker_ComboBox.Sorted = true;
            this.worker_ComboBox.TabIndex = 0;
            this.worker_ComboBox.SelectedIndexChanged += new System.EventHandler(this.Worker_ComboBox_SelectedIndexChanged);
            // 
            // area_ComboBox
            // 
            this.area_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.area_ComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.area_ComboBox.FormattingEnabled = true;
            this.area_ComboBox.Location = new System.Drawing.Point(3, 78);
            this.area_ComboBox.Name = "area_ComboBox";
            this.area_ComboBox.Size = new System.Drawing.Size(231, 24);
            this.area_ComboBox.TabIndex = 0;
            this.area_ComboBox.SelectedIndexChanged += new System.EventHandler(this.selectedAreaChanged);
            // 
            // button_Panel
            // 
            this.button_Panel.Controls.Add(this.button_Table);
            this.button_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_Panel.Location = new System.Drawing.Point(243, 168);
            this.button_Panel.Name = "button_Panel";
            this.button_Panel.Size = new System.Drawing.Size(235, 194);
            this.button_Panel.TabIndex = 12;
            // 
            // button_Table
            // 
            this.button_Table.ColumnCount = 1;
            this.button_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.button_Table.Controls.Add(this.delete_Button, 0, 2);
            this.button_Table.Controls.Add(this.accept_Button, 0, 0);
            this.button_Table.Controls.Add(this.abort_Button, 0, 1);
            this.button_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_Table.Location = new System.Drawing.Point(0, 0);
            this.button_Table.Name = "button_Table";
            this.button_Table.RowCount = 3;
            this.button_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.button_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.button_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.button_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.button_Table.Size = new System.Drawing.Size(235, 194);
            this.button_Table.TabIndex = 0;
            // 
            // delete_Button
            // 
            this.delete_Button.BackColor = System.Drawing.Color.White;
            this.delete_Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.delete_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.delete_Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Lime;
            this.delete_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delete_Button.Image = ((System.Drawing.Image)(resources.GetObject("delete_Button.Image")));
            this.delete_Button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.delete_Button.Location = new System.Drawing.Point(3, 131);
            this.delete_Button.Name = "delete_Button";
            this.delete_Button.Size = new System.Drawing.Size(229, 60);
            this.delete_Button.TabIndex = 2;
            this.delete_Button.Text = "                Löschen";
            this.delete_Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.delete_Button.UseVisualStyleBackColor = false;
            this.delete_Button.Click += new System.EventHandler(this.Delete_Button_Click);
            // 
            // accept_Button
            // 
            this.accept_Button.BackColor = System.Drawing.Color.White;
            this.accept_Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.accept_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.accept_Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Lime;
            this.accept_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accept_Button.Image = ((System.Drawing.Image)(resources.GetObject("accept_Button.Image")));
            this.accept_Button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.accept_Button.Location = new System.Drawing.Point(3, 3);
            this.accept_Button.Name = "accept_Button";
            this.accept_Button.Size = new System.Drawing.Size(229, 58);
            this.accept_Button.TabIndex = 0;
            this.accept_Button.Text = "                Fertig";
            this.accept_Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.accept_Button.UseVisualStyleBackColor = false;
            this.accept_Button.Click += new System.EventHandler(this.Accept_Button_Click);
            // 
            // abort_Button
            // 
            this.abort_Button.BackColor = System.Drawing.Color.White;
            this.abort_Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.abort_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.abort_Button.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Lime;
            this.abort_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.abort_Button.Image = ((System.Drawing.Image)(resources.GetObject("abort_Button.Image")));
            this.abort_Button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.abort_Button.Location = new System.Drawing.Point(3, 67);
            this.abort_Button.Name = "abort_Button";
            this.abort_Button.Size = new System.Drawing.Size(229, 58);
            this.abort_Button.TabIndex = 1;
            this.abort_Button.Text = "           Abbrechen";
            this.abort_Button.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.abort_Button.UseVisualStyleBackColor = false;
            this.abort_Button.Click += new System.EventHandler(this.Abort_Button_Click);
            // 
            // ShiftForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 365);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "ShiftForm";
            this.Text = "Schicht-Editor";
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.restM_Panel.ResumeLayout(false);
            this.restM_Panel.PerformLayout();
            this.rest_Panel.ResumeLayout(false);
            this.rest_Panel.PerformLayout();
            this.end_Panel.ResumeLayout(false);
            this.end_Panel.PerformLayout();
            this.start_Panel.ResumeLayout(false);
            this.start_Panel.PerformLayout();
            this.worker_Panel.ResumeLayout(false);
            this.worker_Panel.PerformLayout();
            this.button_Panel.ResumeLayout(false);
            this.button_Table.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Panel rest_Panel;
        private System.Windows.Forms.Label rest_Label;
        private System.Windows.Forms.DateTimePicker rest_Picker;
        private System.Windows.Forms.Panel end_Panel;
        private System.Windows.Forms.Label end_Label;
        private System.Windows.Forms.DateTimePicker end_Picker;
        private System.Windows.Forms.Panel start_Panel;
        private System.Windows.Forms.Label start_Label;
        private System.Windows.Forms.DateTimePicker start_Picker;
        private System.Windows.Forms.Panel worker_Panel;
        private System.Windows.Forms.Label worker_Label;
        private System.Windows.Forms.ComboBox worker_ComboBox;
        private System.Windows.Forms.ComboBox station_ComboBox;
        private System.Windows.Forms.Panel restM_Panel;
        private System.Windows.Forms.Label restM_Label;
        private System.Windows.Forms.DateTimePicker restM_Picker;
        private System.Windows.Forms.ComboBox area_ComboBox;
        private System.Windows.Forms.Panel button_Panel;
        private System.Windows.Forms.TextBox notes_Textbox;
        private System.Windows.Forms.TableLayoutPanel button_Table;
        private System.Windows.Forms.Button accept_Button;
        private System.Windows.Forms.Button delete_Button;
        private System.Windows.Forms.Button abort_Button;
        private System.Windows.Forms.CheckBox rest2_CheckBox;
        private System.Windows.Forms.CheckBox rest1_CheckBox;
    }
}