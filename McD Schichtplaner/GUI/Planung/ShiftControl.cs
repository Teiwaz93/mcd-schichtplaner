﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using McD_Schichtplaner.Database;

namespace McD_Schichtplaner.GUI.Planung
{
    public partial class ShiftControl : UserControl
    {
        private Shift shift;
        public Shift Shift => shift;
        private Database.Database data;
        private DayOverview overview;

        public ShiftControl(Shift s, Database.Database d, DayOverview o)
        {
            InitializeComponent();

            overview = o;
            data = d;
            shift = s;

            name_Label.Text = data.GetWorkerName(shift.WorkerID);
            start_Label.Text = Database.Database.ToShortTime(shift.Start);
            end_Label.Text = Database.Database.ToShortTime(shift.End);
            if (shift.B_Rest)
            {
                rest1_Label.Text = Database.Database.ToShortTime(shift.Rest);
                rest1_Label.Visible = true;
                rest_Label.Visible = true;
            }
            if (!data.WorkerIsMinor(shift.WorkerID, shift.Start.Date))
            {
                rest2_Label.Text = shift.Notes;
                rest2_Label.Visible = true;
            }
            else
            {
                name_Label.BackColor = Color.Red;
                if (shift.B_Rest_Minor)
                {
                    rest2_Label.Text = Database.Database.ToShortTime(shift.Rest_Minor);
                    rest2_Label.Visible = true;
                }
            }
        }

        private void ShiftControl_Click(object sender, EventArgs e)
        {
            ShiftForm sf = new ShiftForm(shift);
            sf.ShowDialog();
            if (sf.DialogResult == DialogResult.No)
            {
                overview.RemoveShift(this);
            }
            else if (sf.DialogResult == DialogResult.OK)
            {
                data.UpdateShift(sf.Shift);
                overview.RemoveShift(this);
                overview.AddShift(shift);
            }
        }
    }
}
