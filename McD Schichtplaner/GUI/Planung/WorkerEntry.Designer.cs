﻿namespace McD_Schichtplaner.GUI.Planung
{
    partial class WorkerEntry
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.date_Label = new System.Windows.Forms.Label();
            this.dow_Label = new System.Windows.Forms.Label();
            this.colour_Panel = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.date_Label, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.dow_Label, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.colour_Panel, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1258, 22);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // date_Label
            // 
            this.date_Label.AutoSize = true;
            this.date_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.date_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.date_Label.Location = new System.Drawing.Point(53, 0);
            this.date_Label.Name = "date_Label";
            this.date_Label.Size = new System.Drawing.Size(69, 22);
            this.date_Label.TabIndex = 1;
            this.date_Label.Text = "date";
            this.date_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dow_Label
            // 
            this.dow_Label.AutoSize = true;
            this.dow_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dow_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dow_Label.Location = new System.Drawing.Point(3, 0);
            this.dow_Label.Name = "dow_Label";
            this.dow_Label.Size = new System.Drawing.Size(44, 22);
            this.dow_Label.TabIndex = 0;
            this.dow_Label.Text = "dow";
            this.dow_Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // colour_Panel
            // 
            this.colour_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.colour_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.colour_Panel.Location = new System.Drawing.Point(128, 3);
            this.colour_Panel.Name = "colour_Panel";
            this.colour_Panel.Size = new System.Drawing.Size(27, 16);
            this.colour_Panel.TabIndex = 2;
            // 
            // WorkerEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "WorkerEntry";
            this.Size = new System.Drawing.Size(1258, 22);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label date_Label;
        private System.Windows.Forms.Label dow_Label;
        private System.Windows.Forms.Panel colour_Panel;
    }
}
