﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using McD_Schichtplaner.Database;
using McD_Schichtplaner.Mailing;
using McD_Schichtplaner.GUI.Fehlerbehandlung;
using System.IO;

namespace McD_Schichtplaner.GUI.Planung
{
    public partial class MonthOverview : UserControl
    {
        private Database.Database.Month month;
        private int year;
        private Database.Database data;
        private Color[] colors = new Color[31];
        private int printedIndex = 0;
        private List<MonthWorkerEntry> workerEntries;
        private Worker printworker;
        private List<Error> errors;
        private bool printFull = false;

        public List<MonthWorkerEntry> WorkerEntries { get => workerEntries;  }
        public Database.Database Data => data;
        public Color[] Colors => colors;
        public int Year => year;
        public Database.Database.Month Month => month;

        public MonthOverview(Size size)
        {
            InitializeComponent();
            data = Database.Database.DB;
            Size = size;
            mainSplitContainer.MinimumSize = size;
            workerEntries = new List<MonthWorkerEntry>();
        }

        public MonthOverview(Database.Database.Month m, int y, Size size) : this(size)
        {
            SetMonth(m, y);
            filter_ComboBox.SelectedIndex = 0;
        }

        public MonthOverview(DateTime date, Size size) : this(Database.Database.IntToMonth(date.Month), date.Year, size) { }

        private void SetMonth(Database.Database.Month m, int y)
        {
            overview_Button.Enabled = true;
            month = m;
            year = y;
            currentMonth_Label.Text = Database.Database.MonthToString(month) + " " + year;
            int i;
            for (i = 0; i < DateTime.DaysInMonth(year, Database.Database.MonthToInt(month)); i++)
            {
                DateTime date = new DateTime(year, Database.Database.MonthToInt(month), i + 1);
                switch (date.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                    case DayOfWeek.Tuesday:
                    case DayOfWeek.Wednesday:
                    case DayOfWeek.Thursday:
                        colors[i] = Color.White;
                        break;
                    case DayOfWeek.Friday:
                        colors[i] = Color.LightGreen;
                        break;
                    case DayOfWeek.Saturday:
                        colors[i] = Color.Lime;
                        break;
                    case DayOfWeek.Sunday:
                        colors[i] = Color.Green;
                        break;
                }
                var v = tableLayoutPanel1.GetControlFromPosition(i + 1, 0);
                v.BackColor = colors[i];
                v.Visible = true;
            }
            for (i = i; i < colors.Length; i++)
            {
                colors[i] = Color.Black;
                var v = tableLayoutPanel1.GetControlFromPosition(i + 1, 0);
                v.Visible = false;
            }
            SetWorkers();
            CheckErrors();
        }

        private void SetWorkers()
        {
            workerTableFlowLayoutPanel.Visible = false;
            workerEntries.Clear();
            workerTableFlowLayoutPanel.Controls.Clear();
            foreach (var worker in data.GetWorkers())
            {
                if (filter_ComboBox.SelectedIndex == 0 ||
                    (filter_ComboBox.SelectedIndex == 1 && worker.Status != Database.Database.Status.Mgm && !worker.Cafe) ||
                    (filter_ComboBox.SelectedIndex == 2 && worker.Cafe) ||
                    (filter_ComboBox.SelectedIndex == 3 && worker.Status == Database.Database.Status.Mgm)
                    )
                {
                    var mwe = new MonthWorkerEntry(worker, this, workerTableFlowLayoutPanel.Size);
                    workerTableFlowLayoutPanel.Controls.Add(mwe);
                    workerEntries.Add(mwe);
                }
            }
            workerTableFlowLayoutPanel.Visible = true;
        }

        internal void CreateShift(Shift shift)
        {
            data.CreateShift(shift);
        }

        internal void UpdateShift(Shift shift)
        {
            data.UpdateShift(shift);
        }

        private void OpenDay(object sender, EventArgs e)
        {
            Label clicked = sender as Label;
            string day = clicked.Text.Replace('.', ' ');
            int d;
            if (!int.TryParse(day, out d))
            {
                return;
            }

            Mainwindow mw = Parent.Parent.Parent.Parent as Mainwindow;
            mw.ChangePanel(new DayOverview(d, month, year, Size));
        }

        private void Print_Button_Click(object sender, EventArgs e)
        {
            printFull = MessageBox.Show("Kompletten Plan drucken ?\nNein druckt nur Urlaubsübersicht.", "Kompletter Plan?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;
            if (printDialog.ShowDialog() == DialogResult.OK)
            {
                printDialog.Document = printDocument;
                printDialog.Document.DefaultPageSettings.Landscape = true;
                printedIndex = 0;
                printDocument.Print();
                GC.Collect();
            }
        }

        private void PrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            int daysInMonth = DateTime.DaysInMonth(year, Database.Database.MonthToInt(month));
            using (Font small = new Font("Arial", 6),
                    normal = new Font("Arial", 10),
                    large = new Font("Arial", 18))
            {
                using (Pen line = new Pen(Brushes.Black))
                {
                    using (Brush friday = new SolidBrush(Color.FromArgb(225, 225, 225)),
                                weekdday = new SolidBrush(Color.White),
                                saturday = new SolidBrush(Color.FromArgb(200, 200, 200)),
                                sunday = new SolidBrush(Color.FromArgb(175, 175, 175)),
                                text = new SolidBrush(Color.Black))
                    {
                        float borderOffset = 75;
                        float extraOffset = -25f;
                        float xMin = e.MarginBounds.Left - borderOffset;
                        float yMin = e.MarginBounds.Top - borderOffset;
                        float xMax = e.MarginBounds.Right + borderOffset + extraOffset;
                        float yMax = e.MarginBounds.Bottom + borderOffset + extraOffset;
                        float centerOffset = 5;
                        float nameWidth = 150;
                        float dayWidth = (xMax - xMin - nameWidth) / daysInMonth;
                        float boxHeight = 25;
                        string s;
                        float textLineSpacing = 18f;

                        float h = yMin;
                        PrintHeader();
                        h += boxHeight + 18f;
                        while (printedIndex < workerEntries.Count)
                        {
                            if (h + boxHeight > yMax)
                            {
                                e.HasMorePages = true;
                                Finalize();
                                return;
                            }
                            PrintWorker(workerEntries[printedIndex], h);
                            h += boxHeight;
                            printedIndex++;
                        }
                        Finalize();

                        void PrintHeader()
                        {
                            s = $"Monatsplan für {Database.Database.MonthToString(month)} / {year}.";
                            e.Graphics.DrawString(s, normal, text, xMin, yMin);

                            s = "Änderungen vorbehalten\t\t" + data.GetRestaurantName() + "\t" + DateTime.Today.ToString("dd.MM.yyyy");
                            var size = e.Graphics.MeasureString(s, small);
                            e.Graphics.DrawString(s, small, text, xMax - size.Width, yMin);

                            yMin += textLineSpacing;

                            float dayCenter = 5;
                            e.Graphics.DrawLine(line, xMin, yMin, xMax, yMin);
                            e.Graphics.DrawLine(line, xMin, yMin, xMin, yMax);
                            e.Graphics.DrawLine(line, xMin, yMax, xMax, yMax);
                            e.Graphics.DrawLine(line, xMax, yMax, xMax, yMin);

                            float x = xMin;
                            float y = yMin + centerOffset;
                            s = "Name";
                            e.Graphics.DrawString(s, normal, text, x, y);

                            x += nameWidth;
                            y = yMin + boxHeight;

                            for (int i = 1; i <= daysInMonth; i++)
                            {
                                e.Graphics.DrawLine(line, x, yMin, x, yMin + boxHeight);
                                y = yMin;
                                DateTime date = new DateTime(year, Database.Database.MonthToInt(month), i);
                                Color color = Color.White;
                                Brush brush;
                                switch (date.DayOfWeek)
                                {
                                    case DayOfWeek.Monday:
                                        s = "Mo";
                                        brush = weekdday;
                                        break;
                                    case DayOfWeek.Tuesday:
                                        s = "Di";
                                        brush = weekdday;
                                        break;
                                    case DayOfWeek.Wednesday:
                                        s = "Mi";
                                        brush = weekdday;
                                        break;
                                    case DayOfWeek.Thursday:
                                        s = "Do";
                                        brush = weekdday;
                                        break;
                                    case DayOfWeek.Friday:
                                        s = "Fr";
                                        brush = friday;
                                        break;
                                    case DayOfWeek.Saturday:
                                        s = "Sa";
                                        brush = saturday;
                                        break;
                                    case DayOfWeek.Sunday:
                                        s = "So";
                                        brush = sunday;
                                        break;
                                    default:
                                        brush = weekdday;
                                        break;
                                }
                                e.Graphics.FillRectangle(brush, x, y, dayWidth, yMax - y);
                                e.Graphics.DrawString(s, small, text, x + dayCenter, y);
                                y += boxHeight * 0.5f;
                                e.Graphics.DrawString(i + ".", small, text, x + dayCenter, y);

                                x += dayWidth;
                            }
                            y = yMin + boxHeight;
                            e.Graphics.DrawLine(line, xMin, y, xMax, y);
                        }

                        void PrintWorker(MonthWorkerEntry mwe, float height)
                        {
                            float x = xMin;
                            float y = height;
                            float offset = boxHeight / 2 + 2;

                            s = mwe.WorkerName;
                            e.Graphics.DrawString(s, normal, text, x, y + centerOffset);
                            x += nameWidth;

                            for (int i = 1; i <= daysInMonth; i++)
                            {
                                s = mwe.GetLabel(i);
                                if (printFull && s.Contains("-"))
                                {
                                    var subs = s.Split('-');
                                    e.Graphics.DrawString(subs[0], small, text, x + (dayWidth - e.Graphics.MeasureString(subs[0], small).Width) / 2, h + 4);
                                    e.Graphics.DrawString(subs[1], small, text, x + (dayWidth - e.Graphics.MeasureString(subs[1], small).Width) / 2, h + offset);
                                }
                                else
                                {
                                    if (printFull && (s == "KUG" || s == "BS"))
                                    {
                                        e.Graphics.DrawString(s, normal, text, x, h + centerOffset);
                                    }
                                    else
                                    {
                                        if(printFull || s == "U" || s == "K") e.Graphics.DrawString(s, large, text, x, h);
                                    }
                                }
                                x += dayWidth;
                            }
                            y += boxHeight;
                            e.Graphics.DrawLine(line, xMin, y, xMax, y);
                        }

                        void Finalize()
                        {

                            float top = yMin + boxHeight;
                            e.Graphics.DrawLine(line, xMin, h, xMax, h);
                            float d = xMin + nameWidth;
                            e.Graphics.DrawLine(line, d, h, d, top);
                            for (int i = 1; i <= daysInMonth; i++)
                            {
                                d += dayWidth;
                                e.Graphics.DrawLine(line, d, h, d, top);
                            }
                            e.Graphics.FillRectangle(Brushes.White, xMin, h, xMax, yMax - h);

                        }
                    }
                }
            }
        }

        private void Send_Button_Click(object sender, EventArgs e)
        {
            MailManager mm = new MailManager();
            foreach (var w in workerEntries)
            {
                if (w.WorkerMail is null) continue;
                string body = $"Hallo {w.WorkerName},\n hier ist dein neuer Schichtplan für {Database.Database.MonthToString(month)}:\n\nBeste Grüße,\ndein supertoller und netter Restaurantleiter ;)";
                int i = 0;
                while (File.Exists($"Temp\\temp{i}.pdf")) i++;
                string path = $"Temp\\temp{i}.pdf";
                var workerpanel = w.GetWorkerOverview();
                workerpanel.CreatePDF(path);
                mm.SendMail(w.WorkerMail, "Neuer Schichtplan ist da", body, path);
            }

        }

        private void Filter_ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetWorkers();
        }

        private void Back_Button_Click(object sender, EventArgs e)
        {
            int m = Database.Database.MonthToInt(month);
            m--;
            if (m == 0)
            {
                m = 12;
                year--;
            }
            SetMonth(Database.Database.IntToMonth(m), year);
        }

        private void Forward_Button_Click(object sender, EventArgs e)
        {
            int m = Database.Database.MonthToInt(month);
            m++;
            if (m == 13)
            {
                m = 1;
                year++;
            }
            SetMonth(Database.Database.IntToMonth(m), year);
        }

        private void ResetSwitch_Button_Click(object sender, EventArgs e)
        {
            MonthWorkerEntry.DeleteSwitcher();
        }

        internal void CheckErrors()
        {
            errors = new List<Error>();
            foreach (var worker in workerEntries)
            {
                worker.CheckValidity();
                errors.AddRange(worker.Errors);
            }

            int minor = 0, severe = 0;
            foreach (var error in errors)
            {
                if (error.Severe) severe++;
                else minor++;
            }
            if (minor > 0) errors_Button.BackColor = Color.Yellow;
            else if (severe > 0) errors_Button.BackColor = Color.Red;
            else errors_Button.BackColor = Color.LimeGreen;

            errors_Button.Text = $"{minor} Warnungen - {severe} Fehler";
            string tip = "";
            foreach (var error in errors)
            {
                tip = tip + $"\n{(error.Severe ? "Fehler!" : "Warnung")}: {error.Message}";
            }
            toolTip.SetToolTip(errors_Button, tip);
        }

        private void errors_Button_Click(object sender, EventArgs e)
        {
            ErrorsForm ef = new ErrorsForm(errors);
            ef.ShowDialog();
        }

        private void errors_Button_MouseEnter(object sender, EventArgs e)
        {
            CheckErrors();
        }

        private void overview_Button_Click(object sender, EventArgs e)
        {
            Mainwindow mw = Parent.Parent.Parent.Parent as Mainwindow;
            mw.ChangePanel(new MonthPlanning.MonthPlanning(new DateTime(year, Database.Database.MonthToInt(month), 1)));
        }
    }
}
