﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using McD_Schichtplaner.Database;
using iTextSharp.text;
using System.Windows.Forms.VisualStyles;
using Color = System.Drawing.Color;

namespace McD_Schichtplaner.GUI.Planung
{

    public partial class WorkerEntry : UserControl
    {
        private DateTime date;
        private Shift shift;
        private string nonShift;
        private Database.Database data = Database.Database.DB;
        private WorkerOverview overview;

        private WorkerEntry(WorkerOverview wo)
        {
            InitializeComponent();
            overview = wo;
        }

        public WorkerEntry(DateTime d, WorkerOverview wo) :this(wo)
        {
            date = d;
            SetDate(d);
            SetupNothing();
        }

        public WorkerEntry(string s, DateTime d, WorkerOverview wo) : this(wo)
        {
            nonShift = s;
            SetDate(d);
            SetupNonShift();
        }

        public WorkerEntry(Shift s, DateTime d, WorkerOverview wo) : this(wo)
        {
            shift = s;
            SetDate(d);
            SetupShift();
        }

        private void SetDate(DateTime d)
        {
            date = d;
            var dow = d.DayOfWeek;
            switch (dow)
            {
                case DayOfWeek.Monday:
                    dow_Label.Text = "Mo";
                    break;
                case DayOfWeek.Tuesday:
                    dow_Label.Text = "Di";
                    break;
                case DayOfWeek.Wednesday:
                    dow_Label.Text = "Mi";
                    break;
                case DayOfWeek.Thursday:
                    dow_Label.Text = "Do";
                    break;
                case DayOfWeek.Friday:
                    dow_Label.Text = "Fr";
                    BackColor = Color.LightGreen;
                    break;
                case DayOfWeek.Saturday:
                    dow_Label.Text = "Sa";
                    BackColor = Color.Lime;
                    break;
                case DayOfWeek.Sunday:
                    dow_Label.Text = "So";
                    BackColor = Color.Green;
                    break;
                default:
                    dow_Label.Text = "??";
                    break;
            }
            date_Label.Text = $"{d.Day}.{d.Month}.";
        }

        private void ChangeShift(object sender, EventArgs arg)
        {
            ShiftForm sf = new ShiftForm(shift);
            sf.ShowDialog();
            switch (sf.DialogResult)
            {
                case DialogResult.OK:
                    shift = sf.Shift;
                    if (shift.WorkerID == overview.Worker.Id)
                    {
                        data.UpdateShift(shift);
                        tableLayoutPanel1.GetControlFromPosition(3, 0).Text = shift.LabelLong;
                        tableLayoutPanel1.GetControlFromPosition(4, 0).Text = data.GetStation(shift.Station).Name; 
                    }
                    else
                    {
                        data.UpdateShift(shift);
                        SetupNothing();
                    }
                    break;
                case DialogResult.No:
                    data.DeleteShift(shift);
                    SetupNothing();
                    break;
                default: return;
            }
        }

        private void ChooseShiftPicker(object sender, EventArgs arg)
        {
            ComboBox picker = sender as ComboBox;
            shift = data.GetSingleShift(((Tuple<string, long>)picker.SelectedItem).Item2);
            shift.WorkerID = overview.Worker.Id;
            data.UpdateShift(shift);
            SetupShift();
        }

        private void SwitchShiftToNonShift(object sender, EventArgs arg)
        {
            nonShift = "W";
            data.SetFree(overview.Worker, date, nonShift);
            SetupNonShift();
        }

        private void RadioButtonEnabled(object sender, EventArgs arg)
        {
            RadioButton button = sender as RadioButton;
            if (button.Checked)
            {
                nonShift = button.Text;
                data.UnsetFree(overview.Worker, date);
                data.SetFree(overview.Worker, date, nonShift); 
            }
        }

        private void Reset()
        {
            tableLayoutPanel1.Controls.Remove(tableLayoutPanel1.GetControlFromPosition(3, 0));
            tableLayoutPanel1.Controls.Remove(tableLayoutPanel1.GetControlFromPosition(4, 0));
        }

        private void SetupNothing()
        {
            Reset();
            List<Tuple<string, long>> availableShifts = new List<Tuple<string, long>>();
            availableShifts.Add(new Tuple<string, long>("", 0));
            var jobs = data.GetWorkerJobs(overview.Worker);
            List<string> jobStrings = new List<string>();
            foreach(var job in jobs)
            {
                jobStrings.Add(Database.Database.JobToString(job));
            }
            var shifts = data.GetAllShiftsDay(date);
            foreach(var shift in shifts)
            {
                var station = data.GetStation(shift.Station);
                if (shift.WorkerID == 0 && jobStrings.Contains(Database.Database.AreaToString(station.Area))) availableShifts.Add(new Tuple<string, long>(shift.LabelPicker, shift.ID));
            }
            ComboBox shiftPicker = new ComboBox()
            {
                Dock = DockStyle.Fill,
                DataSource = availableShifts,
                DisplayMember = "Item1",
                ValueMember = "Item2",
                DropDownStyle = ComboBoxStyle.DropDownList
            };
            shiftPicker.SelectedIndexChanged += ChooseShiftPicker;
            tableLayoutPanel1.Controls.Add(shiftPicker, 4, 0);

            Label nothing = new Label()
            {
                Text = "Kein Einsatz",
                Dock = DockStyle.Fill,
                TextAlign = System.Drawing.ContentAlignment.MiddleCenter
            };
            nothing.Click += SwitchShiftToNonShift;
            tableLayoutPanel1.Controls.Add(nothing, 3, 0);
        }

        private void SetupNonShift()
        {
            Reset();
            FlowLayoutPanel buttons = new FlowLayoutPanel()
            {
                Dock = DockStyle.Fill,
                FlowDirection = FlowDirection.LeftToRight,
                Margin = new Padding(0)
            };

            RadioButton w_Button = new RadioButton()
            {
                Text = "W",
                Checked = nonShift == "W",
                AutoSize = false,
                Size = new Size(60, 16)
            };
            buttons.Controls.Add(w_Button);

            RadioButton u_Button = new RadioButton()
            {
                Text = "U",
                Checked = nonShift == "U",
                AutoSize = false,
                Size = new Size(60, 16)
            };
            buttons.Controls.Add(u_Button);

            RadioButton kug_Button = new RadioButton()
            {
                Text = "KUG",
                Checked = nonShift == "KUG",
                AutoSize = false,
                Size = new Size(60, 16)
            };
            buttons.Controls.Add(kug_Button);

            RadioButton k_Button = new RadioButton()
            {
                Text = "K",
                Checked = nonShift == "K",
                AutoSize = false,
                Size = new Size(60, 16)
            };
            buttons.Controls.Add(k_Button);


            RadioButton s_Button = new RadioButton()
            {
                Text = "S",
                Checked = nonShift == "S",
                AutoSize = false,
                Size = new Size(60, 16)
            };
            buttons.Controls.Add(s_Button);


            RadioButton bs_Button = new RadioButton()
            {
                Text = "BS",
                Checked = nonShift == "BS",
                AutoSize = false,
                Size = new Size(60, 16)
            };
            buttons.Controls.Add(bs_Button);

            w_Button.CheckedChanged += RadioButtonEnabled;
            u_Button.CheckedChanged += RadioButtonEnabled;
            kug_Button.CheckedChanged += RadioButtonEnabled;
            u_Button.CheckedChanged += RadioButtonEnabled;
            s_Button.CheckedChanged += RadioButtonEnabled;
            bs_Button.CheckedChanged += RadioButtonEnabled;

            tableLayoutPanel1.Controls.Add(buttons, 3, 0);

            Button nonShiftBack = new Button()
            {
                Text = "kein Einsatz",
                TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                Dock = DockStyle.Fill,
                Margin = new Padding(0)
            };
            nonShiftBack.Click += NonShiftBack_Click;

            tableLayoutPanel1.Controls.Add(nonShiftBack, 4, 0);
        }

        private void NonShiftBack_Click(object sender, EventArgs e)
        {
            data.UnsetFree(overview.Worker, date);
            SetupNothing();
        }

        private void SetupShift()
        {
            Reset();
            Label times_Label = new Label()
            {
                Text = $"{shift.Start.Hour}:{(shift.Start.Minute < 10 ? $"0{shift.Start.Minute}" : shift.Start.Minute.ToString())} - {shift.End.Hour}:{(shift.End.Minute < 10 ? $"0{shift.End.Minute}" : shift.End.Minute.ToString())}",
                TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                Dock = DockStyle.Fill
            };
            times_Label.Click += ChangeShift;
            tableLayoutPanel1.Controls.Add(times_Label, 3, 0);
            Label station_Label = new Label()
            {
                Text = Database.Database.DB.GetStationName(shift.Station),
                TextAlign = System.Drawing.ContentAlignment.MiddleCenter,
                Dock = DockStyle.Fill
            };
            station_Label.Click += ChangeShift;
            tableLayoutPanel1.Controls.Add(station_Label, 4, 0);
        }
    }
}
