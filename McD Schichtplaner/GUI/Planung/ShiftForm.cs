﻿using McD_Schichtplaner.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace McD_Schichtplaner.GUI.Planung
{
    public partial class ShiftForm : Form
    {
        private Worker worker;
        private Database.Database data;
        private DateTime date;
        private string[] availableJobs = { "Küche", "McDrive", "Theke/Kasse", "Service/Lobby", "McCafe", "Management" };
        private int dow
        {
            get
            {
                switch (date.DayOfWeek)
                {
                    case DayOfWeek.Monday: return 0;
                    case DayOfWeek.Tuesday: return 1;
                    case DayOfWeek.Wednesday: return 2;
                    case DayOfWeek.Thursday: return 3;
                    case DayOfWeek.Friday: return 4;
                    case DayOfWeek.Saturday: return 5;
                    case DayOfWeek.Sunday: return 6;
                    default: return -1;
                }
            }
        }
        private Availabilities availabilities;
        private Shift shift;
        public Shift Shift => shift;

        private ShiftForm()
        {
            InitializeComponent();
            station_ComboBox.Enabled = false;
            data = Database.Database.DB;
            FillAreaBox();
        }
        public ShiftForm(DateTime date) :this()
        {
            this.date = date;
            shift = new Shift();
            FillWorkerBox();
            delete_Button.Enabled = false;
            start_Picker.Value = date;
            end_Picker.Value = date;
            restM_Picker.Value = date;
            rest_Picker.Value = date;
        }

        public ShiftForm(DateTime date, Worker w):this(date)
        {
            worker = w;
            worker_ComboBox.Items.Add(worker.Name);
            worker_ComboBox.Text = worker.Name;
            worker_ComboBox.Enabled = false;
            if (worker.IsMinor(date)) rest2_CheckBox.Enabled = true;
        }

        public ShiftForm(Shift s) : this()
        {
            date = s.Start.Date;
            shift = s;
            var station = data.GetStation(shift.Station);
            var area = data.GetAreaFor(station);
            worker = data.GetWorker(shift.WorkerID);
            if (!(worker is null))
            {
                worker_ComboBox.Text = worker.Name;
                if (worker.Status != Database.Database.Status.Mgm)
                {
                    FillAreaBox();
                    area_ComboBox.Text = Database.Database.AreaToString(area);
                }
                else
                {
                    area_ComboBox.Items.Clear();
                    area_ComboBox.Items.Add("Management");
                    area_ComboBox.SelectedIndex = 0;
                }

            }
            else
            {
                area_ComboBox.Text = Database.Database.AreaToString(area);
                FillWorkerBox();
            }
            station_ComboBox.Text = station.Name;
            start_Picker.Value = shift.Start;
            end_Picker.Value = shift.End;
            if (shift.B_Rest)
            {
                rest1_CheckBox.Checked = true;
                rest_Picker.Value = shift.Rest;
            }
            else
            {
                rest_Picker.Value = shift.Start.Date;
            }
            if (shift.B_Rest_Minor)
            {
                rest2_CheckBox.Checked = true;
                restM_Picker.Value = shift.Rest_Minor;
            }
            else
            {
                restM_Picker.Value = shift.Start.Date;
            }
            notes_Textbox.Text = shift.Notes;
        }

        private void FillWorkerBox()
        {
            worker_ComboBox.Items.Clear();
            var workers = data.GetFreeWorkers(date);
            foreach (var w in workers)
            {
                worker_ComboBox.Items.Add(w.Name);
            }
        }

        private void selectedAreaChanged(object sender, EventArgs e)
        {
            station_ComboBox.Items.Clear();
            var stations = data.GetStations(area_ComboBox.Text);
            foreach( var station in stations)
            {
                station_ComboBox.Items.Add(station);
            }
            station_ComboBox.SelectedItem = 0;
            station_ComboBox.Enabled = true;
            if (area_ComboBox.Text == "Management")
            {

            }
            else if (area_ComboBox.Text != "") 
            {
                worker_ComboBox.Items.Clear();
                var workers = data.GetFreeWorkersTalent(date, area_ComboBox.Text);
                foreach (var worker in workers)
                {
                    worker_ComboBox.Items.Add(worker.Name);
                }
                if (!(worker is null))
                {
                    worker_ComboBox.Text = worker.Name;
                } 
            }
        }

        private void Accept_Button_Click(object sender, EventArgs e)
        {
            if (worker is null)
            {
                MessageBox.Show("Einfügen von Schichten ohne Mitarbeiter noch nicht unterstützt.");
                return;
            }
            if (end_Picker.Value - start_Picker.Value > new TimeSpan(9, 30, 0))
            {
                MessageBox.Show("Schicht ist zu lang.\nNicht länger als 9,5 Stunden (inklusive Pause).");
                return;
            }
            shift.WorkerID = worker.Id;
            shift.Start = start_Picker.Value;
            if (end_Picker.Value < start_Picker.Value)
            {
                shift.End = end_Picker.Value.AddDays(1);
            }
            else shift.End = end_Picker.Value;
            if (rest1_CheckBox.Checked)
            {
                if (rest_Picker.Value < start_Picker.Value) shift.Rest = rest_Picker.Value.AddDays(1);
                else shift.Rest = rest_Picker.Value;
            }
            else shift.Rest = DateTime.MinValue;
            if (rest2_CheckBox.Checked)
            {
                if (restM_Picker.Value < start_Picker.Value) shift.Rest_Minor = restM_Picker.Value.AddDays(1);
                else shift.Rest_Minor = restM_Picker.Value;
            }
            else shift.Rest_Minor = DateTime.MinValue;
            shift.Notes = notes_Textbox.Text;
            if (station_ComboBox.Text == "")
            {
                MessageBox.Show("Station ist nicht gewählt worden.");
                return;
            }
            shift.Station = data.GetStationID(station_ComboBox.Text);
            shift.B_Rest = rest1_CheckBox.Checked;
            shift.B_Rest_Minor = rest2_CheckBox.Checked;
            DialogResult = DialogResult.OK;
        }

        private void Abort_Button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }

        private void Delete_Button_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Sicher ? Schicht löschen ?", "Löschen ?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DialogResult = DialogResult.No;
                Close();
            }
        }

        private void Rest2_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            restM_Picker.Enabled = rest2_CheckBox.Checked;
        }

        private void Rest1_CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            rest_Picker.Enabled = rest1_CheckBox.Checked;
        }

        private void Worker_ComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                worker = data.GetWorker(worker_ComboBox.Text);
                availabilities = data.GetAvailabilities(worker);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception.Message);
                worker_ComboBox.Text = "";
                return;
            }
            //TODO: Hacky fix hier, totaler rework geplant anyway
            end_Picker.Value = end_Picker.Value.AddMinutes(1);
            end_Picker.Value = end_Picker.Value.AddMinutes(-1);
            if (!worker.IsMinor(date)) rest2_CheckBox.Checked = false;
            FillAreaBox();
        }

        private void FillAreaBox()
        {
            if (worker is null)
            {
                area_ComboBox.Items.Clear();
                foreach (string job in availableJobs) area_ComboBox.Items.Add(job);
            }
            else if (worker.Status == Database.Database.Status.Mgm)
            {
                area_ComboBox.Items.Clear();
                area_ComboBox.Items.Add("Management");
                area_ComboBox.SelectedIndex = 0;
            }
            else
            {
                var jobs = data.GetWorkerJobs(worker);
                List<string> jobnames = new List<string>();
                foreach(var job in jobs)
                {
                    jobnames.Add(Database.Database.JobToString(job));
                }
                var areas = data.GetAreas();
                foreach (var job in availableJobs)
                {
                    //ist bislang in liste
                    if (area_ComboBox.Items.Contains(job))
                    {
                        //bleibt in liste
                        if (jobnames.Contains(job))
                        {

                        }
                        //entferne aus liste
                        else
                        {
                            area_ComboBox.Items.Remove(job);
                        }
                    }
                    //ist noch nicht in liste
                    else
                    {
                        //füge liste hinzu
                        if (jobnames.Contains(job))
                        {
                            area_ComboBox.Items.Add(job);
                        }
                        //gehört auch nicht zur liste
                        else
                        {

                        }
                    }
                }
            }
        }

        private void TimesChanged(object sender, EventArgs e)
        {
            if (end_Picker.Value - start_Picker.Value > new TimeSpan(6, 0, 0))
            {
                rest1_CheckBox.Checked = true;
                rest_Picker.Value = start_Picker.Value + new TimeSpan(3, 0, 0);
                if (! (worker is null) && worker.IsMinor(date))
                {
                    rest2_CheckBox.Checked = true;
                    restM_Picker.Value = rest_Picker.Value.AddMinutes(30);
                }
            }
            else
            {
                if (!(worker is null) && !worker.IsMinor(date))
                {
                    rest1_CheckBox.Checked = false; 
                }
                else
                {
                    rest1_CheckBox.Checked = true;
                    rest_Picker.Value = start_Picker.Value.AddHours(2);
                    rest2_CheckBox.Checked = false;
                }
            }
        }

        private void StartChanged(object sender, EventArgs e)
        {
            if (availabilities?.Times[dow].Item1 > start_Picker.Value.TimeOfDay) start_Panel.BackColor = Color.Yellow;
            else start_Panel.BackColor = DefaultBackColor;
            TimesChanged(sender, e);
        }

        private void EndChanged(object sender, EventArgs e)
        {
            if (availabilities?.Times[dow].Item2 < end_Picker.Value.TimeOfDay) end_Panel.BackColor = Color.Yellow;
            else end_Panel.BackColor = DefaultBackColor;
            if (end_Picker.Value < start_Picker.Value) end_Picker.Value = end_Picker.Value.AddDays(1);
            if (end_Picker.Value - start_Picker.Value >= new TimeSpan(24, 0, 0)) end_Picker.Value = end_Picker.Value.AddDays(-1);
            TimesChanged(sender, e);
        }

        private void RestChanged(object sender, EventArgs e)
        {
            if (rest_Picker.Value <= start_Picker.Value ||
                rest_Picker.Value >= end_Picker.Value ||
                rest_Picker.Value - start_Picker.Value > new TimeSpan (6,0,0) ||
                end_Picker.Value - rest_Picker.Value > new TimeSpan(6, 30, 0))
            {
                rest_Picker.Value = start_Picker.Value.AddHours(3);
            }
        }
    }
}
