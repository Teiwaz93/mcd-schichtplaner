﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace McD_Schichtplaner.GUI.Planung.MonthPlanning
{
    public partial class MonthPlanning : UserControl
    {
        DateTime month;
        Database.Database data = Database.Database.DB;

        private MonthPlanning()
        {
            InitializeComponent();
        }

        public MonthPlanning(DateTime d) : this()
        {
            SetDate(d);
        }

        private void SetDate(DateTime d)
        {
            if(month != null)
            {
                month_TablePanel.Controls.Clear();
            }
            Visible = false;
            month = d;
            int dayoffset; 
            switch(new DateTime(month.Year, month.Month, 1).DayOfWeek)
            {
                case DayOfWeek.Monday:
                    dayoffset = -1;
                    break;
                case DayOfWeek.Tuesday:
                    dayoffset = 0;
                    break;
                case DayOfWeek.Wednesday:
                    dayoffset = 1;
                    break;
                case DayOfWeek.Thursday:
                    dayoffset = 2;
                    break;
                case DayOfWeek.Friday:
                    dayoffset = 3;
                    break;
                case DayOfWeek.Saturday:
                    dayoffset = 4;
                    break;
                case DayOfWeek.Sunday:
                    dayoffset = 5;
                    break;
                default:
                    throw new Exception("seltsamer Tag gefunden...");
            }
            for (int i = 1; i <= DateTime.DaysInMonth(month.Year, month.Month); i++)
            {
                DateTime day = new DateTime(month.Year, month.Month, i);
                var entry = new MonthPlanningEntry(day);
                int col;
                switch (day.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        col = 0;
                        break;
                    case DayOfWeek.Tuesday:
                        col = 1;
                        break;
                    case DayOfWeek.Wednesday:
                        col = 2;
                        break;
                    case DayOfWeek.Thursday:
                        col = 3;
                        break;
                    case DayOfWeek.Friday:
                        col = 4;
                        break;
                    case DayOfWeek.Saturday:
                        col = 5;
                        break;
                    case DayOfWeek.Sunday:
                        col = 6;
                        break;
                    default:
                        throw new Exception("seltsamer Tag gefunden...");
                }
                int row = 1 + ((day.Day + dayoffset) / 7);
                month_TablePanel.Controls.Add(entry, col, row);
            }
            month_Label.Text = $"{Database.Database.MonthToString(Database.Database.IntToMonth((int)month.Month))} - {month.Year}";
            Visible = true;
        }

        private void back_Button_Click(object sender, EventArgs e)
        {
            Mainwindow mw = Parent.Parent.Parent.Parent as Mainwindow;
            mw.ChangePanel(new MonthOverview(month, Size));
        }

        private void backwards_Button_Click(object sender, EventArgs e)
        {
            SetDate(month.AddMonths(-1));
        }

        private void forward_Button_Click(object sender, EventArgs e)
        {
            SetDate(month.AddMonths(1));
        }
    }
}
