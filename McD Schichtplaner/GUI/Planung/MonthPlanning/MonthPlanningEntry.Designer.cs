﻿namespace McD_Schichtplaner.GUI.Planung.MonthPlanning
{
    partial class MonthPlanningEntry
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.table_Panel = new System.Windows.Forms.TableLayoutPanel();
            this.productivity_Label = new System.Windows.Forms.Label();
            this.template_Label = new System.Windows.Forms.Label();
            this.comments_Label = new System.Windows.Forms.Label();
            this.date_Label = new System.Windows.Forms.Label();
            this.sales_Label = new System.Windows.Forms.Label();
            this.sales_NumUpDown = new System.Windows.Forms.NumericUpDown();
            this.template_ComboBox = new System.Windows.Forms.ComboBox();
            this.notes_TextBox = new System.Windows.Forms.TextBox();
            this.table_Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sales_NumUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // table_Panel
            // 
            this.table_Panel.ColumnCount = 2;
            this.table_Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.table_Panel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66667F));
            this.table_Panel.Controls.Add(this.productivity_Label, 0, 3);
            this.table_Panel.Controls.Add(this.template_Label, 0, 2);
            this.table_Panel.Controls.Add(this.comments_Label, 0, 4);
            this.table_Panel.Controls.Add(this.date_Label, 0, 0);
            this.table_Panel.Controls.Add(this.sales_Label, 0, 1);
            this.table_Panel.Controls.Add(this.sales_NumUpDown, 1, 1);
            this.table_Panel.Controls.Add(this.template_ComboBox, 1, 2);
            this.table_Panel.Controls.Add(this.notes_TextBox, 1, 4);
            this.table_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.table_Panel.Location = new System.Drawing.Point(0, 0);
            this.table_Panel.Name = "table_Panel";
            this.table_Panel.RowCount = 5;
            this.table_Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.table_Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.table_Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.table_Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.table_Panel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.table_Panel.Size = new System.Drawing.Size(174, 122);
            this.table_Panel.TabIndex = 0;
            // 
            // productivity_Label
            // 
            this.productivity_Label.AutoSize = true;
            this.table_Panel.SetColumnSpan(this.productivity_Label, 2);
            this.productivity_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.productivity_Label.Location = new System.Drawing.Point(3, 70);
            this.productivity_Label.Name = "productivity_Label";
            this.productivity_Label.Size = new System.Drawing.Size(168, 25);
            this.productivity_Label.TabIndex = 4;
            this.productivity_Label.Text = "Produktivität und Mitarbeiterzahl";
            this.productivity_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // template_Label
            // 
            this.template_Label.AutoSize = true;
            this.template_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.template_Label.Location = new System.Drawing.Point(3, 45);
            this.template_Label.Name = "template_Label";
            this.template_Label.Size = new System.Drawing.Size(51, 25);
            this.template_Label.TabIndex = 3;
            this.template_Label.Text = "Vorlage:";
            this.template_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comments_Label
            // 
            this.comments_Label.AutoSize = true;
            this.comments_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comments_Label.Location = new System.Drawing.Point(3, 95);
            this.comments_Label.Name = "comments_Label";
            this.comments_Label.Size = new System.Drawing.Size(51, 27);
            this.comments_Label.TabIndex = 2;
            this.comments_Label.Text = "Notizen:";
            this.comments_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // date_Label
            // 
            this.date_Label.AutoSize = true;
            this.date_Label.BackColor = System.Drawing.Color.Green;
            this.table_Panel.SetColumnSpan(this.date_Label, 2);
            this.date_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.date_Label.Location = new System.Drawing.Point(3, 0);
            this.date_Label.Name = "date_Label";
            this.date_Label.Size = new System.Drawing.Size(168, 20);
            this.date_Label.TabIndex = 0;
            this.date_Label.Text = "1. 5.";
            this.date_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sales_Label
            // 
            this.sales_Label.AutoSize = true;
            this.sales_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sales_Label.Location = new System.Drawing.Point(3, 20);
            this.sales_Label.Name = "sales_Label";
            this.sales_Label.Size = new System.Drawing.Size(51, 25);
            this.sales_Label.TabIndex = 1;
            this.sales_Label.Text = "Umsatz:";
            this.sales_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sales_NumUpDown
            // 
            this.sales_NumUpDown.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sales_NumUpDown.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.sales_NumUpDown.Location = new System.Drawing.Point(60, 23);
            this.sales_NumUpDown.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.sales_NumUpDown.Name = "sales_NumUpDown";
            this.sales_NumUpDown.Size = new System.Drawing.Size(111, 20);
            this.sales_NumUpDown.TabIndex = 5;
            this.sales_NumUpDown.ThousandsSeparator = true;
            this.sales_NumUpDown.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.sales_NumUpDown.ValueChanged += new System.EventHandler(this.sales_NumUpDown_ValueChanged);
            // 
            // template_ComboBox
            // 
            this.template_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.template_ComboBox.FormattingEnabled = true;
            this.template_ComboBox.Location = new System.Drawing.Point(60, 48);
            this.template_ComboBox.Name = "template_ComboBox";
            this.template_ComboBox.Size = new System.Drawing.Size(111, 21);
            this.template_ComboBox.TabIndex = 6;
            this.template_ComboBox.SelectionChangeCommitted += new System.EventHandler(this.template_ComboBox_TextChanged);
            // 
            // notes_TextBox
            // 
            this.notes_TextBox.AcceptsReturn = true;
            this.notes_TextBox.AcceptsTab = true;
            this.notes_TextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.notes_TextBox.Location = new System.Drawing.Point(60, 98);
            this.notes_TextBox.Multiline = true;
            this.notes_TextBox.Name = "notes_TextBox";
            this.notes_TextBox.Size = new System.Drawing.Size(111, 21);
            this.notes_TextBox.TabIndex = 7;
            this.notes_TextBox.Leave += new System.EventHandler(this.notes_TextBox_Leave);
            // 
            // MonthPlanningEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.table_Panel);
            this.Name = "MonthPlanningEntry";
            this.Size = new System.Drawing.Size(174, 122);
            this.table_Panel.ResumeLayout(false);
            this.table_Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sales_NumUpDown)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel table_Panel;
        private System.Windows.Forms.Label date_Label;
        private System.Windows.Forms.Label sales_Label;
        private System.Windows.Forms.Label productivity_Label;
        private System.Windows.Forms.Label template_Label;
        private System.Windows.Forms.Label comments_Label;
        private System.Windows.Forms.NumericUpDown sales_NumUpDown;
        private System.Windows.Forms.ComboBox template_ComboBox;
        private System.Windows.Forms.TextBox notes_TextBox;
    }
}
