﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace McD_Schichtplaner.GUI.Planung.MonthPlanning
{
    public partial class MonthPlanningEntry : UserControl
    {
        private DateTime date;
        private static List<Tuple<string, long>> templates;
        private int? workers;
        private TimeSpan? hours;
        private Database.Database data = Database.Database.DB;

        private MonthPlanningEntry()
        {
            InitializeComponent();
            if (templates == null)
            {
                templates = data.GetTemplates();
                templates.Insert(0, new Tuple<string, long>("", -1));
            }
            template_ComboBox.DataSource = templates;
            template_ComboBox.DisplayMember = "Item1";
            template_ComboBox.ValueMember = "Item2";
        }

        public MonthPlanningEntry(DateTime d) : this()
        {
            date = d;
            date_Label.Text = date.ToString("dd.MM.");
            notes_TextBox.Text = data.GetNotes(date.Year, date.Month, date.Day);
            if (date < DateTime.Today)
            {
                sales_NumUpDown.Value = data.GetActualDaySales(date);
                if (sales_NumUpDown.Value == 0) sales_NumUpDown.Value = data.GetExpDaySales(date);
            }
            else sales_NumUpDown.Value = data.GetExpDaySales(date);
            var templ = data.GetTemplateForDay(date);
            if(templ != -1)
            {
                template_ComboBox.SelectedValue = templ;
            }
            if (data.GetNumShiftsAtDay(date) > 0) template_ComboBox.Enabled = false;
            
        }

        private void sales_NumUpDown_ValueChanged(object sender, EventArgs e)
        {
            if (date < DateTime.Today) data.SetActualDaySales(date, (int)sales_NumUpDown.Value);
            else data.SetExpMonthSales(date.Year, date.Month, date.Day, (int)sales_NumUpDown.Value);
            SetProductivityLabel();
        }

        private void template_ComboBox_TextChanged(object sender, EventArgs e)
        {
            var item = (Tuple<string, long>)template_ComboBox.SelectedItem;
            if (item.Item2 == -1) return;
            SetTemplate(item.Item2);
            data.SetTemplateForDay(date, item.Item2);
        }

        private void SetTemplate(long id)
        {
            foreach (var shift in data.GetAllShiftsDay(date)) data.DeleteShift(shift);
            workers = 0;
            hours = new TimeSpan(0);
            foreach (var shift in data.GetTemplateShifts(id))
            {
                workers++;
                hours += shift.Length;
                data.CreateShift(new Database.Shift(shift, date));
            }
            SetProductivityLabel();
        }
        
        private void SetProductivityLabel()
        {
            productivity_Label.Text = $"{((double)sales_NumUpDown.Value / hours?.TotalHours)?.ToString("C") + "/h":-60} - {$"{workers?.ToString()} Mitarbeiter":60}";
        }

        private void notes_TextBox_Leave(object sender, EventArgs e)
        {
            data.SetNotes(date.Year, date.Month, date.Day, notes_TextBox.Text);
        }
    }
}
