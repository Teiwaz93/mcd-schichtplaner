﻿namespace McD_Schichtplaner.GUI.Planung.MonthPlanning
{
    partial class MonthPlanning
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.controls_Panel = new System.Windows.Forms.Panel();
            this.forward_Button = new System.Windows.Forms.Button();
            this.backwards_Button = new System.Windows.Forms.Button();
            this.month_Label = new System.Windows.Forms.Label();
            this.toprightSpacer_Panel = new System.Windows.Forms.Panel();
            this.back_Button = new System.Windows.Forms.Button();
            this.month_TablePanel = new System.Windows.Forms.TableLayoutPanel();
            this.sunday_Label = new System.Windows.Forms.Label();
            this.saturday_Label = new System.Windows.Forms.Label();
            this.friday_Label = new System.Windows.Forms.Label();
            this.thursday_Label = new System.Windows.Forms.Label();
            this.wednesday_Label = new System.Windows.Forms.Label();
            this.tuesday_Label = new System.Windows.Forms.Label();
            this.monday_Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.controls_Panel.SuspendLayout();
            this.month_TablePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.mainSplitContainer.IsSplitterFixed = true;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mainSplitContainer.Name = "mainSplitContainer";
            this.mainSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.controls_Panel);
            this.mainSplitContainer.Panel1.Controls.Add(this.toprightSpacer_Panel);
            this.mainSplitContainer.Panel1.Controls.Add(this.back_Button);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.month_TablePanel);
            this.mainSplitContainer.Size = new System.Drawing.Size(1264, 891);
            this.mainSplitContainer.SplitterDistance = 80;
            this.mainSplitContainer.TabIndex = 0;
            // 
            // controls_Panel
            // 
            this.controls_Panel.Controls.Add(this.forward_Button);
            this.controls_Panel.Controls.Add(this.backwards_Button);
            this.controls_Panel.Controls.Add(this.month_Label);
            this.controls_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.controls_Panel.Location = new System.Drawing.Point(85, 0);
            this.controls_Panel.Name = "controls_Panel";
            this.controls_Panel.Padding = new System.Windows.Forms.Padding(200, 0, 200, 0);
            this.controls_Panel.Size = new System.Drawing.Size(1094, 77);
            this.controls_Panel.TabIndex = 2;
            // 
            // forward_Button
            // 
            this.forward_Button.BackgroundImage = global::McD_Schichtplaner.Properties.Resources.forward;
            this.forward_Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.forward_Button.Dock = System.Windows.Forms.DockStyle.Right;
            this.forward_Button.Location = new System.Drawing.Point(809, 0);
            this.forward_Button.Name = "forward_Button";
            this.forward_Button.Size = new System.Drawing.Size(85, 77);
            this.forward_Button.TabIndex = 2;
            this.forward_Button.UseVisualStyleBackColor = true;
            this.forward_Button.Click += new System.EventHandler(this.forward_Button_Click);
            // 
            // backwards_Button
            // 
            this.backwards_Button.BackgroundImage = global::McD_Schichtplaner.Properties.Resources.backward;
            this.backwards_Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.backwards_Button.Dock = System.Windows.Forms.DockStyle.Left;
            this.backwards_Button.Location = new System.Drawing.Point(200, 0);
            this.backwards_Button.Name = "backwards_Button";
            this.backwards_Button.Size = new System.Drawing.Size(85, 77);
            this.backwards_Button.TabIndex = 1;
            this.backwards_Button.UseVisualStyleBackColor = true;
            this.backwards_Button.Click += new System.EventHandler(this.backwards_Button_Click);
            // 
            // month_Label
            // 
            this.month_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.month_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.month_Label.Location = new System.Drawing.Point(200, 0);
            this.month_Label.Name = "month_Label";
            this.month_Label.Size = new System.Drawing.Size(694, 77);
            this.month_Label.TabIndex = 0;
            this.month_Label.Text = "Monat des Jahres";
            this.month_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // toprightSpacer_Panel
            // 
            this.toprightSpacer_Panel.Dock = System.Windows.Forms.DockStyle.Right;
            this.toprightSpacer_Panel.Location = new System.Drawing.Point(1179, 0);
            this.toprightSpacer_Panel.Name = "toprightSpacer_Panel";
            this.toprightSpacer_Panel.Size = new System.Drawing.Size(85, 80);
            this.toprightSpacer_Panel.TabIndex = 1;
            // 
            // back_Button
            // 
            this.back_Button.Dock = System.Windows.Forms.DockStyle.Left;
            this.back_Button.Location = new System.Drawing.Point(0, 0);
            this.back_Button.Name = "back_Button";
            this.back_Button.Size = new System.Drawing.Size(85, 80);
            this.back_Button.TabIndex = 0;
            this.back_Button.Text = "Zurück";
            this.back_Button.UseVisualStyleBackColor = true;
            this.back_Button.Click += new System.EventHandler(this.back_Button_Click);
            // 
            // month_TablePanel
            // 
            this.month_TablePanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.month_TablePanel.ColumnCount = 7;
            this.month_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.month_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.month_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.month_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.month_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.month_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.month_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28572F));
            this.month_TablePanel.Controls.Add(this.sunday_Label, 6, 0);
            this.month_TablePanel.Controls.Add(this.saturday_Label, 5, 0);
            this.month_TablePanel.Controls.Add(this.friday_Label, 4, 0);
            this.month_TablePanel.Controls.Add(this.thursday_Label, 3, 0);
            this.month_TablePanel.Controls.Add(this.wednesday_Label, 2, 0);
            this.month_TablePanel.Controls.Add(this.tuesday_Label, 1, 0);
            this.month_TablePanel.Controls.Add(this.monday_Label, 0, 0);
            this.month_TablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.month_TablePanel.Location = new System.Drawing.Point(0, 0);
            this.month_TablePanel.Name = "month_TablePanel";
            this.month_TablePanel.RowCount = 7;
            this.month_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.month_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.month_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.month_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.month_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.month_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.month_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.month_TablePanel.Size = new System.Drawing.Size(1264, 807);
            this.month_TablePanel.TabIndex = 0;
            // 
            // sunday_Label
            // 
            this.sunday_Label.AutoSize = true;
            this.sunday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sunday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sunday_Label.Location = new System.Drawing.Point(1084, 1);
            this.sunday_Label.Name = "sunday_Label";
            this.sunday_Label.Size = new System.Drawing.Size(176, 35);
            this.sunday_Label.TabIndex = 6;
            this.sunday_Label.Text = "Sonntag";
            this.sunday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // saturday_Label
            // 
            this.saturday_Label.AutoSize = true;
            this.saturday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saturday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saturday_Label.Location = new System.Drawing.Point(904, 1);
            this.saturday_Label.Name = "saturday_Label";
            this.saturday_Label.Size = new System.Drawing.Size(173, 35);
            this.saturday_Label.TabIndex = 5;
            this.saturday_Label.Text = "Samstag";
            this.saturday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // friday_Label
            // 
            this.friday_Label.AutoSize = true;
            this.friday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.friday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.friday_Label.Location = new System.Drawing.Point(724, 1);
            this.friday_Label.Name = "friday_Label";
            this.friday_Label.Size = new System.Drawing.Size(173, 35);
            this.friday_Label.TabIndex = 4;
            this.friday_Label.Text = "Freitag";
            this.friday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // thursday_Label
            // 
            this.thursday_Label.AutoSize = true;
            this.thursday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thursday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thursday_Label.Location = new System.Drawing.Point(544, 1);
            this.thursday_Label.Name = "thursday_Label";
            this.thursday_Label.Size = new System.Drawing.Size(173, 35);
            this.thursday_Label.TabIndex = 3;
            this.thursday_Label.Text = "Donnerstag";
            this.thursday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // wednesday_Label
            // 
            this.wednesday_Label.AutoSize = true;
            this.wednesday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wednesday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wednesday_Label.Location = new System.Drawing.Point(364, 1);
            this.wednesday_Label.Name = "wednesday_Label";
            this.wednesday_Label.Size = new System.Drawing.Size(173, 35);
            this.wednesday_Label.TabIndex = 2;
            this.wednesday_Label.Text = "Mittwoch";
            this.wednesday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tuesday_Label
            // 
            this.tuesday_Label.AutoSize = true;
            this.tuesday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tuesday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tuesday_Label.Location = new System.Drawing.Point(184, 1);
            this.tuesday_Label.Name = "tuesday_Label";
            this.tuesday_Label.Size = new System.Drawing.Size(173, 35);
            this.tuesday_Label.TabIndex = 1;
            this.tuesday_Label.Text = "Dienstag";
            this.tuesday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // monday_Label
            // 
            this.monday_Label.AutoSize = true;
            this.monday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.monday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monday_Label.Location = new System.Drawing.Point(4, 1);
            this.monday_Label.Name = "monday_Label";
            this.monday_Label.Size = new System.Drawing.Size(173, 35);
            this.monday_Label.TabIndex = 0;
            this.monday_Label.Text = "Montag";
            this.monday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MonthPlanning
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainSplitContainer);
            this.Name = "MonthPlanning";
            this.Size = new System.Drawing.Size(1264, 891);
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.controls_Panel.ResumeLayout(false);
            this.month_TablePanel.ResumeLayout(false);
            this.month_TablePanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.Panel controls_Panel;
        private System.Windows.Forms.Button forward_Button;
        private System.Windows.Forms.Button backwards_Button;
        private System.Windows.Forms.Label month_Label;
        private System.Windows.Forms.Panel toprightSpacer_Panel;
        private System.Windows.Forms.Button back_Button;
        private System.Windows.Forms.TableLayoutPanel month_TablePanel;
        private System.Windows.Forms.Label sunday_Label;
        private System.Windows.Forms.Label saturday_Label;
        private System.Windows.Forms.Label friday_Label;
        private System.Windows.Forms.Label thursday_Label;
        private System.Windows.Forms.Label wednesday_Label;
        private System.Windows.Forms.Label tuesday_Label;
        private System.Windows.Forms.Label monday_Label;
    }
}
