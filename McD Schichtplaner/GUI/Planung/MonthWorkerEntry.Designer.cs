﻿namespace McD_Schichtplaner.GUI.Planung
{
    partial class MonthWorkerEntry
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.table = new System.Windows.Forms.TableLayoutPanel();
            this.label31 = new System.Windows.Forms.Label();
            this.label_ContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.löschenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kurzarbeitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sonderSchichtToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.krankToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.berufsschuleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tauschenMitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label0 = new System.Windows.Forms.Label();
            this.name_Label = new System.Windows.Forms.Label();
            this.table.SuspendLayout();
            this.label_ContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // table
            // 
            this.table.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.InsetDouble;
            this.table.ColumnCount = 32;
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225797F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.303571F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.303571F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225796F));
            this.table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.226133F));
            this.table.Controls.Add(this.label31, 31, 0);
            this.table.Controls.Add(this.label30, 30, 0);
            this.table.Controls.Add(this.label29, 29, 0);
            this.table.Controls.Add(this.label28, 28, 0);
            this.table.Controls.Add(this.label27, 27, 0);
            this.table.Controls.Add(this.label26, 26, 0);
            this.table.Controls.Add(this.label25, 25, 0);
            this.table.Controls.Add(this.label24, 24, 0);
            this.table.Controls.Add(this.label23, 23, 0);
            this.table.Controls.Add(this.label22, 22, 0);
            this.table.Controls.Add(this.label21, 21, 0);
            this.table.Controls.Add(this.label20, 20, 0);
            this.table.Controls.Add(this.label19, 19, 0);
            this.table.Controls.Add(this.label18, 18, 0);
            this.table.Controls.Add(this.label17, 17, 0);
            this.table.Controls.Add(this.label16, 16, 0);
            this.table.Controls.Add(this.label15, 15, 0);
            this.table.Controls.Add(this.label14, 14, 0);
            this.table.Controls.Add(this.label13, 13, 0);
            this.table.Controls.Add(this.label12, 12, 0);
            this.table.Controls.Add(this.label11, 11, 0);
            this.table.Controls.Add(this.label10, 10, 0);
            this.table.Controls.Add(this.label9, 9, 0);
            this.table.Controls.Add(this.label8, 8, 0);
            this.table.Controls.Add(this.label7, 7, 0);
            this.table.Controls.Add(this.label6, 6, 0);
            this.table.Controls.Add(this.label5, 5, 0);
            this.table.Controls.Add(this.label4, 4, 0);
            this.table.Controls.Add(this.label3, 3, 0);
            this.table.Controls.Add(this.label2, 2, 0);
            this.table.Controls.Add(this.label1, 1, 0);
            this.table.Controls.Add(this.label0, 0, 0);
            this.table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.table.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.table.Location = new System.Drawing.Point(0, 0);
            this.table.Name = "table";
            this.table.Padding = new System.Windows.Forms.Padding(300, 0, 0, 0);
            this.table.RowCount = 1;
            this.table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.table.Size = new System.Drawing.Size(1264, 50);
            this.table.TabIndex = 2;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.LemonChiffon;
            this.label31.ContextMenuStrip = this.label_ContextMenu;
            this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(1227, 3);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(31, 44);
            this.label31.TabIndex = 31;
            this.label31.Text = "X";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label31.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label_ContextMenu
            // 
            this.label_ContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.löschenToolStripMenuItem,
            this.kurzarbeitToolStripMenuItem,
            this.sonderSchichtToolStripMenuItem,
            this.krankToolStripMenuItem,
            this.berufsschuleToolStripMenuItem,
            this.tauschenMitToolStripMenuItem});
            this.label_ContextMenu.Name = "label_ContextMenu";
            this.label_ContextMenu.Size = new System.Drawing.Size(156, 136);
            // 
            // löschenToolStripMenuItem
            // 
            this.löschenToolStripMenuItem.Name = "löschenToolStripMenuItem";
            this.löschenToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.löschenToolStripMenuItem.Text = "Löschen";
            this.löschenToolStripMenuItem.Click += new System.EventHandler(this.LöschenToolStripMenuItem_Click);
            // 
            // kurzarbeitToolStripMenuItem
            // 
            this.kurzarbeitToolStripMenuItem.Name = "kurzarbeitToolStripMenuItem";
            this.kurzarbeitToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.kurzarbeitToolStripMenuItem.Text = "Kurzarbeit";
            this.kurzarbeitToolStripMenuItem.Click += new System.EventHandler(this.kurzarbeitToolStripMenuItem_Click);
            // 
            // sonderSchichtToolStripMenuItem
            // 
            this.sonderSchichtToolStripMenuItem.Name = "sonderSchichtToolStripMenuItem";
            this.sonderSchichtToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.sonderSchichtToolStripMenuItem.Text = "Sonder Schicht";
            this.sonderSchichtToolStripMenuItem.Click += new System.EventHandler(this.SonderSchichtToolStripMenuItem_Click);
            // 
            // krankToolStripMenuItem
            // 
            this.krankToolStripMenuItem.Name = "krankToolStripMenuItem";
            this.krankToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.krankToolStripMenuItem.Text = "Krank";
            this.krankToolStripMenuItem.Click += new System.EventHandler(this.KrankToolStripMenuItem_Click);
            // 
            // berufsschuleToolStripMenuItem
            // 
            this.berufsschuleToolStripMenuItem.Name = "berufsschuleToolStripMenuItem";
            this.berufsschuleToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.berufsschuleToolStripMenuItem.Text = "Berufsschule";
            this.berufsschuleToolStripMenuItem.Click += new System.EventHandler(this.BerufsschuleToolStripMenuItem_Click);
            // 
            // tauschenMitToolStripMenuItem
            // 
            this.tauschenMitToolStripMenuItem.Name = "tauschenMitToolStripMenuItem";
            this.tauschenMitToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.tauschenMitToolStripMenuItem.Text = "tauschen mit ...";
            this.tauschenMitToolStripMenuItem.Click += new System.EventHandler(this.TauschenMitToolStripMenuItem_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label30.ContextMenuStrip = this.label_ContextMenu;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(1199, 3);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(19, 44);
            this.label30.TabIndex = 30;
            this.label30.Text = "X";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label30.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.LemonChiffon;
            this.label29.ContextMenuStrip = this.label_ContextMenu;
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(1171, 3);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(19, 44);
            this.label29.TabIndex = 29;
            this.label29.Text = "X";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label29.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label28.ContextMenuStrip = this.label_ContextMenu;
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(1143, 3);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(19, 44);
            this.label28.TabIndex = 28;
            this.label28.Text = "X";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label28.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.LemonChiffon;
            this.label27.ContextMenuStrip = this.label_ContextMenu;
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(1115, 3);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(19, 44);
            this.label27.TabIndex = 27;
            this.label27.Text = "X";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label27.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label26.ContextMenuStrip = this.label_ContextMenu;
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(1087, 3);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(19, 44);
            this.label26.TabIndex = 26;
            this.label26.Text = "X";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label26.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.LemonChiffon;
            this.label25.ContextMenuStrip = this.label_ContextMenu;
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(1059, 3);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(19, 44);
            this.label25.TabIndex = 25;
            this.label25.Text = "X";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label25.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label24.ContextMenuStrip = this.label_ContextMenu;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(1031, 3);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(19, 44);
            this.label24.TabIndex = 24;
            this.label24.Text = "X";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label24.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.LemonChiffon;
            this.label23.ContextMenuStrip = this.label_ContextMenu;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(1003, 3);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(19, 44);
            this.label23.TabIndex = 23;
            this.label23.Text = "X";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label23.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label22.ContextMenuStrip = this.label_ContextMenu;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(975, 3);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(19, 44);
            this.label22.TabIndex = 22;
            this.label22.Text = "X";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label22.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.LemonChiffon;
            this.label21.ContextMenuStrip = this.label_ContextMenu;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(947, 3);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(19, 44);
            this.label21.TabIndex = 21;
            this.label21.Text = "X";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label21.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label20.ContextMenuStrip = this.label_ContextMenu;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(920, 3);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(18, 44);
            this.label20.TabIndex = 20;
            this.label20.Text = "X";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label20.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.LemonChiffon;
            this.label19.ContextMenuStrip = this.label_ContextMenu;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(892, 3);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 44);
            this.label19.TabIndex = 19;
            this.label19.Text = "X";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label19.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label18.ContextMenuStrip = this.label_ContextMenu;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(864, 3);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(19, 44);
            this.label18.TabIndex = 18;
            this.label18.Text = "X";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label18.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.LemonChiffon;
            this.label17.ContextMenuStrip = this.label_ContextMenu;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(836, 3);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(19, 44);
            this.label17.TabIndex = 17;
            this.label17.Text = "X";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label17.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label16.ContextMenuStrip = this.label_ContextMenu;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(808, 3);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(19, 44);
            this.label16.TabIndex = 16;
            this.label16.Text = "X";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label16.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.LemonChiffon;
            this.label15.ContextMenuStrip = this.label_ContextMenu;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(780, 3);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 44);
            this.label15.TabIndex = 15;
            this.label15.Text = "X";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label15.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label14.ContextMenuStrip = this.label_ContextMenu;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(752, 3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 44);
            this.label14.TabIndex = 14;
            this.label14.Text = "X";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label14.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.LemonChiffon;
            this.label13.ContextMenuStrip = this.label_ContextMenu;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(724, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 44);
            this.label13.TabIndex = 13;
            this.label13.Text = "X";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label13.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label12.ContextMenuStrip = this.label_ContextMenu;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(696, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 44);
            this.label12.TabIndex = 12;
            this.label12.Text = "X";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label12.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.LemonChiffon;
            this.label11.ContextMenuStrip = this.label_ContextMenu;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(668, 3);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 44);
            this.label11.TabIndex = 11;
            this.label11.Text = "X";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label11.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label10.ContextMenuStrip = this.label_ContextMenu;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(640, 3);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(19, 44);
            this.label10.TabIndex = 10;
            this.label10.Text = "X";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label10.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.LemonChiffon;
            this.label9.ContextMenuStrip = this.label_ContextMenu;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(612, 3);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(19, 44);
            this.label9.TabIndex = 9;
            this.label9.Text = "X";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label8.ContextMenuStrip = this.label_ContextMenu;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(584, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(19, 44);
            this.label8.TabIndex = 8;
            this.label8.Text = "X";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.LemonChiffon;
            this.label7.ContextMenuStrip = this.label_ContextMenu;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(557, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 44);
            this.label7.TabIndex = 7;
            this.label7.Text = "X";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label6.ContextMenuStrip = this.label_ContextMenu;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(529, 3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 44);
            this.label6.TabIndex = 6;
            this.label6.Text = "X";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.LemonChiffon;
            this.label5.ContextMenuStrip = this.label_ContextMenu;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(501, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 44);
            this.label5.TabIndex = 5;
            this.label5.Text = "X";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label4.ContextMenuStrip = this.label_ContextMenu;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(473, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 44);
            this.label4.TabIndex = 4;
            this.label4.Text = "X";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.LemonChiffon;
            this.label3.ContextMenuStrip = this.label_ContextMenu;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(445, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 44);
            this.label3.TabIndex = 3;
            this.label3.Text = "X";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label2.ContextMenuStrip = this.label_ContextMenu;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(417, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 44);
            this.label2.TabIndex = 2;
            this.label2.Text = "X";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.LemonChiffon;
            this.label1.ContextMenuStrip = this.label_ContextMenu;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(389, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(19, 44);
            this.label1.TabIndex = 1;
            this.label1.Text = "X";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Label_MouseClick);
            // 
            // label0
            // 
            this.label0.AutoSize = true;
            this.label0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label0.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label0.Location = new System.Drawing.Point(306, 3);
            this.label0.Name = "label0";
            this.label0.Size = new System.Drawing.Size(74, 44);
            this.label0.TabIndex = 0;
            this.label0.Text = "160,5";
            this.label0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // name_Label
            // 
            this.name_Label.Dock = System.Windows.Forms.DockStyle.Left;
            this.name_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_Label.Location = new System.Drawing.Point(0, 0);
            this.name_Label.Margin = new System.Windows.Forms.Padding(0);
            this.name_Label.MinimumSize = new System.Drawing.Size(300, 0);
            this.name_Label.Name = "name_Label";
            this.name_Label.Size = new System.Drawing.Size(300, 50);
            this.name_Label.TabIndex = 1;
            this.name_Label.Text = "Name";
            this.name_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.name_Label.Click += new System.EventHandler(this.name_Label_Click);
            // 
            // MonthWorkerEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.name_Label);
            this.Controls.Add(this.table);
            this.MinimumSize = new System.Drawing.Size(1264, 50);
            this.Name = "MonthWorkerEntry";
            this.Size = new System.Drawing.Size(1264, 50);
            this.table.ResumeLayout(false);
            this.table.PerformLayout();
            this.label_ContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel table;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label0;
        private System.Windows.Forms.Label name_Label;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ContextMenuStrip label_ContextMenu;
        private System.Windows.Forms.ToolStripMenuItem löschenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sonderSchichtToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem krankToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem berufsschuleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tauschenMitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kurzarbeitToolStripMenuItem;
    }
}
