﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using McD_Schichtplaner.Database;
using McD_Schichtplaner.GUI.Fehlerbehandlung;
using Org.BouncyCastle.Asn1.Cms;

namespace McD_Schichtplaner.GUI.Planung
{
    public partial class DayOverview : UserControl
    {
        private List<Shift> shifts = new List<Shift>();
        private List<Shift> kitchen = new List<Shift>();
        private List<Shift> drive = new List<Shift>();
        private List<Shift> service = new List<Shift>();
        private List<Shift> counter = new List<Shift>();
        private List<Shift> cafe = new List<Shift>();
        private List<Shift>[] ShiftsPerDay = new List<Shift>[9];
        private int index;
        private string day;
        private TimeSpan hours;
        private int expectedSales;

        private List<Error> errors = new List<Error>();

        private Database.Database data;
        private DateTime date;
        public DateTime Date => date;

        public DayOverview(DateTime d) : this()
        {
            date = d;
            data = Database.Database.DB;

            LoadWeek();
            SetDate();
        }

        public DayOverview(DateTime d, Size s) : this(d)
        {
            Size = s;
        }

        public DayOverview(int d, Database.Database.Month m, int y, Size s) : this(new DateTime(y, Database.Database.MonthToInt(m), d), s) { }

        public DayOverview()
        {
            InitializeComponent();
            template_Box.SelectedIndex = 0;
            hours = new TimeSpan();
        }

        private void LoadWeek()
        {
            index = date.DayOfWeek == DayOfWeek.Sunday ? 7 : (1 * (int)date.DayOfWeek);
            DateTime start = date.AddDays(-index);
            for (int i = 0; i < ShiftsPerDay.Length; i++)
            {
                ShiftsPerDay[i] = data.GetAllShiftsDay(start.AddDays(i));
            }
        }

        private void Reset()
        {
            hours = new TimeSpan(0);

            errors = new List<Error>();
            shifts = new List<Shift>();
            kitchen = new List<Shift>();
            service = new List<Shift>();
            drive = new List<Shift>();
            counter = new List<Shift>();
            cafe = new List<Shift>();

            kitchen_Table.Controls.Clear();
            side1_Table.Controls.Clear();
            side2_Table.Controls.Clear();
            toaster_Table.Controls.Clear();
            garnish_Table.Controls.Clear();
            uhc_Table.Controls.Clear();
            grill_Table.Controls.Clear();
            fryer_Table.Controls.Clear();
            bunker_Table.Controls.Clear();
            dRunner_Table.Controls.Clear();
            cRunner_Table.Controls.Clear();
            cashier_Table.Controls.Clear();
            drinks_Table.Controls.Clear();
            fries_Table.Controls.Clear();
            ice_Table.Controls.Clear();
            lobby_Table.Controls.Clear();
            presenter_Table.Controls.Clear();
            Jogi_Table.Controls.Clear();
            cafe_Table.Controls.Clear();
            lead_Table.Controls.Clear();
            help_Table.Controls.Clear();
            inventur_Table.Controls.Clear();

            kitchen_Panel.Visible = false;
            side1_Panel.Visible = false;
            side2_Panel.Visible = false;
            toaster_Panel.Visible = false;
            garnish_Panel.Visible = false;
            uhc_Panel.Visible = false;
            grill_Panel.Visible = false;
            fryer_Panel.Visible = false;
            bunker_Panel.Visible = false;
            dRunner_Panel.Visible = false;
            cRunner_Panel.Visible = false;
            cashier_Panel.Visible = false;
            drinks_Panel.Visible = false;
            fries_Panel.Visible = false;
            ice_Panel.Visible = false;
            lobby_Panel.Visible = false;
            presenter_Panel.Visible = false;
            jogi_Panel.Visible = false;
            cafe_Panel.Visible = false;
            lead_Panel.Visible = false;
            help_Panel.Visible = false;
            inventur_Panel.Visible = false;
        }

        private void SetDate()
        {
            Reset();

            date_Label.Text = date.Day + "." + date.Month + "." + date.Year;
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    day_Label.Text = "Montag";
                    break;
                case DayOfWeek.Tuesday:
                    day_Label.Text = "Dienstag";
                    break;
                case DayOfWeek.Wednesday:
                    day_Label.Text = "Mittwoch";
                    break;
                case DayOfWeek.Thursday:
                    day_Label.Text = "Donnerstag";
                    break;
                case DayOfWeek.Friday:
                    day_Label.Text = "Freitag";
                    break;
                case DayOfWeek.Saturday:
                    day_Label.Text = "Samstag";
                    break;
                case DayOfWeek.Sunday:
                    day_Label.Text = "Sonntag";
                    break;
                default:
                    break;
            }

            foreach (var shift in data.GetAllShiftsDay(date))
            {
                AddShift(shift);
            }

            expectedSales = data.GetExpMonthSales(date.Year, date.Month, date.Day);
            expectedSales_Box.Value = expectedSales;
            if (date < DateTime.Today)
            {
                actualSales_Box.Value = data.GetActualDaySales(date);
                if (actualSales_Box.Value == 0) actualSales_Box.Value = data.GetExpDaySales(date);
                actualSales_Box.Enabled = true;
                expectedSales_Box.Enabled = false;
            }
            else 
            {
                actualSales_Box.Enabled = false;
                expectedSales_Box.Enabled = true;
            }

            UpdateHours();
            notes_Box.Text = data.GetNotes(date.Year, date.Month, date.Day);

            CheckErrors();
        }

        private void UpdateHours()
        {
            hours_Box.Text = (hours.Days * 24 + hours.Hours) + "," + hours.Minutes / 6;
            UpdateProductivity();
            CheckErrors();
        }

        private void UpdateProductivity()
        {
            double h;
            double.TryParse(hours_Box.Text, out h);
            if (date >= DateTime.Today)
            {
                productivity_Box.Text = ((double)(expectedSales) / h).ToString(); 
            }
            else productivity_Box.Text = ((double)(actualSales_Box.Value) / h).ToString();
        }

        public void RemoveShift(ShiftControl shift)
        {
            shifts.Remove(shift.Shift);
            cafe.Remove(shift.Shift);
            kitchen.Remove(shift.Shift);
            service.Remove(shift.Shift);
            counter.Remove(shift.Shift);
            drive.Remove(shift.Shift);
            kitchen_Table.Controls.Remove(shift);
            side1_Table.Controls.Remove(shift);
            side2_Table.Controls.Remove(shift);
            toaster_Table.Controls.Remove(shift);
            garnish_Table.Controls.Remove(shift);
            uhc_Table.Controls.Remove(shift);
            grill_Table.Controls.Remove(shift);
            fryer_Table.Controls.Remove(shift);
            bunker_Table.Controls.Remove(shift);
            dRunner_Table.Controls.Remove(shift);
            cRunner_Table.Controls.Remove(shift);
            cashier_Table.Controls.Remove(shift);
            drinks_Table.Controls.Remove(shift);
            fries_Table.Controls.Remove(shift);
            ice_Table.Controls.Remove(shift);
            lobby_Table.Controls.Remove(shift);
            presenter_Table.Controls.Remove(shift);
            Jogi_Table.Controls.Remove(shift);
            cafe_Table.Controls.Remove(shift);
            lead_Table.Controls.Remove(shift);
            help_Table.Controls.Remove(shift);
            inventur_Table.Controls.Remove(shift);
            hours = hours.Subtract(shift.Shift.Length);
            UpdateHours();
        }

        public void AddShift(Shift shift)
        {
            shifts.Add(shift);
            hours = hours.Add(shift.Length);
            var area = data.GetAreaFor(data.GetStation(shift.Station));
            switch (area)
            {
                case Database.Database.Area.cafe:
                    cafe.Add(shift);
                    break;
                case Database.Database.Area.counter:
                    counter.Add(shift);
                    break;
                case Database.Database.Area.drive:
                    drive.Add(shift);
                    break;
                case Database.Database.Area.kitchen:
                    kitchen.Add(shift);
                    break;
                case Database.Database.Area.service:
                    service.Add(shift);
                    break;
            }


            int x1 = (shift.Start.Hour - 6) * 2;
            if (shift.Start.Date != date.Date) x1 += 48;
            if (shift.Start.Minute >= 30) x1++;
            int x2 = 0;
            if (shift.End.Date != date.Date)
            {
                x2 = 36;
                x2 += (shift.End.Hour) * 2;
            }
            else x2 = (shift.End.Hour - 6) * 2;
            if (x2 > 48) x2 = 48;
            int i = 0;
            TableLayoutPanel var_Table;
            switch (shift.Station)
            {
                case 1:
                    var_Table = kitchen_Table;
                    kitchen_Panel.Visible = true;
                    break;
                case 2:
                    var_Table = side1_Table;
                    side1_Panel.Visible = true;
                    break;
                case 3:
                    var_Table = side2_Table;
                    side2_Panel.Visible = true;
                    break;
                case 4:
                    var_Table = toaster_Table;
                    toaster_Panel.Visible = true;
                    break;
                case 5:
                    var_Table = garnish_Table;
                    garnish_Panel.Visible = true;
                    break;
                case 6:
                    var_Table = uhc_Table;
                    uhc_Panel.Visible = true;
                    break;
                case 7:
                    var_Table = grill_Table;
                    grill_Panel.Visible = true;
                    break;
                case 8:
                    var_Table = fryer_Table;
                    fryer_Panel.Visible = true;
                    break;
                case 9:
                    var_Table = bunker_Table;
                    bunker_Panel.Visible = true;
                    break;
                case 10:
                    var_Table = dRunner_Table;
                    dRunner_Panel.Visible = true;
                    break;
                case 11:
                    var_Table = cRunner_Table;
                    cRunner_Panel.Visible = true;
                    break;
                case 12:
                    var_Table = cashier_Table;
                    cashier_Panel.Visible = true;
                    break;
                case 13:
                    var_Table = drinks_Table;
                    drinks_Panel.Visible = true;
                    break;
                case 14:
                    var_Table = fries_Table;
                    fries_Panel.Visible = true;
                    break;
                case 15:
                    var_Table = ice_Table;
                    ice_Panel.Visible = true;
                    break;
                case 16:
                    var_Table = lobby_Table;
                    lobby_Panel.Visible = true;
                    break;
                case 17:
                    var_Table = presenter_Table;
                    presenter_Panel.Visible = true;
                    break;
                case 18:
                    var_Table = Jogi_Table;
                    jogi_Panel.Visible = true;
                    break;
                case 19:
                    var_Table = cafe_Table;
                    cafe_Panel.Visible = true;
                    break;
                case 20:
                    var_Table = lead_Table;
                    lead_Panel.Visible = true;
                    break;
                case 21:
                    var_Table = help_Table;
                    help_Panel.Visible = true;
                    break;
                case 22:
                    var_Table = inventur_Table;
                    inventur_Panel.Visible = true;
                    break;
                default:
                    return;
            }
            while (var_Table.GetControlFromPosition(x1, i) != null)
            {
                i++;
                if (var_Table.RowCount - 1 < i)
                {
                    var_Table.RowCount = var_Table.RowCount + 1;
                    var_Table.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 50F));
                }
            }
            ShiftControl block = var_Table.GetControlFromPosition(x2, i) as ShiftControl;
            int middle = x2 - ((x2 - x1) / 2);
            ShiftControl blockMiddle = var_Table.GetControlFromPosition(middle, i) as ShiftControl;
            ShiftControl sc = new ShiftControl(shift, data, this);
            var_Table.Controls.Add(sc, x1, i);
            Console.WriteLine("added shift into table at: " + x1 + "/" + i);
            var_Table.SetColumnSpan(sc, x2 - x1);
            if (!(block is null))
            {
                Console.WriteLine("switching stuff around");
                RemoveShift(block);
                AddShift(block.Shift);
            }
            if (blockMiddle != block && !(blockMiddle is null))
            {
                RemoveShift(blockMiddle);
                AddShift(blockMiddle.Shift);
            }
            UpdateHours();
        }

        private void CheckErrors()
        {
            errors = new List<Error>();

            foreach(var shift in shifts)
            {
                if (shift.WorkerID == 0) continue;
                string name = data.GetWorkerName(shift.WorkerID);
                int numShifts = 0;
                for (int i = 1; i <= 7; i++)
                {
                    foreach(var shiftDay in ShiftsPerDay[i])
                    {
                        if(shiftDay.WorkerID == shift.WorkerID)
                        {
                            numShifts++;
                            break;
                        }
                    }
                    if (numShifts > 5) errors.Add(new Error(true, $"Mitarbeiter {name} hat in dieser Woche zu viele Schichten."));
                }
                foreach (var shiftPrev in ShiftsPerDay[index - 1])
                {
                    if (shiftPrev.WorkerID == shift.WorkerID)
                    {
                        if (shift.Start - shiftPrev.End < new TimeSpan(12, 0, 0)) errors.Add(new Error(true, $"Mitarbeiter {name} hat nicht genügend Ruhezeit vor dieser Schicht."));
                        break;
                    }
                }
                foreach (var shiftNext in ShiftsPerDay[index + 1])
                {
                    if (shiftNext.WorkerID == shift.WorkerID)
                    {
                        if (shiftNext.Start - shift.End < new TimeSpan(12, 0, 0)) errors.Add(new Error(true, $"Mitarbeiter {name} hat nicht genügend Ruhezeit nach dieser Schicht."));
                        break;
                    }
                }
                if (shift.WorkerID != 0)
                {
                    var availabilities = data.GetAvailabilities(data.GetWorker(shift.WorkerID));
                    if (shift.Start.TimeOfDay < availabilities.Times[index - 1].Item1) errors.Add(new Error(false, $"Mitarbeiter {name} beginnt seine Schicht außerhalb seiner Vefügungszeiten."));
                    if (shift.End.TimeOfDay > availabilities.Times[index - 1].Item2) errors.Add(new Error(false, $"Mitarbeiter {name} beendet seine Schicht außerhalb seiner Vefügungszeiten.")); 
                }
            }



            int minor = 0, severe = 0;
            string tip = "";
            foreach (var error in errors)
            {
                if (error.Severe) severe++;
                else minor++;
                tip += $"{(error.Severe ? "Fehler:" : "Warnung")} - {error.Message}";
            }
            toolTip.SetToolTip(errors_Button, tip);

            errors_Button.Text = $"{minor} Warnungen - {severe} Fehler";
            if (severe > 0) errors_Button.BackColor = Color.Red;
            else if (minor > 0) errors_Button.BackColor = Color.Yellow;
            else errors_Button.BackColor = Color.LimeGreen;
        }

        private void Print_Button_Click(object sender, EventArgs e)
        {
            if (printDialog.ShowDialog() == DialogResult.OK)
            {
                printDialog.Document = plan_Document;
                plan_Document.DefaultPageSettings.Landscape = true;
                plan_Document.Print();

                printDialog.Document = food_Document;
                food_Document.DefaultPageSettings.Landscape = false;
                food_Document.Print();
            }
        }

        private void PrintDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            using (Font font = new Font("Arial", 12))
            {
                using (Font majorFont = new Font(font, FontStyle.Bold))
                {
                    float nameLength = 155;
                    float timeLength = 105;
                    float stationLength = 45;
                    float numberLength = 35;
                    float restLength = 60;
                    float notesLength = 145;
                    using (Brush text = new SolidBrush(Color.Black))
                    {
                        using (Pen line = new Pen(Brushes.Black))
                        {
                            float borderOffset = 75;
                            float extraOFfset = -25f;
                            float xMin = e.MarginBounds.Left - borderOffset;
                            float yMin = e.MarginBounds.Top - borderOffset;
                            float xMax = e.MarginBounds.Right + borderOffset + extraOFfset;
                            float yMax = e.MarginBounds.Bottom + borderOffset + extraOFfset;
                            float leftStart = xMin + nameLength + timeLength + stationLength + restLength + notesLength + numberLength;
                            float lineSpacing = 18;
                            float x = xMin;
                            float y = yMin;
                            float yLeft = y;
                            float yRight = y;
                            float top;

                            while ((nameLength + timeLength + stationLength + numberLength + restLength + notesLength) * 2 > xMax - xMin)
                            {
                                notesLength--;
                            }
                            Console.WriteLine(notesLength);

                            string s = "Schichtplan für " + day + ", den " + +date.Day + "." + date.Month + "." + date.Year;
                            using (Font head = new Font("Arial", 20, FontStyle.Bold))
                            {
                                e.Graphics.DrawString(s, head, text, leftStart - (e.Graphics.MeasureString(s, head).Width / 2), y);
                            }
                            y += 30;
                            e.Graphics.DrawLine(line, xMin, y, xMax, y);

                            s = data.GetRestaurantName();
                            var size = e.Graphics.MeasureString(s, font);
                            e.Graphics.DrawString(s, font, text, xMax - size.Width, y - size.Height);

                            y += lineSpacing;
                            var combined = new List<Shift>(counter);
                            combined.AddRange(service);
                            PrintArea("Service", combined);
                            y += lineSpacing * 2;

                            x = xMin;
                            PrintArea("Drive", drive);
                            y += lineSpacing * 2;

                            x = xMin;
                            PrintArea("Küche", kitchen);
                            y += lineSpacing * 2;

                            x = xMin;
                            PrintArea("Cafè", cafe);

                            e.Graphics.DrawLine(line, xMin, yMin, xMax, yMin);
                            e.Graphics.DrawLine(line, xMin, yMin, xMin, yMax);
                            e.Graphics.DrawLine(line, xMin, yMax, xMax, yMax);
                            e.Graphics.DrawLine(line, xMax, yMax, xMax, yMin);


                            y += lineSpacing * 2;
                            x = xMin;
                            s = "Kommentare:";
                            e.Graphics.DrawString(s, font, text, x, y);
                            e.Graphics.DrawLine(line, xMin, y, xMax, y);
                            y += lineSpacing;
                            foreach (var str in notes_Box.Lines)
                            {
                                e.Graphics.DrawString(str, font, text, x, y);
                                y += lineSpacing;
                            }

                            void PrintArea(string name, List<Shift> list)
                            {
                                s = name;
                                e.Graphics.DrawString(s, majorFont, text, x, y);
                                y += lineSpacing;
                                top = yLeft = yRight = y;
                                e.Graphics.DrawLine(line, xMin, y, xMax, y);
                                foreach (var shift in list)
                                {
                                    if (shift.Start.Hour < 14)
                                    {
                                        y = yLeft;
                                        x = xMin;

                                        s = data.GetWorkerName(shift.WorkerID);
                                        e.Graphics.DrawString(s, majorFont, text, x, y);
                                        x += nameLength;

                                        if (data.WorkerIsMinor(shift.WorkerID, shift.Start.Date))
                                        {
                                            e.Graphics.DrawString("jug.", font, text, x, y);
                                        }
                                        x += numberLength;

                                        s = Database.Database.ToShortTime(shift.Start) + "-" + Database.Database.ToShortTime(shift.End);
                                        e.Graphics.DrawString(s, majorFont, text, x, y);
                                        x += timeLength;

                                        s = data.GetStationShort(shift.Station);
                                        e.Graphics.DrawString(s, font, text, x, y);
                                        x += stationLength;

                                        if (shift.Rest > DateTime.MinValue)
                                        {
                                            s = Database.Database.ToShortTime(shift.Rest);
                                            e.Graphics.DrawString(s, font, text, x, y);
                                        }
                                        x += restLength;

                                        if (shift.Rest_Minor > DateTime.MinValue)
                                        {
                                            s = "Pause 2 " + Database.Database.ToShortTime(shift.Rest_Minor);
                                        }
                                        else s = shift.Notes;
                                        e.Graphics.DrawString(s, font, text, x, y);
                                        x += notesLength;

                                        yLeft += lineSpacing;
                                    }
                                    else
                                    {
                                        y = yRight;
                                        x = xMax;

                                        s = data.GetWorkerName(shift.WorkerID);
                                        e.Graphics.DrawString(s, majorFont, text, x - e.Graphics.MeasureString(s, majorFont).Width, y);
                                        x -= nameLength;

                                        x -= numberLength;
                                        if (data.WorkerIsMinor(shift.WorkerID, date))
                                        {
                                            e.Graphics.DrawString("jug", font, text, x, y); 
                                        }

                                        x -= timeLength;
                                        s = Database.Database.ToShortTime(shift.Start) + "-" + Database.Database.ToShortTime(shift.End);
                                        e.Graphics.DrawString(s, majorFont, text, x, y);

                                        x -= stationLength;
                                        s = data.GetStationShort(shift.Station);
                                        e.Graphics.DrawString(s, font, text, x, y);

                                        x -= restLength;
                                        if (shift.Rest > DateTime.MinValue)
                                        {
                                            s = Database.Database.ToShortTime(shift.Rest);
                                            e.Graphics.DrawString(s, font, text, x, y);
                                        }

                                        x -= notesLength;
                                        if (shift.Rest_Minor > DateTime.MinValue)
                                        {
                                            s = "Pause " + Database.Database.ToShortTime(shift.Rest_Minor);
                                        }
                                        else s = shift.Notes;
                                        e.Graphics.DrawString(s, font, text, x, y);

                                        yRight += lineSpacing;
                                    }
                                    e.Graphics.DrawLine(line, xMin, y, xMax, y);
                                }
                                y = yLeft > yRight ? yLeft : yRight;
                                x = xMin;
                                e.Graphics.DrawLine(line, xMin, y, xMax, y);
                                x += nameLength;
                                e.Graphics.DrawLine(line, x, y, x, top);
                                x += numberLength;
                                e.Graphics.DrawLine(line, x, y, x, top);
                                x += timeLength;
                                e.Graphics.DrawLine(line, x, y, x, top);
                                x += stationLength;
                                e.Graphics.DrawLine(line, x, y, x, top);
                                x += restLength;
                                e.Graphics.DrawLine(line, x, y, x, top);
                                x += notesLength - 1.5f;
                                e.Graphics.DrawLine(line, x, y, x, top);
                                x += 1;
                                e.Graphics.DrawLine(line, x, y, x, top);
                                x += 1;
                                e.Graphics.DrawLine(line, x, y, x, top);
                                e.Graphics.DrawLine(line, x, y, x, top);
                                x = leftStart;
                                e.Graphics.DrawLine(line, xMin, y, xMax, y);
                                x += notesLength;
                                e.Graphics.DrawLine(line, x, y, x, top);
                                x += restLength;
                                e.Graphics.DrawLine(line, x, y, x, top);
                                x += stationLength;
                                e.Graphics.DrawLine(line, x, y, x, top);
                                x += timeLength;
                                e.Graphics.DrawLine(line, x, y, x, top);
                                x += numberLength;
                                e.Graphics.DrawLine(line, x, y, x, top);
                                x += nameLength;
                                e.Graphics.DrawLine(line, x, y, x, top);
                            }
                        }
                    }
                }
            }

        }


        private void Notes_Box_Leave(object sender, EventArgs e)
        {
            data.SetNotes(date.Year, date.Month, date.Day, notes_Box.Text);
        }

        private void NewShift_Button_Click(object sender, EventArgs e)
        {
            ShiftForm sf = new ShiftForm(date);
            sf.ShowDialog();
            if (sf.DialogResult == DialogResult.OK)
            {
                data.CreateShift(sf.Shift);
                sf.Shift.ID = data.GetLastInsert();
                AddShift(sf.Shift);
            }
        }

        private void Food_Document_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            using (Font font = new Font("Calibri", 12))
            {
                float nameLength = 145f;
                float xMin = e.MarginBounds.Left;
                float yMin = e.MarginBounds.Top;
                float xMax = e.MarginBounds.Right;
                float yMax = e.MarginBounds.Bottom;
                float headerSize = 18;
                float lineSpacing = 30;
                Brush text = new SolidBrush(Color.Black);
                Pen line = new Pen(Brushes.Black);

                float x = xMin;
                float y = yMin;

                string s = "Personalessen für " + day + ", den " + +date.Day + "." + date.Month + "." + date.Year;
                e.Graphics.DrawString(s, font, text, x, y);
                y += lineSpacing;
                e.Graphics.DrawLine(line, xMin, yMin + headerSize, xMax, yMin + headerSize);

                foreach (var shift in shifts)
                {
                    s = $"{data.GetWorkerName(shift.WorkerID)}({shift.WorkerID})";
                    e.Graphics.DrawString(s, font, text, xMin, y);
                    e.Graphics.DrawLine(line, xMin, y, xMax, y);

                    y += lineSpacing;
                }
                e.Graphics.DrawLine(line, xMin, y, xMax, y);
                e.Graphics.DrawLine(line, xMin, yMin, xMin, y);
                e.Graphics.DrawLine(line, xMin + nameLength, yMin + lineSpacing, xMin + nameLength, y);
                e.Graphics.DrawLine(line, xMax, yMin, xMax, y);
            }
        }

        private void ExpectedSales_Box_ValueChanged(object sender, EventArgs e)
        {
            expectedSales = (int)expectedSales_Box.Value;
            data.SetExpMonthSales(date.Year, date.Month, date.Day, expectedSales);
            UpdateProductivity();
        }

        private void Template_Button_Click(object sender, EventArgs e)
        {
            if (template_Box.SelectedIndex == 0) //Vorlage einbringen
            {
                var tp = new TemplatePicker.TemplatePicker();
                var result = tp.ShowDialog();
                if (result == DialogResult.OK)
                {
                    data.SetTemplateForDay(date, tp.Active.ID);
                    foreach (var tmp in tp.Active.Shifts)
                    {
                        var shift = new Shift(tmp, date);
                        data.CreateShift(shift);
                        shift.ID = data.GetLastInsert();
                        AddShift(shift);
                    }
                }
            }
            else //Vorlage erstellen
            {
                StringError se = new StringError("Namen für neue Vorlage.", "Vorlage von " + date_Label.Text);
                se.Text = "Neue Vorlage";
                se.ShowDialog();
                var name = se.String;
                var id = data.CreateTemplate(name);
                foreach (var shift in shifts)
                {
                    data.CreateTemplateShift(new TemplateShift(shift, id));
                }
            }
        }

        private void back_Button_Click(object sender, EventArgs e)
        {
            Mainwindow mw = Parent.Parent.Parent.Parent as Mainwindow;
            mw.ChangePanel(new MonthOverview(date, Size));
        }

        private void backward_Button_Click(object sender, EventArgs e)
        {
            date = date.AddDays(-1);
            index--;
            if (index == 0)
            {
                LoadWeek();
            }
            SetDate();
        }

        private void forward_Button_Click(object sender, EventArgs e)
        {
            date = date.AddDays(1);
            index++;
            if (index == 8)
            {
                LoadWeek();
            }
            SetDate();
        }

        private void errors_Button_Click(object sender, EventArgs e)
        {
            ErrorsForm ef = new ErrorsForm(errors);
            ef.ShowDialog();
        }

        private void errors_Button_MouseEnter(object sender, EventArgs e)
        {
            CheckErrors();
        }

        private void actualSales_Box_ValueChanged(object sender, EventArgs e)
        {
            data.SetActualDaySales(date, (int)actualSales_Box.Value);
            UpdateProductivity();
        }
    }
}
