﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using McD_Schichtplaner.Database;
using McD_Schichtplaner.Mailing;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Drawing.Printing;

namespace McD_Schichtplaner.GUI.Planung
{
    public partial class WorkerOverview : UserControl
    {
        private static Database.Database data = Database.Database.DB;
        private Worker worker;
        private DateTime date;
        private string[] nonShifts = new string[33];
        private Shift[] shifts = new Shift[33];

        public Worker Worker => worker;

        public WorkerOverview(Worker w, DateTime d, Size s)
        {
            InitializeComponent();
            Size = s;
            var workers = data.GetWorkers();
            foreach (var worker in workers) worker_ComboBox.Items.Add(worker.Name);
            worker = w;
            date = new DateTime(d.Year, d.Month, 1);
            worker_ComboBox.Text = worker.Name;
        }

        private void worker_ComboBox_TextUpdate(object sender, EventArgs e)
        {
            worker = data.GetWorker(worker_ComboBox.Text);
            tableLayoutPanel1.Controls.Clear();
            tableLayoutPanel1.Controls.Add(top_Panel, 0, 0);
            if (worker.Mail is null) mail_Button.Enabled = false;
            else mail_Button.Enabled = true;
            var nons = data.GetFrees(worker, date.AddDays(-1), date.AddMonths(1));
            nonShifts = new string[32];
            foreach (var non in nons)
            {
                nonShifts[non.Item2.Day] = non.Item1;
            }
            var shifts_temp = data.GetWorkerShiftsInterval(worker, date.AddDays(-1), date.AddMonths(1));
            shifts = new Shift[33];
            foreach (var shift in shifts_temp)
            {
                shifts[shift.Start.Day] = shift;
            }

            for (int d = 1; d <= DateTime.DaysInMonth(date.Year, date.Month); d++)
            {
                WorkerEntry entry;
                if (shifts[d] != null && nonShifts[d] != null)
                {
                    MessageBox.Show($"{worker.Name} hat am {d}.{date.Month}. zwei Einträge.\nHier wird nur Schicht angezeigt, bitte fixen.", "Fehler", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                if (shifts[d] != null) entry = new WorkerEntry(shifts[d], new DateTime(date.Year, date.Month, d), this);
                else if (nonShifts[d] != null) entry = new WorkerEntry(nonShifts[d], new DateTime(date.Year, date.Month, d),this);
                else entry = new WorkerEntry(new DateTime(date.Year, date.Month, d), this);
                tableLayoutPanel1.Controls.Add(entry, 0, d);
            }
        }

        private void mail_Button_Click(object sender, EventArgs e)
        {
            int i = 0;
            while (File.Exists($"Temp\\temp{i}.pdf")) i++;
            string pdf = $"Temp\\temp{i}.pdf";
            CreatePDF(pdf);
            string body = "Hallo, \nneuer Schichtplan ist im Anhang.\n\nGruß";
            var mm = new MailManager();
            mm.SendMail(worker.Mail, "Neuer Schichtplan", body, pdf);
        }

        public void CreatePDF(string path)
        {
            Document doc = new Document();
            FileStream fs = new FileStream(path, FileMode.Create);
            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
            doc.Open();
            doc.Add(new Paragraph($"Plan für {worker.Name} im  Monat {Database.Database.MonthToString(Database.Database.IntToMonth(this.date.Month))} / {this.date.Year}."));
            var date = new DateTime(this.date.Year, this.date.Month, 1);
            float[] widths = { 150f, 850f };
            PdfPTable table = new PdfPTable(2)
            {
                TotalWidth = 500f,
                LockedWidth = true,
                SpacingBefore = 25f,
                SpacingAfter = 15f
            };
            table.SetWidths(widths);
            while (date.Month == this.date.Month)
            {
                string dow = "";
                switch (date.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        dow = "Mo.";
                        break;
                    case DayOfWeek.Tuesday:
                        dow = "Di.";
                        break;
                    case DayOfWeek.Wednesday:
                        dow = "Mi.";
                        break;
                    case DayOfWeek.Thursday:
                        dow = "Do.";
                        break;
                    case DayOfWeek.Friday:
                        dow = "Fr.";
                        break;
                    case DayOfWeek.Saturday:
                        dow = "Sa.";
                        break;
                    case DayOfWeek.Sunday:
                        dow = "So.";
                        break;
                }
                PdfPCell dayCell = new PdfPCell(new Phrase($"{dow} {date:dd.MM}")) { HorizontalAlignment = 1 };
                table.AddCell(dayCell);
                Phrase phrase = new Phrase();
                if (shifts[date.Day] != null)
                {
                    phrase = new Phrase($"{shifts[date.Day].Start:HH:mm} - {shifts[date.Day].End:HH:mm}");
                }
                else if (nonShifts[date.Day] != null)
                {
                    switch (nonShifts[date.Day])
                    {
                        case "W":
                            phrase = new Phrase(nonShifts[date.Day] + " - Wunschfrei");
                            break;
                        case "U":
                            phrase = new Phrase(nonShifts[date.Day] + " - Urlaub");
                            break;
                        case "S":
                            phrase = new Phrase(nonShifts[date.Day] + " - Sonderschicht");
                            break;
                        case "BS":
                            phrase = new Phrase(nonShifts[date.Day] + " - Berufsschule");
                            break;
                        case "K":
                            phrase = new Phrase(nonShifts[date.Day] + " - Krankmeldung");
                            break;
                        case "KUG":
                            phrase = new Phrase(nonShifts[date.Day] + " - Kurarbeit");
                            break;
                        default:
                            phrase = new Phrase(nonShifts[date.Day]);
                            break;
                    }
                }
                else
                {
                    phrase = new Phrase("X - kein Einsatz");
                }
                date = date.AddDays(1);
                PdfPCell cell = new PdfPCell(phrase)
                {
                    HorizontalAlignment = 1,
                    MinimumHeight = 20f
                };
                table.AddCell(cell);
            }

            TimeSpan hours = new TimeSpan(0);
            foreach (var shift in shifts)
            {
                if (shift != null) hours += shift.Length;
            }
            foreach (var nonShift in nonShifts)
            {
                if (nonShifts != null) hours += worker.GetHoursForNonShift(nonShift);
            }
            Phrase hourphrase = new Phrase($"gesamt Studnen: {hours.Days * 24 + hours.Hours},{hours.Minutes / 6}");
            PdfPCell footnote = new PdfPCell(hourphrase)
            {
                Colspan = 2,
                HorizontalAlignment = 2
            };
            table.AddCell(footnote);

            doc.Add(table);
            doc.Close();
            writer.Close();
            fs.Close();
            fs.Dispose();
        }

        private void print_Button_Click(object sender, EventArgs e)
        {
            if (printDialog.ShowDialog() == DialogResult.OK)
            {
                printDialog.Document = printDocument;
                printDocument.Print();
                GC.Collect();
            }
        }

        private void printDocument_PrintPage(object sender, PrintPageEventArgs e)
        {
            var date = new DateTime(this.date.Year, this.date.Month, 1);
            float xmin = e.MarginBounds.Left;
            float xmax = e.MarginBounds.Right;
            float ymin = e.MarginBounds.Top;
            float ymax = e.MarginBounds.Bottom;
            float width = xmax - xmin;
            float height = ymax - ymin;
            float lineHeight = height / DateTime.DaysInMonth(date.Year, date.Month);
            float x, y;
            float lineOffset;
            PointF loc;
            SizeF size;
            string str;
            float dayLength = 0;
            using (Pen line = new Pen(Brushes.Black))
            {
                using (Brush normalTextBrush = new SolidBrush(System.Drawing.Color.Black))
                {
                    using (System.Drawing.Font normalFont = new System.Drawing.Font("Arial", 12),
                        largeFont = new System.Drawing.Font("Arial", 20))
                    {
                        lineOffset = (lineHeight - e.Graphics.MeasureString("hello", normalFont).Height) / 2;

                        e.Graphics.DrawLine(line, xmin, ymin, xmax, ymin);
                        e.Graphics.DrawLine(line, xmin, ymin, xmin, ymax);
                        e.Graphics.DrawLine(line, xmax, ymin, xmax, ymax);
                        e.Graphics.DrawLine(line, xmin, ymax, xmax, ymax);

                        str = $"Plan für {worker.Name} im  Monat {Database.Database.MonthToString(Database.Database.IntToMonth(date.Month))} / {date.Year}.";
                        size = e.Graphics.MeasureString(str, largeFont);
                        if (size.Width > width)
                        {
                            float overhang = size.Width - width;
                            loc = new PointF(xmin - overhang / 2, ymin - size.Height);
                        }
                        else loc = new PointF(xmin, ymin - size.Height);
                        e.Graphics.DrawString(str, largeFont, normalTextBrush, loc);

                        loc = new PointF(xmin, ymin);
                        while (date.Month == this.date.Month)
                        {
                            string dow = "";
                            switch (date.DayOfWeek)
                            {
                                case DayOfWeek.Monday:
                                    dow = "Mo.";
                                    break;
                                case DayOfWeek.Tuesday:
                                    dow = "Di.";
                                    break;
                                case DayOfWeek.Wednesday:
                                    dow = "Mi.";
                                    break;
                                case DayOfWeek.Thursday:
                                    dow = "Do.";
                                    break;
                                case DayOfWeek.Friday:
                                    dow = "Fr.";
                                    break;
                                case DayOfWeek.Saturday:
                                    dow = "Sa.";
                                    break;
                                case DayOfWeek.Sunday:
                                    dow = "So.";
                                    break;
                            }
                            str = $"{dow,-8}\t{date,8:dd.MM.}";
                            e.Graphics.DrawString(str, normalFont, normalTextBrush, new PointF(xmin, loc.Y + lineOffset));
                            dayLength = e.Graphics.MeasureString(str, normalFont).Width + xmin;
                            loc = new PointF(dayLength, loc.Y);
                            var offset = (width - dayLength - e.Graphics.MeasureString(str, normalFont).Width) / 2;
                            //schicht
                            if (shifts[date.Day] != null)
                            {
                                str = $"{shifts[date.Day].Start:HH:mm} - {shifts[date.Day].End:HH:mm}";
                                e.Graphics.DrawString(str, normalFont, normalTextBrush, loc.X + offset, loc.Y + lineOffset);
                            }
                            //sonder schicht
                            else if (nonShifts[date.Day] != null)
                            {
                                switch (nonShifts[date.Day])
                                {
                                    case "W":
                                        str = nonShifts[date.Day] + " - Wunschfrei";
                                        break;
                                    case "U":
                                        str = nonShifts[date.Day] + " - Urlaub";
                                        break;
                                    case "S":
                                        str = nonShifts[date.Day] + " - Sonderschicht";
                                        break;
                                    case "BS":
                                        str = nonShifts[date.Day] + " - Berufsschule";
                                        break;
                                    case "K":
                                        str = nonShifts[date.Day] + " - Krankmeldung";
                                        break;
                                    case "KUG":
                                        str = nonShifts[date.Day] + " - Kurarbeit";
                                        break;
                                    default:
                                        str = nonShifts[date.Day];
                                        break;
                                }
                                e.Graphics.DrawString(str, normalFont, normalTextBrush, loc.X + offset, loc.Y + lineOffset);
                            }
                            //kein einsatz
                            else
                            {
                                str = "X\tkein Einsatz";
                                e.Graphics.DrawString(str, normalFont, normalTextBrush, loc.X + offset, loc.Y + lineOffset);
                            }
                            date = date.AddDays(1);
                            loc = new PointF(xmin, loc.Y + height / DateTime.DaysInMonth(date.Year, date.Month));
                            e.Graphics.DrawLine(line, loc, new PointF(xmax, loc.Y));
                        }
                        e.Graphics.DrawLine(line, dayLength, ymin, dayLength, ymax);
                        TimeSpan hours = new TimeSpan(0);
                        foreach (var shift in shifts)
                        {
                            if (shift != null) hours += shift.Length;
                        }
                        foreach (var nonShift in nonShifts)
                        {
                            if (nonShifts != null) hours += worker.GetHoursForNonShift(nonShift);
                        }
                        str = $"gesamt Stunden: {hours.Days * 24 + hours.Hours},{hours.Minutes / 6}";
                        var length = e.Graphics.MeasureString(str, normalFont).Width;
                        loc = new PointF(xmax - length, ymax);
                        e.Graphics.DrawString(str, normalFont, normalTextBrush, loc);
                    }
                }
            }
        }
    }
}
