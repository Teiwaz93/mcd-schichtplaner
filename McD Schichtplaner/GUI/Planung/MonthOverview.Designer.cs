﻿namespace McD_Schichtplaner.GUI.Planung
{
    partial class MonthOverview
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.mainOptionFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.currentMonth_Label = new System.Windows.Forms.Label();
            this.filter_ComboBox = new System.Windows.Forms.ComboBox();
            this.print_Button = new System.Windows.Forms.Button();
            this.send_Button = new System.Windows.Forms.Button();
            this.resetSwitch_Button = new System.Windows.Forms.Button();
            this.errors_Button = new System.Windows.Forms.Button();
            this.midTableSplitContainer = new System.Windows.Forms.SplitContainer();
            this.name_Label = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.workerTableFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.printDialog = new System.Windows.Forms.PrintDialog();
            this.printDocument = new System.Drawing.Printing.PrintDocument();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.overview_Button = new System.Windows.Forms.Button();
            this.back_Button = new System.Windows.Forms.Button();
            this.forward_Button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.mainOptionFlowLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.midTableSplitContainer)).BeginInit();
            this.midTableSplitContainer.Panel1.SuspendLayout();
            this.midTableSplitContainer.Panel2.SuspendLayout();
            this.midTableSplitContainer.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.BackColor = System.Drawing.SystemColors.Control;
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.mainSplitContainer.IsSplitterFixed = true;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mainSplitContainer.Name = "mainSplitContainer";
            this.mainSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.mainOptionFlowLayoutPanel);
            this.mainSplitContainer.Panel1MinSize = 10;
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.AutoScroll = true;
            this.mainSplitContainer.Panel2.AutoScrollMinSize = new System.Drawing.Size(1264, 0);
            this.mainSplitContainer.Panel2.Controls.Add(this.midTableSplitContainer);
            this.mainSplitContainer.Panel2MinSize = 10;
            this.mainSplitContainer.Size = new System.Drawing.Size(1264, 891);
            this.mainSplitContainer.TabIndex = 0;
            // 
            // mainOptionFlowLayoutPanel
            // 
            this.mainOptionFlowLayoutPanel.Controls.Add(this.back_Button);
            this.mainOptionFlowLayoutPanel.Controls.Add(this.currentMonth_Label);
            this.mainOptionFlowLayoutPanel.Controls.Add(this.forward_Button);
            this.mainOptionFlowLayoutPanel.Controls.Add(this.filter_ComboBox);
            this.mainOptionFlowLayoutPanel.Controls.Add(this.print_Button);
            this.mainOptionFlowLayoutPanel.Controls.Add(this.send_Button);
            this.mainOptionFlowLayoutPanel.Controls.Add(this.resetSwitch_Button);
            this.mainOptionFlowLayoutPanel.Controls.Add(this.errors_Button);
            this.mainOptionFlowLayoutPanel.Controls.Add(this.overview_Button);
            this.mainOptionFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainOptionFlowLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainOptionFlowLayoutPanel.Name = "mainOptionFlowLayoutPanel";
            this.mainOptionFlowLayoutPanel.Size = new System.Drawing.Size(1264, 50);
            this.mainOptionFlowLayoutPanel.TabIndex = 0;
            // 
            // currentMonth_Label
            // 
            this.currentMonth_Label.BackColor = System.Drawing.Color.White;
            this.currentMonth_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentMonth_Label.Location = new System.Drawing.Point(59, 5);
            this.currentMonth_Label.Margin = new System.Windows.Forms.Padding(5);
            this.currentMonth_Label.Name = "currentMonth_Label";
            this.currentMonth_Label.Size = new System.Drawing.Size(111, 40);
            this.currentMonth_Label.TabIndex = 1;
            this.currentMonth_Label.Text = "August 2019";
            this.currentMonth_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // filter_ComboBox
            // 
            this.filter_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.filter_ComboBox.FormattingEnabled = true;
            this.filter_ComboBox.Items.AddRange(new object[] {
            "Alles",
            "Crew",
            "McCafe",
            "Management"});
            this.filter_ComboBox.Location = new System.Drawing.Point(244, 15);
            this.filter_ComboBox.Margin = new System.Windows.Forms.Padding(15);
            this.filter_ComboBox.Name = "filter_ComboBox";
            this.filter_ComboBox.Size = new System.Drawing.Size(121, 21);
            this.filter_ComboBox.TabIndex = 5;
            this.filter_ComboBox.SelectedIndexChanged += new System.EventHandler(this.Filter_ComboBox_SelectedIndexChanged);
            // 
            // print_Button
            // 
            this.print_Button.Location = new System.Drawing.Point(395, 15);
            this.print_Button.Margin = new System.Windows.Forms.Padding(15);
            this.print_Button.Name = "print_Button";
            this.print_Button.Size = new System.Drawing.Size(75, 23);
            this.print_Button.TabIndex = 3;
            this.print_Button.Text = "drucken";
            this.print_Button.UseVisualStyleBackColor = true;
            this.print_Button.Click += new System.EventHandler(this.Print_Button_Click);
            // 
            // send_Button
            // 
            this.send_Button.Location = new System.Drawing.Point(500, 15);
            this.send_Button.Margin = new System.Windows.Forms.Padding(15);
            this.send_Button.Name = "send_Button";
            this.send_Button.Size = new System.Drawing.Size(75, 23);
            this.send_Button.TabIndex = 4;
            this.send_Button.Text = "Senden";
            this.send_Button.UseVisualStyleBackColor = true;
            this.send_Button.Click += new System.EventHandler(this.Send_Button_Click);
            // 
            // resetSwitch_Button
            // 
            this.resetSwitch_Button.AutoSize = true;
            this.resetSwitch_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resetSwitch_Button.Location = new System.Drawing.Point(605, 15);
            this.resetSwitch_Button.Margin = new System.Windows.Forms.Padding(15);
            this.resetSwitch_Button.Name = "resetSwitch_Button";
            this.resetSwitch_Button.Size = new System.Drawing.Size(131, 23);
            this.resetSwitch_Button.TabIndex = 6;
            this.resetSwitch_Button.Text = "Tauschen zurücksetzen";
            this.resetSwitch_Button.UseVisualStyleBackColor = true;
            this.resetSwitch_Button.Click += new System.EventHandler(this.ResetSwitch_Button_Click);
            // 
            // errors_Button
            // 
            this.errors_Button.AutoSize = true;
            this.errors_Button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.errors_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.errors_Button.Location = new System.Drawing.Point(766, 15);
            this.errors_Button.Margin = new System.Windows.Forms.Padding(15);
            this.errors_Button.Name = "errors_Button";
            this.errors_Button.Size = new System.Drawing.Size(131, 23);
            this.errors_Button.TabIndex = 7;
            this.errors_Button.Text = "??? Fehler";
            this.toolTip.SetToolTip(this.errors_Button, "Fehler noch unbekanmt");
            this.errors_Button.UseVisualStyleBackColor = false;
            this.errors_Button.Click += new System.EventHandler(this.errors_Button_Click);
            this.errors_Button.MouseEnter += new System.EventHandler(this.errors_Button_MouseEnter);
            // 
            // midTableSplitContainer
            // 
            this.midTableSplitContainer.BackColor = System.Drawing.Color.White;
            this.midTableSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.midTableSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.midTableSplitContainer.IsSplitterFixed = true;
            this.midTableSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.midTableSplitContainer.Name = "midTableSplitContainer";
            this.midTableSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // midTableSplitContainer.Panel1
            // 
            this.midTableSplitContainer.Panel1.Controls.Add(this.name_Label);
            this.midTableSplitContainer.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.midTableSplitContainer.Panel1MinSize = 35;
            // 
            // midTableSplitContainer.Panel2
            // 
            this.midTableSplitContainer.Panel2.AutoScroll = true;
            this.midTableSplitContainer.Panel2.Controls.Add(this.workerTableFlowLayoutPanel);
            this.midTableSplitContainer.Size = new System.Drawing.Size(1264, 837);
            this.midTableSplitContainer.SplitterDistance = 40;
            this.midTableSplitContainer.TabIndex = 0;
            // 
            // name_Label
            // 
            this.name_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_Label.Location = new System.Drawing.Point(0, 0);
            this.name_Label.Margin = new System.Windows.Forms.Padding(0);
            this.name_Label.Name = "name_Label";
            this.name_Label.Size = new System.Drawing.Size(300, 35);
            this.name_Label.TabIndex = 1;
            this.name_Label.Text = "Name";
            this.name_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel1.ColumnCount = 32;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.225806F));
            this.tableLayoutPanel1.Controls.Add(this.label17, 30, 0);
            this.tableLayoutPanel1.Controls.Add(this.label31, 29, 0);
            this.tableLayoutPanel1.Controls.Add(this.label30, 28, 0);
            this.tableLayoutPanel1.Controls.Add(this.label29, 27, 0);
            this.tableLayoutPanel1.Controls.Add(this.label28, 26, 0);
            this.tableLayoutPanel1.Controls.Add(this.label27, 25, 0);
            this.tableLayoutPanel1.Controls.Add(this.label26, 24, 0);
            this.tableLayoutPanel1.Controls.Add(this.label25, 23, 0);
            this.tableLayoutPanel1.Controls.Add(this.label24, 22, 0);
            this.tableLayoutPanel1.Controls.Add(this.label23, 21, 0);
            this.tableLayoutPanel1.Controls.Add(this.label22, 20, 0);
            this.tableLayoutPanel1.Controls.Add(this.label21, 19, 0);
            this.tableLayoutPanel1.Controls.Add(this.label20, 18, 0);
            this.tableLayoutPanel1.Controls.Add(this.label19, 17, 0);
            this.tableLayoutPanel1.Controls.Add(this.label18, 16, 0);
            this.tableLayoutPanel1.Controls.Add(this.label16, 15, 0);
            this.tableLayoutPanel1.Controls.Add(this.label15, 14, 0);
            this.tableLayoutPanel1.Controls.Add(this.label14, 13, 0);
            this.tableLayoutPanel1.Controls.Add(this.label13, 12, 0);
            this.tableLayoutPanel1.Controls.Add(this.label12, 11, 0);
            this.tableLayoutPanel1.Controls.Add(this.label10, 9, 0);
            this.tableLayoutPanel1.Controls.Add(this.label32, 31, 0);
            this.tableLayoutPanel1.Controls.Add(this.label11, 10, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.MaximumSize = new System.Drawing.Size(0, 35);
            this.tableLayoutPanel1.MinimumSize = new System.Drawing.Size(1264, 35);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(300, 0, 0, 0);
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1264, 35);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(1199, 2);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(20, 31);
            this.label17.TabIndex = 52;
            this.label17.Text = "30";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label17.Click += new System.EventHandler(this.OpenDay);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.LemonChiffon;
            this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(1171, 2);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(20, 31);
            this.label31.TabIndex = 51;
            this.label31.Text = "29";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label31.Click += new System.EventHandler(this.OpenDay);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(1143, 2);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(20, 31);
            this.label30.TabIndex = 50;
            this.label30.Text = "28";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label30.Click += new System.EventHandler(this.OpenDay);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.LemonChiffon;
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(1115, 2);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(20, 31);
            this.label29.TabIndex = 49;
            this.label29.Text = "27";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label29.Click += new System.EventHandler(this.OpenDay);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(1087, 2);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(20, 31);
            this.label28.TabIndex = 48;
            this.label28.Text = "26";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label28.Click += new System.EventHandler(this.OpenDay);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.LemonChiffon;
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(1059, 2);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(20, 31);
            this.label27.TabIndex = 47;
            this.label27.Text = "25";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label27.Click += new System.EventHandler(this.OpenDay);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(1031, 2);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(20, 31);
            this.label26.TabIndex = 46;
            this.label26.Text = "24";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label26.Click += new System.EventHandler(this.OpenDay);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.LemonChiffon;
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(1003, 2);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(20, 31);
            this.label25.TabIndex = 45;
            this.label25.Text = "23";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label25.Click += new System.EventHandler(this.OpenDay);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(975, 2);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(20, 31);
            this.label24.TabIndex = 44;
            this.label24.Text = "22";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label24.Click += new System.EventHandler(this.OpenDay);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.LemonChiffon;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(947, 2);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(20, 31);
            this.label23.TabIndex = 43;
            this.label23.Text = "21";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label23.Click += new System.EventHandler(this.OpenDay);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(919, 2);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(20, 31);
            this.label22.TabIndex = 42;
            this.label22.Text = "20";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label22.Click += new System.EventHandler(this.OpenDay);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.LemonChiffon;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(891, 2);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(20, 31);
            this.label21.TabIndex = 41;
            this.label21.Text = "19";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label21.Click += new System.EventHandler(this.OpenDay);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(863, 2);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(20, 31);
            this.label20.TabIndex = 40;
            this.label20.Text = "18";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label20.Click += new System.EventHandler(this.OpenDay);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.LemonChiffon;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(835, 2);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(20, 31);
            this.label19.TabIndex = 39;
            this.label19.Text = "17";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label19.Click += new System.EventHandler(this.OpenDay);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(807, 2);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(20, 31);
            this.label18.TabIndex = 38;
            this.label18.Text = "16";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label18.Click += new System.EventHandler(this.OpenDay);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.LemonChiffon;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(779, 2);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(20, 31);
            this.label16.TabIndex = 37;
            this.label16.Text = "15";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label16.Click += new System.EventHandler(this.OpenDay);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(751, 2);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(20, 31);
            this.label15.TabIndex = 36;
            this.label15.Text = "14";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label15.Click += new System.EventHandler(this.OpenDay);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.LemonChiffon;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(723, 2);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(20, 31);
            this.label14.TabIndex = 35;
            this.label14.Text = "13";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label14.Click += new System.EventHandler(this.OpenDay);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(695, 2);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(20, 31);
            this.label13.TabIndex = 34;
            this.label13.Text = "12";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label13.Click += new System.EventHandler(this.OpenDay);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.LemonChiffon;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(667, 2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(20, 31);
            this.label12.TabIndex = 33;
            this.label12.Text = "11";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label12.Click += new System.EventHandler(this.OpenDay);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.LemonChiffon;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(611, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(20, 31);
            this.label10.TabIndex = 32;
            this.label10.Text = "9";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label10.Click += new System.EventHandler(this.OpenDay);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.LemonChiffon;
            this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(1227, 2);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(32, 31);
            this.label32.TabIndex = 31;
            this.label32.Text = "31";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label32.Click += new System.EventHandler(this.OpenDay);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(639, 2);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(20, 31);
            this.label11.TabIndex = 10;
            this.label11.Text = "10";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label11.Click += new System.EventHandler(this.OpenDay);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(583, 2);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(20, 31);
            this.label9.TabIndex = 8;
            this.label9.Text = "8";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label9.Click += new System.EventHandler(this.OpenDay);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.LemonChiffon;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(555, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(20, 31);
            this.label8.TabIndex = 7;
            this.label8.Text = "7";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label8.Click += new System.EventHandler(this.OpenDay);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(527, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(20, 31);
            this.label7.TabIndex = 6;
            this.label7.Text = "6";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Click += new System.EventHandler(this.OpenDay);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.LemonChiffon;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(499, 2);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 31);
            this.label6.TabIndex = 5;
            this.label6.Text = "5";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label6.Click += new System.EventHandler(this.OpenDay);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(471, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(20, 31);
            this.label5.TabIndex = 4;
            this.label5.Text = "4";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label5.Click += new System.EventHandler(this.OpenDay);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.LemonChiffon;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(443, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(20, 31);
            this.label4.TabIndex = 3;
            this.label4.Text = "3";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label4.Click += new System.EventHandler(this.OpenDay);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(210)))), ((int)(((byte)(235)))), ((int)(((byte)(255)))));
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(415, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 31);
            this.label3.TabIndex = 2;
            this.label3.Text = "2";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Click += new System.EventHandler(this.OpenDay);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(305, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 31);
            this.label2.TabIndex = 1;
            this.label2.Text = "Stunden";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.LemonChiffon;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(387, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.OpenDay);
            // 
            // workerTableFlowLayoutPanel
            // 
            this.workerTableFlowLayoutPanel.AutoScroll = true;
            this.workerTableFlowLayoutPanel.AutoScrollMinSize = new System.Drawing.Size(-1, 0);
            this.workerTableFlowLayoutPanel.AutoSize = true;
            this.workerTableFlowLayoutPanel.BackColor = System.Drawing.Color.LightGray;
            this.workerTableFlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.workerTableFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.workerTableFlowLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.workerTableFlowLayoutPanel.Name = "workerTableFlowLayoutPanel";
            this.workerTableFlowLayoutPanel.Size = new System.Drawing.Size(1264, 793);
            this.workerTableFlowLayoutPanel.TabIndex = 1;
            this.workerTableFlowLayoutPanel.WrapContents = false;
            // 
            // printDialog
            // 
            this.printDialog.UseEXDialog = true;
            // 
            // printDocument
            // 
            this.printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument_PrintPage);
            // 
            // toolTip
            // 
            this.toolTip.AutomaticDelay = 0;
            this.toolTip.AutoPopDelay = 2147483;
            this.toolTip.InitialDelay = 0;
            this.toolTip.ReshowDelay = 0;
            // 
            // overview_Button
            // 
            this.overview_Button.Location = new System.Drawing.Point(927, 15);
            this.overview_Button.Margin = new System.Windows.Forms.Padding(15);
            this.overview_Button.Name = "overview_Button";
            this.overview_Button.Size = new System.Drawing.Size(75, 23);
            this.overview_Button.TabIndex = 8;
            this.overview_Button.Text = "Übersicht";
            this.overview_Button.UseVisualStyleBackColor = true;
            this.overview_Button.Click += new System.EventHandler(this.overview_Button_Click);
            // 
            // back_Button
            // 
            this.back_Button.BackgroundImage = global::McD_Schichtplaner.Properties.Resources.backward;
            this.back_Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.back_Button.Location = new System.Drawing.Point(3, 3);
            this.back_Button.Name = "back_Button";
            this.back_Button.Size = new System.Drawing.Size(48, 44);
            this.back_Button.TabIndex = 0;
            this.back_Button.UseVisualStyleBackColor = true;
            this.back_Button.Click += new System.EventHandler(this.Back_Button_Click);
            // 
            // forward_Button
            // 
            this.forward_Button.BackgroundImage = global::McD_Schichtplaner.Properties.Resources.forward;
            this.forward_Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.forward_Button.Location = new System.Drawing.Point(178, 3);
            this.forward_Button.Name = "forward_Button";
            this.forward_Button.Size = new System.Drawing.Size(48, 44);
            this.forward_Button.TabIndex = 2;
            this.forward_Button.UseVisualStyleBackColor = true;
            this.forward_Button.Click += new System.EventHandler(this.Forward_Button_Click);
            // 
            // MonthOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.mainSplitContainer);
            this.Name = "MonthOverview";
            this.Size = new System.Drawing.Size(1264, 891);
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.mainOptionFlowLayoutPanel.ResumeLayout(false);
            this.mainOptionFlowLayoutPanel.PerformLayout();
            this.midTableSplitContainer.Panel1.ResumeLayout(false);
            this.midTableSplitContainer.Panel2.ResumeLayout(false);
            this.midTableSplitContainer.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.midTableSplitContainer)).EndInit();
            this.midTableSplitContainer.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.FlowLayoutPanel mainOptionFlowLayoutPanel;
        private System.Windows.Forms.Button back_Button;
        private System.Windows.Forms.Label currentMonth_Label;
        private System.Windows.Forms.Button forward_Button;
        private System.Windows.Forms.Button print_Button;
        private System.Windows.Forms.PrintDialog printDialog;
        private System.Drawing.Printing.PrintDocument printDocument;
        private System.Windows.Forms.Button send_Button;
        private System.Windows.Forms.SplitContainer midTableSplitContainer;
        private System.Windows.Forms.Label name_Label;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel workerTableFlowLayoutPanel;
        private System.Windows.Forms.ComboBox filter_ComboBox;
        private System.Windows.Forms.Button resetSwitch_Button;
        private System.Windows.Forms.Button errors_Button;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.Button overview_Button;
    }
}
