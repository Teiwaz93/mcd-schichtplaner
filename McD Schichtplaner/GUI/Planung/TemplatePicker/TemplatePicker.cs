﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace McD_Schichtplaner.GUI.Planung.TemplatePicker
{
    public partial class TemplatePicker : Form
    {
        private TemplatePickerControl active;
        public TemplatePickerControl Active {
            get => active;
            internal set
            {
                accept_Button.Enabled = true;
                active?.Reset();
                active = value;
            }
        }

        public TemplatePicker()
        {
            InitializeComponent();
            Database.Database data = Database.Database.DB;
            var ids = data.GetAllTemplateIDs();
            foreach (var id in ids)
            {
                var control = new TemplatePickerControl(id, this);
                flowLayoutPanel.Controls.Add(control);
            }
        }

        private void Abort_Button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void Accept_Button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
