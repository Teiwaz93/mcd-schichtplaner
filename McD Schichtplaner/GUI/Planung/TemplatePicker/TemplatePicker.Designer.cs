﻿namespace McD_Schichtplaner.GUI.Planung.TemplatePicker
{
    partial class TemplatePicker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTablePanel = new System.Windows.Forms.TableLayoutPanel();
            this.instruction_Label = new System.Windows.Forms.Label();
            this.abort_Button = new System.Windows.Forms.Button();
            this.accept_Button = new System.Windows.Forms.Button();
            this.buttons_Panel = new System.Windows.Forms.Panel();
            this.flowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.mainTablePanel.SuspendLayout();
            this.buttons_Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTablePanel
            // 
            this.mainTablePanel.ColumnCount = 3;
            this.mainTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.mainTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.mainTablePanel.Controls.Add(this.instruction_Label, 1, 0);
            this.mainTablePanel.Controls.Add(this.buttons_Panel, 1, 2);
            this.mainTablePanel.Controls.Add(this.flowLayoutPanel, 1, 1);
            this.mainTablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTablePanel.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.mainTablePanel.Location = new System.Drawing.Point(0, 0);
            this.mainTablePanel.Name = "mainTablePanel";
            this.mainTablePanel.RowCount = 3;
            this.mainTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.mainTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.mainTablePanel.Size = new System.Drawing.Size(650, 811);
            this.mainTablePanel.TabIndex = 0;
            // 
            // instruction_Label
            // 
            this.instruction_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.instruction_Label.AutoSize = true;
            this.instruction_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instruction_Label.Location = new System.Drawing.Point(103, 55);
            this.instruction_Label.Name = "instruction_Label";
            this.instruction_Label.Size = new System.Drawing.Size(334, 20);
            this.instruction_Label.TabIndex = 1;
            this.instruction_Label.Text = "Eine Vorlage zum Anwenden auswählen:";
            // 
            // abort_Button
            // 
            this.abort_Button.BackgroundImage = global::McD_Schichtplaner.Properties.Resources.cancel;
            this.abort_Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.abort_Button.Dock = System.Windows.Forms.DockStyle.Left;
            this.abort_Button.Location = new System.Drawing.Point(0, 0);
            this.abort_Button.Name = "abort_Button";
            this.abort_Button.Size = new System.Drawing.Size(75, 44);
            this.abort_Button.TabIndex = 2;
            this.abort_Button.Text = "Abbrechen";
            this.abort_Button.UseVisualStyleBackColor = true;
            this.abort_Button.Click += new System.EventHandler(this.Abort_Button_Click);
            // 
            // accept_Button
            // 
            this.accept_Button.BackgroundImage = global::McD_Schichtplaner.Properties.Resources.accept;
            this.accept_Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.accept_Button.Dock = System.Windows.Forms.DockStyle.Right;
            this.accept_Button.Enabled = false;
            this.accept_Button.Location = new System.Drawing.Point(369, 0);
            this.accept_Button.Name = "accept_Button";
            this.accept_Button.Size = new System.Drawing.Size(75, 44);
            this.accept_Button.TabIndex = 3;
            this.accept_Button.Text = "OK";
            this.accept_Button.UseVisualStyleBackColor = true;
            this.accept_Button.Click += new System.EventHandler(this.Accept_Button_Click);
            // 
            // buttons_Panel
            // 
            this.buttons_Panel.Controls.Add(this.abort_Button);
            this.buttons_Panel.Controls.Add(this.accept_Button);
            this.buttons_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttons_Panel.Location = new System.Drawing.Point(103, 764);
            this.buttons_Panel.Name = "buttons_Panel";
            this.buttons_Panel.Size = new System.Drawing.Size(444, 44);
            this.buttons_Panel.TabIndex = 4;
            // 
            // flowLayoutPanel
            // 
            this.flowLayoutPanel.AutoScroll = true;
            this.flowLayoutPanel.BackColor = System.Drawing.Color.White;
            this.flowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel.Location = new System.Drawing.Point(103, 78);
            this.flowLayoutPanel.Name = "flowLayoutPanel";
            this.flowLayoutPanel.Size = new System.Drawing.Size(444, 680);
            this.flowLayoutPanel.TabIndex = 5;
            // 
            // TemplatePicker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 811);
            this.Controls.Add(this.mainTablePanel);
            this.Name = "TemplatePicker";
            this.Text = "TemplatePicker";
            this.mainTablePanel.ResumeLayout(false);
            this.mainTablePanel.PerformLayout();
            this.buttons_Panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTablePanel;
        private System.Windows.Forms.Label instruction_Label;
        private System.Windows.Forms.Button abort_Button;
        private System.Windows.Forms.Button accept_Button;
        private System.Windows.Forms.Panel buttons_Panel;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel;
    }
}