﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using McD_Schichtplaner.Database;

namespace McD_Schichtplaner.GUI.Planung.TemplatePicker
{
    public partial class TemplatePickerControl : UserControl
    {
        public long ID { get; }
        private TemplatePicker form;
        public List<TemplateShift> Shifts { get; }

        public TemplatePickerControl()
        {
            InitializeComponent();
        }

        public TemplatePickerControl(long id, TemplatePicker form)
        {
            InitializeComponent();
            ID = id;
            this.form = form;
            var data = Database.Database.DB;
            Shifts = data.GetTemplateShifts(ID);
            name_Label.Text = data.GetTemplateName(id);
            TimeSpan hours = new TimeSpan();
            foreach (var entry in Shifts)
            {
                hours += entry.Length;
            }
            hours_Label.Text = hours.Days * 24 + hours.Hours + "," + hours.Minutes / 6;
        }

        public void Clicked(object sender, EventArgs e)
        {
            form.Active = this;
            BackColor = Color.SkyBlue;
        }

        public void Reset()
        {
            BackColor = Color.White;
        }
    }

}
