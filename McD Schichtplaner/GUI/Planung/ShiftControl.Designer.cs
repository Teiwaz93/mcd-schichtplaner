﻿namespace McD_Schichtplaner.GUI.Planung
{
    partial class ShiftControl
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.shift_Table = new System.Windows.Forms.TableLayoutPanel();
            this.rest_Label = new System.Windows.Forms.Label();
            this.rest2_Label = new System.Windows.Forms.Label();
            this.rest1_Label = new System.Windows.Forms.Label();
            this.end_Label = new System.Windows.Forms.Label();
            this.start_Label = new System.Windows.Forms.Label();
            this.name_Label = new System.Windows.Forms.Label();
            this.shift_Table.SuspendLayout();
            this.SuspendLayout();
            // 
            // shift_Table
            // 
            this.shift_Table.ColumnCount = 3;
            this.shift_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.shift_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.shift_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.shift_Table.Controls.Add(this.rest_Label, 1, 1);
            this.shift_Table.Controls.Add(this.rest2_Label, 2, 1);
            this.shift_Table.Controls.Add(this.rest1_Label, 0, 1);
            this.shift_Table.Controls.Add(this.end_Label, 2, 0);
            this.shift_Table.Controls.Add(this.start_Label, 0, 0);
            this.shift_Table.Controls.Add(this.name_Label, 1, 0);
            this.shift_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shift_Table.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.shift_Table.Location = new System.Drawing.Point(0, 0);
            this.shift_Table.Name = "shift_Table";
            this.shift_Table.RowCount = 2;
            this.shift_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.shift_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.shift_Table.Size = new System.Drawing.Size(930, 50);
            this.shift_Table.TabIndex = 0;
            this.shift_Table.Click += new System.EventHandler(this.ShiftControl_Click);
            // 
            // rest_Label
            // 
            this.rest_Label.AutoSize = true;
            this.rest_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rest_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rest_Label.Location = new System.Drawing.Point(313, 25);
            this.rest_Label.Name = "rest_Label";
            this.rest_Label.Size = new System.Drawing.Size(304, 25);
            this.rest_Label.TabIndex = 6;
            this.rest_Label.Text = "Pause";
            this.rest_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rest_Label.Visible = false;
            this.rest_Label.Click += new System.EventHandler(this.ShiftControl_Click);
            // 
            // rest2_Label
            // 
            this.rest2_Label.AutoSize = true;
            this.rest2_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rest2_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rest2_Label.Location = new System.Drawing.Point(623, 25);
            this.rest2_Label.Name = "rest2_Label";
            this.rest2_Label.Size = new System.Drawing.Size(304, 25);
            this.rest2_Label.TabIndex = 5;
            this.rest2_Label.Text = "Notiz bzw Pause 2";
            this.rest2_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.rest2_Label.Visible = false;
            this.rest2_Label.Click += new System.EventHandler(this.ShiftControl_Click);
            // 
            // rest1_Label
            // 
            this.rest1_Label.AutoSize = true;
            this.rest1_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rest1_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rest1_Label.Location = new System.Drawing.Point(3, 25);
            this.rest1_Label.Name = "rest1_Label";
            this.rest1_Label.Size = new System.Drawing.Size(304, 25);
            this.rest1_Label.TabIndex = 3;
            this.rest1_Label.Text = "Pause";
            this.rest1_Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rest1_Label.Visible = false;
            this.rest1_Label.Click += new System.EventHandler(this.ShiftControl_Click);
            // 
            // end_Label
            // 
            this.end_Label.AutoSize = true;
            this.end_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.end_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.end_Label.Location = new System.Drawing.Point(623, 0);
            this.end_Label.Name = "end_Label";
            this.end_Label.Size = new System.Drawing.Size(304, 25);
            this.end_Label.TabIndex = 2;
            this.end_Label.Text = "Feierabend ! :)";
            this.end_Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.end_Label.Click += new System.EventHandler(this.ShiftControl_Click);
            // 
            // start_Label
            // 
            this.start_Label.AutoSize = true;
            this.start_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.start_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.start_Label.Location = new System.Drawing.Point(3, 0);
            this.start_Label.Name = "start_Label";
            this.start_Label.Size = new System.Drawing.Size(304, 25);
            this.start_Label.TabIndex = 1;
            this.start_Label.Text = "Startzeit";
            this.start_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.start_Label.Click += new System.EventHandler(this.ShiftControl_Click);
            // 
            // name_Label
            // 
            this.name_Label.AutoSize = true;
            this.name_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_Label.Location = new System.Drawing.Point(313, 0);
            this.name_Label.Name = "name_Label";
            this.name_Label.Size = new System.Drawing.Size(304, 25);
            this.name_Label.TabIndex = 0;
            this.name_Label.Text = "Robin Brümmer";
            this.name_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.name_Label.Click += new System.EventHandler(this.ShiftControl_Click);
            // 
            // ShiftControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Controls.Add(this.shift_Table);
            this.Name = "ShiftControl";
            this.Size = new System.Drawing.Size(930, 50);
            this.Click += new System.EventHandler(this.ShiftControl_Click);
            this.shift_Table.ResumeLayout(false);
            this.shift_Table.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel shift_Table;
        private System.Windows.Forms.Label rest2_Label;
        private System.Windows.Forms.Label rest1_Label;
        private System.Windows.Forms.Label end_Label;
        private System.Windows.Forms.Label start_Label;
        private System.Windows.Forms.Label name_Label;
        private System.Windows.Forms.Label rest_Label;
    }
}
