﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using McD_Schichtplaner.Database;

namespace McD_Schichtplaner.GUI.Planung
{
    public partial class MonthWorkerEntry : UserControl
    {
        private List<Shift> shifts;
        private List<Tuple<string, DateTime>> nonShifts;
        private TimeSpan prevMonth = new TimeSpan(0, 0, 0);
        private TimeSpan nextMonth = new TimeSpan(0, 0, 0);
        private Worker worker;
        private MonthOverview overview;
        private static Color[] colors = new Color[31];
        private Database.Database data;
        private static Tuple<Shift, Label, int, MonthWorkerEntry> switcher;
        private List<Error> errors;
        private DateTime lastEnd;
        private DateTime nextStart;

        public List<Error> Errors { get => errors; }

        public string WorkerMail => worker.Mail;
        public long Worker => worker.Id;
        public string WorkerName => worker.Name;
        public List<Shift> Shifts => shifts;
        public List<Tuple<string, DateTime>> NonShifts => nonShifts;
        public TimeSpan Hours
        {
            get
            {
                TimeSpan ts = new TimeSpan(0, 0, 0);
                foreach (var s in shifts)
                {
                    ts += s.Length;
                }
                foreach (var ns in nonShifts)
                {
                    ts += worker.GetHoursForNonShift(ns.Item1);
                }
                return ts;
            }
        }


        public MonthWorkerEntry()
        {
            InitializeComponent();
        }

        public MonthWorkerEntry(Worker w, MonthOverview o, Size size) : this()
        {
            overview = o;
            data = o.Data;
            Width = size.Width;
            MaximumSize = size;
            for (int i = 0; i < 31; i++)
            {
                var current = table.GetControlFromPosition(i + 1, 0);
                current.BackColor = overview.Colors[i];
                if (current.BackColor == Color.Black)
                {
                    current.Visible = false;
                }
            }

            worker = w;
            name_Label.Text = worker.Name;
            nonShifts = data.GetFrees(worker, overview.Year, Database.Database.MonthToInt(overview.Month));
            shifts = data.GetWorkerShiftsMonth(worker, overview.Month, overview.Year);

            foreach (var shift in shifts)
            {
                var label = table.GetControlFromPosition(shift.Start.Day, 0) as Label;
                if (label.Text != "X")
                {
                    //TODO: fallunterscheidung für urlaub/wunschfrei etc.
                    errors.Add(new Error(true, $"{worker} ist für zwei Schichten am {shift.Start.Date} eingeteilt."));
                    BackColor = Color.Red;
                    label.BackColor = Color.Red;
                }
                label.Text = shift.Label;
            }
            foreach (var date in nonShifts)
            {
                var label = table.GetControlFromPosition(date.Item2.Day, 0) as Label;
                label.Text = date.Item1;
            }

            //ARBEITSstunden in restwoche vor monatsbeginn
            DateTime day = new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), 1);
            if (day.DayOfWeek != DayOfWeek.Monday)
            {
                int end = DateTime.DaysInMonth(overview.Year, Database.Database.MonthToInt(overview.Month));
                day = day.AddDays(-1);
                DateTime dayEnd = day;
                int start = end;
                while (!(day.DayOfWeek == DayOfWeek.Monday))
                {
                    day = day.AddDays(-1);
                }
                var prevShifts = data.GetWorkerShiftsInterval(worker, day, dayEnd);
                foreach (var shift in prevShifts)
                {
                    if (shift.Start.Date == dayEnd.Date) lastEnd = shift.End;
                    else lastEnd = dayEnd;
                    prevMonth = prevMonth.Add(shift.Length);
                }
            }

            //arbeitsstunden in restwoche nach monatsende
            day = new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), DateTime.DaysInMonth(overview.Year, Database.Database.MonthToInt(overview.Month)));
            if (day.DayOfWeek != DayOfWeek.Sunday)
            {
                day = day.AddDays(1);
                var dayEnd = day;
                while (!(dayEnd.DayOfWeek == DayOfWeek.Sunday))
                {
                    dayEnd = dayEnd.AddDays(1);
                }
                var nextShifts = data.GetWorkerShiftsInterval(worker, day, dayEnd);
                foreach (var shift in nextShifts)
                {
                    if (shift.Start.Date == day) nextStart = shift.Start;
                    else nextStart = dayEnd;
                    nextMonth = nextMonth.Add(shift.Length);
                }
            }

            CalculateHours();
        }

        public void CalculateHours()
        {
            int h = Hours.Days * 24 + Hours.Hours;
            label0.Text = h + "," + Hours.Minutes / 6;
            if (worker.Status == Database.Database.Status.GV && h * worker.Wage > 450) BackColor = Color.Red;
            else if (worker.Status == Database.Database.Status.TZ && h > 80) BackColor = Color.Red;
            else if (worker.Status == Database.Database.Status.VZ && h > 210) BackColor = Color.Red;
            else if (worker.Cafe) BackColor = Color.LemonChiffon;
            else if (worker.Status == Database.Database.Status.Mgm) BackColor = Color.LightSkyBlue;
            else BackColor = Color.White;
            //CheckValidity();  --if not triggered in overview
            overview.CheckErrors();
        }

        public void CheckValidity()
        {
            shifts.Sort();
            errors = new List<Error>();
            bool errorFound = false;
            bool warningFound = false;
            /****************************************************************************************************************/
            /******************************************Check for weekhours***************************************************/
            /****************************************************************************************************************/
            TimeSpan[][] times = new TimeSpan[6][];
            for (int i = 0; i < 6; i++) times[i] = new TimeSpan[7];
            var prevEnd = lastEnd;
            var minFreeTime = new TimeSpan(12, 0, 0);
            foreach (var nonShift in nonShifts)
            {
                int d = nonShift.Item2.Day;
                switch (nonShift.Item2.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        d += 6;
                        break;
                    case DayOfWeek.Tuesday:
                        d += 5;
                        break;
                    case DayOfWeek.Wednesday:
                        d += 4;
                        break;
                    case DayOfWeek.Thursday:
                        d += 3;
                        break;
                    case DayOfWeek.Friday:
                        d += 2;
                        break;
                    case DayOfWeek.Saturday:
                        d += 1;
                        break;
                    case DayOfWeek.Sunday: break;
                }
                if (!(nonShift.Item2.DayOfWeek == DayOfWeek.Sunday)) times[d / 7][(int)nonShift.Item2.DayOfWeek - 1] = worker.GetHoursForNonShift(nonShift.Item1);
                else times[d / 7][6] = worker.GetHoursForNonShift(nonShift.Item1);
            }
            foreach (var shift in shifts)
            {
                if (shift.Start - prevEnd < minFreeTime)
                {
                    errorFound = true;
                    errors.Add(new Error(true, $"{WorkerName} hat am {shift.Start.Date.ToShortDateString()} nicht genügend Ruhezeit."));
                }
                prevEnd = shift.End;
                int d = shift.Start.Day;
                switch (shift.Start.DayOfWeek)
                {
                    case DayOfWeek.Monday:
                        d += 6;
                        break;
                    case DayOfWeek.Tuesday:
                        d += 5;
                        break;
                    case DayOfWeek.Wednesday:
                        d += 4;
                        break;
                    case DayOfWeek.Thursday:
                        d += 3;
                        break;
                    case DayOfWeek.Friday:
                        d += 2;
                        break;
                    case DayOfWeek.Saturday:
                        d += 1;
                        break;
                    case DayOfWeek.Sunday: break;
                }
                if (!(shift.Start.DayOfWeek == DayOfWeek.Sunday)) times[d / 7][(int)shift.Start.DayOfWeek - 1] = shift.Length;
                else times[d / 7][6] = shift.Length;
            }
            if (nextStart - prevEnd < minFreeTime && nextStart != DateTime.MinValue)
            {
                errorFound = true;
                errors.Add(new Error(true, $"{WorkerName} bekommt nach der letzten Schicht nicht genügend Ruhezeit (Monatswechsel)."));
            }

            int inRow = 0, inTotal = 0;
            TimeSpan weekhours = prevMonth;
            //erste woche _____________________________________________________________________________________________________
            foreach (var day in times[0])
            {
                if (day.Hours > 0)
                {
                    inRow++;
                    inTotal++;
                    if (inTotal > 5)
                    {
                        errorFound = true;
                        errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 0 mehr als 5 Tage."));
                    }
                    if (inRow > 7)
                    {
                        errorFound = true;
                        errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 0 mehr als 7 Tage am Stück."));
                    }
                }
                else inRow = 0;
                weekhours = weekhours.Add(day);
            }
            inTotal = 0;

            if (weekhours.Hours + weekhours.Days * 24 > worker.WeekHours)
            {
                errorFound = true;
                bool severe = false;
                switch (worker.Status)
                {
                    case Database.Database.Status.GV:
                        if (weekhours.Hours > 20) severe = true;
                        break;
                    case Database.Database.Status.TZ:
                        if (weekhours.Hours > 39) severe = true;
                        break;
                    case Database.Database.Status.VZ:
                    case Database.Database.Status.Mgm:
                        if (weekhours.Hours > 45) severe = true;
                        break;
                }
                errors.Add(new Error(severe, $"{WorkerName} überschreitet Wochenstunden in Woche 0 um {weekhours.Hours - worker.WeekHours}."));
            }
            //ende erste woche _________________________________________________________________________________________________________________

            /*#################################################################################################################################*/

            //zweite woche ______________________________________________________________________________________________________________________
            weekhours = new TimeSpan(0, 0, 0);
            foreach (var day in times[1])
            {
                if (day.Hours > 0)
                {
                    inRow++;
                    inTotal++;
                    if (inTotal > 5)
                    {
                        errorFound = true;
                        errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 1 mehr als 5 Tage."));
                    }
                    if (inRow > 7)
                    {
                        errorFound = true;
                        errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 1 mehr als 7 Tage am Stück."));
                    }
                }
                else inRow = 0;
                weekhours = weekhours.Add(day);
            }
            inTotal = 0;

            if (weekhours.Hours + weekhours.Days * 24 > worker.WeekHours)
            {
                errorFound = true;
                bool severe = false;
                switch (worker.Status)
                {
                    case Database.Database.Status.GV:
                        if (weekhours.Hours > 20) severe = true;
                        break;
                    case Database.Database.Status.TZ:
                        if (weekhours.Hours > 39) severe = true;
                        break;
                    case Database.Database.Status.VZ:
                    case Database.Database.Status.Mgm:
                        if (weekhours.Hours > 45) severe = true;
                        break;
                }
                errors.Add(new Error(severe, $"{WorkerName} überschreitet Wochenstunden in Woche 1 um {weekhours.Hours + weekhours.Days * 24 - worker.WeekHours}."));
            }
            //ende zweite woche ________________________________________________________________________________________________________________

            /*#################################################################################################################################*/

            //dritte woche ______________________________________________________________________________________________________________________
            weekhours = new TimeSpan(0, 0, 0);
            foreach (var day in times[2])
            {
                if (day.Hours > 0)
                {
                    inRow++;
                    inTotal++;
                    if (inTotal > 5)
                    {
                        errorFound = true;
                        errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 2 mehr als 5 Tage."));
                    }
                    if (inRow > 7)
                    {
                        errorFound = true;
                        errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 2 mehr als 7 Tage am Stück."));
                    }
                }
                else inRow = 0;
                weekhours = weekhours.Add(day);
            }
            inTotal = 0;

            if (weekhours.Hours + weekhours.Days * 24 > worker.WeekHours)
            {
                errorFound = true;
                bool severe = false;
                switch (worker.Status)
                {
                    case Database.Database.Status.GV:
                        if (weekhours.Hours > 20) severe = true;
                        break;
                    case Database.Database.Status.TZ:
                        if (weekhours.Hours > 39) severe = true;
                        break;
                    case Database.Database.Status.VZ:
                    case Database.Database.Status.Mgm:
                        if (weekhours.Hours > 45) severe = true;
                        break;
                }
                errors.Add(new Error(severe, $"{WorkerName} überschreitet Wochenstunden in Woche 2 um {weekhours.Hours + weekhours.Days * 24 - worker.WeekHours}."));
            }
            //ende dritte woche ________________________________________________________________________________________________________________

            /*#################################################################################################################################*/

            //vierte woche ______________________________________________________________________________________________________________________
            weekhours = new TimeSpan(0, 0, 0);
            foreach (var day in times[3])
            {
                if (day.Hours > 0)
                {
                    inRow++;
                    inTotal++;
                    if (inTotal > 5)
                    {
                        errorFound = true;
                        errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 3 mehr als 5 Tage."));
                    }
                    if (inRow > 7)
                    {
                        errorFound = true;
                        errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 3 mehr als 7 Tage am Stück."));
                    }
                }
                else inRow = 0;
                weekhours = weekhours.Add(day);
            }
            inTotal = 0;

            if (weekhours.Hours + weekhours.Days * 24 > worker.WeekHours)
            {
                errorFound = true;
                bool severe = false;
                switch (worker.Status)
                {
                    case Database.Database.Status.GV:
                        if (weekhours.Hours > 20) severe = true;
                        break;
                    case Database.Database.Status.TZ:
                        if (weekhours.Hours > 39) severe = true;
                        break;
                    case Database.Database.Status.VZ:
                    case Database.Database.Status.Mgm:
                        if (weekhours.Hours > 45) severe = true;
                        break;
                }
                errors.Add(new Error(severe, $"{WorkerName} überschreitet Wochenstunden in Woche 3 um {weekhours.Hours + weekhours.Days * 24 - worker.WeekHours}."));
            }
            //ende vierte woche ________________________________________________________________________________________________________________

            /*#################################################################################################################################*/


            //letzte woche finden (!) und auswerten
            int m = Database.Database.MonthToInt(overview.Month);
            var dow = new DateTime(overview.Year, m, 1).DayOfWeek;
            if (dow == DayOfWeek.Sunday && DateTime.DaysInMonth(overview.Year, m) >= 30
                || dow == DayOfWeek.Saturday && DateTime.DaysInMonth(overview.Year, m) >= 31)
            {
                //fünfte woche falls sechs wochen
                weekhours = new TimeSpan(0, 0, 0);
                foreach (var day in times[4])
                {
                    if (day.Hours > 0)
                    {
                        inRow++;
                        inTotal++;
                        if (inTotal > 5)
                        {
                            errorFound = true;
                            errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 4 mehr als 5 Tage."));
                        }
                        if (inRow > 7)
                        {
                            errorFound = true;
                            errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 4 mehr als 7 Tage am Stück."));
                        }
                    }
                    else inRow = 0;
                    weekhours = weekhours.Add(day);
                }
                inTotal = 0;

                if (weekhours.Hours + weekhours.Days * 24 > worker.WeekHours)
                {
                    errorFound = true;
                    bool severe = false;
                    switch (worker.Status)
                    {
                        case Database.Database.Status.GV:
                            if (weekhours.Hours > 20) severe = true;
                            break;
                        case Database.Database.Status.TZ:
                            if (weekhours.Hours > 39) severe = true;
                            break;
                        case Database.Database.Status.VZ:
                        case Database.Database.Status.Mgm:
                            if (weekhours.Hours > 45) severe = true;
                            break;
                    }
                    errors.Add(new Error(severe, $"{WorkerName} überschreitet Wochenstunden in Woche 4 um {weekhours.Hours + weekhours.Days * 24 - worker.WeekHours}."));
                }
                //ende fünfte woche && start sechste woche
                weekhours = nextMonth;
                foreach (var day in times[5])
                {
                    if (day.Hours > 0)
                    {
                        inRow++;
                        inTotal++;
                        if (inTotal > 5)
                        {
                            errorFound = true;
                            errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 5 mehr als 5 Tage."));
                        }
                        if (inRow > 7)
                        {
                            errorFound = true;
                            errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 5 mehr als 7 Tage am Stück."));
                        }
                    }
                    else inRow = 0;
                    weekhours = weekhours.Add(day);
                }
                inTotal = 0;

                if (weekhours.Hours + weekhours.Days * 24 > worker.WeekHours)
                {
                    errorFound = true;
                    bool severe = false;
                    switch (worker.Status)
                    {
                        case Database.Database.Status.GV:
                            if (weekhours.Hours > 20) severe = true;
                            break;
                        case Database.Database.Status.TZ:
                            if (weekhours.Hours > 39) severe = true;
                            break;
                        case Database.Database.Status.VZ:
                        case Database.Database.Status.Mgm:
                            if (weekhours.Hours > 45) severe = true;
                            break;
                    }
                    errors.Add(new Error(severe, $"{WorkerName} überschreitet Wochenstunden in Woche 5 um {weekhours.Hours + weekhours.Days * 24 - worker.WeekHours}."));
                }
            }
            else
            {
                //fünfte woche falls nur fünf wochen
                weekhours = nextMonth; foreach (var day in times[4])
                {
                    if (day.Hours > 0)
                    {
                        inRow++;
                        inTotal++;
                        if (inTotal > 5)
                        {
                            errorFound = true;
                            errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 4 mehr als 5 Tage."));
                        }
                        if (inRow > 7)
                        {
                            errorFound = true;
                            errors.Add(new Error(true, $"{WorkerName} arbeitet in Woche 4 mehr als 7 Tage am Stück."));
                        }
                    }
                    else inRow = 0;
                    weekhours = weekhours.Add(day);
                }
                inTotal = 0;

                if (weekhours.Hours + weekhours.Days * 24 > worker.WeekHours)
                {
                    errorFound = true;
                    bool severe = false;
                    switch (worker.Status)
                    {
                        case Database.Database.Status.GV:
                            if (weekhours.Hours > 20) severe = true;
                            break;
                        case Database.Database.Status.TZ:
                            if (weekhours.Hours > 39) severe = true;
                            break;
                        case Database.Database.Status.VZ:
                        case Database.Database.Status.Mgm:
                            if (weekhours.Hours > 45) severe = true;
                            break;
                    }
                    errors.Add(new Error(severe, $"{WorkerName} überschreitet Wochenstunden in Woche 4 um {weekhours.Hours + weekhours.Days * 24 - worker.WeekHours}."));
                }
            }


            /*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/


            /****************************************************************************************************************/
            /**************************************Check for minors**********************************************************/
            /****************************************************************************************************************/

            bool minorStart = worker.IsMinor(new DateTime(overview.Year, m, 1));
            bool minorEnd = worker.IsMinor(new DateTime(overview.Year, (int)overview.Month, DateTime.DaysInMonth(overview.Year, m)));
            //nicht minderjährig -> hier ignorieren
            if (!minorStart && !minorEnd) ;
            //wird während des monats volljährig -> jeden tag extra checken
            else if (minorStart && !minorEnd)
            {
                //TODO: das hier, jeden tag extra checken ob minderjährig
            }
            //minderjährig
            else
            {
                foreach (var shift in shifts)
                {
                    if (shift.Start.Hour < 6)
                    {
                        errors.Add(new Error(true, $"{WorkerName} ist minderjährig und fängt am {shift.Start.Date.ToShortDateString()} zu früh an."));
                        errorFound = true;
                    }

                    if (shift.End.Hour > 22)
                    {
                        errors.Add(new Error(true, $"{WorkerName} ist minderjährig und arbeitet am {shift.Start.Date.ToShortDateString()} zu lange."));
                        errorFound = true;
                    }

                    if (shift.Start.Date != shift.End.Date)
                    {
                        errors.Add(new Error(true, $"Um Gottes Willen !! Minderjähriger {WorkerName} macht am {shift.Start.Date.ToShortDateString()} eine Nachtschicht !!!"));
                        errorFound = true;
                    }
                }
            }


            /****************************************************************************************************************/
            /**************************************Check for availabilities**************************************************/
            /****************************************************************************************************************/

            var av = data.GetAvailabilities(worker);
            foreach(var shift in shifts)
            {
                int i = ((int)shift.Start.DayOfWeek + 6) % 7;
                if (shift.Start.TimeOfDay < av.Times[i].Item1) errors.Add(new Error(false, $"Mitarbeiter {worker.Name} beginnt seine Schicht außerhalb seiner Vefügungszeiten am {shift.Start.Date:dd.MM.yyyy}."));
                if (shift.End.TimeOfDay > av.Times[i].Item2) errors.Add(new Error(false, $"Mitarbeiter {worker.Name} beendet seine Schicht außerhalb seiner Vefügungszeiten am {shift.Start.Date:dd.MM.yyyy}."));
            }

            /*<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>*/


            if (warningFound) BackColor = Color.Yellow;
            else if (errorFound) BackColor = Color.Red;
            else BackColor = DefaultBackColor;
        }

        public string GetLabel(int x)
        {
            var label = table.GetControlFromPosition(x, 0) as Label;
            return label.Text;
        }

        private void Label_MouseClick(object sender, MouseEventArgs e)
        {
            var clicked = sender as Label;
            if (clicked.Text == "U" || clicked.Text == "W" || clicked.Text == "K" || clicked.Text == "S" || clicked.Text == "BS" || clicked.Text == "KUG") return;
            var pos = table.GetColumn(clicked);
            bool update = false;
            Shift old = null;
            foreach (var shift in shifts)
            {
                if (shift.Start.Date == new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos).Date)
                {
                    update = true;
                    old = shift;
                    break;
                }
            }
            ShiftForm sf;
            if (update) sf = new ShiftForm(new Shift(old));
            else sf = new ShiftForm(new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos), worker);
            sf.ShowDialog();
            if (sf.DialogResult == DialogResult.OK)
            {
                if (update)
                {
                    overview.UpdateShift(sf.Shift);
                    shifts.Remove(old);
                    if (old.WorkerID != sf.Shift.WorkerID)
                    {
                        foreach (var entry in overview.WorkerEntries)
                        {
                            if (entry.worker.Id == sf.Shift.WorkerID)
                            {
                                entry.shifts.Add(sf.Shift);
                                entry.table.GetControlFromPosition(sf.Shift.Start.Day, 0).Text = sf.Shift.Label;
                                entry.CalculateHours();
                                clicked.Text = "X";
                            }
                        }
                    }
                    else
                    {
                        shifts.Add(sf.Shift);
                        clicked.Text = sf.Shift.Label; 
                    }
                }
                else
                {
                    overview.CreateShift(sf.Shift);
                    shifts.Add(sf.Shift);
                    clicked.Text = sf.Shift.Label;
                }
            }
            else if (sf.DialogResult == DialogResult.No)
            {
                shifts.Remove(old);
                data.DeleteShift(old);
                clicked.Text = "X";
            }
            CalculateHours();
        }

        private void RemoveNonShift(Tuple<string, DateTime> t)
        {
            Tuple<string, DateTime> remover = new Tuple<string, DateTime>("", DateTime.Now);
            foreach (var entry in nonShifts)
            {
                if (entry.Item1 == t.Item1 && entry.Item2.Day == t.Item2.Day) remover = entry;
            }
            nonShifts.Remove(remover);
        }

        private void LöschenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = sender as ToolStripMenuItem;
            ContextMenuStrip ts = mi.GetCurrentParent() as ContextMenuStrip;
            Label clicked = ts.SourceControl as Label;
            var pos = table.GetColumn(clicked);


            switch (clicked.Text)
            {
                case "X": return;
                case "W":
                case "S":
                case "KUG":
                case "U":
                case "K":
                case "BS":
                    data.UnsetFree(worker, new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos));
                    RemoveNonShift(new Tuple<string, DateTime>(clicked.Text, new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos)));
                    break;
                default:
                    Shift old = null;
                    foreach (var shift in shifts)
                    {
                        if (shift.Start.Date == new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos).Date)
                        {
                            old = shift;
                            break;
                        }
                    }
                    if (!(old is null))
                    {
                        shifts.Remove(old);
                        data.DeleteShift(old);
                    }
                    break;

            }
            clicked.Text = "X";
            CalculateHours();
        }

        private void SonderSchichtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = sender as ToolStripMenuItem;
            ContextMenuStrip ts = mi.GetCurrentParent() as ContextMenuStrip;
            Label clicked = ts.SourceControl as Label;
            var pos = table.GetColumn(clicked);

            switch (clicked.Text)
            {
                case "S": return;
                case "X": break;
                case "W":
                case "KUG":
                case "K":
                case "U":
                case "BS":
                    data.UnsetFree(worker, new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos));
                    RemoveNonShift(new Tuple<string, DateTime>(clicked.Text, new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos)));
                    break;
                default:
                    Shift old = null;
                    foreach (var shift in shifts)
                    {
                        if (shift.Start.Date == new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos).Date)
                        {
                            old = shift;
                            break;
                        }
                    }
                    if (!(old is null))
                    {
                        shifts.Remove(old);
                        data.DeleteShift(old);
                    }
                    break;

            }
            data.SetFree(worker, new DateTime(overview.Year, (int)overview.Month, pos), "S");
            nonShifts.Add(new Tuple<string, DateTime>("S", new DateTime(overview.Year, (int)overview.Month, pos)));
            clicked.Text = "S";
            CalculateHours();
        }

        private void KrankToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = sender as ToolStripMenuItem;
            ContextMenuStrip ts = mi.GetCurrentParent() as ContextMenuStrip;
            Label clicked = ts.SourceControl as Label;
            var pos = table.GetColumn(clicked);

            switch (clicked.Text)
            {
                case "K": return;
                case "X": break;
                case "W":
                case "KUG":
                case "S":
                case "U":
                case "BS":
                    data.UnsetFree(worker, new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos));
                    RemoveNonShift(new Tuple<string, DateTime>(clicked.Text, new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos)));
                    break;
                default:
                    Shift old = null;
                    foreach (var shift in shifts)
                    {
                        if (shift.Start.Date == new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos).Date)
                        {
                            old = shift;
                            break;
                        }
                    }
                    if (!(old is null))
                    {
                        shifts.Remove(old);
                        data.DeleteShift(old);
                    }
                    break;

            }
            data.SetFree(worker, new DateTime(overview.Year, (int)overview.Month, pos), "K");
            nonShifts.Add(new Tuple<string, DateTime>("K", new DateTime(overview.Year, (int)overview.Month, pos)));
            clicked.Text = "K";
            CalculateHours();
        }

        private void BerufsschuleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = sender as ToolStripMenuItem;
            ContextMenuStrip ts = mi.GetCurrentParent() as ContextMenuStrip;
            Label clicked = ts.SourceControl as Label;
            var pos = table.GetColumn(clicked);

            switch (clicked.Text)
            {
                case "BS": return;
                case "X": break;
                case "W":
                case "KUG":
                case "S":
                case "U":
                case "K":
                    data.UnsetFree(worker, new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos));
                    RemoveNonShift(new Tuple<string, DateTime>(clicked.Text, new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos)));
                    break;
                default:
                    Shift old = null;
                    foreach (var shift in shifts)
                    {
                        if (shift.Start.Date == new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos).Date)
                        {
                            old = shift;
                            break;
                        }
                    }
                    if (!(old is null))
                    {
                        shifts.Remove(old);
                        data.DeleteShift(old);
                    }
                    break;

            }
            data.SetFree(worker, new DateTime(overview.Year, (int)overview.Month, pos), "BS");
            nonShifts.Add(new Tuple<string, DateTime>("BS", new DateTime(overview.Year, (int)overview.Month, pos)));
            clicked.Text = "BS";
            CalculateHours();
        }

        private void kurzarbeitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = sender as ToolStripMenuItem;
            ContextMenuStrip ts = mi.GetCurrentParent() as ContextMenuStrip;
            Label clicked = ts.SourceControl as Label;
            var pos = table.GetColumn(clicked);

            switch (clicked.Text)
            {
                case "KUG": return;
                case "X": break;
                case "BS":
                case "W":
                case "U":
                case "K":
                case "S":
                    data.UnsetFree(worker, new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos));
                    RemoveNonShift(new Tuple<string, DateTime>(clicked.Text, new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos)));
                    break;
                default:
                    Shift old = null;
                    foreach (var shift in shifts)
                    {
                        if (shift.Start.Date == new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos).Date)
                        {
                            old = shift;
                            break;
                        }
                    }
                    if (!(old is null))
                    {
                        shifts.Remove(old);
                        data.DeleteShift(old);
                    }
                    break;
            }
            data.SetFree(worker, new DateTime(overview.Year, (int)overview.Month, pos), "KUG");
            nonShifts.Add(new Tuple<string, DateTime>("KUG", new DateTime(overview.Year, (int)overview.Month, pos)));
            clicked.Text = "KUG";
            CalculateHours();
        }

        private void TauschenMitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem mi = sender as ToolStripMenuItem;
            ContextMenuStrip ts = mi.GetCurrentParent() as ContextMenuStrip;
            Label clicked = ts.SourceControl as Label;
            var pos = table.GetColumn(clicked);


            switch (clicked.Text)
            {
                case "X":
                    if (switcher is null) return;
                    var label = table.GetControlFromPosition(switcher.Item3, 0) as Label;
                    if (label.Text != "X")
                    {
                        var result = MessageBox.Show($"Führt dazu, dass {WorkerName} mehr als eine Schicht bekommt.\nFortfahren ?", "Zuviele Schichten", MessageBoxButtons.YesNo);
                        if (result == DialogResult.No)
                        { return; }
                    }
                    data.ChangeWorkerInShift(switcher.Item1, worker);

                    Label otherLabel = table.GetControlFromPosition(switcher.Item3, 0) as Label;
                    otherLabel.Text = switcher.Item2.Text;
                    switcher.Item2.Text = "X";

                    switcher.Item4.shifts.Remove(switcher.Item1);
                    shifts.Add(switcher.Item1);

                    DeleteSwitcher();

                    return;
                case "W": return;
                case "S": return;
                case "U": return;
                case "K": return;
                case "BS": return;
                default:
                    Shift s = null;
                    foreach (var shift in shifts)
                    {
                        if (shift.Start.Date == new DateTime(overview.Year, Database.Database.MonthToInt(overview.Month), pos).Date)
                        {
                            if (switcher is null)
                            {
                                switcher = new Tuple<Shift, Label, int, MonthWorkerEntry>(shift, clicked, pos, this);
                                switcher.Item2.BackColor = Color.Orange;
                                break;
                            }
                            else
                            {
                                Label switchLabel = switcher.Item4.table.GetControlFromPosition(pos, 0) as Label;
                                if (switchLabel.Text != "X")
                                {
                                    var res = MessageBox.Show($"Führt dazu, dass {switcher.Item4.WorkerName} mehr als eine Schicht bekommt.\nFortfahren ?", "Zuviele Schichten", MessageBoxButtons.YesNo);
                                    if (res == DialogResult.No) return;
                                }
                                otherLabel = table.GetControlFromPosition(switcher.Item3, 0) as Label;
                                if (otherLabel.Text != "X")
                                {
                                    var res = MessageBox.Show($"Führt dazu, dass {WorkerName} mehr als eine Schicht bekommt.\nFortfahren ?", "Zuviele Schichten", MessageBoxButtons.YesNo);
                                    if (res == DialogResult.No) return;
                                }
                                data.SwitchShiftWorkers(shift, switcher.Item1);
                                s = shift;

                                string tmp = otherLabel.Text;
                                otherLabel.Text = switcher.Item2.Text;
                                switcher.Item2.Text = tmp;

                                tmp = clicked.Text;
                                clicked.Text = switchLabel.Text;
                                switchLabel.Text = tmp;

                            }
                        }
                    }
                    if (!(s is null))
                    {
                        switcher.Item4.shifts.Remove(switcher.Item1);
                        switcher.Item4.shifts.Add(s);
                        shifts.Remove(s);
                        shifts.Add(switcher.Item1);
                        DeleteSwitcher();
                    }
                    break;

            }
            CalculateHours();
        }

        public static void DeleteSwitcher()
        {
            if (!(switcher is null)) switcher.Item2.BackColor = colors[switcher.Item3];
            switcher = null;
        }

        private void name_Label_Click(object sender, EventArgs e)
        {
            Mainwindow mw = overview.Parent.Parent.Parent.Parent as Mainwindow;
            mw.ChangePanel(new WorkerOverview(worker, new DateTime(overview.Year, (int)overview.Month, 1), overview.Size));
        }

        public WorkerOverview GetWorkerOverview()
        {
            return new WorkerOverview(worker, new DateTime(overview.Year, (int)overview.Month, 1), overview.Size);
        }
    }

}
