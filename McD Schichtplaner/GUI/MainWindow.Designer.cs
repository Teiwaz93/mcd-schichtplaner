﻿namespace McD_Schichtplaner
{
    partial class Mainwindow
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.topTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.vorlagen_Button = new System.Windows.Forms.Button();
            this.plan_Button = new System.Windows.Forms.Button();
            this.personal_Button = new System.Windows.Forms.Button();
            this.workPanel = new System.Windows.Forms.Panel();
            this.maintenance_Button = new System.Windows.Forms.Button();
            this.welcom_Picture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.topTableLayout.SuspendLayout();
            this.workPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.welcom_Picture)).BeginInit();
            this.SuspendLayout();
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.mainSplitContainer.IsSplitterFixed = true;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mainSplitContainer.Name = "mainSplitContainer";
            this.mainSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.topTableLayout);
            this.mainSplitContainer.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.workPanel);
            this.mainSplitContainer.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.mainSplitContainer.Size = new System.Drawing.Size(1264, 945);
            this.mainSplitContainer.TabIndex = 0;
            // 
            // topTableLayout
            // 
            this.topTableLayout.ColumnCount = 4;
            this.topTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.topTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.topTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.topTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.topTableLayout.Controls.Add(this.maintenance_Button, 3, 0);
            this.topTableLayout.Controls.Add(this.vorlagen_Button, 2, 0);
            this.topTableLayout.Controls.Add(this.plan_Button, 1, 0);
            this.topTableLayout.Controls.Add(this.personal_Button, 0, 0);
            this.topTableLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topTableLayout.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.topTableLayout.Location = new System.Drawing.Point(0, 0);
            this.topTableLayout.Name = "topTableLayout";
            this.topTableLayout.RowCount = 1;
            this.topTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.topTableLayout.Size = new System.Drawing.Size(1264, 50);
            this.topTableLayout.TabIndex = 0;
            // 
            // vorlagen_Button
            // 
            this.vorlagen_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vorlagen_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vorlagen_Button.Location = new System.Drawing.Point(813, 3);
            this.vorlagen_Button.Name = "vorlagen_Button";
            this.vorlagen_Button.Size = new System.Drawing.Size(399, 44);
            this.vorlagen_Button.TabIndex = 2;
            this.vorlagen_Button.Text = "Vorlagen";
            this.vorlagen_Button.UseVisualStyleBackColor = true;
            this.vorlagen_Button.Click += new System.EventHandler(this.ChangePanel);
            // 
            // plan_Button
            // 
            this.plan_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.plan_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plan_Button.Location = new System.Drawing.Point(408, 3);
            this.plan_Button.Name = "plan_Button";
            this.plan_Button.Size = new System.Drawing.Size(399, 44);
            this.plan_Button.TabIndex = 1;
            this.plan_Button.Text = "Monatspläne";
            this.plan_Button.UseVisualStyleBackColor = true;
            this.plan_Button.Click += new System.EventHandler(this.ChangePanel);
            // 
            // personal_Button
            // 
            this.personal_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.personal_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.personal_Button.Location = new System.Drawing.Point(3, 3);
            this.personal_Button.Name = "personal_Button";
            this.personal_Button.Size = new System.Drawing.Size(399, 44);
            this.personal_Button.TabIndex = 0;
            this.personal_Button.Text = "Personal";
            this.personal_Button.UseVisualStyleBackColor = true;
            this.personal_Button.Click += new System.EventHandler(this.ChangePanel);
            // 
            // workPanel
            // 
            this.workPanel.BackColor = System.Drawing.SystemColors.Control;
            this.workPanel.Controls.Add(this.welcom_Picture);
            this.workPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.workPanel.Location = new System.Drawing.Point(0, 0);
            this.workPanel.Name = "workPanel";
            this.workPanel.Size = new System.Drawing.Size(1264, 891);
            this.workPanel.TabIndex = 0;
            // 
            // maintenance_Button
            // 
            this.maintenance_Button.BackColor = System.Drawing.SystemColors.Control;
            this.maintenance_Button.BackgroundImage = global::McD_Schichtplaner.Properties.Resources.gear;
            this.maintenance_Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.maintenance_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.maintenance_Button.Location = new System.Drawing.Point(1218, 3);
            this.maintenance_Button.Name = "maintenance_Button";
            this.maintenance_Button.Size = new System.Drawing.Size(43, 44);
            this.maintenance_Button.TabIndex = 1;
            this.maintenance_Button.UseVisualStyleBackColor = false;
            this.maintenance_Button.Click += new System.EventHandler(this.ChangePanel);
            // 
            // welcom_Picture
            // 
            this.welcom_Picture.Image = global::McD_Schichtplaner.Properties.Resources.mcVIP;
            this.welcom_Picture.InitialImage = global::McD_Schichtplaner.Properties.Resources.mcVIP;
            this.welcom_Picture.Location = new System.Drawing.Point(402, 158);
            this.welcom_Picture.Name = "welcom_Picture";
            this.welcom_Picture.Size = new System.Drawing.Size(393, 129);
            this.welcom_Picture.TabIndex = 0;
            this.welcom_Picture.TabStop = false;
            // 
            // Mainwindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 945);
            this.Controls.Add(this.mainSplitContainer);
            this.Name = "Mainwindow";
            this.Text = "Robins Toller Schichtplaner";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Mainwindow_FormClosing);
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.topTableLayout.ResumeLayout(false);
            this.workPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.welcom_Picture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.TableLayoutPanel topTableLayout;
        private System.Windows.Forms.Button vorlagen_Button;
        private System.Windows.Forms.Button plan_Button;
        private System.Windows.Forms.Button personal_Button;
        private System.Windows.Forms.Panel workPanel;
        private System.Windows.Forms.Button maintenance_Button;
        private System.Windows.Forms.PictureBox welcom_Picture;
    }
}

