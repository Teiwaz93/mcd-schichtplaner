﻿namespace McD_Schichtplaner.GUI.Personal
{
    partial class PersonalMainPanel
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.listControlTablePanel = new System.Windows.Forms.TableLayoutPanel();
            this.add_Button = new System.Windows.Forms.Button();
            this.delete_Button = new System.Windows.Forms.Button();
            this.filterBox = new System.Windows.Forms.ComboBox();
            this.workerListFlowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.mainTableLayoutPanel.SuspendLayout();
            this.listControlTablePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainTableLayoutPanel
            // 
            this.mainTableLayoutPanel.BackColor = System.Drawing.Color.White;
            this.mainTableLayoutPanel.ColumnCount = 2;
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.mainTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.mainTableLayoutPanel.Controls.Add(this.listControlTablePanel, 0, 0);
            this.mainTableLayoutPanel.Controls.Add(this.workerListFlowPanel, 0, 1);
            this.mainTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTableLayoutPanel.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.mainTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.mainTableLayoutPanel.Name = "mainTableLayoutPanel";
            this.mainTableLayoutPanel.RowCount = 2;
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.mainTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.mainTableLayoutPanel.Size = new System.Drawing.Size(1264, 891);
            this.mainTableLayoutPanel.TabIndex = 0;
            // 
            // listControlTablePanel
            // 
            this.listControlTablePanel.BackColor = System.Drawing.Color.Green;
            this.listControlTablePanel.ColumnCount = 3;
            this.listControlTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.listControlTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.listControlTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.listControlTablePanel.Controls.Add(this.add_Button, 0, 0);
            this.listControlTablePanel.Controls.Add(this.delete_Button, 1, 0);
            this.listControlTablePanel.Controls.Add(this.filterBox, 2, 0);
            this.listControlTablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listControlTablePanel.Location = new System.Drawing.Point(3, 3);
            this.listControlTablePanel.Name = "listControlTablePanel";
            this.listControlTablePanel.RowCount = 1;
            this.listControlTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.listControlTablePanel.Size = new System.Drawing.Size(394, 69);
            this.listControlTablePanel.TabIndex = 0;
            // 
            // add_Button
            // 
            this.add_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.add_Button.BackColor = System.Drawing.Color.White;
            this.add_Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.add_Button.Location = new System.Drawing.Point(10, 23);
            this.add_Button.Margin = new System.Windows.Forms.Padding(10, 3, 10, 3);
            this.add_Button.Name = "add_Button";
            this.add_Button.Size = new System.Drawing.Size(78, 23);
            this.add_Button.TabIndex = 0;
            this.add_Button.Text = "Neu";
            this.add_Button.UseVisualStyleBackColor = false;
            this.add_Button.Click += new System.EventHandler(this.Add_Button_Click);
            // 
            // delete_Button
            // 
            this.delete_Button.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.delete_Button.BackColor = System.Drawing.Color.White;
            this.delete_Button.ForeColor = System.Drawing.Color.Red;
            this.delete_Button.Location = new System.Drawing.Point(108, 23);
            this.delete_Button.Margin = new System.Windows.Forms.Padding(10);
            this.delete_Button.Name = "delete_Button";
            this.delete_Button.Size = new System.Drawing.Size(78, 23);
            this.delete_Button.TabIndex = 2;
            this.delete_Button.Text = "Entfernen";
            this.delete_Button.UseVisualStyleBackColor = false;
            this.delete_Button.Click += new System.EventHandler(this.Delete_Button_Click);
            // 
            // filterBox
            // 
            this.filterBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.filterBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.filterBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.filterBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.filterBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.filterBox.FormattingEnabled = true;
            this.filterBox.ImeMode = System.Windows.Forms.ImeMode.On;
            this.filterBox.Items.AddRange(new object[] {
            "Alle",
            "Crew",
            "VZ",
            "TZ",
            "GV",
            "Management",
            "Minderjährig",
            "inaktiv"});
            this.filterBox.Location = new System.Drawing.Point(206, 24);
            this.filterBox.Margin = new System.Windows.Forms.Padding(10);
            this.filterBox.Name = "filterBox";
            this.filterBox.Size = new System.Drawing.Size(178, 21);
            this.filterBox.TabIndex = 3;
            this.filterBox.SelectedIndexChanged += new System.EventHandler(this.FilterBox_SelectedIndexChanged);
            // 
            // workerListFlowPanel
            // 
            this.workerListFlowPanel.AutoScroll = true;
            this.workerListFlowPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.workerListFlowPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.workerListFlowPanel.Location = new System.Drawing.Point(3, 78);
            this.workerListFlowPanel.Name = "workerListFlowPanel";
            this.workerListFlowPanel.Size = new System.Drawing.Size(394, 810);
            this.workerListFlowPanel.TabIndex = 1;
            // 
            // PersonalMainPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainTableLayoutPanel);
            this.Name = "PersonalMainPanel";
            this.Size = new System.Drawing.Size(1264, 891);
            this.mainTableLayoutPanel.ResumeLayout(false);
            this.listControlTablePanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainTableLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel listControlTablePanel;
        private System.Windows.Forms.Button delete_Button;
        private System.Windows.Forms.Button add_Button;
        private System.Windows.Forms.ComboBox filterBox;
        private System.Windows.Forms.FlowLayoutPanel workerListFlowPanel;
    }
}
