﻿using McD_Schichtplaner.Database;
using System;
using System.Globalization;
using System.Windows.Forms;
using static McD_Schichtplaner.Database.Database;

namespace McD_Schichtplaner.GUI.Personal
{
    /**
     * <summary>Stellt ein Fenster dar, in dem der Nutzer alle wichtige Informationen über einen Mitarbeiter angeben kann.</summary>
     */
    public partial class WorkerForm : Form
    {
        private Worker worker;

        public Worker Worker { get => worker; }

        /**
         * <summary>einfacher Konstruktor, für einen neuen Mitarbeiter</summary>
         */
        public WorkerForm()
        {
            InitializeComponent();
            donalds_RadioButton.Checked = true;
        }

        public WorkerForm(long id) : this()
        {
            id_Box.Value = id;
        }

        /**
         * <summary>Konstruktor, der die Felder mit den Informationen eines bereits bestehenden Mitarbeiter besetzt.</summary>
         */
        public WorkerForm(Worker w):this()
        {
            Name = w.Name;
            name_TextBox.Text = w.Name;
            id_Box.Value = w.Id;
            mail_TextBox.Text = w.Mail;
            wage_Box.Value = (decimal)w.Wage;
            switch (w.Status)
            {
                case Status.GV:
                    gv_RadioButton.Checked = true;
                    break;
                case Status.Mgm:
                    mgm_RadioButton.Checked = true;
                    break;
                case Status.TZ:
                    tz_RadioButton.Checked = true;
                    break;
                case Status.VZ:
                    vz_RadioButton.Checked = true;
                    break;
                default:
                    break;
            }
            birthday_Picker.Value = w.Birthday;
            if (w.Cafe) cafe_RadioButton.Checked = true;
        }

        /**
         * <summary>Methode, di emit dem Abbrechen Knopf verknüpft ist.<para/>
         */
        private void Abort_Button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }

        /**
         * <summary>methode, die mit dem OK Knopf verknüpft ist.<para/>
         * Checkt, ob alle Informationen den anforderungen entsprechen und setzt bei erfolg den dialogresult auch auf erfolg</summary>
         */
        private void Ok_Button_Click(object sender, EventArgs e)
        {
            if (!Database.Database.CheckMailValidity(mail_TextBox.Text) && mail_TextBox.Text != "")
            {
                MessageBox.Show("Inkorrekte Mail Addresse");
                return;
            }
            if (name_TextBox.Text == "")
            {
                MessageBox.Show("Kein Name angegeben.");
                return;
            }
            int id = (int)id_Box.Value;
            /*if (!Database.Database.WorkerIDFree(id))
            {
                MessageBox.Show("Nummer bereits vergeben.");
                return;
            }*/
            if (wage_Box.Text == "")
            {
                MessageBox.Show("Kein Stundenlohn angegeben.");
                return;
            }
            RadioButton check = null;
            foreach (var control in statusFlowLayoutPanel.Controls)
            {
                RadioButton rb = control as RadioButton;
                if (rb != null && rb.Checked)
                {
                    check = rb;
                }
            }
            if (check == null)
            {
                MessageBox.Show("Ein Status muss ausgewählt werden.");
                return;
            }
            Database.Database.Status status = Database.Database.StringToStatus(check.Text);
            if (birthday_Picker.Value == DateTime.Today)
            {
                MessageBox.Show("Geburtstag kann nicht heute sein.");
                return;
            }
            if (!noExpiration_Box.Checked && expiration_Picker.Value <= start_Picker.Value)
            {
                MessageBox.Show("Mitarbeiter kann nicht auf ein Datum befristet sein, das vor Arbeitsbeginn liegt.");
                return;
            }
            if (noExpiration_Box.Checked)
            {
                worker = new Worker(id, name_TextBox.Text, mail_TextBox.Text, Database.Database.StringToStatus(check.Text), birthday_Picker.Value, (double)wage_Box.Value, cafe_RadioButton.Checked, start_Picker.Value, (int)weekHours_Box.Value);
            }
            else
            {
                worker = new Worker(id, name_TextBox.Text, mail_TextBox.Text, Database.Database.StringToStatus(check.Text), birthday_Picker.Value, (double)wage_Box.Value, cafe_RadioButton.Checked, start_Picker.Value, expiration_Picker.Value, (int)weekHours_Box.Value);
            }
            DialogResult = DialogResult.OK;
        }

        private void NoExpiration_Box_CheckedChanged(object sender, EventArgs e)
        {
            if (noExpiration_Box.Checked) expiration_Picker.Enabled = false;
            else expiration_Picker.Enabled = true;
        }

        private void StatusChanged(object sender, EventArgs e)
        {
            RadioButton button = sender as RadioButton;
            if (button.Checked)
            {
                if (button.Text == "VZ" && weekHours_Box.Value < 39) weekHours_Box.Value = 39;
                else if (button.Text == "TZ" && weekHours_Box.Value > 38) weekHours_Box.Value = 38;
                else if (button.Text == "TZ" && weekHours_Box.Value < 16) weekHours_Box.Value = 16;
                else if (button.Text == "GV" && weekHours_Box.Value > 15) weekHours_Box.Value = 15;
            }
        }

        private void WeekHours_Box_ValueChanged(object sender, EventArgs e)
        {
            if (mgm_RadioButton.Checked) return;
            if (weekHours_Box.Value < 16) gv_RadioButton.Checked = true;
            else if (weekHours_Box.Value > 15 && weekHours_Box.Value < 39) tz_RadioButton.Checked = true;
            else if (weekHours_Box.Value > 38) vz_RadioButton.Checked = true;
        }
    }
}
