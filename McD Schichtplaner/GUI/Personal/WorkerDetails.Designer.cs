﻿namespace McD_Schichtplaner.GUI.Personal
{
    partial class WorkerDetails
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.details_Page = new System.Windows.Forms.TabPage();
            this.mainDetail_Table = new System.Windows.Forms.TableLayoutPanel();
            this.primaryDetails_Table = new System.Windows.Forms.TableLayoutPanel();
            this.name_Box = new System.Windows.Forms.TextBox();
            this.number_Label = new System.Windows.Forms.Label();
            this.mail_Label = new System.Windows.Forms.Label();
            this.name_Label = new System.Windows.Forms.Label();
            this.mail_Box = new System.Windows.Forms.TextBox();
            this.number_Box = new System.Windows.Forms.NumericUpDown();
            this.birthday_Flowlayout = new System.Windows.Forms.FlowLayoutPanel();
            this.birthday_Label = new System.Windows.Forms.Label();
            this.birthday_Picker = new System.Windows.Forms.DateTimePicker();
            this.stations_Label = new System.Windows.Forms.Label();
            this.stations_Table = new System.Windows.Forms.TableLayoutPanel();
            this.breakfast_CheckBox = new System.Windows.Forms.CheckBox();
            this.cafe_CheckBox = new System.Windows.Forms.CheckBox();
            this.service_CheckBox = new System.Windows.Forms.CheckBox();
            this.counter_CheckBox = new System.Windows.Forms.CheckBox();
            this.drive_CheckBox = new System.Windows.Forms.CheckBox();
            this.kitchen_CheckBox = new System.Windows.Forms.CheckBox();
            this.status_Table = new System.Windows.Forms.TableLayoutPanel();
            this.jnt_Button = new System.Windows.Forms.RadioButton();
            this.weekHours_Label = new System.Windows.Forms.Label();
            this.mgm_Button = new System.Windows.Forms.RadioButton();
            this.tz_Button = new System.Windows.Forms.RadioButton();
            this.vz_Button = new System.Windows.Forms.RadioButton();
            this.gv_Button = new System.Windows.Forms.RadioButton();
            this.gvs_Button = new System.Windows.Forms.RadioButton();
            this.weekHours_Box = new System.Windows.Forms.NumericUpDown();
            this.wage_Label = new System.Windows.Forms.Label();
            this.wage_Box = new System.Windows.Forms.NumericUpDown();
            this.contract_Panel = new System.Windows.Forms.Panel();
            this.expire_Picker = new System.Windows.Forms.DateTimePicker();
            this.contract_Picker = new System.Windows.Forms.DateTimePicker();
            this.withEnd_Button = new System.Windows.Forms.RadioButton();
            this.noEnd_Button = new System.Windows.Forms.RadioButton();
            this.begin_Label = new System.Windows.Forms.Label();
            this.cafe_Table = new System.Windows.Forms.TableLayoutPanel();
            this.cafe_Button = new System.Windows.Forms.RadioButton();
            this.donalds_Button = new System.Windows.Forms.RadioButton();
            this.holiday_free_Page = new System.Windows.Forms.TabPage();
            this.calendar_Split = new System.Windows.Forms.SplitContainer();
            this.selection_Table = new System.Windows.Forms.TableLayoutPanel();
            this.holiday_Button = new System.Windows.Forms.RadioButton();
            this.free_Button = new System.Windows.Forms.RadioButton();
            this.year_Selection = new System.Windows.Forms.NumericUpDown();
            this.calendar_Table = new System.Windows.Forms.TableLayoutPanel();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.december_Label = new System.Windows.Forms.Label();
            this.november_Label = new System.Windows.Forms.Label();
            this.october_Label = new System.Windows.Forms.Label();
            this.september_Label = new System.Windows.Forms.Label();
            this.august_Label = new System.Windows.Forms.Label();
            this.july_Label = new System.Windows.Forms.Label();
            this.june_Label = new System.Windows.Forms.Label();
            this.april_Label = new System.Windows.Forms.Label();
            this.march_Label = new System.Windows.Forms.Label();
            this.february_Label = new System.Windows.Forms.Label();
            this.january_Label = new System.Windows.Forms.Label();
            this.blank_Label = new System.Windows.Forms.Label();
            this.may_Label = new System.Windows.Forms.Label();
            this.availability_Page = new System.Windows.Forms.TabPage();
            this.av_Split = new System.Windows.Forms.SplitContainer();
            this.av_TopFlow = new System.Windows.Forms.FlowLayoutPanel();
            this.avSave_Button = new System.Windows.Forms.Button();
            this.availability_Table = new System.Windows.Forms.TableLayoutPanel();
            this.sundayEnd_Picker = new System.Windows.Forms.DateTimePicker();
            this.saturdayEnd_Picker = new System.Windows.Forms.DateTimePicker();
            this.thursdayEnd_Picker = new System.Windows.Forms.DateTimePicker();
            this.tuesdayEnd_Picker = new System.Windows.Forms.DateTimePicker();
            this.wednesdayEnd_Picker = new System.Windows.Forms.DateTimePicker();
            this.fridayEnd_Picker = new System.Windows.Forms.DateTimePicker();
            this.mondayEnd_Picker = new System.Windows.Forms.DateTimePicker();
            this.saturdayStart_Picker = new System.Windows.Forms.DateTimePicker();
            this.wednesdayStart_Picker = new System.Windows.Forms.DateTimePicker();
            this.tuesdayStart_Picker = new System.Windows.Forms.DateTimePicker();
            this.fridayStart_Picker = new System.Windows.Forms.DateTimePicker();
            this.thursdayStart_Picker = new System.Windows.Forms.DateTimePicker();
            this.sundayStart_Picker = new System.Windows.Forms.DateTimePicker();
            this.saturday_Label = new System.Windows.Forms.Label();
            this.friday_Label = new System.Windows.Forms.Label();
            this.thursday_Label = new System.Windows.Forms.Label();
            this.wednesday_Label = new System.Windows.Forms.Label();
            this.tuesday_Label = new System.Windows.Forms.Label();
            this.mondayStart_Picker = new System.Windows.Forms.DateTimePicker();
            this.monday_Label = new System.Windows.Forms.Label();
            this.sunday_Label = new System.Windows.Forms.Label();
            this.monday_PictureBox = new System.Windows.Forms.PictureBox();
            this.tuesday_PictureBox = new System.Windows.Forms.PictureBox();
            this.wednesday_PictureBox = new System.Windows.Forms.PictureBox();
            this.thursday_PictureBox = new System.Windows.Forms.PictureBox();
            this.friday_PictureBox = new System.Windows.Forms.PictureBox();
            this.saturday_PictureBox = new System.Windows.Forms.PictureBox();
            this.sunday_PictureBox = new System.Windows.Forms.PictureBox();
            this.tabControl.SuspendLayout();
            this.details_Page.SuspendLayout();
            this.mainDetail_Table.SuspendLayout();
            this.primaryDetails_Table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.number_Box)).BeginInit();
            this.birthday_Flowlayout.SuspendLayout();
            this.stations_Table.SuspendLayout();
            this.status_Table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weekHours_Box)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wage_Box)).BeginInit();
            this.contract_Panel.SuspendLayout();
            this.cafe_Table.SuspendLayout();
            this.holiday_free_Page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.calendar_Split)).BeginInit();
            this.calendar_Split.Panel1.SuspendLayout();
            this.calendar_Split.Panel2.SuspendLayout();
            this.calendar_Split.SuspendLayout();
            this.selection_Table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.year_Selection)).BeginInit();
            this.calendar_Table.SuspendLayout();
            this.availability_Page.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.av_Split)).BeginInit();
            this.av_Split.Panel1.SuspendLayout();
            this.av_Split.Panel2.SuspendLayout();
            this.av_Split.SuspendLayout();
            this.av_TopFlow.SuspendLayout();
            this.availability_Table.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.monday_PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuesday_PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wednesday_PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.thursday_PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.friday_PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.saturday_PictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sunday_PictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.details_Page);
            this.tabControl.Controls.Add(this.holiday_free_Page);
            this.tabControl.Controls.Add(this.availability_Page);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 0);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(800, 800);
            this.tabControl.TabIndex = 0;
            // 
            // details_Page
            // 
            this.details_Page.AutoScroll = true;
            this.details_Page.Controls.Add(this.mainDetail_Table);
            this.details_Page.Location = new System.Drawing.Point(4, 22);
            this.details_Page.Name = "details_Page";
            this.details_Page.Size = new System.Drawing.Size(792, 774);
            this.details_Page.TabIndex = 2;
            this.details_Page.Text = "Details";
            this.details_Page.UseVisualStyleBackColor = true;
            // 
            // mainDetail_Table
            // 
            this.mainDetail_Table.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.mainDetail_Table.ColumnCount = 1;
            this.mainDetail_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainDetail_Table.Controls.Add(this.primaryDetails_Table, 0, 0);
            this.mainDetail_Table.Controls.Add(this.birthday_Flowlayout, 0, 1);
            this.mainDetail_Table.Controls.Add(this.status_Table, 0, 2);
            this.mainDetail_Table.Controls.Add(this.contract_Panel, 0, 3);
            this.mainDetail_Table.Controls.Add(this.cafe_Table, 0, 4);
            this.mainDetail_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDetail_Table.Location = new System.Drawing.Point(0, 0);
            this.mainDetail_Table.Name = "mainDetail_Table";
            this.mainDetail_Table.RowCount = 5;
            this.mainDetail_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.3351F));
            this.mainDetail_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.mainDetail_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33175F));
            this.mainDetail_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 180F));
            this.mainDetail_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33314F));
            this.mainDetail_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainDetail_Table.Size = new System.Drawing.Size(792, 774);
            this.mainDetail_Table.TabIndex = 0;
            // 
            // primaryDetails_Table
            // 
            this.primaryDetails_Table.ColumnCount = 3;
            this.primaryDetails_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.primaryDetails_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.primaryDetails_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.primaryDetails_Table.Controls.Add(this.name_Box, 0, 1);
            this.primaryDetails_Table.Controls.Add(this.number_Label, 2, 0);
            this.primaryDetails_Table.Controls.Add(this.mail_Label, 1, 0);
            this.primaryDetails_Table.Controls.Add(this.name_Label, 0, 0);
            this.primaryDetails_Table.Controls.Add(this.mail_Box, 1, 1);
            this.primaryDetails_Table.Controls.Add(this.number_Box, 2, 1);
            this.primaryDetails_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.primaryDetails_Table.Location = new System.Drawing.Point(4, 4);
            this.primaryDetails_Table.Name = "primaryDetails_Table";
            this.primaryDetails_Table.RowCount = 2;
            this.primaryDetails_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.primaryDetails_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.primaryDetails_Table.Size = new System.Drawing.Size(784, 130);
            this.primaryDetails_Table.TabIndex = 0;
            // 
            // name_Box
            // 
            this.name_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_Box.Location = new System.Drawing.Point(10, 75);
            this.name_Box.Margin = new System.Windows.Forms.Padding(10);
            this.name_Box.Name = "name_Box";
            this.name_Box.Size = new System.Drawing.Size(174, 26);
            this.name_Box.TabIndex = 0;
            this.name_Box.Click += new System.EventHandler(this.input_Selected);
            this.name_Box.Enter += new System.EventHandler(this.input_Selected);
            // 
            // number_Label
            // 
            this.number_Label.AutoSize = true;
            this.number_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.number_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.number_Label.Location = new System.Drawing.Point(525, 0);
            this.number_Label.Name = "number_Label";
            this.number_Label.Size = new System.Drawing.Size(256, 65);
            this.number_Label.TabIndex = 2;
            this.number_Label.Text = "Nummer:";
            this.number_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mail_Label
            // 
            this.mail_Label.AutoSize = true;
            this.mail_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mail_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mail_Label.Location = new System.Drawing.Point(264, 0);
            this.mail_Label.Name = "mail_Label";
            this.mail_Label.Size = new System.Drawing.Size(255, 65);
            this.mail_Label.TabIndex = 1;
            this.mail_Label.Text = "Mail:";
            this.mail_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // name_Label
            // 
            this.name_Label.AutoSize = true;
            this.name_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_Label.Location = new System.Drawing.Point(3, 0);
            this.name_Label.Name = "name_Label";
            this.name_Label.Size = new System.Drawing.Size(255, 65);
            this.name_Label.TabIndex = 0;
            this.name_Label.Text = "Name:";
            this.name_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mail_Box
            // 
            this.mail_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mail_Box.Location = new System.Drawing.Point(271, 75);
            this.mail_Box.Margin = new System.Windows.Forms.Padding(10);
            this.mail_Box.Name = "mail_Box";
            this.mail_Box.Size = new System.Drawing.Size(174, 26);
            this.mail_Box.TabIndex = 1;
            this.mail_Box.Click += new System.EventHandler(this.input_Selected);
            this.mail_Box.Enter += new System.EventHandler(this.input_Selected);
            // 
            // number_Box
            // 
            this.number_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.number_Box.Location = new System.Drawing.Point(532, 75);
            this.number_Box.Margin = new System.Windows.Forms.Padding(10);
            this.number_Box.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.number_Box.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.number_Box.Name = "number_Box";
            this.number_Box.Size = new System.Drawing.Size(176, 26);
            this.number_Box.TabIndex = 2;
            this.number_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.number_Box.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.number_Box.Click += new System.EventHandler(this.input_Selected);
            this.number_Box.Enter += new System.EventHandler(this.input_Selected);
            // 
            // birthday_Flowlayout
            // 
            this.birthday_Flowlayout.Controls.Add(this.birthday_Label);
            this.birthday_Flowlayout.Controls.Add(this.birthday_Picker);
            this.birthday_Flowlayout.Controls.Add(this.stations_Label);
            this.birthday_Flowlayout.Controls.Add(this.stations_Table);
            this.birthday_Flowlayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.birthday_Flowlayout.Location = new System.Drawing.Point(4, 141);
            this.birthday_Flowlayout.Name = "birthday_Flowlayout";
            this.birthday_Flowlayout.Size = new System.Drawing.Size(784, 174);
            this.birthday_Flowlayout.TabIndex = 1;
            // 
            // birthday_Label
            // 
            this.birthday_Label.AutoSize = true;
            this.birthday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.birthday_Label.Location = new System.Drawing.Point(3, 80);
            this.birthday_Label.Margin = new System.Windows.Forms.Padding(3, 80, 3, 80);
            this.birthday_Label.Name = "birthday_Label";
            this.birthday_Label.Size = new System.Drawing.Size(94, 20);
            this.birthday_Label.TabIndex = 0;
            this.birthday_Label.Text = "Geburtstag:";
            // 
            // birthday_Picker
            // 
            this.birthday_Picker.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.birthday_Picker.Location = new System.Drawing.Point(100, 80);
            this.birthday_Picker.Margin = new System.Windows.Forms.Padding(0, 80, 33, 80);
            this.birthday_Picker.Name = "birthday_Picker";
            this.birthday_Picker.Size = new System.Drawing.Size(200, 20);
            this.birthday_Picker.TabIndex = 0;
            this.birthday_Picker.Value = new System.DateTime(2020, 3, 31, 17, 13, 48, 0);
            // 
            // stations_Label
            // 
            this.stations_Label.AutoSize = true;
            this.stations_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stations_Label.Location = new System.Drawing.Point(336, 80);
            this.stations_Label.Margin = new System.Windows.Forms.Padding(3, 80, 3, 80);
            this.stations_Label.Name = "stations_Label";
            this.stations_Label.Size = new System.Drawing.Size(82, 20);
            this.stations_Label.TabIndex = 3;
            this.stations_Label.Text = "Stationen:";
            this.stations_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // stations_Table
            // 
            this.stations_Table.ColumnCount = 1;
            this.stations_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.stations_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.stations_Table.Controls.Add(this.breakfast_CheckBox, 0, 5);
            this.stations_Table.Controls.Add(this.cafe_CheckBox, 0, 4);
            this.stations_Table.Controls.Add(this.service_CheckBox, 0, 3);
            this.stations_Table.Controls.Add(this.counter_CheckBox, 0, 2);
            this.stations_Table.Controls.Add(this.drive_CheckBox, 0, 1);
            this.stations_Table.Controls.Add(this.kitchen_CheckBox, 0, 0);
            this.stations_Table.Location = new System.Drawing.Point(421, 3);
            this.stations_Table.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.stations_Table.Name = "stations_Table";
            this.stations_Table.RowCount = 6;
            this.stations_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.stations_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.stations_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.stations_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.stations_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.stations_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.stations_Table.Size = new System.Drawing.Size(360, 168);
            this.stations_Table.TabIndex = 2;
            // 
            // breakfast_CheckBox
            // 
            this.breakfast_CheckBox.AutoSize = true;
            this.breakfast_CheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.breakfast_CheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.breakfast_CheckBox.Location = new System.Drawing.Point(3, 138);
            this.breakfast_CheckBox.Name = "breakfast_CheckBox";
            this.breakfast_CheckBox.Size = new System.Drawing.Size(354, 27);
            this.breakfast_CheckBox.TabIndex = 5;
            this.breakfast_CheckBox.Text = "Frühstück";
            this.breakfast_CheckBox.UseVisualStyleBackColor = true;
            // 
            // cafe_CheckBox
            // 
            this.cafe_CheckBox.AutoSize = true;
            this.cafe_CheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cafe_CheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cafe_CheckBox.Location = new System.Drawing.Point(3, 111);
            this.cafe_CheckBox.Name = "cafe_CheckBox";
            this.cafe_CheckBox.Size = new System.Drawing.Size(354, 21);
            this.cafe_CheckBox.TabIndex = 4;
            this.cafe_CheckBox.Text = "McCafe";
            this.cafe_CheckBox.UseVisualStyleBackColor = true;
            // 
            // service_CheckBox
            // 
            this.service_CheckBox.AutoSize = true;
            this.service_CheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.service_CheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.service_CheckBox.Location = new System.Drawing.Point(3, 84);
            this.service_CheckBox.Name = "service_CheckBox";
            this.service_CheckBox.Size = new System.Drawing.Size(354, 21);
            this.service_CheckBox.TabIndex = 3;
            this.service_CheckBox.Text = "Service/Lobby";
            this.service_CheckBox.UseVisualStyleBackColor = true;
            // 
            // counter_CheckBox
            // 
            this.counter_CheckBox.AutoSize = true;
            this.counter_CheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.counter_CheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.counter_CheckBox.Location = new System.Drawing.Point(3, 57);
            this.counter_CheckBox.Name = "counter_CheckBox";
            this.counter_CheckBox.Size = new System.Drawing.Size(354, 21);
            this.counter_CheckBox.TabIndex = 2;
            this.counter_CheckBox.Text = "Theke/Kasse";
            this.counter_CheckBox.UseVisualStyleBackColor = true;
            // 
            // drive_CheckBox
            // 
            this.drive_CheckBox.AutoSize = true;
            this.drive_CheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.drive_CheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drive_CheckBox.Location = new System.Drawing.Point(3, 30);
            this.drive_CheckBox.Name = "drive_CheckBox";
            this.drive_CheckBox.Size = new System.Drawing.Size(354, 21);
            this.drive_CheckBox.TabIndex = 1;
            this.drive_CheckBox.Text = "McDrive";
            this.drive_CheckBox.UseVisualStyleBackColor = true;
            // 
            // kitchen_CheckBox
            // 
            this.kitchen_CheckBox.AutoSize = true;
            this.kitchen_CheckBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kitchen_CheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kitchen_CheckBox.Location = new System.Drawing.Point(3, 3);
            this.kitchen_CheckBox.Name = "kitchen_CheckBox";
            this.kitchen_CheckBox.Size = new System.Drawing.Size(354, 21);
            this.kitchen_CheckBox.TabIndex = 0;
            this.kitchen_CheckBox.Text = "Küche";
            this.kitchen_CheckBox.UseVisualStyleBackColor = true;
            // 
            // status_Table
            // 
            this.status_Table.ColumnCount = 6;
            this.status_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00034F));
            this.status_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00036F));
            this.status_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.49986F));
            this.status_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.49986F));
            this.status_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.49979F));
            this.status_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.49979F));
            this.status_Table.Controls.Add(this.jnt_Button, 5, 0);
            this.status_Table.Controls.Add(this.weekHours_Label, 0, 1);
            this.status_Table.Controls.Add(this.mgm_Button, 4, 0);
            this.status_Table.Controls.Add(this.tz_Button, 1, 0);
            this.status_Table.Controls.Add(this.vz_Button, 0, 0);
            this.status_Table.Controls.Add(this.gv_Button, 2, 0);
            this.status_Table.Controls.Add(this.gvs_Button, 3, 0);
            this.status_Table.Controls.Add(this.weekHours_Box, 1, 1);
            this.status_Table.Controls.Add(this.wage_Label, 2, 1);
            this.status_Table.Controls.Add(this.wage_Box, 4, 1);
            this.status_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.status_Table.Location = new System.Drawing.Point(4, 322);
            this.status_Table.Name = "status_Table";
            this.status_Table.RowCount = 2;
            this.status_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.status_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.status_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.status_Table.Size = new System.Drawing.Size(784, 129);
            this.status_Table.TabIndex = 2;
            // 
            // jnt_Button
            // 
            this.jnt_Button.AutoSize = true;
            this.jnt_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.jnt_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jnt_Button.Location = new System.Drawing.Point(686, 3);
            this.jnt_Button.Name = "jnt_Button";
            this.jnt_Button.Size = new System.Drawing.Size(95, 58);
            this.jnt_Button.TabIndex = 6;
            this.jnt_Button.TabStop = true;
            this.jnt_Button.Text = "Hausmeister";
            this.jnt_Button.UseVisualStyleBackColor = true;
            this.jnt_Button.CheckedChanged += new System.EventHandler(this.StatusCheckChanged);
            // 
            // weekHours_Label
            // 
            this.weekHours_Label.AutoSize = true;
            this.weekHours_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.weekHours_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weekHours_Label.Location = new System.Drawing.Point(3, 64);
            this.weekHours_Label.Name = "weekHours_Label";
            this.weekHours_Label.Size = new System.Drawing.Size(190, 65);
            this.weekHours_Label.TabIndex = 6;
            this.weekHours_Label.Text = "Wochenstunden:";
            this.weekHours_Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // mgm_Button
            // 
            this.mgm_Button.AutoSize = true;
            this.mgm_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mgm_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mgm_Button.Location = new System.Drawing.Point(589, 3);
            this.mgm_Button.Name = "mgm_Button";
            this.mgm_Button.Size = new System.Drawing.Size(91, 58);
            this.mgm_Button.TabIndex = 5;
            this.mgm_Button.TabStop = true;
            this.mgm_Button.Text = "Management";
            this.mgm_Button.UseVisualStyleBackColor = true;
            this.mgm_Button.CheckedChanged += new System.EventHandler(this.StatusCheckChanged);
            // 
            // tz_Button
            // 
            this.tz_Button.AutoSize = true;
            this.tz_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tz_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tz_Button.Location = new System.Drawing.Point(199, 3);
            this.tz_Button.Name = "tz_Button";
            this.tz_Button.Size = new System.Drawing.Size(190, 58);
            this.tz_Button.TabIndex = 2;
            this.tz_Button.TabStop = true;
            this.tz_Button.Text = "Teilzeit (TZ)";
            this.tz_Button.UseVisualStyleBackColor = true;
            this.tz_Button.CheckedChanged += new System.EventHandler(this.StatusCheckChanged);
            // 
            // vz_Button
            // 
            this.vz_Button.AutoSize = true;
            this.vz_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vz_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vz_Button.Location = new System.Drawing.Point(3, 3);
            this.vz_Button.Name = "vz_Button";
            this.vz_Button.Size = new System.Drawing.Size(190, 58);
            this.vz_Button.TabIndex = 1;
            this.vz_Button.TabStop = true;
            this.vz_Button.Text = "Vollzeit (VZ)";
            this.vz_Button.UseVisualStyleBackColor = true;
            this.vz_Button.CheckedChanged += new System.EventHandler(this.StatusCheckChanged);
            // 
            // gv_Button
            // 
            this.gv_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gv_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gv_Button.Location = new System.Drawing.Point(395, 3);
            this.gv_Button.Name = "gv_Button";
            this.gv_Button.Size = new System.Drawing.Size(91, 58);
            this.gv_Button.TabIndex = 3;
            this.gv_Button.TabStop = true;
            this.gv_Button.Text = "geringfügig \r\n(GV)";
            this.gv_Button.UseVisualStyleBackColor = true;
            this.gv_Button.CheckedChanged += new System.EventHandler(this.StatusCheckChanged);
            // 
            // gvs_Button
            // 
            this.gvs_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvs_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gvs_Button.Location = new System.Drawing.Point(492, 3);
            this.gvs_Button.Name = "gvs_Button";
            this.gvs_Button.Size = new System.Drawing.Size(91, 58);
            this.gvs_Button.TabIndex = 4;
            this.gvs_Button.TabStop = true;
            this.gvs_Button.Text = "geringfügig Student\r\n(GVS)";
            this.gvs_Button.UseVisualStyleBackColor = true;
            this.gvs_Button.CheckedChanged += new System.EventHandler(this.StatusCheckChanged);
            // 
            // weekHours_Box
            // 
            this.weekHours_Box.Location = new System.Drawing.Point(206, 79);
            this.weekHours_Box.Margin = new System.Windows.Forms.Padding(10, 15, 10, 15);
            this.weekHours_Box.Maximum = new decimal(new int[] {
            39,
            0,
            0,
            0});
            this.weekHours_Box.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.weekHours_Box.Name = "weekHours_Box";
            this.weekHours_Box.Size = new System.Drawing.Size(126, 20);
            this.weekHours_Box.TabIndex = 0;
            this.weekHours_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.weekHours_Box.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.weekHours_Box.ValueChanged += new System.EventHandler(this.WeekHours_Box_ValueChanged);
            this.weekHours_Box.Click += new System.EventHandler(this.input_Selected);
            this.weekHours_Box.Enter += new System.EventHandler(this.input_Selected);
            // 
            // wage_Label
            // 
            this.wage_Label.AutoSize = true;
            this.status_Table.SetColumnSpan(this.wage_Label, 2);
            this.wage_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wage_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wage_Label.Location = new System.Drawing.Point(395, 64);
            this.wage_Label.Name = "wage_Label";
            this.wage_Label.Size = new System.Drawing.Size(188, 65);
            this.wage_Label.TabIndex = 4;
            this.wage_Label.Text = "Stundenlohn (€):";
            this.wage_Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // wage_Box
            // 
            this.status_Table.SetColumnSpan(this.wage_Box, 2);
            this.wage_Box.DecimalPlaces = 2;
            this.wage_Box.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.wage_Box.Location = new System.Drawing.Point(596, 79);
            this.wage_Box.Margin = new System.Windows.Forms.Padding(10, 15, 10, 15);
            this.wage_Box.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.wage_Box.Name = "wage_Box";
            this.wage_Box.Size = new System.Drawing.Size(128, 20);
            this.wage_Box.TabIndex = 7;
            this.wage_Box.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.wage_Box.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.wage_Box.Click += new System.EventHandler(this.input_Selected);
            this.wage_Box.Enter += new System.EventHandler(this.input_Selected);
            // 
            // contract_Panel
            // 
            this.contract_Panel.Controls.Add(this.expire_Picker);
            this.contract_Panel.Controls.Add(this.contract_Picker);
            this.contract_Panel.Controls.Add(this.withEnd_Button);
            this.contract_Panel.Controls.Add(this.noEnd_Button);
            this.contract_Panel.Controls.Add(this.begin_Label);
            this.contract_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contract_Panel.Location = new System.Drawing.Point(4, 458);
            this.contract_Panel.Name = "contract_Panel";
            this.contract_Panel.Size = new System.Drawing.Size(784, 174);
            this.contract_Panel.TabIndex = 3;
            // 
            // expire_Picker
            // 
            this.expire_Picker.Location = new System.Drawing.Point(480, 98);
            this.expire_Picker.Name = "expire_Picker";
            this.expire_Picker.Size = new System.Drawing.Size(200, 20);
            this.expire_Picker.TabIndex = 2;
            // 
            // contract_Picker
            // 
            this.contract_Picker.Location = new System.Drawing.Point(157, 77);
            this.contract_Picker.Name = "contract_Picker";
            this.contract_Picker.Size = new System.Drawing.Size(200, 20);
            this.contract_Picker.TabIndex = 0;
            // 
            // withEnd_Button
            // 
            this.withEnd_Button.AutoSize = true;
            this.withEnd_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.withEnd_Button.Location = new System.Drawing.Point(367, 98);
            this.withEnd_Button.Name = "withEnd_Button";
            this.withEnd_Button.Size = new System.Drawing.Size(113, 24);
            this.withEnd_Button.TabIndex = 4;
            this.withEnd_Button.Text = "befristet bis:";
            this.withEnd_Button.UseVisualStyleBackColor = true;
            // 
            // noEnd_Button
            // 
            this.noEnd_Button.AutoSize = true;
            this.noEnd_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noEnd_Button.Location = new System.Drawing.Point(367, 47);
            this.noEnd_Button.Name = "noEnd_Button";
            this.noEnd_Button.Size = new System.Drawing.Size(103, 24);
            this.noEnd_Button.TabIndex = 1;
            this.noEnd_Button.Text = "unbefristet";
            this.noEnd_Button.UseVisualStyleBackColor = true;
            // 
            // begin_Label
            // 
            this.begin_Label.AutoSize = true;
            this.begin_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.begin_Label.Location = new System.Drawing.Point(6, 77);
            this.begin_Label.Margin = new System.Windows.Forms.Padding(3, 80, 3, 80);
            this.begin_Label.Name = "begin_Label";
            this.begin_Label.Size = new System.Drawing.Size(132, 24);
            this.begin_Label.TabIndex = 1;
            this.begin_Label.Text = "Arbeitsbeginn:";
            // 
            // cafe_Table
            // 
            this.cafe_Table.ColumnCount = 2;
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.cafe_Table.Controls.Add(this.cafe_Button, 1, 0);
            this.cafe_Table.Controls.Add(this.donalds_Button, 0, 0);
            this.cafe_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cafe_Table.Location = new System.Drawing.Point(4, 639);
            this.cafe_Table.Name = "cafe_Table";
            this.cafe_Table.RowCount = 1;
            this.cafe_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.cafe_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 99F));
            this.cafe_Table.Size = new System.Drawing.Size(784, 131);
            this.cafe_Table.TabIndex = 4;
            // 
            // cafe_Button
            // 
            this.cafe_Button.AutoSize = true;
            this.cafe_Button.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cafe_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cafe_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cafe_Button.Location = new System.Drawing.Point(395, 3);
            this.cafe_Button.Name = "cafe_Button";
            this.cafe_Button.Size = new System.Drawing.Size(386, 125);
            this.cafe_Button.TabIndex = 1;
            this.cafe_Button.TabStop = true;
            this.cafe_Button.Text = "McCafè";
            this.cafe_Button.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cafe_Button.UseVisualStyleBackColor = true;
            // 
            // donalds_Button
            // 
            this.donalds_Button.AutoSize = true;
            this.donalds_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.donalds_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donalds_Button.Location = new System.Drawing.Point(3, 3);
            this.donalds_Button.Name = "donalds_Button";
            this.donalds_Button.Size = new System.Drawing.Size(386, 125);
            this.donalds_Button.TabIndex = 0;
            this.donalds_Button.TabStop = true;
            this.donalds_Button.Text = "McDonald\'s";
            this.donalds_Button.UseVisualStyleBackColor = true;
            // 
            // holiday_free_Page
            // 
            this.holiday_free_Page.Controls.Add(this.calendar_Split);
            this.holiday_free_Page.Location = new System.Drawing.Point(4, 22);
            this.holiday_free_Page.Name = "holiday_free_Page";
            this.holiday_free_Page.Padding = new System.Windows.Forms.Padding(3);
            this.holiday_free_Page.Size = new System.Drawing.Size(792, 774);
            this.holiday_free_Page.TabIndex = 0;
            this.holiday_free_Page.Text = "Wunschfrei/Urlaub";
            this.holiday_free_Page.UseVisualStyleBackColor = true;
            // 
            // calendar_Split
            // 
            this.calendar_Split.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendar_Split.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.calendar_Split.IsSplitterFixed = true;
            this.calendar_Split.Location = new System.Drawing.Point(3, 3);
            this.calendar_Split.Name = "calendar_Split";
            this.calendar_Split.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // calendar_Split.Panel1
            // 
            this.calendar_Split.Panel1.BackColor = System.Drawing.Color.Green;
            this.calendar_Split.Panel1.Controls.Add(this.selection_Table);
            // 
            // calendar_Split.Panel2
            // 
            this.calendar_Split.Panel2.Controls.Add(this.calendar_Table);
            this.calendar_Split.Size = new System.Drawing.Size(786, 768);
            this.calendar_Split.SplitterDistance = 45;
            this.calendar_Split.TabIndex = 0;
            // 
            // selection_Table
            // 
            this.selection_Table.ColumnCount = 4;
            this.selection_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.selection_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.selection_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.selection_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.selection_Table.Controls.Add(this.holiday_Button, 1, 0);
            this.selection_Table.Controls.Add(this.free_Button, 2, 0);
            this.selection_Table.Controls.Add(this.year_Selection, 0, 0);
            this.selection_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.selection_Table.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.selection_Table.Location = new System.Drawing.Point(0, 0);
            this.selection_Table.Name = "selection_Table";
            this.selection_Table.RowCount = 1;
            this.selection_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.selection_Table.Size = new System.Drawing.Size(786, 45);
            this.selection_Table.TabIndex = 0;
            // 
            // holiday_Button
            // 
            this.holiday_Button.AutoSize = true;
            this.holiday_Button.Dock = System.Windows.Forms.DockStyle.Right;
            this.holiday_Button.Location = new System.Drawing.Point(333, 3);
            this.holiday_Button.Name = "holiday_Button";
            this.holiday_Button.Size = new System.Drawing.Size(56, 39);
            this.holiday_Button.TabIndex = 1;
            this.holiday_Button.Text = "Urlaub";
            this.holiday_Button.UseVisualStyleBackColor = true;
            // 
            // free_Button
            // 
            this.free_Button.AutoSize = true;
            this.free_Button.Checked = true;
            this.free_Button.Dock = System.Windows.Forms.DockStyle.Left;
            this.free_Button.Location = new System.Drawing.Point(395, 3);
            this.free_Button.Name = "free_Button";
            this.free_Button.Size = new System.Drawing.Size(79, 39);
            this.free_Button.TabIndex = 0;
            this.free_Button.TabStop = true;
            this.free_Button.Text = "Wunschfrei";
            this.free_Button.UseVisualStyleBackColor = true;
            // 
            // year_Selection
            // 
            this.year_Selection.Dock = System.Windows.Forms.DockStyle.Left;
            this.year_Selection.Location = new System.Drawing.Point(3, 3);
            this.year_Selection.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.year_Selection.Minimum = new decimal(new int[] {
            2018,
            0,
            0,
            0});
            this.year_Selection.Name = "year_Selection";
            this.year_Selection.Size = new System.Drawing.Size(54, 20);
            this.year_Selection.TabIndex = 44;
            this.year_Selection.Value = new decimal(new int[] {
            2019,
            0,
            0,
            0});
            // 
            // calendar_Table
            // 
            this.calendar_Table.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.calendar_Table.ColumnCount = 13;
            this.calendar_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.calendar_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.calendar_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.calendar_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.calendar_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.calendar_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.calendar_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.calendar_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.calendar_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.calendar_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.calendar_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.calendar_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.calendar_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.692307F));
            this.calendar_Table.Controls.Add(this.label31, 0, 31);
            this.calendar_Table.Controls.Add(this.label30, 0, 30);
            this.calendar_Table.Controls.Add(this.label29, 0, 29);
            this.calendar_Table.Controls.Add(this.label28, 0, 28);
            this.calendar_Table.Controls.Add(this.label27, 0, 27);
            this.calendar_Table.Controls.Add(this.label26, 0, 26);
            this.calendar_Table.Controls.Add(this.label25, 0, 25);
            this.calendar_Table.Controls.Add(this.label24, 0, 24);
            this.calendar_Table.Controls.Add(this.label23, 0, 23);
            this.calendar_Table.Controls.Add(this.label22, 0, 22);
            this.calendar_Table.Controls.Add(this.label21, 0, 21);
            this.calendar_Table.Controls.Add(this.label20, 0, 20);
            this.calendar_Table.Controls.Add(this.label19, 0, 19);
            this.calendar_Table.Controls.Add(this.label18, 0, 18);
            this.calendar_Table.Controls.Add(this.label17, 0, 17);
            this.calendar_Table.Controls.Add(this.label16, 0, 16);
            this.calendar_Table.Controls.Add(this.label15, 0, 15);
            this.calendar_Table.Controls.Add(this.label14, 0, 14);
            this.calendar_Table.Controls.Add(this.label13, 0, 13);
            this.calendar_Table.Controls.Add(this.label12, 0, 12);
            this.calendar_Table.Controls.Add(this.label11, 0, 11);
            this.calendar_Table.Controls.Add(this.label10, 0, 10);
            this.calendar_Table.Controls.Add(this.label9, 0, 9);
            this.calendar_Table.Controls.Add(this.label8, 0, 8);
            this.calendar_Table.Controls.Add(this.label7, 0, 7);
            this.calendar_Table.Controls.Add(this.label6, 0, 6);
            this.calendar_Table.Controls.Add(this.label5, 0, 5);
            this.calendar_Table.Controls.Add(this.label4, 0, 4);
            this.calendar_Table.Controls.Add(this.label3, 0, 3);
            this.calendar_Table.Controls.Add(this.label2, 0, 2);
            this.calendar_Table.Controls.Add(this.label1, 0, 1);
            this.calendar_Table.Controls.Add(this.december_Label, 12, 0);
            this.calendar_Table.Controls.Add(this.november_Label, 11, 0);
            this.calendar_Table.Controls.Add(this.october_Label, 10, 0);
            this.calendar_Table.Controls.Add(this.september_Label, 9, 0);
            this.calendar_Table.Controls.Add(this.august_Label, 8, 0);
            this.calendar_Table.Controls.Add(this.july_Label, 7, 0);
            this.calendar_Table.Controls.Add(this.june_Label, 6, 0);
            this.calendar_Table.Controls.Add(this.april_Label, 4, 0);
            this.calendar_Table.Controls.Add(this.march_Label, 3, 0);
            this.calendar_Table.Controls.Add(this.february_Label, 2, 0);
            this.calendar_Table.Controls.Add(this.january_Label, 1, 0);
            this.calendar_Table.Controls.Add(this.blank_Label, 0, 0);
            this.calendar_Table.Controls.Add(this.may_Label, 5, 0);
            this.calendar_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendar_Table.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.calendar_Table.Location = new System.Drawing.Point(0, 0);
            this.calendar_Table.Name = "calendar_Table";
            this.calendar_Table.RowCount = 32;
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 3.125F));
            this.calendar_Table.Size = new System.Drawing.Size(786, 719);
            this.calendar_Table.TabIndex = 0;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label31.Location = new System.Drawing.Point(4, 683);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(53, 35);
            this.label31.TabIndex = 43;
            this.label31.Text = "31";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Location = new System.Drawing.Point(4, 661);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(53, 21);
            this.label30.TabIndex = 42;
            this.label30.Text = "30";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Location = new System.Drawing.Point(4, 639);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 21);
            this.label29.TabIndex = 41;
            this.label29.Text = "29";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Location = new System.Drawing.Point(4, 617);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 21);
            this.label28.TabIndex = 40;
            this.label28.Text = "28";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Location = new System.Drawing.Point(4, 595);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 21);
            this.label27.TabIndex = 39;
            this.label27.Text = "27";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Location = new System.Drawing.Point(4, 573);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(53, 21);
            this.label26.TabIndex = 38;
            this.label26.Text = "26";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Location = new System.Drawing.Point(4, 551);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(53, 21);
            this.label25.TabIndex = 37;
            this.label25.Text = "25";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Location = new System.Drawing.Point(4, 529);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(53, 21);
            this.label24.TabIndex = 36;
            this.label24.Text = "24";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Location = new System.Drawing.Point(4, 507);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 21);
            this.label23.TabIndex = 35;
            this.label23.Text = "23";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Location = new System.Drawing.Point(4, 485);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(53, 21);
            this.label22.TabIndex = 34;
            this.label22.Text = "22";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Location = new System.Drawing.Point(4, 463);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(53, 21);
            this.label21.TabIndex = 33;
            this.label21.Text = "21";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Location = new System.Drawing.Point(4, 441);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(53, 21);
            this.label20.TabIndex = 32;
            this.label20.Text = "20";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Location = new System.Drawing.Point(4, 419);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(53, 21);
            this.label19.TabIndex = 31;
            this.label19.Text = "19";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Location = new System.Drawing.Point(4, 397);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 21);
            this.label18.TabIndex = 30;
            this.label18.Text = "18";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Location = new System.Drawing.Point(4, 375);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 21);
            this.label17.TabIndex = 29;
            this.label17.Text = "17";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Location = new System.Drawing.Point(4, 353);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(53, 21);
            this.label16.TabIndex = 28;
            this.label16.Text = "16";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Location = new System.Drawing.Point(4, 331);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 21);
            this.label15.TabIndex = 27;
            this.label15.Text = "15";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Location = new System.Drawing.Point(4, 309);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 21);
            this.label14.TabIndex = 26;
            this.label14.Text = "14";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Location = new System.Drawing.Point(4, 287);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(53, 21);
            this.label13.TabIndex = 25;
            this.label13.Text = "13";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Location = new System.Drawing.Point(4, 265);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 21);
            this.label12.TabIndex = 24;
            this.label12.Text = "12";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Location = new System.Drawing.Point(4, 243);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 21);
            this.label11.TabIndex = 23;
            this.label11.Text = "11";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Location = new System.Drawing.Point(4, 221);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 21);
            this.label10.TabIndex = 22;
            this.label10.Text = "10";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Location = new System.Drawing.Point(4, 199);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 21);
            this.label9.TabIndex = 21;
            this.label9.Text = "9";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Location = new System.Drawing.Point(4, 177);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 21);
            this.label8.TabIndex = 20;
            this.label8.Text = "8";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(4, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 21);
            this.label7.TabIndex = 19;
            this.label7.Text = "7";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(4, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 21);
            this.label6.TabIndex = 18;
            this.label6.Text = "6";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(4, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 21);
            this.label5.TabIndex = 17;
            this.label5.Text = "5";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(4, 89);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 21);
            this.label4.TabIndex = 16;
            this.label4.Text = "4";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(4, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 21);
            this.label3.TabIndex = 15;
            this.label3.Text = "3";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(4, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 21);
            this.label2.TabIndex = 14;
            this.label2.Text = "2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(4, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 21);
            this.label1.TabIndex = 13;
            this.label1.Text = "1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // december_Label
            // 
            this.december_Label.AutoSize = true;
            this.december_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.december_Label.Location = new System.Drawing.Point(724, 1);
            this.december_Label.Name = "december_Label";
            this.december_Label.Size = new System.Drawing.Size(58, 21);
            this.december_Label.TabIndex = 12;
            this.december_Label.Text = "Dezember";
            this.december_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // november_Label
            // 
            this.november_Label.AutoSize = true;
            this.november_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.november_Label.Location = new System.Drawing.Point(664, 1);
            this.november_Label.Name = "november_Label";
            this.november_Label.Size = new System.Drawing.Size(53, 21);
            this.november_Label.TabIndex = 11;
            this.november_Label.Text = "Nov.";
            this.november_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // october_Label
            // 
            this.october_Label.AutoSize = true;
            this.october_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.october_Label.Location = new System.Drawing.Point(604, 1);
            this.october_Label.Name = "october_Label";
            this.october_Label.Size = new System.Drawing.Size(53, 21);
            this.october_Label.TabIndex = 10;
            this.october_Label.Text = "Oktober";
            this.october_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // september_Label
            // 
            this.september_Label.AutoSize = true;
            this.september_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.september_Label.Location = new System.Drawing.Point(544, 1);
            this.september_Label.Name = "september_Label";
            this.september_Label.Size = new System.Drawing.Size(53, 21);
            this.september_Label.TabIndex = 9;
            this.september_Label.Text = "Sept.";
            this.september_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // august_Label
            // 
            this.august_Label.AutoSize = true;
            this.august_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.august_Label.Location = new System.Drawing.Point(484, 1);
            this.august_Label.Name = "august_Label";
            this.august_Label.Size = new System.Drawing.Size(53, 21);
            this.august_Label.TabIndex = 8;
            this.august_Label.Text = "August";
            this.august_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // july_Label
            // 
            this.july_Label.AutoSize = true;
            this.july_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.july_Label.Location = new System.Drawing.Point(424, 1);
            this.july_Label.Name = "july_Label";
            this.july_Label.Size = new System.Drawing.Size(53, 21);
            this.july_Label.TabIndex = 7;
            this.july_Label.Text = "Juli";
            this.july_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // june_Label
            // 
            this.june_Label.AutoSize = true;
            this.june_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.june_Label.Location = new System.Drawing.Point(364, 1);
            this.june_Label.Name = "june_Label";
            this.june_Label.Size = new System.Drawing.Size(53, 21);
            this.june_Label.TabIndex = 6;
            this.june_Label.Text = "Juni";
            this.june_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // april_Label
            // 
            this.april_Label.AutoSize = true;
            this.april_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.april_Label.Location = new System.Drawing.Point(244, 1);
            this.april_Label.Name = "april_Label";
            this.april_Label.Size = new System.Drawing.Size(53, 21);
            this.april_Label.TabIndex = 5;
            this.april_Label.Text = "April";
            this.april_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // march_Label
            // 
            this.march_Label.AutoSize = true;
            this.march_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.march_Label.Location = new System.Drawing.Point(184, 1);
            this.march_Label.Name = "march_Label";
            this.march_Label.Size = new System.Drawing.Size(53, 21);
            this.march_Label.TabIndex = 4;
            this.march_Label.Text = "März";
            this.march_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // february_Label
            // 
            this.february_Label.AutoSize = true;
            this.february_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.february_Label.Location = new System.Drawing.Point(124, 1);
            this.february_Label.Name = "february_Label";
            this.february_Label.Size = new System.Drawing.Size(53, 21);
            this.february_Label.TabIndex = 3;
            this.february_Label.Text = "Februar";
            this.february_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // january_Label
            // 
            this.january_Label.AutoSize = true;
            this.january_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.january_Label.Location = new System.Drawing.Point(64, 1);
            this.january_Label.Name = "january_Label";
            this.january_Label.Size = new System.Drawing.Size(53, 21);
            this.january_Label.TabIndex = 2;
            this.january_Label.Text = "Januar";
            this.january_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // blank_Label
            // 
            this.blank_Label.AutoSize = true;
            this.blank_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.blank_Label.Location = new System.Drawing.Point(4, 1);
            this.blank_Label.Name = "blank_Label";
            this.blank_Label.Size = new System.Drawing.Size(53, 21);
            this.blank_Label.TabIndex = 1;
            this.blank_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // may_Label
            // 
            this.may_Label.AutoSize = true;
            this.may_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.may_Label.Location = new System.Drawing.Point(304, 1);
            this.may_Label.Name = "may_Label";
            this.may_Label.Size = new System.Drawing.Size(53, 21);
            this.may_Label.TabIndex = 0;
            this.may_Label.Text = "Mai";
            this.may_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // availability_Page
            // 
            this.availability_Page.Controls.Add(this.av_Split);
            this.availability_Page.Location = new System.Drawing.Point(4, 22);
            this.availability_Page.Name = "availability_Page";
            this.availability_Page.Padding = new System.Windows.Forms.Padding(3);
            this.availability_Page.Size = new System.Drawing.Size(792, 774);
            this.availability_Page.TabIndex = 1;
            this.availability_Page.Text = "Verfügbarkeiten";
            this.availability_Page.UseVisualStyleBackColor = true;
            // 
            // av_Split
            // 
            this.av_Split.Dock = System.Windows.Forms.DockStyle.Fill;
            this.av_Split.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.av_Split.IsSplitterFixed = true;
            this.av_Split.Location = new System.Drawing.Point(3, 3);
            this.av_Split.Name = "av_Split";
            this.av_Split.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // av_Split.Panel1
            // 
            this.av_Split.Panel1.Controls.Add(this.av_TopFlow);
            // 
            // av_Split.Panel2
            // 
            this.av_Split.Panel2.Controls.Add(this.availability_Table);
            this.av_Split.Size = new System.Drawing.Size(786, 768);
            this.av_Split.SplitterDistance = 45;
            this.av_Split.TabIndex = 0;
            // 
            // av_TopFlow
            // 
            this.av_TopFlow.BackColor = System.Drawing.Color.Green;
            this.av_TopFlow.Controls.Add(this.avSave_Button);
            this.av_TopFlow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.av_TopFlow.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.av_TopFlow.Location = new System.Drawing.Point(0, 0);
            this.av_TopFlow.Name = "av_TopFlow";
            this.av_TopFlow.Size = new System.Drawing.Size(786, 45);
            this.av_TopFlow.TabIndex = 0;
            // 
            // avSave_Button
            // 
            this.avSave_Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.avSave_Button.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.avSave_Button.BackgroundImage = global::McD_Schichtplaner.Properties.Resources.accept;
            this.avSave_Button.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.avSave_Button.Location = new System.Drawing.Point(696, 5);
            this.avSave_Button.Margin = new System.Windows.Forms.Padding(15, 5, 15, 0);
            this.avSave_Button.Name = "avSave_Button";
            this.avSave_Button.Size = new System.Drawing.Size(75, 35);
            this.avSave_Button.TabIndex = 0;
            this.avSave_Button.Text = "Speichern";
            this.avSave_Button.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.avSave_Button.UseVisualStyleBackColor = false;
            this.avSave_Button.Click += new System.EventHandler(this.AvSave_Button_Click);
            // 
            // availability_Table
            // 
            this.availability_Table.ColumnCount = 9;
            this.availability_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.availability_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.availability_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.availability_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.availability_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.availability_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.availability_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.availability_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.availability_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.availability_Table.Controls.Add(this.sundayEnd_Picker, 7, 3);
            this.availability_Table.Controls.Add(this.saturdayEnd_Picker, 6, 3);
            this.availability_Table.Controls.Add(this.thursdayEnd_Picker, 4, 3);
            this.availability_Table.Controls.Add(this.tuesdayEnd_Picker, 2, 3);
            this.availability_Table.Controls.Add(this.wednesdayEnd_Picker, 3, 3);
            this.availability_Table.Controls.Add(this.fridayEnd_Picker, 5, 3);
            this.availability_Table.Controls.Add(this.mondayEnd_Picker, 1, 3);
            this.availability_Table.Controls.Add(this.saturdayStart_Picker, 6, 1);
            this.availability_Table.Controls.Add(this.wednesdayStart_Picker, 3, 1);
            this.availability_Table.Controls.Add(this.tuesdayStart_Picker, 2, 1);
            this.availability_Table.Controls.Add(this.fridayStart_Picker, 5, 1);
            this.availability_Table.Controls.Add(this.thursdayStart_Picker, 4, 1);
            this.availability_Table.Controls.Add(this.sundayStart_Picker, 7, 1);
            this.availability_Table.Controls.Add(this.saturday_Label, 6, 0);
            this.availability_Table.Controls.Add(this.friday_Label, 5, 0);
            this.availability_Table.Controls.Add(this.thursday_Label, 4, 0);
            this.availability_Table.Controls.Add(this.wednesday_Label, 3, 0);
            this.availability_Table.Controls.Add(this.tuesday_Label, 2, 0);
            this.availability_Table.Controls.Add(this.mondayStart_Picker, 1, 1);
            this.availability_Table.Controls.Add(this.monday_Label, 1, 0);
            this.availability_Table.Controls.Add(this.sunday_Label, 7, 0);
            this.availability_Table.Controls.Add(this.monday_PictureBox, 1, 2);
            this.availability_Table.Controls.Add(this.tuesday_PictureBox, 2, 2);
            this.availability_Table.Controls.Add(this.wednesday_PictureBox, 3, 2);
            this.availability_Table.Controls.Add(this.thursday_PictureBox, 4, 2);
            this.availability_Table.Controls.Add(this.friday_PictureBox, 5, 2);
            this.availability_Table.Controls.Add(this.saturday_PictureBox, 6, 2);
            this.availability_Table.Controls.Add(this.sunday_PictureBox, 7, 2);
            this.availability_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.availability_Table.Location = new System.Drawing.Point(0, 0);
            this.availability_Table.Name = "availability_Table";
            this.availability_Table.RowCount = 5;
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.availability_Table.Size = new System.Drawing.Size(786, 719);
            this.availability_Table.TabIndex = 0;
            // 
            // sundayEnd_Picker
            // 
            this.sundayEnd_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sundayEnd_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sundayEnd_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.sundayEnd_Picker.Location = new System.Drawing.Point(623, 647);
            this.sundayEnd_Picker.Name = "sundayEnd_Picker";
            this.sundayEnd_Picker.ShowUpDown = true;
            this.sundayEnd_Picker.Size = new System.Drawing.Size(89, 21);
            this.sundayEnd_Picker.TabIndex = 24;
            this.sundayEnd_Picker.Value = new System.DateTime(2019, 10, 3, 6, 0, 0, 0);
            this.sundayEnd_Picker.ValueChanged += new System.EventHandler(this.SundayAvailabilityChanged);
            // 
            // saturdayEnd_Picker
            // 
            this.saturdayEnd_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saturdayEnd_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saturdayEnd_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.saturdayEnd_Picker.Location = new System.Drawing.Point(528, 647);
            this.saturdayEnd_Picker.Name = "saturdayEnd_Picker";
            this.saturdayEnd_Picker.ShowUpDown = true;
            this.saturdayEnd_Picker.Size = new System.Drawing.Size(89, 21);
            this.saturdayEnd_Picker.TabIndex = 23;
            this.saturdayEnd_Picker.Value = new System.DateTime(2019, 10, 3, 6, 0, 0, 0);
            this.saturdayEnd_Picker.ValueChanged += new System.EventHandler(this.SaturdayAvailabilityChanged);
            // 
            // thursdayEnd_Picker
            // 
            this.thursdayEnd_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thursdayEnd_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thursdayEnd_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.thursdayEnd_Picker.Location = new System.Drawing.Point(338, 647);
            this.thursdayEnd_Picker.Name = "thursdayEnd_Picker";
            this.thursdayEnd_Picker.ShowUpDown = true;
            this.thursdayEnd_Picker.Size = new System.Drawing.Size(89, 21);
            this.thursdayEnd_Picker.TabIndex = 22;
            this.thursdayEnd_Picker.Value = new System.DateTime(2019, 10, 3, 6, 0, 0, 0);
            this.thursdayEnd_Picker.ValueChanged += new System.EventHandler(this.ThursdayAvailabilityChanged);
            // 
            // tuesdayEnd_Picker
            // 
            this.tuesdayEnd_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tuesdayEnd_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tuesdayEnd_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.tuesdayEnd_Picker.Location = new System.Drawing.Point(148, 647);
            this.tuesdayEnd_Picker.Name = "tuesdayEnd_Picker";
            this.tuesdayEnd_Picker.ShowUpDown = true;
            this.tuesdayEnd_Picker.Size = new System.Drawing.Size(89, 21);
            this.tuesdayEnd_Picker.TabIndex = 21;
            this.tuesdayEnd_Picker.Value = new System.DateTime(2019, 10, 3, 6, 0, 0, 0);
            this.tuesdayEnd_Picker.ValueChanged += new System.EventHandler(this.TuesdayAvailabilityChanged);
            // 
            // wednesdayEnd_Picker
            // 
            this.wednesdayEnd_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wednesdayEnd_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wednesdayEnd_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.wednesdayEnd_Picker.Location = new System.Drawing.Point(243, 647);
            this.wednesdayEnd_Picker.Name = "wednesdayEnd_Picker";
            this.wednesdayEnd_Picker.ShowUpDown = true;
            this.wednesdayEnd_Picker.Size = new System.Drawing.Size(89, 21);
            this.wednesdayEnd_Picker.TabIndex = 20;
            this.wednesdayEnd_Picker.Value = new System.DateTime(2019, 10, 3, 6, 0, 0, 0);
            this.wednesdayEnd_Picker.ValueChanged += new System.EventHandler(this.WednesdayAvailabilityChanged);
            // 
            // fridayEnd_Picker
            // 
            this.fridayEnd_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fridayEnd_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fridayEnd_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.fridayEnd_Picker.Location = new System.Drawing.Point(433, 647);
            this.fridayEnd_Picker.Name = "fridayEnd_Picker";
            this.fridayEnd_Picker.ShowUpDown = true;
            this.fridayEnd_Picker.Size = new System.Drawing.Size(89, 21);
            this.fridayEnd_Picker.TabIndex = 19;
            this.fridayEnd_Picker.Value = new System.DateTime(2019, 10, 3, 6, 0, 0, 0);
            this.fridayEnd_Picker.ValueChanged += new System.EventHandler(this.FridayAvailabilityChanged);
            // 
            // mondayEnd_Picker
            // 
            this.mondayEnd_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mondayEnd_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mondayEnd_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.mondayEnd_Picker.Location = new System.Drawing.Point(53, 647);
            this.mondayEnd_Picker.Name = "mondayEnd_Picker";
            this.mondayEnd_Picker.ShowUpDown = true;
            this.mondayEnd_Picker.Size = new System.Drawing.Size(89, 21);
            this.mondayEnd_Picker.TabIndex = 18;
            this.mondayEnd_Picker.Value = new System.DateTime(2019, 10, 3, 6, 0, 0, 0);
            this.mondayEnd_Picker.ValueChanged += new System.EventHandler(this.MondayAvailabilityChanged);
            // 
            // saturdayStart_Picker
            // 
            this.saturdayStart_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saturdayStart_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saturdayStart_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.saturdayStart_Picker.Location = new System.Drawing.Point(528, 53);
            this.saturdayStart_Picker.Name = "saturdayStart_Picker";
            this.saturdayStart_Picker.ShowUpDown = true;
            this.saturdayStart_Picker.Size = new System.Drawing.Size(89, 21);
            this.saturdayStart_Picker.TabIndex = 17;
            this.saturdayStart_Picker.Value = new System.DateTime(2019, 10, 2, 6, 0, 0, 0);
            this.saturdayStart_Picker.ValueChanged += new System.EventHandler(this.SaturdayAvailabilityChanged);
            // 
            // wednesdayStart_Picker
            // 
            this.wednesdayStart_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wednesdayStart_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wednesdayStart_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.wednesdayStart_Picker.Location = new System.Drawing.Point(243, 53);
            this.wednesdayStart_Picker.Name = "wednesdayStart_Picker";
            this.wednesdayStart_Picker.ShowUpDown = true;
            this.wednesdayStart_Picker.Size = new System.Drawing.Size(89, 21);
            this.wednesdayStart_Picker.TabIndex = 16;
            this.wednesdayStart_Picker.Value = new System.DateTime(2019, 10, 2, 6, 0, 0, 0);
            this.wednesdayStart_Picker.ValueChanged += new System.EventHandler(this.WednesdayAvailabilityChanged);
            // 
            // tuesdayStart_Picker
            // 
            this.tuesdayStart_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tuesdayStart_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tuesdayStart_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.tuesdayStart_Picker.Location = new System.Drawing.Point(148, 53);
            this.tuesdayStart_Picker.Name = "tuesdayStart_Picker";
            this.tuesdayStart_Picker.ShowUpDown = true;
            this.tuesdayStart_Picker.Size = new System.Drawing.Size(89, 21);
            this.tuesdayStart_Picker.TabIndex = 15;
            this.tuesdayStart_Picker.Value = new System.DateTime(2019, 10, 2, 6, 0, 0, 0);
            this.tuesdayStart_Picker.ValueChanged += new System.EventHandler(this.TuesdayAvailabilityChanged);
            // 
            // fridayStart_Picker
            // 
            this.fridayStart_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fridayStart_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fridayStart_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.fridayStart_Picker.Location = new System.Drawing.Point(433, 53);
            this.fridayStart_Picker.Name = "fridayStart_Picker";
            this.fridayStart_Picker.ShowUpDown = true;
            this.fridayStart_Picker.Size = new System.Drawing.Size(89, 21);
            this.fridayStart_Picker.TabIndex = 10;
            this.fridayStart_Picker.Value = new System.DateTime(2019, 10, 2, 6, 0, 0, 0);
            this.fridayStart_Picker.ValueChanged += new System.EventHandler(this.FridayAvailabilityChanged);
            // 
            // thursdayStart_Picker
            // 
            this.thursdayStart_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thursdayStart_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thursdayStart_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.thursdayStart_Picker.Location = new System.Drawing.Point(338, 53);
            this.thursdayStart_Picker.Name = "thursdayStart_Picker";
            this.thursdayStart_Picker.ShowUpDown = true;
            this.thursdayStart_Picker.Size = new System.Drawing.Size(89, 21);
            this.thursdayStart_Picker.TabIndex = 9;
            this.thursdayStart_Picker.Value = new System.DateTime(2019, 10, 2, 6, 0, 0, 0);
            this.thursdayStart_Picker.ValueChanged += new System.EventHandler(this.ThursdayAvailabilityChanged);
            // 
            // sundayStart_Picker
            // 
            this.sundayStart_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sundayStart_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sundayStart_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.sundayStart_Picker.Location = new System.Drawing.Point(623, 53);
            this.sundayStart_Picker.Name = "sundayStart_Picker";
            this.sundayStart_Picker.ShowUpDown = true;
            this.sundayStart_Picker.Size = new System.Drawing.Size(89, 21);
            this.sundayStart_Picker.TabIndex = 8;
            this.sundayStart_Picker.Value = new System.DateTime(2019, 10, 2, 6, 0, 0, 0);
            this.sundayStart_Picker.ValueChanged += new System.EventHandler(this.SundayAvailabilityChanged);
            // 
            // saturday_Label
            // 
            this.saturday_Label.AutoSize = true;
            this.saturday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saturday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saturday_Label.Location = new System.Drawing.Point(528, 0);
            this.saturday_Label.Name = "saturday_Label";
            this.saturday_Label.Size = new System.Drawing.Size(89, 50);
            this.saturday_Label.TabIndex = 7;
            this.saturday_Label.Text = "Samstag";
            this.saturday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // friday_Label
            // 
            this.friday_Label.AutoSize = true;
            this.friday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.friday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.friday_Label.Location = new System.Drawing.Point(433, 0);
            this.friday_Label.Name = "friday_Label";
            this.friday_Label.Size = new System.Drawing.Size(89, 50);
            this.friday_Label.TabIndex = 6;
            this.friday_Label.Text = "Freitag";
            this.friday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // thursday_Label
            // 
            this.thursday_Label.AutoSize = true;
            this.thursday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thursday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thursday_Label.Location = new System.Drawing.Point(338, 0);
            this.thursday_Label.Name = "thursday_Label";
            this.thursday_Label.Size = new System.Drawing.Size(89, 50);
            this.thursday_Label.TabIndex = 5;
            this.thursday_Label.Text = "Donnerstag";
            this.thursday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // wednesday_Label
            // 
            this.wednesday_Label.AutoSize = true;
            this.wednesday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wednesday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wednesday_Label.Location = new System.Drawing.Point(243, 0);
            this.wednesday_Label.Name = "wednesday_Label";
            this.wednesday_Label.Size = new System.Drawing.Size(89, 50);
            this.wednesday_Label.TabIndex = 4;
            this.wednesday_Label.Text = "Mittwoch";
            this.wednesday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tuesday_Label
            // 
            this.tuesday_Label.AutoSize = true;
            this.tuesday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tuesday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tuesday_Label.Location = new System.Drawing.Point(148, 0);
            this.tuesday_Label.Name = "tuesday_Label";
            this.tuesday_Label.Size = new System.Drawing.Size(89, 50);
            this.tuesday_Label.TabIndex = 3;
            this.tuesday_Label.Text = "Dienstag";
            this.tuesday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mondayStart_Picker
            // 
            this.mondayStart_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mondayStart_Picker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mondayStart_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.mondayStart_Picker.Location = new System.Drawing.Point(53, 53);
            this.mondayStart_Picker.Name = "mondayStart_Picker";
            this.mondayStart_Picker.ShowUpDown = true;
            this.mondayStart_Picker.Size = new System.Drawing.Size(89, 21);
            this.mondayStart_Picker.TabIndex = 0;
            this.mondayStart_Picker.Value = new System.DateTime(2019, 10, 2, 6, 0, 0, 0);
            this.mondayStart_Picker.ValueChanged += new System.EventHandler(this.MondayAvailabilityChanged);
            // 
            // monday_Label
            // 
            this.monday_Label.AutoSize = true;
            this.monday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.monday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.monday_Label.Location = new System.Drawing.Point(53, 0);
            this.monday_Label.Name = "monday_Label";
            this.monday_Label.Size = new System.Drawing.Size(89, 50);
            this.monday_Label.TabIndex = 1;
            this.monday_Label.Text = "Montag";
            this.monday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // sunday_Label
            // 
            this.sunday_Label.AutoSize = true;
            this.sunday_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sunday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sunday_Label.Location = new System.Drawing.Point(623, 0);
            this.sunday_Label.Name = "sunday_Label";
            this.sunday_Label.Size = new System.Drawing.Size(89, 50);
            this.sunday_Label.TabIndex = 2;
            this.sunday_Label.Text = "Sonntag";
            this.sunday_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // monday_PictureBox
            // 
            this.monday_PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.monday_PictureBox.Location = new System.Drawing.Point(53, 78);
            this.monday_PictureBox.Name = "monday_PictureBox";
            this.monday_PictureBox.Size = new System.Drawing.Size(89, 563);
            this.monday_PictureBox.TabIndex = 25;
            this.monday_PictureBox.TabStop = false;
            // 
            // tuesday_PictureBox
            // 
            this.tuesday_PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tuesday_PictureBox.Location = new System.Drawing.Point(148, 78);
            this.tuesday_PictureBox.Name = "tuesday_PictureBox";
            this.tuesday_PictureBox.Size = new System.Drawing.Size(89, 563);
            this.tuesday_PictureBox.TabIndex = 26;
            this.tuesday_PictureBox.TabStop = false;
            // 
            // wednesday_PictureBox
            // 
            this.wednesday_PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wednesday_PictureBox.Location = new System.Drawing.Point(243, 78);
            this.wednesday_PictureBox.Name = "wednesday_PictureBox";
            this.wednesday_PictureBox.Size = new System.Drawing.Size(89, 563);
            this.wednesday_PictureBox.TabIndex = 27;
            this.wednesday_PictureBox.TabStop = false;
            // 
            // thursday_PictureBox
            // 
            this.thursday_PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thursday_PictureBox.Location = new System.Drawing.Point(338, 78);
            this.thursday_PictureBox.Name = "thursday_PictureBox";
            this.thursday_PictureBox.Size = new System.Drawing.Size(89, 563);
            this.thursday_PictureBox.TabIndex = 28;
            this.thursday_PictureBox.TabStop = false;
            // 
            // friday_PictureBox
            // 
            this.friday_PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.friday_PictureBox.Location = new System.Drawing.Point(433, 78);
            this.friday_PictureBox.Name = "friday_PictureBox";
            this.friday_PictureBox.Size = new System.Drawing.Size(89, 563);
            this.friday_PictureBox.TabIndex = 29;
            this.friday_PictureBox.TabStop = false;
            // 
            // saturday_PictureBox
            // 
            this.saturday_PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.saturday_PictureBox.Location = new System.Drawing.Point(528, 78);
            this.saturday_PictureBox.Name = "saturday_PictureBox";
            this.saturday_PictureBox.Size = new System.Drawing.Size(89, 563);
            this.saturday_PictureBox.TabIndex = 30;
            this.saturday_PictureBox.TabStop = false;
            // 
            // sunday_PictureBox
            // 
            this.sunday_PictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sunday_PictureBox.Location = new System.Drawing.Point(623, 78);
            this.sunday_PictureBox.Name = "sunday_PictureBox";
            this.sunday_PictureBox.Size = new System.Drawing.Size(89, 563);
            this.sunday_PictureBox.TabIndex = 31;
            this.sunday_PictureBox.TabStop = false;
            // 
            // WorkerDetails
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.Controls.Add(this.tabControl);
            this.Name = "WorkerDetails";
            this.Size = new System.Drawing.Size(800, 800);
            this.tabControl.ResumeLayout(false);
            this.details_Page.ResumeLayout(false);
            this.mainDetail_Table.ResumeLayout(false);
            this.primaryDetails_Table.ResumeLayout(false);
            this.primaryDetails_Table.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.number_Box)).EndInit();
            this.birthday_Flowlayout.ResumeLayout(false);
            this.birthday_Flowlayout.PerformLayout();
            this.stations_Table.ResumeLayout(false);
            this.stations_Table.PerformLayout();
            this.status_Table.ResumeLayout(false);
            this.status_Table.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weekHours_Box)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wage_Box)).EndInit();
            this.contract_Panel.ResumeLayout(false);
            this.contract_Panel.PerformLayout();
            this.cafe_Table.ResumeLayout(false);
            this.cafe_Table.PerformLayout();
            this.holiday_free_Page.ResumeLayout(false);
            this.calendar_Split.Panel1.ResumeLayout(false);
            this.calendar_Split.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.calendar_Split)).EndInit();
            this.calendar_Split.ResumeLayout(false);
            this.selection_Table.ResumeLayout(false);
            this.selection_Table.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.year_Selection)).EndInit();
            this.calendar_Table.ResumeLayout(false);
            this.calendar_Table.PerformLayout();
            this.availability_Page.ResumeLayout(false);
            this.av_Split.Panel1.ResumeLayout(false);
            this.av_Split.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.av_Split)).EndInit();
            this.av_Split.ResumeLayout(false);
            this.av_TopFlow.ResumeLayout(false);
            this.availability_Table.ResumeLayout(false);
            this.availability_Table.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.monday_PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuesday_PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wednesday_PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.thursday_PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.friday_PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.saturday_PictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sunday_PictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage holiday_free_Page;
        private System.Windows.Forms.SplitContainer calendar_Split;
        private System.Windows.Forms.TabPage availability_Page;
        private System.Windows.Forms.TableLayoutPanel selection_Table;
        private System.Windows.Forms.RadioButton holiday_Button;
        private System.Windows.Forms.RadioButton free_Button;
        private System.Windows.Forms.TableLayoutPanel calendar_Table;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label december_Label;
        private System.Windows.Forms.Label november_Label;
        private System.Windows.Forms.Label october_Label;
        private System.Windows.Forms.Label september_Label;
        private System.Windows.Forms.Label august_Label;
        private System.Windows.Forms.Label july_Label;
        private System.Windows.Forms.Label june_Label;
        private System.Windows.Forms.Label april_Label;
        private System.Windows.Forms.Label march_Label;
        private System.Windows.Forms.Label february_Label;
        private System.Windows.Forms.Label january_Label;
        private System.Windows.Forms.Label blank_Label;
        private System.Windows.Forms.Label may_Label;
        private System.Windows.Forms.NumericUpDown year_Selection;
        private System.Windows.Forms.TabPage details_Page;
        private System.Windows.Forms.TableLayoutPanel mainDetail_Table;
        private System.Windows.Forms.TableLayoutPanel primaryDetails_Table;
        private System.Windows.Forms.TextBox name_Box;
        private System.Windows.Forms.Label number_Label;
        private System.Windows.Forms.Label mail_Label;
        private System.Windows.Forms.Label name_Label;
        private System.Windows.Forms.TextBox mail_Box;
        private System.Windows.Forms.NumericUpDown number_Box;
        private System.Windows.Forms.FlowLayoutPanel birthday_Flowlayout;
        private System.Windows.Forms.Label birthday_Label;
        private System.Windows.Forms.Label stations_Label;
        private System.Windows.Forms.TableLayoutPanel stations_Table;
        private System.Windows.Forms.CheckBox breakfast_CheckBox;
        private System.Windows.Forms.CheckBox cafe_CheckBox;
        private System.Windows.Forms.CheckBox service_CheckBox;
        private System.Windows.Forms.CheckBox counter_CheckBox;
        private System.Windows.Forms.CheckBox drive_CheckBox;
        private System.Windows.Forms.CheckBox kitchen_CheckBox;
        private System.Windows.Forms.TableLayoutPanel status_Table;
        private System.Windows.Forms.RadioButton mgm_Button;
        private System.Windows.Forms.RadioButton gv_Button;
        private System.Windows.Forms.RadioButton tz_Button;
        private System.Windows.Forms.RadioButton vz_Button;
        private System.Windows.Forms.Label wage_Label;
        private System.Windows.Forms.NumericUpDown weekHours_Box;
        private System.Windows.Forms.Panel contract_Panel;
        private System.Windows.Forms.RadioButton withEnd_Button;
        private System.Windows.Forms.RadioButton noEnd_Button;
        private System.Windows.Forms.Label begin_Label;
        private System.Windows.Forms.TableLayoutPanel cafe_Table;
        private System.Windows.Forms.RadioButton cafe_Button;
        private System.Windows.Forms.RadioButton donalds_Button;
        private System.Windows.Forms.NumericUpDown wage_Box;
        private System.Windows.Forms.Label weekHours_Label;
        private System.Windows.Forms.SplitContainer av_Split;
        private System.Windows.Forms.FlowLayoutPanel av_TopFlow;
        private System.Windows.Forms.Button avSave_Button;
        private System.Windows.Forms.TableLayoutPanel availability_Table;
        private System.Windows.Forms.Label saturday_Label;
        private System.Windows.Forms.Label friday_Label;
        private System.Windows.Forms.Label thursday_Label;
        private System.Windows.Forms.Label wednesday_Label;
        private System.Windows.Forms.Label tuesday_Label;
        private System.Windows.Forms.DateTimePicker mondayStart_Picker;
        private System.Windows.Forms.Label monday_Label;
        private System.Windows.Forms.Label sunday_Label;
        private System.Windows.Forms.DateTimePicker sundayEnd_Picker;
        private System.Windows.Forms.DateTimePicker saturdayEnd_Picker;
        private System.Windows.Forms.DateTimePicker thursdayEnd_Picker;
        private System.Windows.Forms.DateTimePicker tuesdayEnd_Picker;
        private System.Windows.Forms.DateTimePicker wednesdayEnd_Picker;
        private System.Windows.Forms.DateTimePicker fridayEnd_Picker;
        private System.Windows.Forms.DateTimePicker mondayEnd_Picker;
        private System.Windows.Forms.DateTimePicker saturdayStart_Picker;
        private System.Windows.Forms.DateTimePicker wednesdayStart_Picker;
        private System.Windows.Forms.DateTimePicker tuesdayStart_Picker;
        private System.Windows.Forms.DateTimePicker fridayStart_Picker;
        private System.Windows.Forms.DateTimePicker thursdayStart_Picker;
        private System.Windows.Forms.DateTimePicker sundayStart_Picker;
        private System.Windows.Forms.PictureBox monday_PictureBox;
        private System.Windows.Forms.PictureBox tuesday_PictureBox;
        private System.Windows.Forms.PictureBox wednesday_PictureBox;
        private System.Windows.Forms.PictureBox thursday_PictureBox;
        private System.Windows.Forms.PictureBox friday_PictureBox;
        private System.Windows.Forms.PictureBox saturday_PictureBox;
        private System.Windows.Forms.PictureBox sunday_PictureBox;
        private System.Windows.Forms.RadioButton gvs_Button;
        private System.Windows.Forms.RadioButton jnt_Button;
        private System.Windows.Forms.DateTimePicker birthday_Picker;
        private System.Windows.Forms.DateTimePicker expire_Picker;
        private System.Windows.Forms.DateTimePicker contract_Picker;
    }
}
