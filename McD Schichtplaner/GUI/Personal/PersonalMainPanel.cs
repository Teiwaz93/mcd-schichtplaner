﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using McD_Schichtplaner.Database;

namespace McD_Schichtplaner.GUI.Personal
{
    /**
     * <summary>Panel, das alle Infos über die Mitarbeiter bereithält.</summary>
     * <remarks>
     * <para>Dazu ist es in zwei Spalten aufgeteilt. Die Linke Spalte beinhaltet eine Liste aller Mitarbeiter, aus der ein Mitarbeiter ausgewählt werden kann, für den direkt alle
     * direkten Infos angzeigt werden.</para>
     * <para>Die linke Spalte kann zwischen einem Kalender, in dem alle Wunschfrei und Urlaubstage eingetragen werdne können, und einer Ansicht für die Verfügungszeiten hin und her
     * gewechselt werden.</para></remarks>
     */
    public partial class PersonalMainPanel : UserControl
    {
        private Database.Database data;
        private WorkerDetails workerDetails;

        /**
         * <summary>einfacher Konstruktor</summary>
         */
        public PersonalMainPanel()
        {
            InitializeComponent();
            data = Database.Database.DB;
            filterBox.SelectedIndex = 0;
        }

        public PersonalMainPanel(Size size) : this()
        {
            Size = size;
            mainTableLayoutPanel.Size = Size;
            workerDetails = new WorkerDetails(new Size(Size.Width - workerListFlowPanel.Width, Size.Height));
            workerListFlowPanel.Height = Height - listControlTablePanel.Height;
            mainTableLayoutPanel.Controls.Add(workerDetails, 1, 0);
            mainTableLayoutPanel.SetRowSpan(workerDetails, 2);
            Console.WriteLine("Size of mainpanel: " + Size);
            Console.WriteLine("Size of table in mainpanel: " + mainTableLayoutPanel.Size);
            Console.WriteLine("Size of details: " + workerDetails.Size + "\nShould be: " + new Size(mainTableLayoutPanel.GetColumnWidths()[1], Size.Height));
            if (!(WorkerListEntry.Active == null)) workerDetails.Worker = WorkerListEntry.Active;
        }

        /**
         * Updatet die Mitarbeiter Liste entsprechend des ausgewählten filters.
         */
        private void UpdateList()
        {
            //mainTableLayoutPanel.GetControlFromPosition(1, 1).Controls.Clear();
            switch (filterBox.SelectedIndex)
            /* 0 Alle
             * 1 Crew
             * 2 VZ
             * 3 TZ
             * 4 GV
             * 5 Mgm
             * 6 minor
             * 7 inactive
             */
            {
                case 0:
                    FillWorkerList(data.GetWorkers());
                    break;
                case 1:
                    FillWorkerList(data.GetCrew());
                    break;
                case 2:
                    FillWorkerList(data.GetWorkers(Database.Database.Status.VZ));
                    break;
                case 3:
                    FillWorkerList(data.GetWorkers(Database.Database.Status.TZ));
                    break;
                case 4:
                    FillWorkerList(data.GetWorkers(Database.Database.Status.GV));
                    break;
                case 5:
                    FillWorkerList(data.GetWorkers(Database.Database.Status.Mgm));
                    break;
                case 6:
                    var list = data.GetWorkers();
                    var result = new List<Worker>();
                    foreach (var worker in list)
                    {
                        if (worker.IsMinor()) result.Add(worker);
                    }
                    FillWorkerList(result);
                    break;
                case 7:
                    FillWorkerList(data.GetWorkers(Database.Database.Status.inaktiv));
                    break;
                default: break;
            }
        }

        /**
         * <summary>Methode, die ausgeführt wird, sobald die auswahl im filter sich ändert.</summary>
         */
        private void FilterBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            /* 0 Alle
             * 1 Crew
             * 2 VZ
             * 3 TZ
             * 4 GV
             * 5 Mgm
             * 6 minor
             * 7 inactive
             */
            UpdateList();
        }

        /**
         * <summary>Zeigt alle Mitarbeiter aus gegebener Liste an.</summary>
         */
        private void FillWorkerList(List<Worker> list)
        {
            workerListFlowPanel.Controls.Clear();
            foreach (Worker w in list){
                WorkerListEntry entry = new WorkerListEntry(w);
                workerListFlowPanel.Controls.Add(entry);
            }
        }

        /**
         * <summary>Methode, die mit dem Hinzufügen Knopf verknüft ist.<para/>
         * Öffnet ein leeres Mitarbeiter fenster.</summary>
         */
        private void Add_Button_Click(object sender, EventArgs e)
        {
            var id  = data.CreateWorker();
            bool manualUpdate = filterBox.SelectedIndex == 7;
            filterBox.SelectedIndex = 7;
            if (manualUpdate) UpdateList();
            var workers = workerListFlowPanel.Controls;
            foreach (var worker in workers)
            {
                if (((WorkerListEntry)worker).Worker?.Id == id)
                {
                    ((WorkerListEntry)worker).SetActive();
                }
            }
        }

        /**
         * <summary>Methode, die mit dem Bearbeiten Knopf verknüpft ist.<para/>
         * Öffnet ein Mitarbeiter fenster, das mit den derzeitigen Informationen über den ausgewählten Mitarbeiter ausgefüllt ist.</summary>
         */
        private void Edit_Button_Click(object sender, EventArgs e)
        {
            long oldID = WorkerListEntry.Active.Id;
            WorkerForm wf = new WorkerForm(WorkerListEntry.Active);
            wf.ShowDialog();
            if (wf.DialogResult == DialogResult.OK)
            {
                if (oldID == wf.Worker.Id)
                {
                    data.UpdateWorker(wf.Worker);
                }
                else data.UpdateWorker(wf.Worker, oldID);
            }
            else
            UpdateList();
        }

        public void UpdateDetails(Worker worker)
        {
            workerDetails.Worker = worker;
        }

        private void Delete_Button_Click(object sender, EventArgs e)
        {
            data.SetStatus(WorkerListEntry.Active, Database.Database.Status.inaktiv);
            workerDetails.DeleteStatus();
            UpdateList();
        }

        public void SaveDetails()
        {
            workerDetails.SaveDetails();
        }
    }

}
