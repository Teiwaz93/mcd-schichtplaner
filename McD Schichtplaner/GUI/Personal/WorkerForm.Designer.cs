﻿namespace McD_Schichtplaner.GUI.Personal
{
    partial class WorkerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.id_Label = new System.Windows.Forms.Label();
            this.cafe_FlowLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.donalds_RadioButton = new System.Windows.Forms.RadioButton();
            this.cafe_RadioButton = new System.Windows.Forms.RadioButton();
            this.abort_Button = new System.Windows.Forms.Button();
            this.ok_Button = new System.Windows.Forms.Button();
            this.wage_Label = new System.Windows.Forms.Label();
            this.mail_TextBox = new System.Windows.Forms.TextBox();
            this.mail_Label = new System.Windows.Forms.Label();
            this.name_Label = new System.Windows.Forms.Label();
            this.name_TextBox = new System.Windows.Forms.TextBox();
            this.wage_Box = new System.Windows.Forms.NumericUpDown();
            this.statusFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.vz_RadioButton = new System.Windows.Forms.RadioButton();
            this.tz_RadioButton = new System.Windows.Forms.RadioButton();
            this.gv_RadioButton = new System.Windows.Forms.RadioButton();
            this.mgm_RadioButton = new System.Windows.Forms.RadioButton();
            this.weekHours_Label = new System.Windows.Forms.Label();
            this.weekHours_Box = new System.Windows.Forms.NumericUpDown();
            this.contract_FlowLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.begin_Label = new System.Windows.Forms.Label();
            this.start_Picker = new System.Windows.Forms.DateTimePicker();
            this.noExpiration_Box = new System.Windows.Forms.CheckBox();
            this.expiration_Label = new System.Windows.Forms.Label();
            this.expiration_Picker = new System.Windows.Forms.DateTimePicker();
            this.id_Box = new System.Windows.Forms.NumericUpDown();
            this.birthday_Panel = new System.Windows.Forms.Panel();
            this.birthday_Label = new System.Windows.Forms.Label();
            this.birthday_Picker = new System.Windows.Forms.DateTimePicker();
            this.tableLayoutPanel.SuspendLayout();
            this.cafe_FlowLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.wage_Box)).BeginInit();
            this.statusFlowLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weekHours_Box)).BeginInit();
            this.contract_FlowLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.id_Box)).BeginInit();
            this.birthday_Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.Controls.Add(this.id_Label, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.cafe_FlowLayout, 0, 5);
            this.tableLayoutPanel.Controls.Add(this.abort_Button, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.ok_Button, 0, 6);
            this.tableLayoutPanel.Controls.Add(this.wage_Label, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.mail_TextBox, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.mail_Label, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.name_Label, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.name_TextBox, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.wage_Box, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.statusFlowLayoutPanel, 0, 4);
            this.tableLayoutPanel.Controls.Add(this.contract_FlowLayout, 1, 5);
            this.tableLayoutPanel.Controls.Add(this.id_Box, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.birthday_Panel, 1, 4);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 7;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.629603F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.63408F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.629603F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.629603F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.75451F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.093F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.629603F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(541, 624);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // id_Label
            // 
            this.id_Label.AutoSize = true;
            this.id_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.id_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.id_Label.Location = new System.Drawing.Point(3, 53);
            this.id_Label.Name = "id_Label";
            this.id_Label.Size = new System.Drawing.Size(264, 53);
            this.id_Label.TabIndex = 21;
            this.id_Label.Text = "Nummer:";
            this.id_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cafe_FlowLayout
            // 
            this.cafe_FlowLayout.Controls.Add(this.donalds_RadioButton);
            this.cafe_FlowLayout.Controls.Add(this.cafe_RadioButton);
            this.cafe_FlowLayout.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.cafe_FlowLayout.Location = new System.Drawing.Point(3, 450);
            this.cafe_FlowLayout.Name = "cafe_FlowLayout";
            this.cafe_FlowLayout.Size = new System.Drawing.Size(264, 113);
            this.cafe_FlowLayout.TabIndex = 6;
            // 
            // donalds_RadioButton
            // 
            this.donalds_RadioButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.donalds_RadioButton.Checked = true;
            this.donalds_RadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donalds_RadioButton.Location = new System.Drawing.Point(15, 5);
            this.donalds_RadioButton.Margin = new System.Windows.Forms.Padding(15, 5, 15, 5);
            this.donalds_RadioButton.Name = "donalds_RadioButton";
            this.donalds_RadioButton.Size = new System.Drawing.Size(105, 38);
            this.donalds_RadioButton.TabIndex = 2;
            this.donalds_RadioButton.TabStop = true;
            this.donalds_RadioButton.Text = "McDonald\'s";
            this.donalds_RadioButton.UseVisualStyleBackColor = true;
            // 
            // cafe_RadioButton
            // 
            this.cafe_RadioButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.cafe_RadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cafe_RadioButton.Location = new System.Drawing.Point(15, 53);
            this.cafe_RadioButton.Margin = new System.Windows.Forms.Padding(15, 5, 15, 5);
            this.cafe_RadioButton.Name = "cafe_RadioButton";
            this.cafe_RadioButton.Size = new System.Drawing.Size(114, 38);
            this.cafe_RadioButton.TabIndex = 3;
            this.cafe_RadioButton.TabStop = true;
            this.cafe_RadioButton.Text = "McCafé";
            this.cafe_RadioButton.UseVisualStyleBackColor = true;
            // 
            // abort_Button
            // 
            this.abort_Button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.abort_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.abort_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.abort_Button.Location = new System.Drawing.Point(3, 569);
            this.abort_Button.Name = "abort_Button";
            this.abort_Button.Size = new System.Drawing.Size(264, 52);
            this.abort_Button.TabIndex = 17;
            this.abort_Button.Text = "Abbrechen";
            this.abort_Button.UseVisualStyleBackColor = false;
            this.abort_Button.Click += new System.EventHandler(this.Abort_Button_Click);
            // 
            // ok_Button
            // 
            this.ok_Button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.ok_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ok_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ok_Button.Location = new System.Drawing.Point(273, 569);
            this.ok_Button.Name = "ok_Button";
            this.ok_Button.Size = new System.Drawing.Size(265, 52);
            this.ok_Button.TabIndex = 16;
            this.ok_Button.Text = "OK";
            this.ok_Button.UseVisualStyleBackColor = false;
            this.ok_Button.Click += new System.EventHandler(this.Ok_Button_Click);
            // 
            // wage_Label
            // 
            this.wage_Label.AutoSize = true;
            this.wage_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wage_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wage_Label.Location = new System.Drawing.Point(3, 159);
            this.wage_Label.Name = "wage_Label";
            this.wage_Label.Size = new System.Drawing.Size(264, 53);
            this.wage_Label.TabIndex = 8;
            this.wage_Label.Text = "Stundenlohn:";
            this.wage_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mail_TextBox
            // 
            this.mail_TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.mail_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mail_TextBox.Location = new System.Drawing.Point(280, 119);
            this.mail_TextBox.Margin = new System.Windows.Forms.Padding(10);
            this.mail_TextBox.Name = "mail_TextBox";
            this.mail_TextBox.Size = new System.Drawing.Size(251, 26);
            this.mail_TextBox.TabIndex = 2;
            // 
            // mail_Label
            // 
            this.mail_Label.AutoSize = true;
            this.mail_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mail_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mail_Label.Location = new System.Drawing.Point(3, 106);
            this.mail_Label.Name = "mail_Label";
            this.mail_Label.Size = new System.Drawing.Size(264, 53);
            this.mail_Label.TabIndex = 2;
            this.mail_Label.Text = "E-Mail:";
            this.mail_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // name_Label
            // 
            this.name_Label.AutoSize = true;
            this.name_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_Label.Location = new System.Drawing.Point(3, 0);
            this.name_Label.Name = "name_Label";
            this.name_Label.Size = new System.Drawing.Size(264, 53);
            this.name_Label.TabIndex = 0;
            this.name_Label.Text = "Name:";
            this.name_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // name_TextBox
            // 
            this.name_TextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.name_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_TextBox.Location = new System.Drawing.Point(280, 13);
            this.name_TextBox.Margin = new System.Windows.Forms.Padding(10);
            this.name_TextBox.Name = "name_TextBox";
            this.name_TextBox.Size = new System.Drawing.Size(251, 26);
            this.name_TextBox.TabIndex = 0;
            // 
            // wage_Box
            // 
            this.wage_Box.DecimalPlaces = 2;
            this.wage_Box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wage_Box.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.wage_Box.Location = new System.Drawing.Point(280, 179);
            this.wage_Box.Margin = new System.Windows.Forms.Padding(10, 20, 10, 20);
            this.wage_Box.Name = "wage_Box";
            this.wage_Box.Size = new System.Drawing.Size(251, 20);
            this.wage_Box.TabIndex = 3;
            // 
            // statusFlowLayoutPanel
            // 
            this.statusFlowLayoutPanel.Controls.Add(this.vz_RadioButton);
            this.statusFlowLayoutPanel.Controls.Add(this.tz_RadioButton);
            this.statusFlowLayoutPanel.Controls.Add(this.gv_RadioButton);
            this.statusFlowLayoutPanel.Controls.Add(this.mgm_RadioButton);
            this.statusFlowLayoutPanel.Controls.Add(this.weekHours_Label);
            this.statusFlowLayoutPanel.Controls.Add(this.weekHours_Box);
            this.statusFlowLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.statusFlowLayoutPanel.Location = new System.Drawing.Point(3, 215);
            this.statusFlowLayoutPanel.Name = "statusFlowLayoutPanel";
            this.statusFlowLayoutPanel.Size = new System.Drawing.Size(264, 229);
            this.statusFlowLayoutPanel.TabIndex = 4;
            // 
            // vz_RadioButton
            // 
            this.vz_RadioButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.vz_RadioButton.Checked = true;
            this.vz_RadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vz_RadioButton.Location = new System.Drawing.Point(15, 5);
            this.vz_RadioButton.Margin = new System.Windows.Forms.Padding(15, 5, 15, 5);
            this.vz_RadioButton.Name = "vz_RadioButton";
            this.vz_RadioButton.Size = new System.Drawing.Size(256, 30);
            this.vz_RadioButton.TabIndex = 1;
            this.vz_RadioButton.TabStop = true;
            this.vz_RadioButton.Text = "VZ";
            this.vz_RadioButton.UseVisualStyleBackColor = true;
            this.vz_RadioButton.CheckedChanged += new System.EventHandler(this.StatusChanged);
            // 
            // tz_RadioButton
            // 
            this.tz_RadioButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.tz_RadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tz_RadioButton.Location = new System.Drawing.Point(15, 45);
            this.tz_RadioButton.Margin = new System.Windows.Forms.Padding(15, 5, 15, 5);
            this.tz_RadioButton.Name = "tz_RadioButton";
            this.tz_RadioButton.Size = new System.Drawing.Size(256, 30);
            this.tz_RadioButton.TabIndex = 1;
            this.tz_RadioButton.Text = "TZ";
            this.tz_RadioButton.UseVisualStyleBackColor = true;
            this.tz_RadioButton.CheckedChanged += new System.EventHandler(this.StatusChanged);
            // 
            // gv_RadioButton
            // 
            this.gv_RadioButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.gv_RadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gv_RadioButton.Location = new System.Drawing.Point(15, 85);
            this.gv_RadioButton.Margin = new System.Windows.Forms.Padding(15, 5, 15, 5);
            this.gv_RadioButton.Name = "gv_RadioButton";
            this.gv_RadioButton.Size = new System.Drawing.Size(256, 30);
            this.gv_RadioButton.TabIndex = 3;
            this.gv_RadioButton.Text = "GV";
            this.gv_RadioButton.UseVisualStyleBackColor = true;
            this.gv_RadioButton.CheckedChanged += new System.EventHandler(this.StatusChanged);
            // 
            // mgm_RadioButton
            // 
            this.mgm_RadioButton.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.mgm_RadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mgm_RadioButton.Location = new System.Drawing.Point(15, 125);
            this.mgm_RadioButton.Margin = new System.Windows.Forms.Padding(15, 5, 15, 5);
            this.mgm_RadioButton.Name = "mgm_RadioButton";
            this.mgm_RadioButton.Size = new System.Drawing.Size(256, 30);
            this.mgm_RadioButton.TabIndex = 4;
            this.mgm_RadioButton.Text = "Management";
            this.mgm_RadioButton.UseVisualStyleBackColor = true;
            this.mgm_RadioButton.CheckedChanged += new System.EventHandler(this.StatusChanged);
            // 
            // weekHours_Label
            // 
            this.weekHours_Label.AutoSize = true;
            this.weekHours_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.weekHours_Label.Location = new System.Drawing.Point(3, 175);
            this.weekHours_Label.Margin = new System.Windows.Forms.Padding(3, 15, 3, 0);
            this.weekHours_Label.Name = "weekHours_Label";
            this.weekHours_Label.Size = new System.Drawing.Size(115, 17);
            this.weekHours_Label.TabIndex = 6;
            this.weekHours_Label.Text = "Wochenstunden:";
            // 
            // weekHours_Box
            // 
            this.weekHours_Box.Location = new System.Drawing.Point(3, 195);
            this.weekHours_Box.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.weekHours_Box.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.weekHours_Box.Name = "weekHours_Box";
            this.weekHours_Box.Size = new System.Drawing.Size(135, 20);
            this.weekHours_Box.TabIndex = 0;
            this.weekHours_Box.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            this.weekHours_Box.ValueChanged += new System.EventHandler(this.WeekHours_Box_ValueChanged);
            // 
            // contract_FlowLayout
            // 
            this.contract_FlowLayout.Controls.Add(this.begin_Label);
            this.contract_FlowLayout.Controls.Add(this.start_Picker);
            this.contract_FlowLayout.Controls.Add(this.noExpiration_Box);
            this.contract_FlowLayout.Controls.Add(this.expiration_Label);
            this.contract_FlowLayout.Controls.Add(this.expiration_Picker);
            this.contract_FlowLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.contract_FlowLayout.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.contract_FlowLayout.Location = new System.Drawing.Point(273, 450);
            this.contract_FlowLayout.Name = "contract_FlowLayout";
            this.contract_FlowLayout.Size = new System.Drawing.Size(265, 113);
            this.contract_FlowLayout.TabIndex = 7;
            // 
            // begin_Label
            // 
            this.begin_Label.AutoSize = true;
            this.begin_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.begin_Label.Location = new System.Drawing.Point(3, 0);
            this.begin_Label.Name = "begin_Label";
            this.begin_Label.Size = new System.Drawing.Size(99, 17);
            this.begin_Label.TabIndex = 0;
            this.begin_Label.Text = "Arbeitsbeginn:";
            // 
            // start_Picker
            // 
            this.start_Picker.Location = new System.Drawing.Point(3, 20);
            this.start_Picker.Name = "start_Picker";
            this.start_Picker.Size = new System.Drawing.Size(200, 20);
            this.start_Picker.TabIndex = 0;
            // 
            // noExpiration_Box
            // 
            this.noExpiration_Box.AutoSize = true;
            this.noExpiration_Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noExpiration_Box.Location = new System.Drawing.Point(3, 46);
            this.noExpiration_Box.Name = "noExpiration_Box";
            this.noExpiration_Box.Size = new System.Drawing.Size(96, 21);
            this.noExpiration_Box.TabIndex = 2;
            this.noExpiration_Box.Text = "Unbefristet";
            this.noExpiration_Box.UseVisualStyleBackColor = true;
            this.noExpiration_Box.CheckedChanged += new System.EventHandler(this.NoExpiration_Box_CheckedChanged);
            // 
            // expiration_Label
            // 
            this.expiration_Label.AutoSize = true;
            this.expiration_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.expiration_Label.Location = new System.Drawing.Point(3, 70);
            this.expiration_Label.Name = "expiration_Label";
            this.expiration_Label.Size = new System.Drawing.Size(86, 17);
            this.expiration_Label.TabIndex = 3;
            this.expiration_Label.Text = "Befristet bis:";
            // 
            // expiration_Picker
            // 
            this.expiration_Picker.Location = new System.Drawing.Point(3, 90);
            this.expiration_Picker.Name = "expiration_Picker";
            this.expiration_Picker.Size = new System.Drawing.Size(200, 20);
            this.expiration_Picker.TabIndex = 3;
            // 
            // id_Box
            // 
            this.id_Box.Dock = System.Windows.Forms.DockStyle.Fill;
            this.id_Box.Location = new System.Drawing.Point(280, 73);
            this.id_Box.Margin = new System.Windows.Forms.Padding(10, 20, 10, 20);
            this.id_Box.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.id_Box.Name = "id_Box";
            this.id_Box.Size = new System.Drawing.Size(251, 20);
            this.id_Box.TabIndex = 1;
            this.id_Box.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // birthday_Panel
            // 
            this.birthday_Panel.Controls.Add(this.birthday_Label);
            this.birthday_Panel.Controls.Add(this.birthday_Picker);
            this.birthday_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.birthday_Panel.Location = new System.Drawing.Point(273, 215);
            this.birthday_Panel.Name = "birthday_Panel";
            this.birthday_Panel.Size = new System.Drawing.Size(265, 229);
            this.birthday_Panel.TabIndex = 5;
            // 
            // birthday_Label
            // 
            this.birthday_Label.AutoSize = true;
            this.birthday_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.birthday_Label.Location = new System.Drawing.Point(95, 55);
            this.birthday_Label.Name = "birthday_Label";
            this.birthday_Label.Size = new System.Drawing.Size(94, 20);
            this.birthday_Label.TabIndex = 24;
            this.birthday_Label.Text = "Geburtstag;";
            // 
            // birthday_Picker
            // 
            this.birthday_Picker.Location = new System.Drawing.Point(39, 125);
            this.birthday_Picker.Name = "birthday_Picker";
            this.birthday_Picker.Size = new System.Drawing.Size(200, 20);
            this.birthday_Picker.TabIndex = 0;
            // 
            // WorkerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 624);
            this.Controls.Add(this.tableLayoutPanel);
            this.Name = "WorkerForm";
            this.Text = "Neuer Mitarbeiter";
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            this.cafe_FlowLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.wage_Box)).EndInit();
            this.statusFlowLayoutPanel.ResumeLayout(false);
            this.statusFlowLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.weekHours_Box)).EndInit();
            this.contract_FlowLayout.ResumeLayout(false);
            this.contract_FlowLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.id_Box)).EndInit();
            this.birthday_Panel.ResumeLayout(false);
            this.birthday_Panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label wage_Label;
        private System.Windows.Forms.TextBox mail_TextBox;
        private System.Windows.Forms.Label mail_Label;
        private System.Windows.Forms.Label name_Label;
        private System.Windows.Forms.TextBox name_TextBox;
        private System.Windows.Forms.FlowLayoutPanel statusFlowLayoutPanel;
        private System.Windows.Forms.RadioButton vz_RadioButton;
        private System.Windows.Forms.RadioButton tz_RadioButton;
        private System.Windows.Forms.RadioButton gv_RadioButton;
        private System.Windows.Forms.RadioButton mgm_RadioButton;
        private System.Windows.Forms.Button abort_Button;
        private System.Windows.Forms.Button ok_Button;
        private System.Windows.Forms.FlowLayoutPanel cafe_FlowLayout;
        private System.Windows.Forms.RadioButton donalds_RadioButton;
        private System.Windows.Forms.RadioButton cafe_RadioButton;
        private System.Windows.Forms.NumericUpDown wage_Box;
        private System.Windows.Forms.Label weekHours_Label;
        private System.Windows.Forms.NumericUpDown weekHours_Box;
        private System.Windows.Forms.FlowLayoutPanel contract_FlowLayout;
        private System.Windows.Forms.Label begin_Label;
        private System.Windows.Forms.DateTimePicker start_Picker;
        private System.Windows.Forms.CheckBox noExpiration_Box;
        private System.Windows.Forms.Label expiration_Label;
        private System.Windows.Forms.DateTimePicker expiration_Picker;
        private System.Windows.Forms.Label id_Label;
        private System.Windows.Forms.NumericUpDown id_Box;
        private System.Windows.Forms.Panel birthday_Panel;
        private System.Windows.Forms.Label birthday_Label;
        private System.Windows.Forms.DateTimePicker birthday_Picker;
    }
}