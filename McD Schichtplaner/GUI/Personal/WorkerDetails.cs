﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using McD_Schichtplaner.Database;
using System.Text.RegularExpressions;

namespace McD_Schichtplaner.GUI.Personal
{
    public partial class WorkerDetails : UserControl
    {
        private static int year = DateTime.Now.Year;
        private List<Label> dayLabels = new List<Label>();
        private Worker worker;
        private Database.Database data;

        private Bitmap[] availabilityBitMaps = new Bitmap[7];
        private long minticks = new TimeSpan(6, 0, 0).Ticks;
        private long ticks = new TimeSpan(30, 0, 0).Ticks - new TimeSpan(6, 0, 0).Ticks;

        public Worker Worker
        {
            get => worker;
            set
            {
                if (worker == value) return;
                if (worker != null) UpdateWorker(value);
            }
        }
        public WorkerDetails()
        {
            InitializeComponent();
            year_Selection.Value = year;
            year_Selection.ValueChanged += Year_Selection_ValueChanged;

            data = Database.Database.DB;
            Visible = false;
            free_Button.Checked = true;

            for (int d = 1; d <= 31; d++)
            {
                for (int m = 1; m <= 12; m++)
                {
                    Label day = new Label()
                    {
                        Name = m + "_Label_" + d,
                        TextAlign = ContentAlignment.MiddleCenter,
                        Text = ""
                    };
                    day.Click += Label_Click;
                    try
                    {
                        DayOfWeek dow = new DateTime(year, m, d).DayOfWeek;
                        if (dow == DayOfWeek.Saturday || dow == DayOfWeek.Sunday) day.BackColor = Color.LemonChiffon;
                        else day.BackColor = Color.Transparent;
                        day.Enabled = true;
                    }
                    catch (ArgumentOutOfRangeException aore)
                    {
                        day.BackColor = Color.DarkGray;
                        day.Enabled = false;
                    }
                    dayLabels.Add(day);
                    calendar_Table.Controls.Add(day, m, d);
                }
            }

            Visible = true;
        }

        public WorkerDetails(Size size) : this()
        {
            Size = size;
        }

        private void UpdateWorker(Worker w)
        {
            if (!(worker is null)) SaveDetails();   
            worker = w;
            RefillDays();
            ReFillDetails();
            RefillAvailability();
        }

        private void Year_Selection_ValueChanged(object sender, EventArgs e)
        {
            year = (int)year_Selection.Value;

            foreach (var day in dayLabels)
            {
                int d, m;
                string[] match = Regex.Split(day.Name, @"\D+");
                m = Int32.Parse(match[0]);
                d = Int32.Parse(match[1]);
                try
                {
                    DayOfWeek dow = new DateTime(year, m, d).DayOfWeek;
                    if (dow == DayOfWeek.Saturday || dow == DayOfWeek.Sunday) day.BackColor = Color.LemonChiffon;
                    else day.BackColor = Color.Transparent;
                    day.Enabled = true;
                }
                catch (ArgumentOutOfRangeException aore)
                {
                    day.BackColor = Color.DarkGray;
                    day.Enabled = false;
                }
            }
            RefillDays();
        }

        private void ReFillDetails()
        {
            if (worker is null) return;
            number_Box.Value = worker.Id;
            name_Box.Text = worker.Name;
            mail_Box.Text = worker.Mail;
            birthday_Picker.Value = worker.Birthday;
            switch (worker.Status)
            {
                case Database.Database.Status.GV:
                    gv_Button.Checked = true;
                    break;
                case Database.Database.Status.GVS:
                    gvs_Button.Checked = true;
                    break;
                case Database.Database.Status.Mgm:
                    mgm_Button.Checked = true;
                    break;
                case Database.Database.Status.TZ:
                    tz_Button.Checked = true;
                    break;
                case Database.Database.Status.VZ:
                    vz_Button.Checked = true;
                    break;
                case Database.Database.Status.Hsm:
                    jnt_Button.Checked = true;
                    break;
                case Database.Database.Status.inaktiv:
                    gv_Button.Checked = false;
                    mgm_Button.Checked = false;
                    tz_Button.Checked = false;
                    vz_Button.Checked = false;
                    break;
            }
            weekHours_Box.Value = worker.WeekHours;
            wage_Box.Value = (decimal)worker.Wage;
            if (worker.Cafe) cafe_Button.Checked = true;
            else donalds_Button.Checked = true;
            contract_Picker.Value = worker.Contract;
            if (worker.DoesExpire())
            {
                withEnd_Button.Checked = true;
                expire_Picker.Value = worker.Expires;
            }
            else noEnd_Button.Checked = true;

            breakfast_CheckBox.Checked = false;
            cafe_CheckBox.Checked = false;
            counter_CheckBox.Checked = false;
            drive_CheckBox.Checked = false;
            kitchen_CheckBox.Checked = false;
            service_CheckBox.Checked = false;

            var jobs = data.GetWorkerJobs(worker);
            foreach (var job in jobs)
            {
                switch (job)
                {
                    case Database.Database.Job.breakfast:
                        breakfast_CheckBox.Checked = true;
                        break;
                    case Database.Database.Job.cafe:
                        cafe_CheckBox.Checked = true;
                        break;
                    case Database.Database.Job.counter:
                        counter_CheckBox.Checked = true;
                        break;
                    case Database.Database.Job.drive:
                        drive_CheckBox.Checked = true;
                        break;
                    case Database.Database.Job.kitchen:
                        kitchen_CheckBox.Checked = true;
                        break;
                    case Database.Database.Job.service:
                        service_CheckBox.Checked = true;
                        break;
                }
            }
        }

        private void RefillDays()
        {
            //Resetting
            foreach (var day in dayLabels)
            {
                day.Text = "";
            }
            var dates = data.GetFrees(worker, year);
            foreach (var date in dates)
            {
                Label label = calendar_Table.GetControlFromPosition(date.Item2.Month, date.Item2.Day) as Label;
                label.Text = date.Item1;
            }
        }

        private void RefillAvailability()
        {
            if (worker is null) return;

            var times = data.GetAvailabilities(worker);
            int day = 0;

            mondayStart_Picker.Value = DateTime.Today.Add(times.Times[day].Item1);
            mondayEnd_Picker.Value = DateTime.Today.Add(times.Times[day++].Item2);

            tuesdayStart_Picker.Value = DateTime.Today.Add(times.Times[day].Item1);
            tuesdayEnd_Picker.Value = DateTime.Today.Add(times.Times[day++].Item2);

            wednesdayStart_Picker.Value = DateTime.Today.Add(times.Times[day].Item1);
            wednesdayEnd_Picker.Value = DateTime.Today.Add(times.Times[day++].Item2);

            thursdayStart_Picker.Value = DateTime.Today.Add(times.Times[day].Item1);
            thursdayEnd_Picker.Value = DateTime.Today.Add(times.Times[day++].Item2);

            fridayStart_Picker.Value = DateTime.Today.Add(times.Times[day].Item1);
            fridayEnd_Picker.Value = DateTime.Today.Add(times.Times[day++].Item2);

            saturdayStart_Picker.Value = DateTime.Today.Add(times.Times[day].Item1);
            saturdayEnd_Picker.Value = DateTime.Today.Add(times.Times[day++].Item2);

            sundayStart_Picker.Value = DateTime.Today.Add(times.Times[day].Item1);
            sundayEnd_Picker.Value = DateTime.Today.Add(times.Times[day++].Item2);
        }

        private void Label_Click(object sender, EventArgs e)
        {
            if (worker is null) return;
            Label day = sender as Label;
            int d, m;
            string[] match = Regex.Split(day.Name, @"\D+");
            m = Int32.Parse(match[0]);
            d = Int32.Parse(match[1]);
            DateTime date = new DateTime(year, m, d);

            switch (day.Text)
            {
                case "U":
                    if (free_Button.Checked) ;
                    else if (holiday_Button.Checked)
                    {
                        data.UnsetFree(worker, date);
                        day.Text = "";
                    }
                    break;
                case "W":
                    if (free_Button.Checked)
                    {
                        data.UnsetFree(worker, date);
                        day.Text = "";
                    }
                    else if (holiday_Button.Checked)
                    {
                        data.UnsetFree(worker, date);
                        data.SetFree(worker, date, "U");
                        day.Text = "U";
                    }
                    break;
                case "K": break;
                case "BS": break;
                case "S":
                case "KUG":
                    if (holiday_Button.Checked)
                    {
                        data.UnsetFree(worker, date);
                        data.SetFree(worker, date, "U");
                        day.Text = "U";
                    }
                    break;
                default:
                    if (free_Button.Checked)
                    { 
                        data.SetFree(worker, year, m, d, "W");
                        day.Text = "W";
                    }
                    else if (holiday_Button.Checked)
                    {
                        data.SetFree(worker, year, m, d, "U");
                        day.Text = "U";
                    }
                    break;
            }
        }


        public void SaveDetails(object sender, EventArgs e)
        { 
            SaveDetails();
        }

        public void SaveDetails()
        {
            if (worker is null) return;
            long id = worker.Id;
            worker.Name = name_Box.Text;
            if (mail_Box.Text != "")
            {
                if (Database.Database.CheckMailValidity(mail_Box.Text)) worker.Mail = mail_Box.Text;
                else MessageBox.Show("Unkorrekte Mail eingegeben, nutze weiterhin alte Addresse.");
            }
            else worker.Mail = mail_Box.Text;
            worker.Id = (long)number_Box.Value;
            worker.Birthday = birthday_Picker.Value.Date;
            worker.WeekHours = (int)weekHours_Box.Value;
            worker.Wage = (double)wage_Box.Value;
            worker.Contract = contract_Picker.Value.Date;
            if (noEnd_Button.Checked) worker.Expires = new DateTime();
            else if (withEnd_Button.Checked)
            {
                if (expire_Picker.Value.Date - DateTime.Now < new TimeSpan(0, 0, 0) && worker.Status != Database.Database.Status.inaktiv)
                {
                    MessageBox.Show("Vertrag kann nicht vor 'heute' ablaufen. Wird nicht gespeichert ...");
                    return;
                }
                else worker.Expires = expire_Picker.Value.Date;
            }
            else
            {
                MessageBox.Show("Ungültiver Zustand (weder befristet noch unbefristet, eines von beiden muss wahr sein).");
                return;
            }
            if (donalds_Button.Checked) worker.Cafe = false;
            else if (cafe_Button.Checked) worker.Cafe = true;
            else
            {
                MessageBox.Show("Ungültiver Zustand (weder Cafe noch Donalds, eines von beiden muss wahr sein).");
                return;
            }
            Database.Database.Status status = worker.Status;
            if (vz_Button.Checked) status = Database.Database.Status.VZ;
            else if (tz_Button.Checked) status = Database.Database.Status.TZ;
            else if (gv_Button.Checked) status = Database.Database.Status.GV;
            else if (mgm_Button.Checked) status = Database.Database.Status.Mgm;
            else if (gvs_Button.Checked) status = Database.Database.Status.GVS;
            else if (jnt_Button.Checked) status = Database.Database.Status.Hsm;
            else status = Database.Database.Status.inaktiv;
            worker.Status = status;

            List<Database.Database.Job> jobs = new List<Database.Database.Job>();
            if (kitchen_CheckBox.Checked) jobs.Add(Database.Database.Job.kitchen);
            if (drive_CheckBox.Checked) jobs.Add(Database.Database.Job.drive);
            if (counter_CheckBox.Checked) jobs.Add(Database.Database.Job.counter);
            if (service_CheckBox.Checked) jobs.Add(Database.Database.Job.service);
            if (cafe_CheckBox.Checked) jobs.Add(Database.Database.Job.cafe);
            if (breakfast_CheckBox.Checked) jobs.Add(Database.Database.Job.breakfast);

            data.UpdateWorker(worker, id);
            data.ReSetWorkerJobs(worker, jobs);
            WorkerListEntry.RereadDB(worker.Id);

            //enable_CheckBox.Checked = false;

            Console.WriteLine($"{worker.Name} gespeichert");
        }

        private void StatusCheckChanged(object sender, EventArgs e)
        {
            weekHours_Box.Enabled = true;
            var button = sender as RadioButton;
            if (button.Checked)
            {
                if (button.Name == "vz_Button" && weekHours_Box.Value < 39) weekHours_Box.Value = 39;
                else if (button.Name == "tz_Button" && weekHours_Box.Value > 38) weekHours_Box.Value = 38;
                else if (button.Name == "tz_Button" && weekHours_Box.Value < 16) weekHours_Box.Value = 16;
                else if (button.Name == "gv_Button" && weekHours_Box.Value > 15) weekHours_Box.Value = 15;
                else if (button.Name == "gvs_Button")
                {
                    weekHours_Box.Value = 20;
                    weekHours_Box.Enabled = false;
                }
                else if (button.Name == "jnt_Button")
                {
                    weekHours_Box.Value = 39;
                    weekHours_Box.Enabled = false;
                }
            }
        }

        private void WeekHours_Box_ValueChanged(object sender, EventArgs e)
        {
            if (worker.Status == Database.Database.Status.inaktiv) return;
            if (mgm_Button.Checked || gvs_Button.Checked || jnt_Button.Checked) return;
            if (weekHours_Box.Value > 38) vz_Button.Checked = true;
            else if (weekHours_Box.Value > 15 && weekHours_Box.Value < 39) tz_Button.Checked = true;
            else if (weekHours_Box.Value < 16) gv_Button.Checked = true;
        }

        private void AvSave_Button_Click(object sender, EventArgs e)
        {
            var av = new Availabilities(worker);
            int d = 0;
            av.Times[d++] = new Tuple<TimeSpan, TimeSpan>(mondayStart_Picker.Value.TimeOfDay, (mondayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? mondayEnd_Picker.Value.TimeOfDay : mondayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0))));
            av.Times[d++] = new Tuple<TimeSpan, TimeSpan>(tuesdayStart_Picker.Value.TimeOfDay, (tuesdayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? tuesdayEnd_Picker.Value.TimeOfDay : tuesdayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0))));
            av.Times[d++] = new Tuple<TimeSpan, TimeSpan>(wednesdayStart_Picker.Value.TimeOfDay, (wednesdayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? wednesdayEnd_Picker.Value.TimeOfDay : wednesdayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0))));
            av.Times[d++] = new Tuple<TimeSpan, TimeSpan>(thursdayStart_Picker.Value.TimeOfDay, (thursdayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? thursdayEnd_Picker.Value.TimeOfDay : thursdayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0))));
            av.Times[d++] = new Tuple<TimeSpan, TimeSpan>(fridayStart_Picker.Value.TimeOfDay, (fridayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? fridayEnd_Picker.Value.TimeOfDay : fridayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0))));
            av.Times[d++] = new Tuple<TimeSpan, TimeSpan>(saturdayStart_Picker.Value.TimeOfDay, (saturdayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? saturdayEnd_Picker.Value.TimeOfDay : saturdayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0))));
            av.Times[d++] = new Tuple<TimeSpan, TimeSpan>(sundayStart_Picker.Value.TimeOfDay, (sundayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? sundayEnd_Picker.Value.TimeOfDay : sundayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0))));
            data.SetAvailabilities(av);
        }

        private void MondayAvailabilityChanged(object sender, EventArgs args)
        {
            if (mondayStart_Picker.Value.TimeOfDay < new TimeSpan(6, 0, 0)) mondayStart_Picker.Value = DateTime.Today.AddHours(6);
            int width = monday_PictureBox.Size.Width;
            int height = monday_PictureBox.Size.Height;
            var image = new Bitmap(width, height);
            var graphics = Graphics.FromImage(image);
            var red = new SolidBrush(Color.Red);
            var green = new SolidBrush(Color.Green);

            graphics.FillRectangle(green, 0, 0, width, height);

            bool reason = mondayStart_Picker.Value.TimeOfDay.Ticks > double.MaxValue;

            double factor = (double)(mondayStart_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks;
            int y = (int)(factor * height);
            graphics.FillRectangle(red, 0, 0, width, y);

            factor = mondayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? (double)(mondayEnd_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks : (double)(mondayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0)).Ticks - minticks) / (double)ticks;
            y = (int)(factor * height);
            graphics.FillRectangle(red, 0, y, width, height);

            monday_PictureBox.Image = image;
        }
        private void TuesdayAvailabilityChanged(object sender, EventArgs args)
        {
            if (tuesdayStart_Picker.Value.TimeOfDay < new TimeSpan(6, 0, 0)) tuesdayStart_Picker.Value = DateTime.Today.AddHours(6);
            int width = tuesday_PictureBox.Size.Width;
            int height = tuesday_PictureBox.Size.Height;
            var image = new Bitmap(width, height);
            var graphics = Graphics.FromImage(image);
            var red = new SolidBrush(Color.Red);
            var green = new SolidBrush(Color.Green);

            graphics.FillRectangle(green, 0, 0, width, height);

            bool reason = tuesdayStart_Picker.Value.TimeOfDay.Ticks > double.MaxValue;

            double factor = (double)(tuesdayStart_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks;
            int y = (int)(factor * height);
            graphics.FillRectangle(red, 0, 0, width, y);

            factor = tuesdayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? (double)(tuesdayEnd_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks : (double)(tuesdayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0)).Ticks - minticks) / (double)ticks;
            y = (int)(factor * height);
            graphics.FillRectangle(red, 0, y, width, height);

            tuesday_PictureBox.Image = image;
        }
        private void WednesdayAvailabilityChanged(object sender, EventArgs args)
        {
            if (wednesdayStart_Picker.Value.TimeOfDay < new TimeSpan(6, 0, 0)) wednesdayStart_Picker.Value = DateTime.Today.AddHours(6);
            int width = wednesday_PictureBox.Size.Width;
            int height = wednesday_PictureBox.Size.Height;
            var image = new Bitmap(width, height);
            var graphics = Graphics.FromImage(image);
            var red = new SolidBrush(Color.Red);
            var green = new SolidBrush(Color.Green);

            graphics.FillRectangle(green, 0, 0, width, height);

            bool reason = wednesdayStart_Picker.Value.TimeOfDay.Ticks > double.MaxValue;

            double factor = (double)(wednesdayStart_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks;
            int y = (int)(factor * height);
            graphics.FillRectangle(red, 0, 0, width, y);

            factor = wednesdayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? (double)(wednesdayEnd_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks : (double)(wednesdayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0)).Ticks - minticks) / (double)ticks;
            y = (int)(factor * height);
            graphics.FillRectangle(red, 0, y, width, height);

            wednesday_PictureBox.Image = image;
        }
        private void ThursdayAvailabilityChanged(object sender, EventArgs args)
        {
            if (thursdayStart_Picker.Value.TimeOfDay < new TimeSpan(6, 0, 0)) thursdayStart_Picker.Value = DateTime.Today.AddHours(6);
            int width = thursday_PictureBox.Size.Width;
            int height = thursday_PictureBox.Size.Height;
            var image = new Bitmap(width, height);
            var graphics = Graphics.FromImage(image);
            var red = new SolidBrush(Color.Red);
            var green = new SolidBrush(Color.Green);

            graphics.FillRectangle(green, 0, 0, width, height);

            bool reason = thursdayStart_Picker.Value.TimeOfDay.Ticks > double.MaxValue;

            double factor = (double)(thursdayStart_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks;
            int y = (int)(factor * height);
            graphics.FillRectangle(red, 0, 0, width, y);

            factor = thursdayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? (double)(thursdayEnd_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks : (double)(thursdayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0)).Ticks - minticks) / (double)ticks;
            y = (int)(factor * height);
            graphics.FillRectangle(red, 0, y, width, height);

            thursday_PictureBox.Image = image;
        }
        private void FridayAvailabilityChanged(object sender, EventArgs args)
        {
            if (fridayStart_Picker.Value.TimeOfDay < new TimeSpan(6, 0, 0)) fridayStart_Picker.Value = DateTime.Today.AddHours(6);
            int width = friday_PictureBox.Size.Width;
            int height = friday_PictureBox.Size.Height;
            var image = new Bitmap(width, height);
            var graphics = Graphics.FromImage(image);
            var red = new SolidBrush(Color.Red);
            var green = new SolidBrush(Color.Green);

            graphics.FillRectangle(green, 0, 0, width, height);

            bool reason = fridayStart_Picker.Value.TimeOfDay.Ticks > double.MaxValue;

            double factor = (double)(fridayStart_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks;
            int y = (int)(factor * height);
            graphics.FillRectangle(red, 0, 0, width, y);

            factor = fridayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? (double)(fridayEnd_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks : (double)(fridayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0)).Ticks - minticks) / (double)ticks;
            y = (int)(factor * height);
            graphics.FillRectangle(red, 0, y, width, height);

            friday_PictureBox.Image = image;
        }
        private void SaturdayAvailabilityChanged(object sender, EventArgs args)
        {
            if (saturdayStart_Picker.Value.TimeOfDay < new TimeSpan(6, 0, 0)) saturdayStart_Picker.Value = DateTime.Today.AddHours(6);
            int width = saturday_PictureBox.Size.Width;
            int height = saturday_PictureBox.Size.Height;
            var image = new Bitmap(width, height);
            var graphics = Graphics.FromImage(image);
            var red = new SolidBrush(Color.Red);
            var green = new SolidBrush(Color.Green);

            graphics.FillRectangle(green, 0, 0, width, height);

            bool reason = saturdayStart_Picker.Value.TimeOfDay.Ticks > double.MaxValue;

            double factor = (double)(saturdayStart_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks;
            int y = (int)(factor * height);
            graphics.FillRectangle(red, 0, 0, width, y);

            factor = saturdayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? (double)(saturdayEnd_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks : (double)(saturdayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0)).Ticks - minticks) / (double)ticks;
            y = (int)(factor * height);
            graphics.FillRectangle(red, 0, y, width, height);

            saturday_PictureBox.Image = image;
        }
        private void SundayAvailabilityChanged(object sender, EventArgs args)
        {
            if (sundayStart_Picker.Value.TimeOfDay < new TimeSpan(6, 0, 0)) sundayStart_Picker.Value = DateTime.Today.AddHours(6);
            int width = sunday_PictureBox.Size.Width;
            int height = sunday_PictureBox.Size.Height;
            var image = new Bitmap(width, height);
            var graphics = Graphics.FromImage(image);
            var red = new SolidBrush(Color.Red);
            var green = new SolidBrush(Color.Green);

            graphics.FillRectangle(green, 0, 0, width, height);

            bool reason = sundayStart_Picker.Value.TimeOfDay.Ticks > double.MaxValue;

            double factor = (double)(sundayStart_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks;
            int y = (int)(factor * height);
            graphics.FillRectangle(red, 0, 0, width, y);

            factor = sundayEnd_Picker.Value.TimeOfDay > new TimeSpan(6, 0, 0) ? (double)(sundayEnd_Picker.Value.TimeOfDay.Ticks - minticks) / (double)ticks : (double)(sundayEnd_Picker.Value.TimeOfDay.Add(new TimeSpan(24, 0, 0)).Ticks - minticks) / (double)ticks;
            y = (int)(factor * height);
            graphics.FillRectangle(red, 0, y, width, height);

            sunday_PictureBox.Image = image;
        }

        private void input_Selected(object sender, EventArgs e)
        {
            var text = sender as TextBox;
            var num = sender as NumericUpDown;
            text?.SelectAll();
            num?.Select(0, num.Text.Length);
        }

        public void DeleteStatus()
        {
            vz_Button.Checked = false;
            tz_Button.Checked = false;
            gv_Button.Checked = false;
            gvs_Button.Checked = false;
            mgm_Button.Checked = false;
            jnt_Button.Checked = false;
        }
    }
}
