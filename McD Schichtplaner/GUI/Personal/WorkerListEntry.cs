﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using McD_Schichtplaner.Database;

namespace McD_Schichtplaner.GUI.Personal
{
    /**
     * <summary>Stellt einen Eintrag aus der Mitarbeiterliste da</summary>
     */
    public partial class WorkerListEntry : UserControl
    {
        private static WorkerListEntry active;
        private Worker worker;
        private Database.Database data;
        public Worker Worker { get => worker; }

        public static Worker Active { get => active?.worker; }

        /**
         * <summary>leerer Konstruktor</summary>
         */
        public WorkerListEntry()
        {
            InitializeComponent();
            Size = MinimumSize;
            data = Database.Database.DB;
        }


        /**
         * <summary>Füllt alle Labels mit den Informationen aus dem Mitarbeiterobjekt</summary>
         */
        public WorkerListEntry(Worker w): this()
        {
            worker = w;
            name_Label.Text = w.Name;

            Colorize();

            if (active != null && active.worker == worker)
            {
                Size = MaximumSize;
                BackColor = Color.SkyBlue;
                active = this;
            }
        }

        /**
         * <summary>Methode, die ausgeführt wird, wenn irgendwo auf den Eintrag geklickt wird. Setzt diesen Eintrag als ausgewählt.</summary>
         */
        private void WorkerListEntry_Click(object sender, EventArgs e)
        {
            SetActive();
        }

        public static void RereadDB(long id)
        {
            active.worker = active.data.GetWorker(id);
            active.name_Label.Text = active.worker.Name;
        }

        private void Colorize()
        {
            switch (worker.Status)
            {
                case Database.Database.Status.GV:
                    BackColor = Color.LemonChiffon;
                    break;
                case Database.Database.Status.GVS:
                    BackColor = Color.LightGreen;
                    break;
                case Database.Database.Status.Hsm:
                    BackColor = Color.LightSteelBlue;
                    break;
                case Database.Database.Status.Mgm:
                    BackColor = Color.LightPink;
                    break;
                case Database.Database.Status.TZ:
                    BackColor = Color.Beige;
                    break;
                case Database.Database.Status.VZ:
                case Database.Database.Status.inaktiv:
                    BackColor = Color.White;
                    break;
            }
        }

        internal void SetActive()
        {
            if (active != null)
            {
                active.Colorize();
            }
            ((PersonalMainPanel)Parent.Parent.Parent).UpdateDetails(worker);
            active = this;
            BackColor = Color.SkyBlue;
        }
    }
}
