﻿namespace McD_Schichtplaner.GUI.Personal
{
    partial class WorkerListEntry
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.name_Label = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // name_Label
            // 
            this.name_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.name_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name_Label.Location = new System.Drawing.Point(0, 0);
            this.name_Label.Name = "name_Label";
            this.name_Label.Size = new System.Drawing.Size(375, 35);
            this.name_Label.TabIndex = 0;
            this.name_Label.Text = "Robin Brummer";
            this.name_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.toolTip.SetToolTip(this.name_Label, "Name");
            this.name_Label.Click += new System.EventHandler(this.WorkerListEntry_Click);
            // 
            // WorkerListEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.name_Label);
            this.MaximumSize = new System.Drawing.Size(375, 35);
            this.MinimumSize = new System.Drawing.Size(375, 35);
            this.Name = "WorkerListEntry";
            this.Size = new System.Drawing.Size(375, 35);
            this.Click += new System.EventHandler(this.WorkerListEntry_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label name_Label;
        private System.Windows.Forms.ToolTip toolTip;
    }
}
