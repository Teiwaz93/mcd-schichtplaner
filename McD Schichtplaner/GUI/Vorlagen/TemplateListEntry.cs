﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace McD_Schichtplaner.GUI.Vorlagen
{
    public partial class TemplateListEntry : UserControl
    {
        private static TemplateListEntry active;
        public static TemplateListEntry Active => active;
        private long id;
        public long ID => id;
        public string Name
        {
            get
            {
                return name_Label.Text;
            }
            set
            {
                name_Label.Text = value;
            }
        }

        public TemplateListEntry(string name, long i)
        {
            InitializeComponent();
            id = i;
            name_Label.Text = name + $" [{id}]";
            if (active?.id == id)
            {
                BackColor = Color.SkyBlue;
            }
        }

        private void Clicked(object sender, EventArgs e)
        {
            if (active != null)
            {
                active.BackColor = Color.White; 
            }
            active = this;
            BackColor = Color.SkyBlue;
            ((TemplateOverview)Parent.Parent.Parent.Parent.Parent).ReDrawTemplate(id);
        }
    }
}
