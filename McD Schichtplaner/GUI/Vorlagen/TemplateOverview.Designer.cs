﻿namespace McD_Schichtplaner.GUI.Vorlagen
{
    partial class TemplateOverview
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainSplit = new System.Windows.Forms.SplitContainer();
            this.listTablePanel = new System.Windows.Forms.TableLayoutPanel();
            this.new_Button = new System.Windows.Forms.Button();
            this.rename_Button = new System.Windows.Forms.Button();
            this.copy_Button = new System.Windows.Forms.Button();
            this.delete_Button = new System.Windows.Forms.Button();
            this.list_FlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.detailSplitContainer = new System.Windows.Forms.SplitContainer();
            this.mainDetailTable = new System.Windows.Forms.TableLayoutPanel();
            this.hours_TablePanel = new System.Windows.Forms.TableLayoutPanel();
            this.hours_Label = new System.Windows.Forms.Label();
            this.hoursName_Label = new System.Windows.Forms.Label();
            this.tab_Control = new System.Windows.Forms.TabControl();
            this.kitchenPage = new System.Windows.Forms.TabPage();
            this.fryer_Panel = new System.Windows.Forms.Panel();
            this.fryer_Table = new System.Windows.Forms.TableLayoutPanel();
            this.fryer_Label = new System.Windows.Forms.Label();
            this.grill_Panel = new System.Windows.Forms.Panel();
            this.grill_Table = new System.Windows.Forms.TableLayoutPanel();
            this.grill_Label = new System.Windows.Forms.Label();
            this.uhc_Panel = new System.Windows.Forms.Panel();
            this.uhc_Table = new System.Windows.Forms.TableLayoutPanel();
            this.uhc_Label = new System.Windows.Forms.Label();
            this.garnish_Panel = new System.Windows.Forms.Panel();
            this.garnish_Table = new System.Windows.Forms.TableLayoutPanel();
            this.garnish_Label = new System.Windows.Forms.Label();
            this.toaster_Panel = new System.Windows.Forms.Panel();
            this.toaster_Table = new System.Windows.Forms.TableLayoutPanel();
            this.toaster_Label = new System.Windows.Forms.Label();
            this.side2_Panel = new System.Windows.Forms.Panel();
            this.side2_Table = new System.Windows.Forms.TableLayoutPanel();
            this.side2_Label = new System.Windows.Forms.Label();
            this.side1_Panel = new System.Windows.Forms.Panel();
            this.side1_Table = new System.Windows.Forms.TableLayoutPanel();
            this.side1_Label = new System.Windows.Forms.Label();
            this.kitchen_Panel = new System.Windows.Forms.Panel();
            this.kitchen_Table = new System.Windows.Forms.TableLayoutPanel();
            this.kitchen_Label = new System.Windows.Forms.Label();
            this.drive_Page = new System.Windows.Forms.TabPage();
            this.dRunner_Panel = new System.Windows.Forms.Panel();
            this.dRunner_Table = new System.Windows.Forms.TableLayoutPanel();
            this.dRunner_Label = new System.Windows.Forms.Label();
            this.bunker_Panel = new System.Windows.Forms.Panel();
            this.bunker_Table = new System.Windows.Forms.TableLayoutPanel();
            this.bunker_Label = new System.Windows.Forms.Label();
            this.counter_Page = new System.Windows.Forms.TabPage();
            this.ice_Panel = new System.Windows.Forms.Panel();
            this.ice_Table = new System.Windows.Forms.TableLayoutPanel();
            this.ice_Label = new System.Windows.Forms.Label();
            this.fries_Panel = new System.Windows.Forms.Panel();
            this.fries_Table = new System.Windows.Forms.TableLayoutPanel();
            this.fries_Label = new System.Windows.Forms.Label();
            this.drinks_Panel = new System.Windows.Forms.Panel();
            this.drinks_Table = new System.Windows.Forms.TableLayoutPanel();
            this.drinks_Label = new System.Windows.Forms.Label();
            this.cashier_Panel = new System.Windows.Forms.Panel();
            this.cashier_Table = new System.Windows.Forms.TableLayoutPanel();
            this.cashier_Label = new System.Windows.Forms.Label();
            this.cRunner_Panel = new System.Windows.Forms.Panel();
            this.cRunner_Table = new System.Windows.Forms.TableLayoutPanel();
            this.cRunner_Label = new System.Windows.Forms.Label();
            this.service_Page = new System.Windows.Forms.TabPage();
            this.jogi_Panel = new System.Windows.Forms.Panel();
            this.Jogi_Table = new System.Windows.Forms.TableLayoutPanel();
            this.jogi_Label = new System.Windows.Forms.Label();
            this.presenter_Panel = new System.Windows.Forms.Panel();
            this.presenter_Table = new System.Windows.Forms.TableLayoutPanel();
            this.presenter_Label = new System.Windows.Forms.Label();
            this.lobby_Panel = new System.Windows.Forms.Panel();
            this.lobby_Table = new System.Windows.Forms.TableLayoutPanel();
            this.lobby_Label = new System.Windows.Forms.Label();
            this.cafe_Page = new System.Windows.Forms.TabPage();
            this.cafe_Panel = new System.Windows.Forms.Panel();
            this.cafe_Table = new System.Windows.Forms.TableLayoutPanel();
            this.cafe_Label = new System.Windows.Forms.Label();
            this.mgm_Page = new System.Windows.Forms.TabPage();
            this.inventur_Panel = new System.Windows.Forms.Panel();
            this.inventur_Table = new System.Windows.Forms.TableLayoutPanel();
            this.inventur_Label = new System.Windows.Forms.Label();
            this.help_Panel = new System.Windows.Forms.Panel();
            this.help_Table = new System.Windows.Forms.TableLayoutPanel();
            this.help_Label = new System.Windows.Forms.Label();
            this.lead_Panel = new System.Windows.Forms.Panel();
            this.lead_Table = new System.Windows.Forms.TableLayoutPanel();
            this.lead_Label = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplit)).BeginInit();
            this.mainSplit.Panel1.SuspendLayout();
            this.mainSplit.Panel2.SuspendLayout();
            this.mainSplit.SuspendLayout();
            this.listTablePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detailSplitContainer)).BeginInit();
            this.detailSplitContainer.Panel1.SuspendLayout();
            this.detailSplitContainer.Panel2.SuspendLayout();
            this.detailSplitContainer.SuspendLayout();
            this.mainDetailTable.SuspendLayout();
            this.hours_TablePanel.SuspendLayout();
            this.tab_Control.SuspendLayout();
            this.kitchenPage.SuspendLayout();
            this.fryer_Panel.SuspendLayout();
            this.grill_Panel.SuspendLayout();
            this.uhc_Panel.SuspendLayout();
            this.garnish_Panel.SuspendLayout();
            this.toaster_Panel.SuspendLayout();
            this.side2_Panel.SuspendLayout();
            this.side1_Panel.SuspendLayout();
            this.kitchen_Panel.SuspendLayout();
            this.drive_Page.SuspendLayout();
            this.dRunner_Panel.SuspendLayout();
            this.bunker_Panel.SuspendLayout();
            this.counter_Page.SuspendLayout();
            this.ice_Panel.SuspendLayout();
            this.fries_Panel.SuspendLayout();
            this.drinks_Panel.SuspendLayout();
            this.cashier_Panel.SuspendLayout();
            this.cRunner_Panel.SuspendLayout();
            this.service_Page.SuspendLayout();
            this.jogi_Panel.SuspendLayout();
            this.presenter_Panel.SuspendLayout();
            this.lobby_Panel.SuspendLayout();
            this.cafe_Page.SuspendLayout();
            this.cafe_Panel.SuspendLayout();
            this.mgm_Page.SuspendLayout();
            this.inventur_Panel.SuspendLayout();
            this.help_Panel.SuspendLayout();
            this.lead_Panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainSplit
            // 
            this.mainSplit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplit.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.mainSplit.IsSplitterFixed = true;
            this.mainSplit.Location = new System.Drawing.Point(0, 0);
            this.mainSplit.Name = "mainSplit";
            // 
            // mainSplit.Panel1
            // 
            this.mainSplit.Panel1.Controls.Add(this.listTablePanel);
            this.mainSplit.Panel1MinSize = 300;
            // 
            // mainSplit.Panel2
            // 
            this.mainSplit.Panel2.Controls.Add(this.detailSplitContainer);
            this.mainSplit.Size = new System.Drawing.Size(1584, 807);
            this.mainSplit.SplitterDistance = 315;
            this.mainSplit.TabIndex = 0;
            // 
            // listTablePanel
            // 
            this.listTablePanel.BackColor = System.Drawing.Color.Green;
            this.listTablePanel.ColumnCount = 4;
            this.listTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.listTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.listTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.listTablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.listTablePanel.Controls.Add(this.new_Button, 0, 0);
            this.listTablePanel.Controls.Add(this.rename_Button, 2, 0);
            this.listTablePanel.Controls.Add(this.copy_Button, 1, 0);
            this.listTablePanel.Controls.Add(this.delete_Button, 3, 0);
            this.listTablePanel.Controls.Add(this.list_FlowLayoutPanel, 0, 1);
            this.listTablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listTablePanel.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.listTablePanel.Location = new System.Drawing.Point(0, 0);
            this.listTablePanel.Name = "listTablePanel";
            this.listTablePanel.RowCount = 2;
            this.listTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.listTablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.listTablePanel.Size = new System.Drawing.Size(315, 807);
            this.listTablePanel.TabIndex = 0;
            // 
            // new_Button
            // 
            this.new_Button.BackColor = System.Drawing.Color.White;
            this.new_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.new_Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.new_Button.Location = new System.Drawing.Point(3, 3);
            this.new_Button.Name = "new_Button";
            this.new_Button.Size = new System.Drawing.Size(72, 29);
            this.new_Button.TabIndex = 4;
            this.new_Button.Text = "Neu";
            this.new_Button.UseVisualStyleBackColor = false;
            this.new_Button.Click += new System.EventHandler(this.New_Button_Click);
            // 
            // rename_Button
            // 
            this.rename_Button.BackColor = System.Drawing.Color.White;
            this.rename_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rename_Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.rename_Button.Location = new System.Drawing.Point(159, 3);
            this.rename_Button.Name = "rename_Button";
            this.rename_Button.Size = new System.Drawing.Size(72, 29);
            this.rename_Button.TabIndex = 3;
            this.rename_Button.Text = "Umbennen";
            this.rename_Button.UseVisualStyleBackColor = false;
            this.rename_Button.Click += new System.EventHandler(this.Rename_Button_Click);
            // 
            // copy_Button
            // 
            this.copy_Button.BackColor = System.Drawing.Color.White;
            this.copy_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.copy_Button.ForeColor = System.Drawing.Color.Blue;
            this.copy_Button.Location = new System.Drawing.Point(81, 3);
            this.copy_Button.Name = "copy_Button";
            this.copy_Button.Size = new System.Drawing.Size(72, 29);
            this.copy_Button.TabIndex = 2;
            this.copy_Button.Text = "Kopieren";
            this.copy_Button.UseVisualStyleBackColor = false;
            this.copy_Button.Click += new System.EventHandler(this.Copy_Button_Click);
            // 
            // delete_Button
            // 
            this.delete_Button.BackColor = System.Drawing.Color.White;
            this.delete_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.delete_Button.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.delete_Button.Location = new System.Drawing.Point(237, 3);
            this.delete_Button.Name = "delete_Button";
            this.delete_Button.Size = new System.Drawing.Size(75, 29);
            this.delete_Button.TabIndex = 0;
            this.delete_Button.Text = "Löschen";
            this.delete_Button.UseVisualStyleBackColor = false;
            this.delete_Button.Click += new System.EventHandler(this.Delete_Button_Click);
            // 
            // list_FlowLayoutPanel
            // 
            this.list_FlowLayoutPanel.AutoScroll = true;
            this.list_FlowLayoutPanel.BackColor = System.Drawing.Color.White;
            this.listTablePanel.SetColumnSpan(this.list_FlowLayoutPanel, 4);
            this.list_FlowLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.list_FlowLayoutPanel.Location = new System.Drawing.Point(3, 38);
            this.list_FlowLayoutPanel.Name = "list_FlowLayoutPanel";
            this.list_FlowLayoutPanel.Size = new System.Drawing.Size(309, 766);
            this.list_FlowLayoutPanel.TabIndex = 5;
            // 
            // detailSplitContainer
            // 
            this.detailSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.detailSplitContainer.IsSplitterFixed = true;
            this.detailSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.detailSplitContainer.Name = "detailSplitContainer";
            this.detailSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // detailSplitContainer.Panel1
            // 
            this.detailSplitContainer.Panel1.BackColor = System.Drawing.Color.Green;
            this.detailSplitContainer.Panel1.Controls.Add(this.mainDetailTable);
            // 
            // detailSplitContainer.Panel2
            // 
            this.detailSplitContainer.Panel2.Controls.Add(this.tab_Control);
            this.detailSplitContainer.Size = new System.Drawing.Size(1265, 807);
            this.detailSplitContainer.SplitterDistance = 35;
            this.detailSplitContainer.TabIndex = 2;
            // 
            // mainDetailTable
            // 
            this.mainDetailTable.ColumnCount = 3;
            this.mainDetailTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainDetailTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainDetailTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainDetailTable.Controls.Add(this.hours_TablePanel, 1, 0);
            this.mainDetailTable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainDetailTable.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.mainDetailTable.Location = new System.Drawing.Point(0, 0);
            this.mainDetailTable.Name = "mainDetailTable";
            this.mainDetailTable.RowCount = 1;
            this.mainDetailTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.mainDetailTable.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.mainDetailTable.Size = new System.Drawing.Size(1265, 35);
            this.mainDetailTable.TabIndex = 0;
            // 
            // hours_TablePanel
            // 
            this.hours_TablePanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.hours_TablePanel.ColumnCount = 2;
            this.hours_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.hours_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.hours_TablePanel.Controls.Add(this.hours_Label, 1, 0);
            this.hours_TablePanel.Controls.Add(this.hoursName_Label, 0, 0);
            this.hours_TablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hours_TablePanel.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.hours_TablePanel.Location = new System.Drawing.Point(424, 3);
            this.hours_TablePanel.Name = "hours_TablePanel";
            this.hours_TablePanel.RowCount = 1;
            this.hours_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.hours_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.hours_TablePanel.Size = new System.Drawing.Size(415, 29);
            this.hours_TablePanel.TabIndex = 0;
            // 
            // hours_Label
            // 
            this.hours_Label.AutoSize = true;
            this.hours_Label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.hours_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hours_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hours_Label.Location = new System.Drawing.Point(211, 2);
            this.hours_Label.Name = "hours_Label";
            this.hours_Label.Size = new System.Drawing.Size(199, 25);
            this.hours_Label.TabIndex = 1;
            this.hours_Label.Text = "0";
            this.hours_Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // hoursName_Label
            // 
            this.hoursName_Label.AutoSize = true;
            this.hoursName_Label.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.hoursName_Label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hoursName_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hoursName_Label.Location = new System.Drawing.Point(5, 2);
            this.hoursName_Label.Name = "hoursName_Label";
            this.hoursName_Label.Size = new System.Drawing.Size(198, 25);
            this.hoursName_Label.TabIndex = 0;
            this.hoursName_Label.Text = "Mitarbeiter [Stunden]:";
            this.hoursName_Label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tab_Control
            // 
            this.tab_Control.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tab_Control.Controls.Add(this.kitchenPage);
            this.tab_Control.Controls.Add(this.drive_Page);
            this.tab_Control.Controls.Add(this.counter_Page);
            this.tab_Control.Controls.Add(this.service_Page);
            this.tab_Control.Controls.Add(this.cafe_Page);
            this.tab_Control.Controls.Add(this.mgm_Page);
            this.tab_Control.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tab_Control.Location = new System.Drawing.Point(0, 0);
            this.tab_Control.Name = "tab_Control";
            this.tab_Control.SelectedIndex = 0;
            this.tab_Control.Size = new System.Drawing.Size(1265, 768);
            this.tab_Control.TabIndex = 1;
            // 
            // kitchenPage
            // 
            this.kitchenPage.AutoScroll = true;
            this.kitchenPage.Controls.Add(this.fryer_Panel);
            this.kitchenPage.Controls.Add(this.grill_Panel);
            this.kitchenPage.Controls.Add(this.uhc_Panel);
            this.kitchenPage.Controls.Add(this.garnish_Panel);
            this.kitchenPage.Controls.Add(this.toaster_Panel);
            this.kitchenPage.Controls.Add(this.side2_Panel);
            this.kitchenPage.Controls.Add(this.side1_Panel);
            this.kitchenPage.Controls.Add(this.kitchen_Panel);
            this.kitchenPage.Location = new System.Drawing.Point(4, 25);
            this.kitchenPage.Name = "kitchenPage";
            this.kitchenPage.Padding = new System.Windows.Forms.Padding(3);
            this.kitchenPage.Size = new System.Drawing.Size(1257, 739);
            this.kitchenPage.TabIndex = 0;
            this.kitchenPage.Text = "Küche";
            this.kitchenPage.UseVisualStyleBackColor = true;
            // 
            // fryer_Panel
            // 
            this.fryer_Panel.AutoSize = true;
            this.fryer_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.fryer_Panel.Controls.Add(this.fryer_Table);
            this.fryer_Panel.Controls.Add(this.fryer_Label);
            this.fryer_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.fryer_Panel.Location = new System.Drawing.Point(3, 521);
            this.fryer_Panel.Name = "fryer_Panel";
            this.fryer_Panel.Size = new System.Drawing.Size(1251, 74);
            this.fryer_Panel.TabIndex = 7;
            // 
            // fryer_Table
            // 
            this.fryer_Table.AutoSize = true;
            this.fryer_Table.ColumnCount = 48;
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fryer_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fryer_Table.Location = new System.Drawing.Point(0, 24);
            this.fryer_Table.Name = "fryer_Table";
            this.fryer_Table.RowCount = 1;
            this.fryer_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.fryer_Table.Size = new System.Drawing.Size(1251, 50);
            this.fryer_Table.TabIndex = 1;
            this.fryer_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // fryer_Label
            // 
            this.fryer_Label.AutoSize = true;
            this.fryer_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.fryer_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fryer_Label.Location = new System.Drawing.Point(0, 0);
            this.fryer_Label.Name = "fryer_Label";
            this.fryer_Label.Size = new System.Drawing.Size(82, 24);
            this.fryer_Label.TabIndex = 0;
            this.fryer_Label.Text = "Fritteuse";
            // 
            // grill_Panel
            // 
            this.grill_Panel.AutoSize = true;
            this.grill_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.grill_Panel.Controls.Add(this.grill_Table);
            this.grill_Panel.Controls.Add(this.grill_Label);
            this.grill_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.grill_Panel.Location = new System.Drawing.Point(3, 447);
            this.grill_Panel.Name = "grill_Panel";
            this.grill_Panel.Size = new System.Drawing.Size(1251, 74);
            this.grill_Panel.TabIndex = 6;
            // 
            // grill_Table
            // 
            this.grill_Table.AutoSize = true;
            this.grill_Table.ColumnCount = 48;
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.grill_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grill_Table.Location = new System.Drawing.Point(0, 24);
            this.grill_Table.Name = "grill_Table";
            this.grill_Table.RowCount = 1;
            this.grill_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.grill_Table.Size = new System.Drawing.Size(1251, 50);
            this.grill_Table.TabIndex = 1;
            this.grill_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // grill_Label
            // 
            this.grill_Label.AutoSize = true;
            this.grill_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.grill_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grill_Label.Location = new System.Drawing.Point(0, 0);
            this.grill_Label.Name = "grill_Label";
            this.grill_Label.Size = new System.Drawing.Size(42, 24);
            this.grill_Label.TabIndex = 0;
            this.grill_Label.Text = "Grill";
            // 
            // uhc_Panel
            // 
            this.uhc_Panel.AutoSize = true;
            this.uhc_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.uhc_Panel.Controls.Add(this.uhc_Table);
            this.uhc_Panel.Controls.Add(this.uhc_Label);
            this.uhc_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.uhc_Panel.Location = new System.Drawing.Point(3, 373);
            this.uhc_Panel.Name = "uhc_Panel";
            this.uhc_Panel.Size = new System.Drawing.Size(1251, 74);
            this.uhc_Panel.TabIndex = 5;
            // 
            // uhc_Table
            // 
            this.uhc_Table.AutoSize = true;
            this.uhc_Table.ColumnCount = 48;
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.uhc_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uhc_Table.Location = new System.Drawing.Point(0, 24);
            this.uhc_Table.Name = "uhc_Table";
            this.uhc_Table.RowCount = 1;
            this.uhc_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.uhc_Table.Size = new System.Drawing.Size(1251, 50);
            this.uhc_Table.TabIndex = 1;
            this.uhc_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // uhc_Label
            // 
            this.uhc_Label.AutoSize = true;
            this.uhc_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.uhc_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uhc_Label.Location = new System.Drawing.Point(0, 0);
            this.uhc_Label.Name = "uhc_Label";
            this.uhc_Label.Size = new System.Drawing.Size(50, 24);
            this.uhc_Label.TabIndex = 0;
            this.uhc_Label.Text = "UHC";
            // 
            // garnish_Panel
            // 
            this.garnish_Panel.AutoSize = true;
            this.garnish_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.garnish_Panel.Controls.Add(this.garnish_Table);
            this.garnish_Panel.Controls.Add(this.garnish_Label);
            this.garnish_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.garnish_Panel.Location = new System.Drawing.Point(3, 299);
            this.garnish_Panel.Name = "garnish_Panel";
            this.garnish_Panel.Size = new System.Drawing.Size(1251, 74);
            this.garnish_Panel.TabIndex = 4;
            // 
            // garnish_Table
            // 
            this.garnish_Table.AutoSize = true;
            this.garnish_Table.ColumnCount = 48;
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.garnish_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.garnish_Table.Location = new System.Drawing.Point(0, 24);
            this.garnish_Table.Name = "garnish_Table";
            this.garnish_Table.RowCount = 1;
            this.garnish_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.garnish_Table.Size = new System.Drawing.Size(1251, 50);
            this.garnish_Table.TabIndex = 1;
            this.garnish_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // garnish_Label
            // 
            this.garnish_Label.AutoSize = true;
            this.garnish_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.garnish_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.garnish_Label.Location = new System.Drawing.Point(0, 0);
            this.garnish_Label.Name = "garnish_Label";
            this.garnish_Label.Size = new System.Drawing.Size(110, 24);
            this.garnish_Label.TabIndex = 0;
            this.garnish_Label.Text = "Garniertisch";
            // 
            // toaster_Panel
            // 
            this.toaster_Panel.AutoSize = true;
            this.toaster_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.toaster_Panel.Controls.Add(this.toaster_Table);
            this.toaster_Panel.Controls.Add(this.toaster_Label);
            this.toaster_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.toaster_Panel.Location = new System.Drawing.Point(3, 225);
            this.toaster_Panel.Name = "toaster_Panel";
            this.toaster_Panel.Size = new System.Drawing.Size(1251, 74);
            this.toaster_Panel.TabIndex = 3;
            // 
            // toaster_Table
            // 
            this.toaster_Table.AutoSize = true;
            this.toaster_Table.ColumnCount = 48;
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.toaster_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toaster_Table.Location = new System.Drawing.Point(0, 24);
            this.toaster_Table.Name = "toaster_Table";
            this.toaster_Table.RowCount = 1;
            this.toaster_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.toaster_Table.Size = new System.Drawing.Size(1251, 50);
            this.toaster_Table.TabIndex = 1;
            this.toaster_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // toaster_Label
            // 
            this.toaster_Label.AutoSize = true;
            this.toaster_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.toaster_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toaster_Label.Location = new System.Drawing.Point(0, 0);
            this.toaster_Label.Name = "toaster_Label";
            this.toaster_Label.Size = new System.Drawing.Size(73, 24);
            this.toaster_Label.TabIndex = 0;
            this.toaster_Label.Text = "Toaster";
            // 
            // side2_Panel
            // 
            this.side2_Panel.AutoSize = true;
            this.side2_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.side2_Panel.Controls.Add(this.side2_Table);
            this.side2_Panel.Controls.Add(this.side2_Label);
            this.side2_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.side2_Panel.Location = new System.Drawing.Point(3, 151);
            this.side2_Panel.Name = "side2_Panel";
            this.side2_Panel.Size = new System.Drawing.Size(1251, 74);
            this.side2_Panel.TabIndex = 2;
            // 
            // side2_Table
            // 
            this.side2_Table.AutoSize = true;
            this.side2_Table.ColumnCount = 48;
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side2_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.side2_Table.Location = new System.Drawing.Point(0, 24);
            this.side2_Table.Name = "side2_Table";
            this.side2_Table.RowCount = 1;
            this.side2_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.side2_Table.Size = new System.Drawing.Size(1251, 50);
            this.side2_Table.TabIndex = 1;
            this.side2_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // side2_Label
            // 
            this.side2_Label.AutoSize = true;
            this.side2_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.side2_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.side2_Label.Location = new System.Drawing.Point(0, 0);
            this.side2_Label.Name = "side2_Label";
            this.side2_Label.Size = new System.Drawing.Size(67, 24);
            this.side2_Label.TabIndex = 0;
            this.side2_Label.Text = "Seite 2";
            // 
            // side1_Panel
            // 
            this.side1_Panel.AutoSize = true;
            this.side1_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.side1_Panel.Controls.Add(this.side1_Table);
            this.side1_Panel.Controls.Add(this.side1_Label);
            this.side1_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.side1_Panel.Location = new System.Drawing.Point(3, 77);
            this.side1_Panel.Name = "side1_Panel";
            this.side1_Panel.Size = new System.Drawing.Size(1251, 74);
            this.side1_Panel.TabIndex = 1;
            // 
            // side1_Table
            // 
            this.side1_Table.AutoSize = true;
            this.side1_Table.ColumnCount = 48;
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.side1_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.side1_Table.Location = new System.Drawing.Point(0, 24);
            this.side1_Table.Name = "side1_Table";
            this.side1_Table.RowCount = 1;
            this.side1_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.side1_Table.Size = new System.Drawing.Size(1251, 50);
            this.side1_Table.TabIndex = 1;
            this.side1_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // side1_Label
            // 
            this.side1_Label.AutoSize = true;
            this.side1_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.side1_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.side1_Label.Location = new System.Drawing.Point(0, 0);
            this.side1_Label.Name = "side1_Label";
            this.side1_Label.Size = new System.Drawing.Size(67, 24);
            this.side1_Label.TabIndex = 0;
            this.side1_Label.Text = "Seite 1";
            // 
            // kitchen_Panel
            // 
            this.kitchen_Panel.AutoSize = true;
            this.kitchen_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.kitchen_Panel.Controls.Add(this.kitchen_Table);
            this.kitchen_Panel.Controls.Add(this.kitchen_Label);
            this.kitchen_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.kitchen_Panel.Location = new System.Drawing.Point(3, 3);
            this.kitchen_Panel.Name = "kitchen_Panel";
            this.kitchen_Panel.Size = new System.Drawing.Size(1251, 74);
            this.kitchen_Panel.TabIndex = 0;
            // 
            // kitchen_Table
            // 
            this.kitchen_Table.AutoSize = true;
            this.kitchen_Table.ColumnCount = 48;
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.kitchen_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kitchen_Table.Location = new System.Drawing.Point(0, 24);
            this.kitchen_Table.Name = "kitchen_Table";
            this.kitchen_Table.RowCount = 1;
            this.kitchen_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.kitchen_Table.Size = new System.Drawing.Size(1251, 50);
            this.kitchen_Table.TabIndex = 1;
            this.kitchen_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // kitchen_Label
            // 
            this.kitchen_Label.AutoSize = true;
            this.kitchen_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.kitchen_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kitchen_Label.Location = new System.Drawing.Point(0, 0);
            this.kitchen_Label.Name = "kitchen_Label";
            this.kitchen_Label.Size = new System.Drawing.Size(65, 24);
            this.kitchen_Label.TabIndex = 0;
            this.kitchen_Label.Text = "Küche";
            // 
            // drive_Page
            // 
            this.drive_Page.Controls.Add(this.dRunner_Panel);
            this.drive_Page.Controls.Add(this.bunker_Panel);
            this.drive_Page.Location = new System.Drawing.Point(4, 25);
            this.drive_Page.Name = "drive_Page";
            this.drive_Page.Padding = new System.Windows.Forms.Padding(3);
            this.drive_Page.Size = new System.Drawing.Size(1257, 739);
            this.drive_Page.TabIndex = 1;
            this.drive_Page.Text = "McDrive";
            this.drive_Page.UseVisualStyleBackColor = true;
            // 
            // dRunner_Panel
            // 
            this.dRunner_Panel.AutoSize = true;
            this.dRunner_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dRunner_Panel.Controls.Add(this.dRunner_Table);
            this.dRunner_Panel.Controls.Add(this.dRunner_Label);
            this.dRunner_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.dRunner_Panel.Location = new System.Drawing.Point(3, 77);
            this.dRunner_Panel.Name = "dRunner_Panel";
            this.dRunner_Panel.Size = new System.Drawing.Size(1251, 74);
            this.dRunner_Panel.TabIndex = 2;
            // 
            // dRunner_Table
            // 
            this.dRunner_Table.AutoSize = true;
            this.dRunner_Table.ColumnCount = 48;
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.dRunner_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dRunner_Table.Location = new System.Drawing.Point(0, 24);
            this.dRunner_Table.Name = "dRunner_Table";
            this.dRunner_Table.RowCount = 1;
            this.dRunner_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.dRunner_Table.Size = new System.Drawing.Size(1251, 50);
            this.dRunner_Table.TabIndex = 1;
            this.dRunner_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // dRunner_Label
            // 
            this.dRunner_Label.AutoSize = true;
            this.dRunner_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.dRunner_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dRunner_Label.Location = new System.Drawing.Point(0, 0);
            this.dRunner_Label.Name = "dRunner_Label";
            this.dRunner_Label.Size = new System.Drawing.Size(73, 24);
            this.dRunner_Label.TabIndex = 0;
            this.dRunner_Label.Text = "Runner";
            // 
            // bunker_Panel
            // 
            this.bunker_Panel.AutoSize = true;
            this.bunker_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.bunker_Panel.Controls.Add(this.bunker_Table);
            this.bunker_Panel.Controls.Add(this.bunker_Label);
            this.bunker_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunker_Panel.Location = new System.Drawing.Point(3, 3);
            this.bunker_Panel.Name = "bunker_Panel";
            this.bunker_Panel.Size = new System.Drawing.Size(1251, 74);
            this.bunker_Panel.TabIndex = 1;
            // 
            // bunker_Table
            // 
            this.bunker_Table.AutoSize = true;
            this.bunker_Table.ColumnCount = 48;
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.bunker_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bunker_Table.Location = new System.Drawing.Point(0, 24);
            this.bunker_Table.Name = "bunker_Table";
            this.bunker_Table.RowCount = 1;
            this.bunker_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.bunker_Table.Size = new System.Drawing.Size(1251, 50);
            this.bunker_Table.TabIndex = 1;
            this.bunker_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // bunker_Label
            // 
            this.bunker_Label.AutoSize = true;
            this.bunker_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunker_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunker_Label.Location = new System.Drawing.Point(0, 0);
            this.bunker_Label.Name = "bunker_Label";
            this.bunker_Label.Size = new System.Drawing.Size(70, 24);
            this.bunker_Label.TabIndex = 0;
            this.bunker_Label.Text = "Bunker";
            // 
            // counter_Page
            // 
            this.counter_Page.Controls.Add(this.ice_Panel);
            this.counter_Page.Controls.Add(this.fries_Panel);
            this.counter_Page.Controls.Add(this.drinks_Panel);
            this.counter_Page.Controls.Add(this.cashier_Panel);
            this.counter_Page.Controls.Add(this.cRunner_Panel);
            this.counter_Page.Location = new System.Drawing.Point(4, 25);
            this.counter_Page.Name = "counter_Page";
            this.counter_Page.Size = new System.Drawing.Size(1257, 739);
            this.counter_Page.TabIndex = 2;
            this.counter_Page.Text = "Theke / Kasse";
            this.counter_Page.UseVisualStyleBackColor = true;
            // 
            // ice_Panel
            // 
            this.ice_Panel.AutoSize = true;
            this.ice_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ice_Panel.Controls.Add(this.ice_Table);
            this.ice_Panel.Controls.Add(this.ice_Label);
            this.ice_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.ice_Panel.Location = new System.Drawing.Point(0, 296);
            this.ice_Panel.Name = "ice_Panel";
            this.ice_Panel.Size = new System.Drawing.Size(1257, 74);
            this.ice_Panel.TabIndex = 6;
            // 
            // ice_Table
            // 
            this.ice_Table.AutoSize = true;
            this.ice_Table.ColumnCount = 48;
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.ice_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ice_Table.Location = new System.Drawing.Point(0, 24);
            this.ice_Table.Name = "ice_Table";
            this.ice_Table.RowCount = 1;
            this.ice_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.ice_Table.Size = new System.Drawing.Size(1257, 50);
            this.ice_Table.TabIndex = 1;
            this.ice_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // ice_Label
            // 
            this.ice_Label.AutoSize = true;
            this.ice_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.ice_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ice_Label.Location = new System.Drawing.Point(0, 0);
            this.ice_Label.Name = "ice_Label";
            this.ice_Label.Size = new System.Drawing.Size(36, 24);
            this.ice_Label.TabIndex = 0;
            this.ice_Label.Text = "Eis";
            // 
            // fries_Panel
            // 
            this.fries_Panel.AutoSize = true;
            this.fries_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.fries_Panel.Controls.Add(this.fries_Table);
            this.fries_Panel.Controls.Add(this.fries_Label);
            this.fries_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.fries_Panel.Location = new System.Drawing.Point(0, 222);
            this.fries_Panel.Name = "fries_Panel";
            this.fries_Panel.Size = new System.Drawing.Size(1257, 74);
            this.fries_Panel.TabIndex = 5;
            // 
            // fries_Table
            // 
            this.fries_Table.AutoSize = true;
            this.fries_Table.ColumnCount = 48;
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.fries_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fries_Table.Location = new System.Drawing.Point(0, 24);
            this.fries_Table.Name = "fries_Table";
            this.fries_Table.RowCount = 1;
            this.fries_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.fries_Table.Size = new System.Drawing.Size(1257, 50);
            this.fries_Table.TabIndex = 1;
            this.fries_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // fries_Label
            // 
            this.fries_Label.AutoSize = true;
            this.fries_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.fries_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fries_Label.Location = new System.Drawing.Point(0, 0);
            this.fries_Label.Name = "fries_Label";
            this.fries_Label.Size = new System.Drawing.Size(85, 24);
            this.fries_Label.TabIndex = 0;
            this.fries_Label.Text = "Pommes";
            // 
            // drinks_Panel
            // 
            this.drinks_Panel.AutoSize = true;
            this.drinks_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.drinks_Panel.Controls.Add(this.drinks_Table);
            this.drinks_Panel.Controls.Add(this.drinks_Label);
            this.drinks_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.drinks_Panel.Location = new System.Drawing.Point(0, 148);
            this.drinks_Panel.Name = "drinks_Panel";
            this.drinks_Panel.Size = new System.Drawing.Size(1257, 74);
            this.drinks_Panel.TabIndex = 4;
            // 
            // drinks_Table
            // 
            this.drinks_Table.AutoSize = true;
            this.drinks_Table.ColumnCount = 48;
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.drinks_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.drinks_Table.Location = new System.Drawing.Point(0, 24);
            this.drinks_Table.Name = "drinks_Table";
            this.drinks_Table.RowCount = 1;
            this.drinks_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.drinks_Table.Size = new System.Drawing.Size(1257, 50);
            this.drinks_Table.TabIndex = 1;
            this.drinks_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // drinks_Label
            // 
            this.drinks_Label.AutoSize = true;
            this.drinks_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.drinks_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drinks_Label.Location = new System.Drawing.Point(0, 0);
            this.drinks_Label.Name = "drinks_Label";
            this.drinks_Label.Size = new System.Drawing.Size(86, 24);
            this.drinks_Label.TabIndex = 0;
            this.drinks_Label.Text = "Getränke";
            // 
            // cashier_Panel
            // 
            this.cashier_Panel.AutoSize = true;
            this.cashier_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.cashier_Panel.Controls.Add(this.cashier_Table);
            this.cashier_Panel.Controls.Add(this.cashier_Label);
            this.cashier_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.cashier_Panel.Location = new System.Drawing.Point(0, 74);
            this.cashier_Panel.Name = "cashier_Panel";
            this.cashier_Panel.Size = new System.Drawing.Size(1257, 74);
            this.cashier_Panel.TabIndex = 3;
            // 
            // cashier_Table
            // 
            this.cashier_Table.AutoSize = true;
            this.cashier_Table.ColumnCount = 48;
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cashier_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cashier_Table.Location = new System.Drawing.Point(0, 24);
            this.cashier_Table.Name = "cashier_Table";
            this.cashier_Table.RowCount = 1;
            this.cashier_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.cashier_Table.Size = new System.Drawing.Size(1257, 50);
            this.cashier_Table.TabIndex = 1;
            this.cashier_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // cashier_Label
            // 
            this.cashier_Label.AutoSize = true;
            this.cashier_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.cashier_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cashier_Label.Location = new System.Drawing.Point(0, 0);
            this.cashier_Label.Name = "cashier_Label";
            this.cashier_Label.Size = new System.Drawing.Size(61, 24);
            this.cashier_Label.TabIndex = 0;
            this.cashier_Label.Text = "Kasse";
            // 
            // cRunner_Panel
            // 
            this.cRunner_Panel.AutoSize = true;
            this.cRunner_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.cRunner_Panel.Controls.Add(this.cRunner_Table);
            this.cRunner_Panel.Controls.Add(this.cRunner_Label);
            this.cRunner_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.cRunner_Panel.Location = new System.Drawing.Point(0, 0);
            this.cRunner_Panel.Name = "cRunner_Panel";
            this.cRunner_Panel.Size = new System.Drawing.Size(1257, 74);
            this.cRunner_Panel.TabIndex = 2;
            // 
            // cRunner_Table
            // 
            this.cRunner_Table.AutoSize = true;
            this.cRunner_Table.ColumnCount = 48;
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cRunner_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cRunner_Table.Location = new System.Drawing.Point(0, 24);
            this.cRunner_Table.Name = "cRunner_Table";
            this.cRunner_Table.RowCount = 1;
            this.cRunner_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.cRunner_Table.Size = new System.Drawing.Size(1257, 50);
            this.cRunner_Table.TabIndex = 1;
            this.cRunner_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // cRunner_Label
            // 
            this.cRunner_Label.AutoSize = true;
            this.cRunner_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.cRunner_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cRunner_Label.Location = new System.Drawing.Point(0, 0);
            this.cRunner_Label.Name = "cRunner_Label";
            this.cRunner_Label.Size = new System.Drawing.Size(143, 24);
            this.cRunner_Label.TabIndex = 0;
            this.cRunner_Label.Text = "Theken Runner";
            // 
            // service_Page
            // 
            this.service_Page.Controls.Add(this.jogi_Panel);
            this.service_Page.Controls.Add(this.presenter_Panel);
            this.service_Page.Controls.Add(this.lobby_Panel);
            this.service_Page.Location = new System.Drawing.Point(4, 25);
            this.service_Page.Name = "service_Page";
            this.service_Page.Size = new System.Drawing.Size(1257, 739);
            this.service_Page.TabIndex = 3;
            this.service_Page.Text = "Service / Lobby";
            this.service_Page.UseVisualStyleBackColor = true;
            // 
            // jogi_Panel
            // 
            this.jogi_Panel.AutoSize = true;
            this.jogi_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.jogi_Panel.Controls.Add(this.Jogi_Table);
            this.jogi_Panel.Controls.Add(this.jogi_Label);
            this.jogi_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.jogi_Panel.Location = new System.Drawing.Point(0, 148);
            this.jogi_Panel.Name = "jogi_Panel";
            this.jogi_Panel.Size = new System.Drawing.Size(1257, 74);
            this.jogi_Panel.TabIndex = 4;
            // 
            // Jogi_Table
            // 
            this.Jogi_Table.AutoSize = true;
            this.Jogi_Table.ColumnCount = 48;
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.Jogi_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Jogi_Table.Location = new System.Drawing.Point(0, 24);
            this.Jogi_Table.Name = "Jogi_Table";
            this.Jogi_Table.RowCount = 1;
            this.Jogi_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.Jogi_Table.Size = new System.Drawing.Size(1257, 50);
            this.Jogi_Table.TabIndex = 1;
            this.Jogi_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // jogi_Label
            // 
            this.jogi_Label.AutoSize = true;
            this.jogi_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.jogi_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jogi_Label.Location = new System.Drawing.Point(0, 0);
            this.jogi_Label.Name = "jogi_Label";
            this.jogi_Label.Size = new System.Drawing.Size(45, 24);
            this.jogi_Label.TabIndex = 0;
            this.jogi_Label.Text = "Jogi";
            // 
            // presenter_Panel
            // 
            this.presenter_Panel.AutoSize = true;
            this.presenter_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.presenter_Panel.Controls.Add(this.presenter_Table);
            this.presenter_Panel.Controls.Add(this.presenter_Label);
            this.presenter_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.presenter_Panel.Location = new System.Drawing.Point(0, 74);
            this.presenter_Panel.Name = "presenter_Panel";
            this.presenter_Panel.Size = new System.Drawing.Size(1257, 74);
            this.presenter_Panel.TabIndex = 3;
            // 
            // presenter_Table
            // 
            this.presenter_Table.AutoSize = true;
            this.presenter_Table.ColumnCount = 48;
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.presenter_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.presenter_Table.Location = new System.Drawing.Point(0, 24);
            this.presenter_Table.Name = "presenter_Table";
            this.presenter_Table.RowCount = 1;
            this.presenter_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.presenter_Table.Size = new System.Drawing.Size(1257, 50);
            this.presenter_Table.TabIndex = 1;
            this.presenter_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // presenter_Label
            // 
            this.presenter_Label.AutoSize = true;
            this.presenter_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.presenter_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.presenter_Label.Location = new System.Drawing.Point(0, 0);
            this.presenter_Label.Name = "presenter_Label";
            this.presenter_Label.Size = new System.Drawing.Size(91, 24);
            this.presenter_Label.TabIndex = 0;
            this.presenter_Label.Text = "Presenter";
            // 
            // lobby_Panel
            // 
            this.lobby_Panel.AutoSize = true;
            this.lobby_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.lobby_Panel.Controls.Add(this.lobby_Table);
            this.lobby_Panel.Controls.Add(this.lobby_Label);
            this.lobby_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.lobby_Panel.Location = new System.Drawing.Point(0, 0);
            this.lobby_Panel.Name = "lobby_Panel";
            this.lobby_Panel.Size = new System.Drawing.Size(1257, 74);
            this.lobby_Panel.TabIndex = 2;
            // 
            // lobby_Table
            // 
            this.lobby_Table.AutoSize = true;
            this.lobby_Table.ColumnCount = 48;
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lobby_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lobby_Table.Location = new System.Drawing.Point(0, 24);
            this.lobby_Table.Name = "lobby_Table";
            this.lobby_Table.RowCount = 1;
            this.lobby_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.lobby_Table.Size = new System.Drawing.Size(1257, 50);
            this.lobby_Table.TabIndex = 1;
            this.lobby_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // lobby_Label
            // 
            this.lobby_Label.AutoSize = true;
            this.lobby_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.lobby_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lobby_Label.Location = new System.Drawing.Point(0, 0);
            this.lobby_Label.Name = "lobby_Label";
            this.lobby_Label.Size = new System.Drawing.Size(62, 24);
            this.lobby_Label.TabIndex = 0;
            this.lobby_Label.Text = "Lobby";
            // 
            // cafe_Page
            // 
            this.cafe_Page.Controls.Add(this.cafe_Panel);
            this.cafe_Page.Location = new System.Drawing.Point(4, 25);
            this.cafe_Page.Name = "cafe_Page";
            this.cafe_Page.Size = new System.Drawing.Size(1257, 739);
            this.cafe_Page.TabIndex = 4;
            this.cafe_Page.Text = "McCafè";
            this.cafe_Page.UseVisualStyleBackColor = true;
            // 
            // cafe_Panel
            // 
            this.cafe_Panel.AutoSize = true;
            this.cafe_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.cafe_Panel.Controls.Add(this.cafe_Table);
            this.cafe_Panel.Controls.Add(this.cafe_Label);
            this.cafe_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.cafe_Panel.Location = new System.Drawing.Point(0, 0);
            this.cafe_Panel.Name = "cafe_Panel";
            this.cafe_Panel.Size = new System.Drawing.Size(1257, 74);
            this.cafe_Panel.TabIndex = 2;
            // 
            // cafe_Table
            // 
            this.cafe_Table.AutoSize = true;
            this.cafe_Table.ColumnCount = 48;
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.cafe_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cafe_Table.Location = new System.Drawing.Point(0, 24);
            this.cafe_Table.Name = "cafe_Table";
            this.cafe_Table.RowCount = 1;
            this.cafe_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.cafe_Table.Size = new System.Drawing.Size(1257, 50);
            this.cafe_Table.TabIndex = 1;
            this.cafe_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // cafe_Label
            // 
            this.cafe_Label.AutoSize = true;
            this.cafe_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.cafe_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cafe_Label.Location = new System.Drawing.Point(0, 0);
            this.cafe_Label.Name = "cafe_Label";
            this.cafe_Label.Size = new System.Drawing.Size(48, 24);
            this.cafe_Label.TabIndex = 0;
            this.cafe_Label.Text = "Cafe";
            // 
            // mgm_Page
            // 
            this.mgm_Page.Controls.Add(this.inventur_Panel);
            this.mgm_Page.Controls.Add(this.help_Panel);
            this.mgm_Page.Controls.Add(this.lead_Panel);
            this.mgm_Page.Location = new System.Drawing.Point(4, 25);
            this.mgm_Page.Name = "mgm_Page";
            this.mgm_Page.Size = new System.Drawing.Size(1257, 739);
            this.mgm_Page.TabIndex = 5;
            this.mgm_Page.Text = "Management";
            this.mgm_Page.UseVisualStyleBackColor = true;
            // 
            // inventur_Panel
            // 
            this.inventur_Panel.AutoSize = true;
            this.inventur_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.inventur_Panel.Controls.Add(this.inventur_Table);
            this.inventur_Panel.Controls.Add(this.inventur_Label);
            this.inventur_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.inventur_Panel.Location = new System.Drawing.Point(0, 148);
            this.inventur_Panel.Name = "inventur_Panel";
            this.inventur_Panel.Size = new System.Drawing.Size(1257, 74);
            this.inventur_Panel.TabIndex = 4;
            // 
            // inventur_Table
            // 
            this.inventur_Table.AutoSize = true;
            this.inventur_Table.ColumnCount = 48;
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.inventur_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inventur_Table.Location = new System.Drawing.Point(0, 24);
            this.inventur_Table.Name = "inventur_Table";
            this.inventur_Table.RowCount = 1;
            this.inventur_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.inventur_Table.Size = new System.Drawing.Size(1257, 50);
            this.inventur_Table.TabIndex = 1;
            this.inventur_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // inventur_Label
            // 
            this.inventur_Label.AutoSize = true;
            this.inventur_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.inventur_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inventur_Label.Location = new System.Drawing.Point(0, 0);
            this.inventur_Label.Name = "inventur_Label";
            this.inventur_Label.Size = new System.Drawing.Size(77, 24);
            this.inventur_Label.TabIndex = 0;
            this.inventur_Label.Text = "Inventur";
            // 
            // help_Panel
            // 
            this.help_Panel.AutoSize = true;
            this.help_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.help_Panel.Controls.Add(this.help_Table);
            this.help_Panel.Controls.Add(this.help_Label);
            this.help_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.help_Panel.Location = new System.Drawing.Point(0, 74);
            this.help_Panel.Name = "help_Panel";
            this.help_Panel.Size = new System.Drawing.Size(1257, 74);
            this.help_Panel.TabIndex = 3;
            // 
            // help_Table
            // 
            this.help_Table.AutoSize = true;
            this.help_Table.ColumnCount = 48;
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.help_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.help_Table.Location = new System.Drawing.Point(0, 24);
            this.help_Table.Name = "help_Table";
            this.help_Table.RowCount = 1;
            this.help_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.help_Table.Size = new System.Drawing.Size(1257, 50);
            this.help_Table.TabIndex = 1;
            this.help_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // help_Label
            // 
            this.help_Label.AutoSize = true;
            this.help_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.help_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.help_Label.Location = new System.Drawing.Point(0, 0);
            this.help_Label.Name = "help_Label";
            this.help_Label.Size = new System.Drawing.Size(77, 24);
            this.help_Label.TabIndex = 0;
            this.help_Label.Text = "Aushilfe";
            // 
            // lead_Panel
            // 
            this.lead_Panel.AutoSize = true;
            this.lead_Panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lead_Panel.Controls.Add(this.lead_Table);
            this.lead_Panel.Controls.Add(this.lead_Label);
            this.lead_Panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.lead_Panel.Location = new System.Drawing.Point(0, 0);
            this.lead_Panel.Name = "lead_Panel";
            this.lead_Panel.Size = new System.Drawing.Size(1257, 74);
            this.lead_Panel.TabIndex = 2;
            // 
            // lead_Table
            // 
            this.lead_Table.AutoSize = true;
            this.lead_Table.ColumnCount = 48;
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.083332F));
            this.lead_Table.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lead_Table.Location = new System.Drawing.Point(0, 24);
            this.lead_Table.Name = "lead_Table";
            this.lead_Table.RowCount = 1;
            this.lead_Table.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.lead_Table.Size = new System.Drawing.Size(1257, 50);
            this.lead_Table.TabIndex = 1;
            this.lead_Table.Click += new System.EventHandler(this.TableClicked);
            // 
            // lead_Label
            // 
            this.lead_Label.AutoSize = true;
            this.lead_Label.Dock = System.Windows.Forms.DockStyle.Top;
            this.lead_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lead_Label.Location = new System.Drawing.Point(0, 0);
            this.lead_Label.Name = "lead_Label";
            this.lead_Label.Size = new System.Drawing.Size(137, 24);
            this.lead_Label.TabIndex = 0;
            this.lead_Label.Text = "Schichtführung";
            // 
            // TemplateOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.mainSplit);
            this.Name = "TemplateOverview";
            this.Size = new System.Drawing.Size(1584, 807);
            this.mainSplit.Panel1.ResumeLayout(false);
            this.mainSplit.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplit)).EndInit();
            this.mainSplit.ResumeLayout(false);
            this.listTablePanel.ResumeLayout(false);
            this.detailSplitContainer.Panel1.ResumeLayout(false);
            this.detailSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.detailSplitContainer)).EndInit();
            this.detailSplitContainer.ResumeLayout(false);
            this.mainDetailTable.ResumeLayout(false);
            this.hours_TablePanel.ResumeLayout(false);
            this.hours_TablePanel.PerformLayout();
            this.tab_Control.ResumeLayout(false);
            this.kitchenPage.ResumeLayout(false);
            this.kitchenPage.PerformLayout();
            this.fryer_Panel.ResumeLayout(false);
            this.fryer_Panel.PerformLayout();
            this.grill_Panel.ResumeLayout(false);
            this.grill_Panel.PerformLayout();
            this.uhc_Panel.ResumeLayout(false);
            this.uhc_Panel.PerformLayout();
            this.garnish_Panel.ResumeLayout(false);
            this.garnish_Panel.PerformLayout();
            this.toaster_Panel.ResumeLayout(false);
            this.toaster_Panel.PerformLayout();
            this.side2_Panel.ResumeLayout(false);
            this.side2_Panel.PerformLayout();
            this.side1_Panel.ResumeLayout(false);
            this.side1_Panel.PerformLayout();
            this.kitchen_Panel.ResumeLayout(false);
            this.kitchen_Panel.PerformLayout();
            this.drive_Page.ResumeLayout(false);
            this.drive_Page.PerformLayout();
            this.dRunner_Panel.ResumeLayout(false);
            this.dRunner_Panel.PerformLayout();
            this.bunker_Panel.ResumeLayout(false);
            this.bunker_Panel.PerformLayout();
            this.counter_Page.ResumeLayout(false);
            this.counter_Page.PerformLayout();
            this.ice_Panel.ResumeLayout(false);
            this.ice_Panel.PerformLayout();
            this.fries_Panel.ResumeLayout(false);
            this.fries_Panel.PerformLayout();
            this.drinks_Panel.ResumeLayout(false);
            this.drinks_Panel.PerformLayout();
            this.cashier_Panel.ResumeLayout(false);
            this.cashier_Panel.PerformLayout();
            this.cRunner_Panel.ResumeLayout(false);
            this.cRunner_Panel.PerformLayout();
            this.service_Page.ResumeLayout(false);
            this.service_Page.PerformLayout();
            this.jogi_Panel.ResumeLayout(false);
            this.jogi_Panel.PerformLayout();
            this.presenter_Panel.ResumeLayout(false);
            this.presenter_Panel.PerformLayout();
            this.lobby_Panel.ResumeLayout(false);
            this.lobby_Panel.PerformLayout();
            this.cafe_Page.ResumeLayout(false);
            this.cafe_Page.PerformLayout();
            this.cafe_Panel.ResumeLayout(false);
            this.cafe_Panel.PerformLayout();
            this.mgm_Page.ResumeLayout(false);
            this.mgm_Page.PerformLayout();
            this.inventur_Panel.ResumeLayout(false);
            this.inventur_Panel.PerformLayout();
            this.help_Panel.ResumeLayout(false);
            this.help_Panel.PerformLayout();
            this.lead_Panel.ResumeLayout(false);
            this.lead_Panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer mainSplit;
        private System.Windows.Forms.TableLayoutPanel listTablePanel;
        private System.Windows.Forms.Button rename_Button;
        private System.Windows.Forms.Button copy_Button;
        private System.Windows.Forms.Button delete_Button;
        private System.Windows.Forms.Button new_Button;
        private System.Windows.Forms.FlowLayoutPanel list_FlowLayoutPanel;
        private System.Windows.Forms.TabControl tab_Control;
        private System.Windows.Forms.TabPage kitchenPage;
        private System.Windows.Forms.Panel fryer_Panel;
        private System.Windows.Forms.TableLayoutPanel fryer_Table;
        private System.Windows.Forms.Label fryer_Label;
        private System.Windows.Forms.Panel grill_Panel;
        private System.Windows.Forms.TableLayoutPanel grill_Table;
        private System.Windows.Forms.Label grill_Label;
        private System.Windows.Forms.Panel uhc_Panel;
        private System.Windows.Forms.TableLayoutPanel uhc_Table;
        private System.Windows.Forms.Label uhc_Label;
        private System.Windows.Forms.Panel garnish_Panel;
        private System.Windows.Forms.TableLayoutPanel garnish_Table;
        private System.Windows.Forms.Label garnish_Label;
        private System.Windows.Forms.Panel toaster_Panel;
        private System.Windows.Forms.TableLayoutPanel toaster_Table;
        private System.Windows.Forms.Label toaster_Label;
        private System.Windows.Forms.Panel side2_Panel;
        private System.Windows.Forms.TableLayoutPanel side2_Table;
        private System.Windows.Forms.Label side2_Label;
        private System.Windows.Forms.Panel side1_Panel;
        private System.Windows.Forms.TableLayoutPanel side1_Table;
        private System.Windows.Forms.Label side1_Label;
        private System.Windows.Forms.Panel kitchen_Panel;
        private System.Windows.Forms.TableLayoutPanel kitchen_Table;
        private System.Windows.Forms.Label kitchen_Label;
        private System.Windows.Forms.TabPage drive_Page;
        private System.Windows.Forms.Panel dRunner_Panel;
        private System.Windows.Forms.TableLayoutPanel dRunner_Table;
        private System.Windows.Forms.Label dRunner_Label;
        private System.Windows.Forms.Panel bunker_Panel;
        private System.Windows.Forms.TableLayoutPanel bunker_Table;
        private System.Windows.Forms.Label bunker_Label;
        private System.Windows.Forms.TabPage counter_Page;
        private System.Windows.Forms.Panel ice_Panel;
        private System.Windows.Forms.TableLayoutPanel ice_Table;
        private System.Windows.Forms.Label ice_Label;
        private System.Windows.Forms.Panel fries_Panel;
        private System.Windows.Forms.TableLayoutPanel fries_Table;
        private System.Windows.Forms.Label fries_Label;
        private System.Windows.Forms.Panel drinks_Panel;
        private System.Windows.Forms.TableLayoutPanel drinks_Table;
        private System.Windows.Forms.Label drinks_Label;
        private System.Windows.Forms.Panel cashier_Panel;
        private System.Windows.Forms.TableLayoutPanel cashier_Table;
        private System.Windows.Forms.Label cashier_Label;
        private System.Windows.Forms.Panel cRunner_Panel;
        private System.Windows.Forms.TableLayoutPanel cRunner_Table;
        private System.Windows.Forms.Label cRunner_Label;
        private System.Windows.Forms.TabPage service_Page;
        private System.Windows.Forms.Panel jogi_Panel;
        private System.Windows.Forms.TableLayoutPanel Jogi_Table;
        private System.Windows.Forms.Label jogi_Label;
        private System.Windows.Forms.Panel presenter_Panel;
        private System.Windows.Forms.TableLayoutPanel presenter_Table;
        private System.Windows.Forms.Label presenter_Label;
        private System.Windows.Forms.Panel lobby_Panel;
        private System.Windows.Forms.TableLayoutPanel lobby_Table;
        private System.Windows.Forms.Label lobby_Label;
        private System.Windows.Forms.TabPage cafe_Page;
        private System.Windows.Forms.Panel cafe_Panel;
        private System.Windows.Forms.TableLayoutPanel cafe_Table;
        private System.Windows.Forms.Label cafe_Label;
        private System.Windows.Forms.TabPage mgm_Page;
        private System.Windows.Forms.Panel inventur_Panel;
        private System.Windows.Forms.TableLayoutPanel inventur_Table;
        private System.Windows.Forms.Label inventur_Label;
        private System.Windows.Forms.Panel help_Panel;
        private System.Windows.Forms.TableLayoutPanel help_Table;
        private System.Windows.Forms.Label help_Label;
        private System.Windows.Forms.Panel lead_Panel;
        private System.Windows.Forms.TableLayoutPanel lead_Table;
        private System.Windows.Forms.Label lead_Label;
        private System.Windows.Forms.SplitContainer detailSplitContainer;
        private System.Windows.Forms.TableLayoutPanel mainDetailTable;
        private System.Windows.Forms.TableLayoutPanel hours_TablePanel;
        private System.Windows.Forms.Label hours_Label;
        private System.Windows.Forms.Label hoursName_Label;
    }
}
