﻿using McD_Schichtplaner.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace McD_Schichtplaner.GUI.Vorlagen
{
    public partial class TemplateShiftForm : Form
    {
        private TemplateShift shift;
        public TemplateShift Shift => shift;
        private long station;
        private long template;
        public TemplateShiftForm(long s, long t)
        {
            InitializeComponent();
            delete_Button.Enabled = false;
            rest_Picker.Enabled = false;
            station = s;
            template = t;
            start_Picker.Value = DateTime.Today;
            end_Picker.Value = DateTime.Today;
            rest_Picker.Value = DateTime.Today;
        }

        public TemplateShiftForm(TemplateShift shift)
        {
            this.shift = shift;
            InitializeComponent();
            DateTime date = DateTime.Today;
            start_Picker.Value = date.Add(shift.Start);
            end_Picker.Value = date.Add(shift.End);
            if (shift.Rest > TimeSpan.MinValue) rest_Picker.Value = date.Add(shift.Rest);
            notes_Textbox.Text = shift.Notes;
        }

        private void Accept_Button_Click(object sender, EventArgs e)
        {
            TimeSpan fullDay = new TimeSpan(24, 0, 0);
            TimeSpan start = start_Picker.Value.TimeOfDay.Hours >= 6 ? start_Picker.Value.TimeOfDay : start_Picker.Value.TimeOfDay.Add(fullDay);
            TimeSpan end;
            end = (start_Picker.Value.TimeOfDay > end_Picker.Value.TimeOfDay ? end_Picker.Value.TimeOfDay.Add(fullDay) : end_Picker.Value.TimeOfDay);
            if (end < start) end = end.Add(fullDay);
            if (end - start > new TimeSpan(9, 30, 0))
            {
                MessageBox.Show("Schicht ist zu lang.\nNicht länger als 9,5 Stunden (inkl. Pause).");
                return;
            }
            TimeSpan rest;
            if (rest1_CheckBox.Checked ) rest = (start_Picker.Value.TimeOfDay > rest_Picker.Value.TimeOfDay ? rest_Picker.Value.TimeOfDay.Add(fullDay) : rest_Picker.Value.TimeOfDay);
            else rest = TimeSpan.MinValue;
            if (rest1_CheckBox.Checked == true && (rest > end || rest < start))
            {
                MessageBox.Show("Pause muss sich zwischen Start und Ende liegen.");
                return;
            }
            if (shift == null)
            {
                shift = new TemplateShift
                {
                    Template = template,
                    Start = start,
                    End = end,
                    Rest = rest,
                    Station = station,
                    Notes = notes_Textbox.Text,
                    B_Rest = rest1_CheckBox.Checked
                }; 
            }
            else
            {
                shift.Start = start;
                shift.End = end;
                shift.Rest = rest;
                shift.Notes = notes_Textbox.Text;
                shift.B_Rest = rest1_CheckBox.Checked;
            }
            DialogResult = DialogResult.OK;
        }

        private void Abort_Button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Abort;
        }

        private void Delete_Button_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Sicher ? Schicht löschen ?", "Löschen ?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DialogResult = DialogResult.No;
                Close();
            }
        }

        private void TimesChanged(object sender, EventArgs e)
        {
            if (end_Picker.Value - start_Picker.Value > new TimeSpan(6, 0, 0))
            {
                rest1_CheckBox.Checked = true;
                rest_Picker.Value = start_Picker.Value + new TimeSpan(3, 0, 0);
                rest_Picker.Enabled = true;
            }
            else
            {
                if (end_Picker.Value < start_Picker.Value)
                {
                    if (end_Picker.Value.AddDays(1) - start_Picker.Value > new TimeSpan(6, 0, 0))
                    {
                        rest1_CheckBox.Checked = true;
                        rest_Picker.Value = start_Picker.Value + new TimeSpan(3, 0, 0);
                        rest_Picker.Enabled = true;
                    }
                    else
                    {
                        rest1_CheckBox.Checked = false;
                        rest_Picker.Enabled = false;
                    }
                }
                else
                {
                    rest1_CheckBox.Checked = false;
                    rest_Picker.Enabled = false;
                }
            }
        }
    }
}
