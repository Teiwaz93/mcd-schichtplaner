﻿namespace McD_Schichtplaner.GUI.Vorlagen
{
    partial class TemplateShiftForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.main_TablePanel = new System.Windows.Forms.TableLayoutPanel();
            this.rest_Panel = new System.Windows.Forms.Panel();
            this.rest1_CheckBox = new System.Windows.Forms.CheckBox();
            this.rest_Label = new System.Windows.Forms.Label();
            this.end_Picker = new System.Windows.Forms.DateTimePicker();
            this.rest_Picker = new System.Windows.Forms.DateTimePicker();
            this.end_Panel = new System.Windows.Forms.Panel();
            this.end_Label = new System.Windows.Forms.Label();
            this.start_Panel = new System.Windows.Forms.Panel();
            this.start_Label = new System.Windows.Forms.Label();
            this.start_Picker = new System.Windows.Forms.DateTimePicker();
            this.notes_Textbox = new System.Windows.Forms.TextBox();
            this.button_FlowLayout = new System.Windows.Forms.FlowLayoutPanel();
            this.accept_Button = new System.Windows.Forms.Button();
            this.abort_Button = new System.Windows.Forms.Button();
            this.delete_Button = new System.Windows.Forms.Button();
            this.main_TablePanel.SuspendLayout();
            this.rest_Panel.SuspendLayout();
            this.end_Panel.SuspendLayout();
            this.start_Panel.SuspendLayout();
            this.button_FlowLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // main_TablePanel
            // 
            this.main_TablePanel.ColumnCount = 2;
            this.main_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_TablePanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.main_TablePanel.Controls.Add(this.rest_Panel, 0, 2);
            this.main_TablePanel.Controls.Add(this.end_Picker, 1, 1);
            this.main_TablePanel.Controls.Add(this.rest_Picker, 1, 2);
            this.main_TablePanel.Controls.Add(this.end_Panel, 0, 1);
            this.main_TablePanel.Controls.Add(this.start_Panel, 0, 0);
            this.main_TablePanel.Controls.Add(this.start_Picker, 1, 0);
            this.main_TablePanel.Controls.Add(this.notes_Textbox, 0, 3);
            this.main_TablePanel.Controls.Add(this.button_FlowLayout, 1, 3);
            this.main_TablePanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_TablePanel.Location = new System.Drawing.Point(0, 0);
            this.main_TablePanel.Name = "main_TablePanel";
            this.main_TablePanel.RowCount = 4;
            this.main_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.main_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.main_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.main_TablePanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.main_TablePanel.Size = new System.Drawing.Size(481, 365);
            this.main_TablePanel.TabIndex = 0;
            // 
            // rest_Panel
            // 
            this.rest_Panel.Controls.Add(this.rest1_CheckBox);
            this.rest_Panel.Controls.Add(this.rest_Label);
            this.rest_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rest_Panel.Location = new System.Drawing.Point(3, 112);
            this.rest_Panel.Name = "rest_Panel";
            this.rest_Panel.Size = new System.Drawing.Size(234, 49);
            this.rest_Panel.TabIndex = 4;
            // 
            // rest1_CheckBox
            // 
            this.rest1_CheckBox.AutoSize = true;
            this.rest1_CheckBox.Enabled = false;
            this.rest1_CheckBox.Location = new System.Drawing.Point(167, 18);
            this.rest1_CheckBox.Name = "rest1_CheckBox";
            this.rest1_CheckBox.Size = new System.Drawing.Size(15, 14);
            this.rest1_CheckBox.TabIndex = 1;
            this.rest1_CheckBox.UseVisualStyleBackColor = true;
            // 
            // rest_Label
            // 
            this.rest_Label.AutoSize = true;
            this.rest_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rest_Label.Location = new System.Drawing.Point(74, 15);
            this.rest_Label.Name = "rest_Label";
            this.rest_Label.Size = new System.Drawing.Size(58, 20);
            this.rest_Label.TabIndex = 0;
            this.rest_Label.Text = "Pause:";
            this.rest_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // end_Picker
            // 
            this.end_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.end_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.end_Picker.Location = new System.Drawing.Point(255, 69);
            this.end_Picker.Margin = new System.Windows.Forms.Padding(15);
            this.end_Picker.Name = "end_Picker";
            this.end_Picker.ShowUpDown = true;
            this.end_Picker.Size = new System.Drawing.Size(211, 20);
            this.end_Picker.TabIndex = 1;
            this.end_Picker.ValueChanged += new System.EventHandler(this.TimesChanged);
            // 
            // rest_Picker
            // 
            this.rest_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rest_Picker.Enabled = false;
            this.rest_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.rest_Picker.Location = new System.Drawing.Point(255, 124);
            this.rest_Picker.Margin = new System.Windows.Forms.Padding(15);
            this.rest_Picker.Name = "rest_Picker";
            this.rest_Picker.ShowUpDown = true;
            this.rest_Picker.Size = new System.Drawing.Size(211, 20);
            this.rest_Picker.TabIndex = 2;
            // 
            // end_Panel
            // 
            this.end_Panel.Controls.Add(this.end_Label);
            this.end_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.end_Panel.Location = new System.Drawing.Point(3, 57);
            this.end_Panel.Name = "end_Panel";
            this.end_Panel.Size = new System.Drawing.Size(234, 49);
            this.end_Panel.TabIndex = 3;
            // 
            // end_Label
            // 
            this.end_Label.AutoSize = true;
            this.end_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.end_Label.Location = new System.Drawing.Point(81, 12);
            this.end_Label.Name = "end_Label";
            this.end_Label.Size = new System.Drawing.Size(51, 20);
            this.end_Label.TabIndex = 0;
            this.end_Label.Text = "Ende:";
            this.end_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // start_Panel
            // 
            this.start_Panel.Controls.Add(this.start_Label);
            this.start_Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.start_Panel.Location = new System.Drawing.Point(3, 3);
            this.start_Panel.Name = "start_Panel";
            this.start_Panel.Size = new System.Drawing.Size(234, 48);
            this.start_Panel.TabIndex = 3;
            // 
            // start_Label
            // 
            this.start_Label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.start_Label.AutoSize = true;
            this.start_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.start_Label.Location = new System.Drawing.Point(78, 15);
            this.start_Label.Name = "start_Label";
            this.start_Label.Size = new System.Drawing.Size(48, 20);
            this.start_Label.TabIndex = 0;
            this.start_Label.Text = "Start:";
            this.start_Label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // start_Picker
            // 
            this.start_Picker.Dock = System.Windows.Forms.DockStyle.Fill;
            this.start_Picker.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.start_Picker.Location = new System.Drawing.Point(255, 15);
            this.start_Picker.Margin = new System.Windows.Forms.Padding(15);
            this.start_Picker.Name = "start_Picker";
            this.start_Picker.ShowUpDown = true;
            this.start_Picker.Size = new System.Drawing.Size(211, 20);
            this.start_Picker.TabIndex = 0;
            this.start_Picker.ValueChanged += new System.EventHandler(this.TimesChanged);
            // 
            // notes_Textbox
            // 
            this.notes_Textbox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.notes_Textbox.Location = new System.Drawing.Point(3, 249);
            this.notes_Textbox.Margin = new System.Windows.Forms.Padding(3, 85, 3, 3);
            this.notes_Textbox.MaxLength = 13;
            this.notes_Textbox.Name = "notes_Textbox";
            this.notes_Textbox.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.notes_Textbox.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.notes_Textbox.Size = new System.Drawing.Size(234, 20);
            this.notes_Textbox.TabIndex = 3;
            // 
            // button_FlowLayout
            // 
            this.button_FlowLayout.Controls.Add(this.accept_Button);
            this.button_FlowLayout.Controls.Add(this.abort_Button);
            this.button_FlowLayout.Controls.Add(this.delete_Button);
            this.button_FlowLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_FlowLayout.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.button_FlowLayout.Location = new System.Drawing.Point(243, 167);
            this.button_FlowLayout.Name = "button_FlowLayout";
            this.button_FlowLayout.Size = new System.Drawing.Size(235, 195);
            this.button_FlowLayout.TabIndex = 4;
            // 
            // accept_Button
            // 
            this.accept_Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.accept_Button.BackColor = System.Drawing.Color.White;
            this.accept_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accept_Button.Image = global::McD_Schichtplaner.Properties.Resources.accept;
            this.accept_Button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.accept_Button.Location = new System.Drawing.Point(3, 3);
            this.accept_Button.Name = "accept_Button";
            this.accept_Button.Size = new System.Drawing.Size(225, 55);
            this.accept_Button.TabIndex = 0;
            this.accept_Button.Text = "Fertig";
            this.accept_Button.UseVisualStyleBackColor = false;
            this.accept_Button.Click += new System.EventHandler(this.Accept_Button_Click);
            // 
            // abort_Button
            // 
            this.abort_Button.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.abort_Button.BackColor = System.Drawing.Color.White;
            this.abort_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.abort_Button.Image = global::McD_Schichtplaner.Properties.Resources.cancel;
            this.abort_Button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.abort_Button.Location = new System.Drawing.Point(3, 64);
            this.abort_Button.Name = "abort_Button";
            this.abort_Button.Size = new System.Drawing.Size(225, 60);
            this.abort_Button.TabIndex = 1;
            this.abort_Button.Text = "Abbrechen";
            this.abort_Button.UseVisualStyleBackColor = false;
            this.abort_Button.Click += new System.EventHandler(this.Abort_Button_Click);
            // 
            // delete_Button
            // 
            this.delete_Button.BackColor = System.Drawing.Color.White;
            this.delete_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.delete_Button.Image = global::McD_Schichtplaner.Properties.Resources.delete;
            this.delete_Button.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.delete_Button.Location = new System.Drawing.Point(3, 130);
            this.delete_Button.Name = "delete_Button";
            this.delete_Button.Size = new System.Drawing.Size(225, 60);
            this.delete_Button.TabIndex = 2;
            this.delete_Button.Text = "Löschen";
            this.delete_Button.UseVisualStyleBackColor = false;
            this.delete_Button.Click += new System.EventHandler(this.Delete_Button_Click);
            // 
            // TemplateShiftForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 365);
            this.Controls.Add(this.main_TablePanel);
            this.Name = "TemplateShiftForm";
            this.Text = "Schicht Vorlage";
            this.main_TablePanel.ResumeLayout(false);
            this.main_TablePanel.PerformLayout();
            this.rest_Panel.ResumeLayout(false);
            this.rest_Panel.PerformLayout();
            this.end_Panel.ResumeLayout(false);
            this.end_Panel.PerformLayout();
            this.start_Panel.ResumeLayout(false);
            this.start_Panel.PerformLayout();
            this.button_FlowLayout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel main_TablePanel;
        private System.Windows.Forms.FlowLayoutPanel button_FlowLayout;
        private System.Windows.Forms.Button accept_Button;
        private System.Windows.Forms.Button abort_Button;
        private System.Windows.Forms.Button delete_Button;
        private System.Windows.Forms.TextBox notes_Textbox;
        private System.Windows.Forms.Panel start_Panel;
        private System.Windows.Forms.Label start_Label;
        private System.Windows.Forms.DateTimePicker start_Picker;
        private System.Windows.Forms.Panel end_Panel;
        private System.Windows.Forms.Label end_Label;
        private System.Windows.Forms.DateTimePicker end_Picker;
        private System.Windows.Forms.Panel rest_Panel;
        private System.Windows.Forms.CheckBox rest1_CheckBox;
        private System.Windows.Forms.Label rest_Label;
        private System.Windows.Forms.DateTimePicker rest_Picker;
    }
}