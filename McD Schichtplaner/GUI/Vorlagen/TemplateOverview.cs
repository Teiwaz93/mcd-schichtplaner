﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using McD_Schichtplaner.GUI.Fehlerbehandlung;
using McD_Schichtplaner.Database;

namespace McD_Schichtplaner.GUI.Vorlagen
{
    public partial class TemplateOverview : UserControl
    {
        Database.Database data = Database.Database.DB;
        List<TemplateShiftControl> shifts = new List<TemplateShiftControl>();

        public TemplateOverview(Size size)
        {
            InitializeComponent();
            Size = size;
            var list = data.GetTemplates();
            foreach (var entry in list)
            {
                TemplateListEntry tle = new TemplateListEntry(entry.Item1, entry.Item2);
                list_FlowLayoutPanel.Controls.Add(tle);
            }
            if (TemplateListEntry.Active == null) tab_Control.Visible = false;
        }

        private void Delete_Button_Click(object sender, EventArgs e)
        {
            if (TemplateListEntry.Active != null)
            {
                data.DeleteTemplate(TemplateListEntry.Active.ID);
                list_FlowLayoutPanel.Controls.Remove(TemplateListEntry.Active);
            }
        }

        private void Rename_Button_Click(object sender, EventArgs e)
        {
            StringError se = new StringError("Vorlage umbenennen.", TemplateListEntry.Active.Name);
            se.Text = "Vorlage umbenennen";
            se.ShowDialog();
            TemplateListEntry.Active.Name = se.String;
            data.RenameTemplate(se.String, TemplateListEntry.Active.ID);
        }

        private void Copy_Button_Click(object sender, EventArgs e)
        {
            StringError se = new StringError("Namen für neue Vorlage.", "Kopie von " + TemplateListEntry.Active.Name);
            se.Text = "Neue Vorlage";
            se.ShowDialog();
            var name = se.String;
            var id = data.CreateTemplate(name);
            TemplateListEntry tle = new TemplateListEntry(name, id);
            list_FlowLayoutPanel.Controls.Add(tle);

            foreach (var shift in shifts)
            {
                data.CreateTemplateShift(new TemplateShift(shift.Shift, id));
            }
        }

        private void New_Button_Click(object sender, EventArgs e)
        {
            StringError se = new StringError("Namen für neue Vorlage.", "Vorlage " + list_FlowLayoutPanel.Controls.Count);
            se.Text = "Neue Vorlage";
            se.ShowDialog();
            var name = se.String;
            var id = data.CreateTemplate(name);
            TemplateListEntry tle = new TemplateListEntry(name, id);
            list_FlowLayoutPanel.Controls.Add(tle);
        }

        public void ReDrawTemplate(long id)
        {
            tab_Control.Visible = true;
            var shifts2 = new List<TemplateShiftControl>(shifts);
            foreach (var entry in shifts2)
            {
                RemoveShift(entry);
            }
            shifts = new List<TemplateShiftControl>();

            var list = data.GetTemplateShifts(id);
            foreach (var entry in list)
            {
                AddShift(entry);
            }
        }

        public void AddShift(TemplateShift shift)
        {
            int x1 = shift.Start.Hours >= 6 ? (shift.Start.Hours - 6) * 2 : (shift.Start.Hours - 6) * 2 + 48;
            if (shift.Start.Minutes >= 30) x1++;
            int x2 = 0;
            if (shift.End.Days == 1)
            {
                x2 = 36;
                x2 += (shift.End.Hours) * 2;
            }
            else x2 = (shift.End.Hours - 6) * 2;
            if (x2 > 48) x2 = 48;
            if (x2 <= x1) x2 = x1 + 1;
            int i = 0;
            TableLayoutPanel var_Table;
            switch (shift.Station)
            {
                case 1:
                    var_Table = kitchen_Table;
                    break;
                case 2:
                    var_Table = side1_Table;
                    break;
                case 3:
                    var_Table = side2_Table;
                    break;
                case 4:
                    var_Table = toaster_Table;
                    break;
                case 5:
                    var_Table = garnish_Table;
                    break;
                case 6:
                    var_Table = uhc_Table;
                    break;
                case 7:
                    var_Table = grill_Table;
                    break;
                case 8:
                    var_Table = fryer_Table;
                    break;
                case 9:
                    var_Table = bunker_Table;
                    break;
                case 10:
                    var_Table = dRunner_Table;
                    break;
                case 11:
                    var_Table = cRunner_Table;
                    break;
                case 12:
                    var_Table = cashier_Table;
                    break;
                case 13:
                    var_Table = drinks_Table;
                    break;
                case 14:
                    var_Table = fries_Table;
                    break;
                case 15:
                    var_Table = ice_Table;
                    break;
                case 16:
                    var_Table = lobby_Table;
                    break;
                case 17:
                    var_Table = presenter_Table;
                    break;
                case 18:
                    var_Table = Jogi_Table;
                    break;
                case 19:
                    var_Table = cafe_Table;
                    break;
                case 20:
                    var_Table = lead_Table;
                    break;
                case 21:
                    var_Table = help_Table;
                    break;
                case 22:
                    var_Table = inventur_Table;
                    break;
                default:
                    return;
            }
            while (var_Table.GetControlFromPosition(x1, i) != null)
            {
                i++;
                if (var_Table.RowCount - 1 < i)
                {
                    var_Table.RowCount = var_Table.RowCount + 1;
                    var_Table.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 50F));
                }
            }
            TemplateShiftControl block = var_Table.GetControlFromPosition(x2, i) as TemplateShiftControl;
            int middle = x2 - ((x2 - x1) / 2);
            TemplateShiftControl blockMiddle = var_Table.GetControlFromPosition(middle, i) as TemplateShiftControl;
            TemplateShiftControl sc = new TemplateShiftControl(shift, this);
            var_Table.Controls.Add(sc, x1, i);
            Console.WriteLine("added shift into table at: " + x1 + "/" + i);
            var_Table.SetColumnSpan(sc, x2 - x1);
            if (!(block is null))
            {
                Console.WriteLine("switching stuff around");
                RemoveShift(block);
                AddShift(block.Shift);
            }
            if (blockMiddle != block && !(blockMiddle is null))
            {
                RemoveShift(blockMiddle);
                AddShift(blockMiddle.Shift);
            }
            shifts.Add(sc);
            CalculateHours();
        }

        public void RemoveShift(TemplateShiftControl shift)
        {
            kitchen_Table.Controls.Remove(shift);
            side1_Table.Controls.Remove(shift);
            side2_Table.Controls.Remove(shift);
            toaster_Table.Controls.Remove(shift);
            garnish_Table.Controls.Remove(shift);
            uhc_Table.Controls.Remove(shift);
            grill_Table.Controls.Remove(shift);
            fryer_Table.Controls.Remove(shift);
            bunker_Table.Controls.Remove(shift);
            dRunner_Table.Controls.Remove(shift);
            cRunner_Table.Controls.Remove(shift);
            cashier_Table.Controls.Remove(shift);
            drinks_Table.Controls.Remove(shift);
            fries_Table.Controls.Remove(shift);
            ice_Table.Controls.Remove(shift);
            lobby_Table.Controls.Remove(shift);
            presenter_Table.Controls.Remove(shift);
            Jogi_Table.Controls.Remove(shift);
            cafe_Table.Controls.Remove(shift);
            lead_Table.Controls.Remove(shift);
            help_Table.Controls.Remove(shift);
            inventur_Table.Controls.Remove(shift);
            shifts.Remove(shift);
            CalculateHours();
        }

        private void TableClicked(object sender, EventArgs e)
        {
            var table = sender as TableLayoutPanel;
            long station = 1;
            switch (table.Name)
            {
                case "kitchen_Table":
                    station = 1;
                    break;
                case "side1_Table":
                    station = 2;
                    break;
                case "side2_Table":
                    station = 3;
                    break;
                case "toaster_Table":
                    station = 4;
                    break;
                case "garnish_Table":
                    station = 5;
                    break;
                case "uhc_Table":
                    station = 6;
                    break;
                case "grill_Table":
                    station = 7;
                    break;
                case "fryer_Table":
                    station = 8;
                    break;
                case "bunker_Table":
                    station = 9;
                    break;
                case "dRunner_Table":
                    station = 10;
                    break;
                case "cRunner_Table":
                    station = 11;
                    break;
                case "cashier_Table":
                    station = 12;
                    break;
                case "drinks_Table":
                    station = 13;
                    break;
                case "fries_Table":
                    station = 14;
                    break;
                case "ice_Table":
                    station = 15;
                    break;
                case "lobby_Table":
                    station = 16;
                    break;
                case "presenter_Table":
                    station = 17;
                    break;
                case "Jogi_Table":
                    station = 18;
                    break;
                case "cafe_Table":
                    station = 19;
                    break;
                case "lead_Table":
                    station = 20;
                    break;
                case "help_Table":
                    station = 21;
                    break;
                case "inventur_Table":
                    station = 22;
                    break;
            }
            TemplateShiftForm tsf = new TemplateShiftForm(station, TemplateListEntry.Active.ID);
            if (tsf.ShowDialog() == DialogResult.OK)
            {
                data.CreateTemplateShift(tsf.Shift);
                AddShift(data.GetSingleTemplateShift(data.GetLastInsert()));
            }
        }

        public void CalculateHours()
        {
            TimeSpan hours = new TimeSpan();
            foreach(var shift in shifts)
            {
                hours += shift.Shift.Length;
            }
            hours_Label.Text = shifts.Count + " [" + (hours.Days * 24 + hours.Hours) + "," + hours.Minutes / 6 + "]";
        }
    }
}
