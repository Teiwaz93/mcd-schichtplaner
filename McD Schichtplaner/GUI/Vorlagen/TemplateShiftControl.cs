﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using McD_Schichtplaner.Database;

namespace McD_Schichtplaner.GUI.Vorlagen
{
    public partial class TemplateShiftControl : UserControl
    {
        private TemplateShift shift;
        public TemplateShift Shift => shift;
        private Database.Database data =  Database.Database.DB;
        private TemplateOverview overview;

        private TemplateShiftControl()
        {
            InitializeComponent();
        }

        public TemplateShiftControl(TemplateShift shift, TemplateOverview o) :this()
        {
            overview = o;
            this.shift = shift;
            start_Label.Text = $"{shift.Start.Hours}:{((shift.Start.Minutes < 10) ? "0" + shift.Start.Minutes : "" + shift.Start.Minutes)}";
            end_Label.Text = $"{shift.End.Hours}:{((shift.End.Minutes < 10) ? "0" + shift.End.Minutes : "" + shift.End.Minutes)}";
            rest1_Label.Text = $"{shift.Rest.Hours}:{((shift.Rest.Minutes < 10) ? "0" + shift.Rest.Minutes : "" + shift.Rest.Minutes)}";
            rest1_Label.Visible = shift.B_Rest;
            if (Shift.Rest >= new TimeSpan (0,0,0)) rest1_Label.Visible = true;
            rest2_Label.Text = shift.Notes;
            rest2_Label.Visible = true;
        }

        private void TemplateShiftControl_Click(object sender, EventArgs e)
        {
            TemplateShiftForm tsf = new TemplateShiftForm(shift);
            var result = tsf.ShowDialog();
            if (result == DialogResult.OK)
            {
                data.UpdateTemplateShift(tsf.Shift);
                overview.RemoveShift(this);
                overview.AddShift(tsf.Shift);
            }
            else if (result == DialogResult.No)
            {
                data.DeleteTemplateShift(shift);
                overview.RemoveShift(this);
            }
        }
    }
}
