﻿using McD_Schichtplaner.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace McD_Schichtplaner.GUI.Fehlerbehandlung
{
    public partial class ErrorsForm : Form
    {
        public ErrorsForm()
        {
            InitializeComponent();
        }

        public ErrorsForm(List<Error> errors) : this()
        {
            foreach (var error in errors)
            {
                Label label = new Label()
                {
                    Size = new Size(mainPanel.Width - 50, 50),
                    AutoSize = false,
                    Image = error.Severe ? Properties.Resources.error : Properties.Resources.warning,
                    ImageAlign = ContentAlignment.MiddleLeft,
                    Text = error.Message,
                    TextAlign = ContentAlignment.MiddleRight
                };
                mainPanel.Controls.Add(label);
            }

        }

        private void OK_Button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }
    }
}
