﻿namespace McD_Schichtplaner.GUI.Fehlerbehandlung
{
    partial class StringError
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.input_TextBox = new System.Windows.Forms.TextBox();
            this.ok_Button = new System.Windows.Forms.Button();
            this.error_Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // input_TextBox
            // 
            this.input_TextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.input_TextBox.Location = new System.Drawing.Point(12, 113);
            this.input_TextBox.Name = "input_TextBox";
            this.input_TextBox.Size = new System.Drawing.Size(294, 23);
            this.input_TextBox.TabIndex = 0;
            // 
            // ok_Button
            // 
            this.ok_Button.Location = new System.Drawing.Point(127, 142);
            this.ok_Button.Name = "ok_Button";
            this.ok_Button.Size = new System.Drawing.Size(75, 23);
            this.ok_Button.TabIndex = 1;
            this.ok_Button.Text = "OK";
            this.ok_Button.UseVisualStyleBackColor = true;
            this.ok_Button.Click += new System.EventHandler(this.Ok_Button_Click);
            // 
            // error_Label
            // 
            this.error_Label.AutoSize = true;
            this.error_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.error_Label.Location = new System.Drawing.Point(13, 13);
            this.error_Label.Name = "error_Label";
            this.error_Label.Size = new System.Drawing.Size(0, 20);
            this.error_Label.TabIndex = 2;
            // 
            // StringError
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(318, 186);
            this.Controls.Add(this.error_Label);
            this.Controls.Add(this.ok_Button);
            this.Controls.Add(this.input_TextBox);
            this.Name = "StringError";
            this.Text = "StringError";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox input_TextBox;
        private System.Windows.Forms.Button ok_Button;
        private System.Windows.Forms.Label error_Label;
    }
}