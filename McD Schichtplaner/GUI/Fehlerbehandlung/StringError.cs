﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace McD_Schichtplaner.GUI.Fehlerbehandlung
{
    /**
     * <summary>Fenster, das den Benutzer einen String korrigieren lässt</summary>
     */
    public partial class StringError : Form
    {
        public string String => input_TextBox.Text;

        /**
         * <summary>standard Konstruktor</summary>
         */
        public StringError()
        {
            InitializeComponent();
        }

        /**
         * <summary>Konstrukotr, der zusätzlich eine Nachricht anzeigt</summary>
         */
        public StringError(string message) :this()
        {
            error_Label.Text = message;
        }

        /**
         * <summary>Konstruktor, der zu der Nachricht auch ncoh ein Beispiel, wie es richtig wäre, anzeigt.</summary>
         */
        public StringError(string message, string example) : this(message)
        {
            input_TextBox.Text = example;
        }

        /**
         * <summary>beendet den Dialog</summary>
         */
        private void Ok_Button_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        /**
         * <summary>gibt den vom user eingegeben string an das programm zurück</summary>
         */
        public string GetString() => input_TextBox.Text;
    }
}
