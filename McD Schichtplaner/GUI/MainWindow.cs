﻿using McD_Schichtplaner.Database;
using McD_Schichtplaner.GUI.Fehlerbehandlung;
using McD_Schichtplaner.GUI.Personal;
using McD_Schichtplaner.GUI.Planung;
using McD_Schichtplaner.GUI.Vorlagen;
using McD_Schichtplaner.Maintenance;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace McD_Schichtplaner
{

    /**
     * <summary>steltl das hauptfenster dar.</summary>
     */
    public partial class Mainwindow : Form
    {
        private Database.Database data;

        /**
         * <summary>einfacher konstruktor
         * </summary>
         */
        public Mainwindow()
        {
            InitializeComponent();
            Directory.CreateDirectory("Temp");
            var files = Directory.GetFiles("Temp");
            foreach (var file in files)
            {
                File.Delete(file);
            }
            data = Database.Database.DB;
            Text = $"{data.GetRestaurantName()}s Schichtplaner";
            CheckExpirations();
        }

        /**
         * <summary>Wechselt das Hauptpanel durch übergebenes Panel</summary>
         */
        internal void ChangePanel(UserControl panel)
        {
            if (workPanel.Controls[0].Name == "PersonalMainPanel") ((PersonalMainPanel)workPanel.Controls[0]).SaveDetails();
            else if (workPanel.Controls[0].Name == "MaintenanceControl") ((MaintenanceControl)workPanel.Controls[0]).Save();
            workPanel.Controls.Clear();
            workPanel.Controls.Add(panel);
        }

        /**
         * <summary>Wechselt das Hauptpanel abhängig von dem sender(einer der drei Buttons oben)</summary>
         */
        private void ChangePanel(object sender, System.EventArgs e)
        {
            Button button = (Button)sender;
            UserControl panel;
            switch (button.Name)
            {
                case "personal_Button":
                    panel = new PersonalMainPanel(workPanel.Size);
                    break;
                case "plan_Button":
                    panel = new MonthOverview(DateTime.Now.AddMonths(1), workPanel.Size);
                    break;
                case "vorlagen_Button":
                    panel = new TemplateOverview(workPanel.Size);
                    break;
                case "maintenance_Button":
                    panel = new MaintenanceControl(workPanel.Size);
                    break;
                default: return;
            }
            ChangePanel(panel);
        }


        private void CheckExpirations()
        {
            var aboutTo = data.FindAboutToExpireWorkers();
            var expired = data.FindExpiredWorkers();
            var errors = new List<Error>();
            foreach (var entry in expired) errors.Add(new Error(true, $"{entry.Name}s Vertrag ist abgelaufen und wurde als inaktiv markiert."));
            foreach (var entry in aboutTo) errors.Add(new Error(false, entry + "s Vertrag läuft in weniger als 14 Tagen aus.")); 
            if (errors.Count > 0)
            {
                var form = new ErrorsForm(errors);
                form.ShowDialog(); 
            }
        }

        private void Mainwindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (workPanel.Controls[0].Name == "PersonalMainPanel") ((PersonalMainPanel)workPanel.Controls[0]).SaveDetails();
            else if (workPanel.Controls[0].Name == "MaintenanceControl") ((MaintenanceControl)workPanel.Controls[0]).Save();
            if (MessageBox.Show("Soll das Programm geschlossen werden?", "Schließen?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) e.Cancel = true;
        }
    }
}
